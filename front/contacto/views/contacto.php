	<style type="text/css">#nav-bt4 {color: #464646;} #nav-bt4 span {background-position:-114px -60px;}</style>


  <section>
    <div class="con-section">
      <div class="mg-section cfx">
        <h1 class="int-tt cfx">Contáctenos</h1>
        <p align="justify">
            <?php echo nl2br($info->texto) ?>
        </p>
        <div class="contact-col fr">
        	<!--Map desktop-->
          <div class="con-map map-dk">
            <div class="map-pick"></div>
            <div class="map" style="background:url(<?php echo $info->mapa ?>);">
            </div><div class="map-bor-1"></div><div class="map-bor-2"></div><div class="map-bor-3"></div><div class="map-bor-4"></div>
          </div>
          <h2>Información de contacto</h2>
            <?php echo $info->informacion ?>
          <!--<p><strong>Teléfono 1: </strong>(571) 332 4578</p>
          <p><strong>Teléfono 2: </strong>(571) 325 8696</p>
          <p><strong>Fax: </strong>(571) 345 9867</p>
          <p><strong>Correo electrónico: </strong><a class="tr" href="mailto:#">info@screntamuebles.com</a></p>
          <p><strong>Dirección: </strong>Carrera 52 No 79 - 20</p>
          <p><strong>Bogotá - Colombia</strong></p>-->
          <!--Map device-->
          <div class="con-map map-dv">
            <div class="map-pick"></div>
            <div class="map" style="background:url(http://maps.google.com/maps/api/staticmap?center=4.598056,-74.075833&zoom=14&size=600x300&sensor=false);">
            </div><div class="map-bor-1"></div><div class="map-bor-2"></div><div class="map-bor-3"></div><div class="map-bor-4"></div>
          </div>
        </div>
        <div class="contact-col fl">
        	<form action="<?php echo base_url() ?>contacto" id="for1" class="grl-form cfx" method="post" role="form">
          	<input class="tr fl" data-validation="alphanumeric" type="text" placeholder="Nombre..." value="">
            <input class="tr fr" data-validation="email" type="text" placeholder="E-mail..." value="">
            <input class="tr fl" type="text" placeholder="Celular..." value=""><!--data-validation="number"-->
            <input class="tr fr" type="text" placeholder="Teléfono..." value=""><!--data-validation="number"-->
            <textarea class="tr" placeholder="Comentarios..."></textarea><!--data-validation="alphanumeric"-->
            <input class="bt-submit tr fr" id="prueba" type="submit" value="Enviar">
          </form>
        </div>
        <!--div class="contact-col fl">
        	<form action="#" class="grl-form cfx" method="post">
          	<input class="validate[required] tr fl" type="text" placeholder="Nombre..." value="">
            <input class="validate[required, custom[email]] tr fr" type="text" placeholder="E-mail..." value="">
            <input class="tr fl" type="text" placeholder="Celular..." value="">
            <input class="tr fr" type="text" placeholder="Teléfono..." value="">
            <textarea class="validate[required] tr" placeholder="Comentarios..."></textarea>
            <input class="bt-submit tr fr" type="submit" value="Enviar">
          </form>
        </div-->
      </div>
    </div>
  </section>

   

