<section class="contenido">
    <article class="slide">
        <div class="flexslider carousel ">
            <ul class="slides">
               <?php echo $banner; ?>

            </ul>
        </div>
    </article>
</section>

<section class="contenido-bg-negro" >
    <article class="wrap clearfix">
    	<div class="left slider_left">
        
          <div class="content_slide_serv">
                      <div class="flexslider_dos carousel ">
                           <ul class="slides">
                                <?php echo $logos_dos; ?>
                            </ul>
                        </div>
                      </div>
        </div>
        <div class="right slider_right">
        <div id="NAV-ID" class="crsl-nav">
            <a href="#" class="previous">Previous</a>
            <a href="#" class="next">Next</a>
        </div>
        <div class="crsl-items" data-navigation="NAV-ID">
            <div class="crsl-wrap" >
                <?php echo $logos; ?>
            </div>
        </div>
        </div>

    </article>

</section>