<?php

class _Coloresproducto extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "COLORES PRODUCTO";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {

        $this->_data['save'] = $value;
        $b = new Coloresproducto();
        $info = $b->getColoresproducto();
        $this->_data["info"] = $info;
        $this->_data["count"] = count($info);
        return $this->build('coloresproducto');

    }

    public function lista($idp = '', $value = '') {

        $this->load->model(array(
            CMSPREFIX."colores/colores",
        ));

        $c = new Colores();
        $colores = $c->getColores();

        $color = array();
        foreach($colores as $co){
            $color[$co->id] = $co->nombre;
        }

        $this->_data['color']= $color;

        $sesion_prodecto= array(
            'idp' => $idp
        );
        $this->session->set_userdata($sesion_prodecto);

        $this->_data['save'] = $value;
        $b = new Coloresproducto();
        $info = $b->getColoresproducto2($this->session->userdata['idp']);
        $i = 0;
        foreach($info as $d){
            $i++;
        }        $this->_data["info"] = $info;
        $this->_data["count"] = $i;


        return $this->build('coloresproducto');

    }

    public function add() {
        $this->load->model(array(
            CMSPREFIX."colores/colores",
        ));
        $this->_data['nombremodulo'] = "COLORES / New";
        $c = new Colores();
        $colores = $c->getColores();
        $this->_data['colores'] = $colores;
        $this->build('coloresproducto_new');
    }

    public function new_coloresproducto() {
        $datos = array(
            'color' => $this->input->post('color'),
            'productos' => $this->session->userdata['idp'],
        );

        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'coloresproducto';
        $b = new Coloresproducto();

        if ($datos['color'] != ''){

            $this->_data['save'] = FALSE;
            $build = 'coloresproducto_new';
            if ($b->saveColoresproducto($datos)){
                $build = 'coloresproducto';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getColoresproducto();
        $this->_data["info"] = $info;
        return redirect('cms/coloresproducto/lista/'.$this->session->userdata['idp'].'/'.$save);

    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'coloresproducto/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."coloresproducto/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;
            $resize['width']	 =300;
            $resize['height']	= 160;
            $resize['new_image'] = UPLOADSFOLDER."coloresproducto/new/".$data["data"]["file_name"];

            $this->load->library('image_lib', $resize);

            if ( ! $this->image_lib->resize())
            {
                $data = array(
                    'ok' => false,
                    'resize' => $this->image_lib->display_errors()
                );
            }else{
                $data = array(
                    'ok' => true,
                    'resize' => true,
                    'data' => $this->upload->data()
                );
            }
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $this->load->model(array(
            CMSPREFIX."colores/colores",
        ));
        $c = new Colores();
        $colores = $c->getColores();
        $this->_data['colores'] = $colores;

        $b = new Coloresproducto();
        $dat = $b->getColoresproductoById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("coloresproducto_edit");
    }
    
    public function update_Coloresproducto(){
        $b = new Coloresproducto();
        $datos = array(
            'id' => $this->input->post('id'),
            'color' => $this->input->post('color'),
        );
        $update = $b->updateColoresproducto($datos); 

        if ($update){
            $a = TRUE;
        }
        return redirect("cms/coloresproducto/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Coloresproducto();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/coloresproducto/lista/".$this->session->userdata['idp'].'/'.$return);
    }

}

