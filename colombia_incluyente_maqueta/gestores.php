<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  
<style type="text/css">
#s4 {
	color:#fff;
	background-position:0px -100px;
}
#s3 {
	color:#fff;
	background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -50px;
}
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>

<link href="assets/css/jquery-filestyle.css" rel="stylesheet" />
  	
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
<script src="assets/js/jquery.sticky.js"></script>
<script>
  $(document).ready(function(){
    $(".sticker").sticky({topSpacing:30});
	
	
	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
		   $('.sticker').addClass('fixed_button_history');
		   }else{
			   $('.sticker').removeClass('fixed_button_history');
		   }
	});
  });
</script>
</head>

<body>
<?php include"header.php" ?>  
<?php include"paso_paso.php" ?>

<div class="txt_reglamento">
	<h2 class="h2_b">Gestores del Proyecto </h2>
    Conocer a los gestores del proyecto genera confianza en el público y les da la certeza de que es una iniciativa legítima, respaldada por una organización que velará por la consecusión del proyecto.
</div>

<div class="box_a pad_box clearfix">
    
    <div class="col_c">
      <div class="box_shadow box_c" style="margin-bottom:30px;">
       	  <h6 style="color:#662d91">Información de la Organización que gestiona el proyecto.</h6>
          <div style="margin-bottom:10px;">Todos los proyectos deben pertenecer a una organización registrada, con minimo 3 años de antiguedad.</div>
		 
          <input type="text" class="input_c" />
          <div style="margin-bottom:50px"></div>          
          <div class="left" style="width:470px; margin-top:-40px;">
          	<h6>Imagen del Logotipo de la Organización</h6>
          	El Logotipo es la cara de su organización. Muéstrelo a todos.<br />
            Tamaño máximo: 5mb <br />
          Dimensiones recomendadas: 200px x 200px
          </div>
            <div class="right relative" style="width:111px; height:90px">
               <input value="" type="file" class="jfilestyle" data-theme="blue2">
            </div>
        
        <div class="div_line_2 clear spacer"></div>	
            
            <h6>Descripción de la organización a la que pertenece el proyecto</h6>
            <p>Háblenos de la organización, cuáles han sido sus logros y el impacto generado.</p>
          <textarea class="text_area_big"></textarea>
          <br />
          Máximo de caracteres 0/1000
            <div class="spacer"></div>
		
        
            <div class="col_int_box_c left">
                    <div class="label">País originario</div>
                    <input type="text" class="input_a" />
                    <div class="label">Municipio</div>
                    <input type="text" class="input_a" />
                    <div class="label">Teléfono 1</div>
                    <input type="text" class="input_a" />
                    <div class="label">Email</div>
                    <input type="text" class="input_a" />
                    <div class="label">Twitter (opcional) </div>
                    <input type="text" class="input_a" placeholder="@ejemplo" />
            </div>
            <div class="col_int_box_c right">
           		<div class="label">Departamento / Región</div>
            	<input type="text" class="input_a" />
            	<div class="label">Dirección</div>
            	<input type="text" class="input_a" />
            	<div class="label">Teléfono 2 (opcional) </div>
            	<input type="text" class="input_a" />
            	<div class="label">Página web (opcional) </div>
            	<input type="text" class="input_a" />
            	<div class="label">Facebook (opcional)  </div>
            	<input type="text" class="input_a" placeholder="facebook.com/ejemplo" />
            </div>
            <div class="clear div_line_2 spacer"></div>
            
            
            
            <h6>Certificado de Cámara de Comercio <span style="color:#999; font-size:14px">(No mayor a 30 días)</span></h6>
            <p>¿Cuáles son los objetivos y metas del proyecto? Unos objetivos medibles ayudarán a definir las necesidades del proyecto y permitirán monitorear su desempeño.</p>
           	<input type="file" class="jfilestyle" data-theme="green">
            <h6 style="margin-bottom:5px">Nit / Rut</h6>
           	<input type="file" class="jfilestyle" data-theme="green">

            <div class="div_line_2 spacer"></div>


			<h6 style="color:#662d91">Información del líder del proyecto</h6>
            <div class="left" style="width:470px;">
            El Logotipo es la cara de su organización. Muéstrelo a todos.<br />
            Tamaño máximo: 5mb <br />
            Dimensiones recomendadas: 200px x 200px
            </div>
            <div class="right relative" style="width:111px; height:90px">
               <input value="" type="file" class="jfilestyle" data-theme="blue3">
            </div>
			<div class="spacer"></div>

        <div class="div_line_2 clear"></div>

        	<h6>Acerca del líder</h6>
            <p>Describa quién es el líder del proyecto, cuáles son las cualidades que lo hacen la persona ideal para liderarlo y en qué otros proyectos ha participado.</p>
			<textarea class="text_area_big"></textarea>
            <br />
            Máximo de caracteres 0/1000
        <div class="div_line_2"></div>
      </div>
        <div class="div_green clear"></div>

        
	  <div class="box_shadow box_c">
        <h6>Preguntas y Respuestas del Proyecto</h6>
            <p>Adelántese a las preguntas que puedan surgir, y respóndalas para que todos las puedan ver.</p>
<textarea class="text_area_big"></textarea>
            <br />
            Máximo de caracteres 0/1000
        <div class="div_line_2"></div>
      </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Sube un video del proyecto</h6>
            <p>Los proyectos con videos tienen las más altas posibilidades de éxito. El video es lo que más recordarán las personas y, si les gusta, seguramente lo compartirán. Así que entre más creativo y explicativo, mejor.</p>
            <center><b>Enlaza un video de Youtube o Vimeo</b></center><br />
          <input style="margin-bottom:5px;" type="text" class="input_c" placeholder="URL">
            Si cuentas con un video didactico del proyecto.
            <div class="clear"></div>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
      <div class="box_shadow box_c" style="margin-bottom:30px;">
            <h6>Imagen miniatura del proyecto <span>*</span> </h6>
          <div class="txt_aclara">Entre más imagenes mejor, no sobra ver la locación del proyecto, las personas a las que beneficiara, el equipo que gestiona el proyecto, referencias del resultado final, y la organización que lidera al proyecto.</div>
          <div class="left" style="width:470px">
            <p>Escoja máximo 5 imagenes complementarias para su proyecto, <br />
              en formato JPEG, PNG, GIF, o BMP • Tamaño máximo: 2MB.<br />
              Dimensiones mínimas: 640x480 pixeles • 4:3 relación radio.</p>
          </div>
            <div class="right relative" style="width:111px; height:90px">
               <input type="file" class="jfilestyle" data-theme="blue">
            </div>
      </div>
      
        <div class="clear" style="margin-bottom:0px"></div>
            <input type="submit" value="Borrar Proyecto" class="boton_c left" style="margin-right:10px;">
            <input type="submit" value="Guardar Cambios" class="boton_a left" style="margin-right:10px;">
            <a href="reglamentos.php" class="boton_b left" style="margin-right:10px;">Atrás</a>
            <a href="reglamentos.php" class="boton_b left">Siguiente</a>

        
    </div><!-- // columna C -->
    
    <div class="col_d mar_right">
        <div class="project_box sticker">
            <div class="imgLiquidFill imgLiquid liquid_a">
                <a href="#"  target="_blank">
                    <img class="thumbnail_a" src="assets/img/024.jpg" />
                </a>
            </div>
            <div class="title">Por un país mejor <br />Colombia</div>
            <div class="des">
             Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
            </div>
            
            <div class="resumen">
                <ul class="data_project_min"> 
                    <li class="by">Fundación pies descalsos</li>
                    <li class="by">Rio Magdalena, Colombia </li>
                    <li class="by">6 dias faltantes</li>
                </ul>
            </div>
            
            <div class="line_div_project"></div>
            
            <div class="percent_icon_box first_precent">
                <div class="money_icon_a icons"></div>
                <div class="percent_circle">100%</div>
            </div>
            <div class="percent_icon_box">
                <div class="members_icon_a icons"></div>
                <div class="percent_circle">50%</div>
            </div>
            <div class="percent_icon_box">
                <div class="shop_icon_a icons"></div>
                <div class="percent_circle">20%</div>
            </div>
            <div class="clear"></div>
            
            
            <div class="barra_porcentaje">
                <div class="bar" style="width:45%">45%</div>
            </div>
            
        </div><!-- // project box -->
    </div>
</div>
<script type="text/javascript" src="assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="assets/js/aplication.js"></script>

<?php include"footer.php" ?>  
</body>
</html>