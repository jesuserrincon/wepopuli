<?php

class News extends DataMapper {

    public $model = 'news';
    public $table = 'newsletters';
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getNewsletter(){
        return $this->get();
    }
    
    public function saveNewsletter($object = "") {
        $this->email = $object['email'];
        return $this->save();
    }
    
    public function getNewsletterById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateNewsletter($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
     public function deleteNewsletter($id) {
        $this->where('id',$id)->get();
        return $this->delete();
    } 
    
    public function SuscritoNews($email){
        
        return $this->query("select * from cms_newsletters where email = '$email'");
    }
    public function SuscritoNewspro($email,$idp){
        
        return $this->query("select * from cms_newsletter_proyectos where email = '$email' AND proyecto_id =".$idp);
    }
    public function saveNewsletterproyecto($object){
        
        $email = $object['email'];
        $idpro = $object['idpro'];
        return $this->query_void("INSERT INTO cms_newsletter_proyectos (email,proyecto_id) VALUES ('$email',".$idpro.")");
    }

}

