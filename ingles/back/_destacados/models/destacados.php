<?php

class Destacados extends DataMapper {

    public $model = 'Destacados';
    public $table = 'destacados';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getDestacados(){
        return $this->get();
    }
    
    public function saveDestacados($object = "") {
        $this->titulo1 = $object['titulo'];
        $this->titulo2 = $object['titulo2'];
        $this->texto = $object['texto'];
        $this->imagen = $object['imagen'];
        return $this->save();
    }
    
    public function getDestacadosById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateDestacados($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

