<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author jesusrincon
 */
class Home extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
         $this->load->model(array(
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."banner_home/banner_home",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."usuarios/usuarios",
           ));
    }

    // ----------------------------------------------------------------------    
       public function index($Erno = '') {

           $this->set_title('Bienvenidos a ' . SITENAME, true);
           $categoriasOBJ = new Categorias();
           $results = $categoriasOBJ->getCategorias();
           $this->_data['categorias'] = $results;
           
           $bannershomeDAO = new Banner_home();
            
           $banners_superior= $bannershomeDAO->getBanner_home_by_tipo(1);
           $this->_data['banners_superior'] = $banners_superior;
           
           $bannershomeDAO2 = new Banner_home();
            
           $banners_inferior = $bannershomeDAO2->getBanner_home_by_tipo(2);
           $this->_data['banners_inferior'] = $banners_inferior;
           
           $bannershomeDAO3 = new Banner_home();
            
           $banners_medio = $bannershomeDAO3->getBanner_home_by_tipo(3);
           $this->_data['banners_medio'] = $banners_medio;
           
           //Creamos el objeto y enviamos los proyectos destacados
           $proyectoOBJ = new Proyectos();
           $this->_data['proyectosdestacados'] = $proyectoOBJ->getProyectosDestacados();
           //Fin proyecyos destacados
           
           //Creamos el objeto y enviamos los proyectos 
           $proyectoOBJ = new Proyectos();
           $this->_data['proyectos'] = $proyectoOBJ->getProyectosLimit(0,8);
           //Fin proyecyos 
           //Creamos el objeto y enviamos la cantidad de proyectos que tenemos en la pagina 
           $proyectoOBJ = new Proyectos();
           $this->_data['cantidad'] = $proyectoOBJ->getProyectosByEstado(1);
           //Fin cantidad proyectos 
           
           $this->build('home');
                   
    }
    
    
     public function cambiodecorreo($cedula = '') {
           $this->nativesession->delete('usuario');
           $usuarioOBJ1 = new Usuarios();
           $datosUsuariocon = $usuarioOBJ1->usuarioByCc($cedula);
           if($datosUsuariocon->cambio_correo != ''){ 
           $datos = array(
            'email' => $datosUsuariocon->cambio_correo,
            'cambio_correo' => '',
            'id' => $datosUsuariocon->id,
            );
            $usuarioOBJ = new Usuarios();
            $usuarioOBJ->updateUsuarios($datos);
           }
           $this->_data['cambiodecorreo'] = TRUE;
             
           $this->set_title('Bienvenidos a ' . SITENAME, true);
           $categoriasOBJ = new Categorias();
           $results = $categoriasOBJ->getCategorias();
           $this->_data['categorias'] = $results;
           
           $bannershomeDAO = new Banner_home();
            
           $banners_superior= $bannershomeDAO->getBanner_home_by_tipo(1);
           $this->_data['banners_superior'] = $banners_superior;
           
           $bannershomeDAO2 = new Banner_home();
            
           $banners_inferior = $bannershomeDAO2->getBanner_home_by_tipo(2);
           $this->_data['banners_inferior'] = $banners_inferior;
           
           //Creamos el objeto y enviamos los proyectos destacados
           $proyectoOBJ = new Proyectos();
           $this->_data['proyectosdestacados'] = $proyectoOBJ->getProyectosDestacados();
           //Fin proyecyos destacados
           
           //Creamos el objeto y enviamos los proyectos 
           $proyectoOBJ = new Proyectos();
           $this->_data['proyectos'] = $proyectoOBJ->getProyectosLimit(0,8);
           //Fin proyecyos 
           //Creamos el objeto y enviamos la cantidad de proyectos que tenemos en la pagina 
           $proyectoOBJ = new Proyectos();
           $this->_data['cantidad'] = $proyectoOBJ->getProyectosByEstado(1);
           //Fin cantidad proyectos 
           
           $this->build('home');
                   
    }
    
    public function cargarproyectos(){
        $cantidadactual = $this->input->post('cantidadactual');
           //Creamos el objeto y cargamos los proyectos 
           $proyectoOBJ = new Proyectos();
           $proyectos = $proyectoOBJ->getProyectosLimit($cantidadactual,8);
           //Fin proyecyos 
            $html = '';
            foreach ($proyectos as $item){
                $exitoso = '';
                $urgente = '';
                $cerrado = '';
                if($item->exitoso == 1){ $exitoso = '<div class="banda bnd1">Exitoso</div>';}
                if($item->urgente == 1){ $urgente = '<div class="banda bnd2">Urgente</div>';}
                if($item->estados_id == 3){ $cerrado = '<div class="banda bnd3">Cerrado</div>';}
                
                if($item->valormonetario < 20000){ $desmon = 'ico_desactivado'; }else{ $desmon = ''; }
                if($item->cantidadvoluntariado == 0){ $desvol = 'ico_desactivado'; }else{ $desvol = ''; }
                if($item->cantidadespecie == 0){ $desesp = 'ico_desactivado'; }else{ $desesp = ''; }
                 
                $html .= ' <div class="project_box">
                         '.$exitoso.'
                         '.$urgente.'
                         '.$cerrado.'    
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">
                                <img class="thumbnail_a" src="'.base_url().'uploads/proyectominiatura/'.$item->imagen_miniatura.'" />
                            </a>
                        </div>
                        <div class="cat">'.$item->nombrecateglria.'</div>
                        <div class="title"><a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">'.$item->titulo.' </a></div>
                        <div class="des">'.$item->descripcion_corta.'</div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by">'.$item->fundacion.' </li>
                                <li class="by">Departamento, Pais </li>
                                <li class="by">'.$item->dias_para_cerrar.' dias restantes</li>
                            </ul>
                        </div>
                        <div class="pos_data">
                        <div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                            <div class="money_icon_a '.$desmon.' icons"></div>
                            <div class="percent_circle '.$desmon.'">'.$item->porcentaje_monetario.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a '.$desvol.' icons"></div>
                            <div class="percent_circle '.$desvol.'">'.$item->porcentaje_voluntariado.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a '.$desesp.' icons"></div>
                            <div class="percent_circle '.$desesp.'">'.$item->porcentaje_especie.'%</div>
                        </div>
                        <div class="clear"></div>
                        
                            <center><div style="font-size:12px; margin-bottom:4px">Porcentaje recaudado total</div></center>
                        <div class="barra_porcentaje">
                            <div class="bar" style="width:'.$item->porcentaje_total.'%">'.$item->porcentaje_total.'%</div>
                        </div>
                        </div>
                        
                    </div>';
              }
            echo $html;
    }

    // ----------------------------------------------------------------------
    

}
