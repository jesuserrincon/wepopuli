<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class voluntariado_front extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
         $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."textos/textos",
               CMSPREFIX."gestores/gestores",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0) {
        $this->set_title('Bienvenidos a ' . SITENAME, true);
         $proyectoOBJ = new Proyectos();
        $gestorOBJ = new Gestores();
        //seleccionamos el proyecto a mostrar
        $this->_data["proyecto"] = $proyectoOBJ->getProyectosById($proyectoid);
        //Fin mostrar el proyecto 
        //traemos la informacion de la fundacion y lider
        $this->_data["gestor"] = $gestorOBJ->getGestoresByIdProyecto($proyectoid);
        //Fin informacion fundacion
        //enviamos las ayudas de voluntariado creadas por el usuario
        $voluntariadoOBJ = new Voluntariado();
        $this->_data['voluntariados'] = $voluntariadoOBJ->getVoluntariadoByProyecto($proyectoid);
        //Fin envio voluntariado
        return $this->build('voluntariado_front');
    }
    


    // ----------------------------------------------------------------------


} ?>