<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Front_Controller extends CMS_Controller {

    protected $layout;
    protected $userinfo = null;
    protected $urls = array();

    /*
     * Activa el estilo secundario para el layout activo
     */
    protected $layout_secondary_style = true;
    protected $current_inshaka_url = null;
    protected $current_username = null;

    public function __construct() {
        parent::__construct();

        $this->layout = 'general';
        
        $this->userinfo = $this->ACL->user()->row();

        $this->_data['urls'] = $this->urls;

        $this->load->model(array(
            CMSPREFIX."proyectos/proyectos",
            CMSPREFIX."gestores/gestores",
            CMSPREFIX."categorias/categorias",
            CMSPREFIX."secciones/secciones",
        ));

        $secciones = new Secciones();
        $proyectos = $secciones->seccionesByUbucacion('proyectos');
        $secciones = new Secciones();
        $aprende = $secciones->seccionesByUbucacion('aprende');
        $secciones = new Secciones();
        $acerca = $secciones->seccionesByUbucacion('acerca');
        $secciones = new Secciones();
        $social = $secciones->seccionesByUbucacion('social');

        $this->_data['s_acerca']=$acerca;
        $this->_data['s_aprende']=$aprende;
        $this->_data['s_proyectos']=$proyectos;
        $this->_data['s_social']=$social;
    }

    public function index($proyectoid){

        //enviamos el registro del proyecto
        $proyectoOBJ = new Proyectos();
        $proyecto = $proyectoOBJ->getProyectosById($proyectoid);
        $this->_data["proyecto"] = $proyecto;
        //Fin envio registro del proyecto

        //enviamos el nombre de la categoria
        $categorias2OBJ = new Categorias();
        $categoria = $categorias2OBJ->getCategoriasById($proyecto->categorias_id);
        $this->_data['nomcategoria'] = $categoria->nombre;
        //Fin envio nombre categoria

        $gestoresOBJ = new Gestores();
        $this->_data["gestor"] = $gestoresOBJ->getGestoresByIdProyecto($proyectoid);

    }

    // ----------------------------------------------------------------------

    /**
     * Build mejorado del Front
     * 
     * @param string $view
     * @param type $data
     * @return type
     */
    protected function build($view = null, $data = array()) {

        if (empty($view)) {
            $view = 'body';
        }
        
        // Obtener footer
        $this->get_footer();
        
        $data = array_merge($data, $this->_data);

        
        return $this->template
                        ->set_partial('header', FRONTTEMPLATE . 'partials/header')
                        //->set_partial('header_eng', FRONTTEMPLATE . 'partials/header_eng')
                        ->set_partial('footer', FRONTTEMPLATE . 'partials/footer')
                        //->set_partial('footer_eng', FRONTTEMPLATE . 'partials/footer_eng')
                        ->set_layout(FRONTTEMPLATE . 'layouts/' . $this->layout)
                        ->build($view, $data, false, 'assets');
    }

    protected function build2($view = null, $data = array()) {

        if (empty($view)) {
            $view = 'body';
        }

        // Obtener footer
        //$this->get_footer();

        $data = array_merge($data, $this->_data);

        return $this->template
            ->set_layout(FRONTTEMPLATE . 'layouts/general2')
            ->build($view, $data);
    }

    protected function build3($view = null, $data = array()) {

        if (empty($view)) {
            $view = 'body';
        }

        // Obtener footer
        //$this->get_footer();

        $data = array_merge($data, $this->_data);

        return $this->template
            ->set_layout(FRONTTEMPLATE . 'layouts/general3')
            ->build($view, $data);
    }


    // ----------------------------------------------------------------------

    public function get_footer() {
        # Inicio Footer

        

        # Fin Footer
    }

    // ----------------------------------------------------------------------
    public function enviarCorreo($email,$asunto,$titulo,$mensaje){

        $this->load->library('email');
        $this->load->library('encrypt');

        $this->email->from('info@colombiaincluyente.com', 'Colombia incluyente');
        $this->email->to($email);
        $this->email->subject($asunto);

        $message = $this->vistaEmail($titulo, $mensaje);

        $this->email->message($message);
        if(!$this->email->send()){
            //die('error al enviar el mensaje');
        }
    }

    public function vistaEmail($titulo,$mensaje){

        $vista = '<div class="deviceWidth_b">
        <!-- Wrapper -->
  		

        <table class="deviceWidth_b" width="100%" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; color:#707070" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
        	<tbody>
        	<tr>
            	<td style="padding-left:20px; padding-top:20px">
                	<div style="font-size:20px;">'.$titulo.'</div>
                </td>
            </tr>

        </tbody>
		</table>

        <table class="deviceWidth_b" width="100%" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; color:#707070" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
        	<tbody><tr>
				<td style="padding:20px">
                    '.$mensaje.'
                </td>
			</tr>
        </tbody></table>



        <table class="deviceWidth_b" width="100%" bgcolor="#f0f0f0" style="font-family:Arial, Helvetica, sans-serif; color:#707070; " align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
		 <thead>
         	<tr>
            	<td>
                	<img src="http://3holograms.com.co/colombiaincluyente/mail/img/005.png" width="682" height="80" alt="Colombia Incluyente Logo">
                </td>
            </tr>
         </thead>
        </table>
</div>';

        return $vista;

    }
}
