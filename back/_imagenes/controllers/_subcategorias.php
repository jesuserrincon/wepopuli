<?php

class _Subcategorias extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "SUBCATEGORIAS";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Subcategorias();
        $info = $b->getSubcategorias($idc = '');
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('subcategorias');
    }

    public function lista($idc = '', $value = '') {

        $sesion_categoria = array(
            'id' => $idc
        );
        $this->session->set_userdata($sesion_categoria);

        $this->_data['save'] = $value;
        $b = new Subcategorias();
        $info = $b->getSubcategorias($idc);
        $this->_data['categoria_id'] = $this->session->userdata['id'];
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('subcategorias');
    }

    public function add() {
        $this->_data['nombremodulo'] = "CATEGORIAS / New";
        $this->build('subcategorias_new');
    }

    public function new_subcategorias($idc = '') {
        $datos = array(
            'imagen' => $this->input->post('imagen'),
            'texto' => $this->input->post('texto'),
            'nombre' => $this->input->post('nombre'),
            'cms_categorias_id' => $this->session->userdata['id'],
        );

        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'subcategorias';
        $b = new Subcategorias();
        if ($datos['imagen'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'subcategorias_new';
            if ($b->saveSubcategorias($datos)){
                $build = 'subcategorias';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getSubcategorias($idc);
        $this->_data["info"] = $info;
        return redirect('cms/subcategorias/lista/'.$this->session->userdata['id'].'/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'subcategorias/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."subcategorias/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('subcategorias/'.$data["data"]["file_name"], array('longside'=>'subcategorias/new','width' => 211, 'height' => 227,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Subcategorias();
        $dat = $b->getSubcategoriasById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("subcategorias_edit");
    }
    
    public function update_subcategorias(){
        $b = new Subcategorias();
        $datos = array(
            'id' => $this->input->post('id'),
            'nombre' => $this->input->post('nombre'),
            'texto' => $this->input->post('texto'),
            'cms_categorias_id' => $this->session->userdata['id'],
        );

        $update = $b->updateSubcategorias($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateSubcategorias($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/subcategorias/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Subcategorias();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/subcategorias/lista/".$this->session->userdata['id']."/".$return);
    }

}

