<?php

class Imagenes extends DataMapper {

    public $model = 'imagenes';
    public $table = 'imagenes_complementarias';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getImagenes($idp){
        $this->where('proyectos_id',$idp);
        return $this->get();
    }
    
    public function saveImagenes($object = "") {
        $this->proyecto_id = $object['proyecto_id'];
        $this->imagen = $object['imagen'];
        return $this->save();
    }
    
    public function getImagenesById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateImagenes($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

