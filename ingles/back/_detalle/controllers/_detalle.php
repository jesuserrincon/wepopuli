<?php

class _Detalle extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "PRODUCTOS";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Detalle();
        $info = $b->getDetalle($idcp = '');
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('detalle');
    }

    public function lista($idcp = '', $value = '') {


           $sesion_coloresproducto = array(
              'idcp' => $idcp
           );
            $this->session->set_userdata($sesion_coloresproducto);

        $this->_data['save'] = $value;
        $b = new Detalle();
        $info = $b->getDetalle($this->session->userdata['idcp']);
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('detalle');
    }

    public function add() {
        $this->_data['nombremodulo'] = "DETALLE PRODUCTO / New";
        $this->build('detalle_new');
    }

    public function new_detalle($idcp = '') {
        $datos = array(
            'imagen' => $this->input->post('imagen'),
            'cms_coloresproducto_id' => $this->session->userdata['idcp'],
        );

        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'detalle';
        $b = new Detalle();
        if ($datos['imagen'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'detalle_new';
            if ($b->saveDetalle($datos)){
                $build = 'detalle';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getDetalle($idcp);
        $this->_data["info"] = $info;
        $idcp = $this->session->userdata['idcp'];
        return redirect('cms/detalle/lista/'.$idcp.'/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'detalle/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."detalle/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('detalle/'.$data["data"]["file_name"], array('longside'=>'detalle/new','width' => 387, 'height' => 387,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Detalle();
        $dat = $b->getDetalleById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("detalle_edit");
    }
    
    
    public function update_detalle(){
        $b = new Detalle();
        $datos = array(
            'id' => $this->input->post('id'),
            'cms_coloresproducto_id' => $this->session->userdata['idcp'],
        );

        $update = $b->updateDetalle($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateDetalle($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/detalle/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Detalle();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/detalle/lista/".$this->session->userdata['idcp']."/".$return);
    }

}

