
<!--------------- Menu para usuarios de rol admin -->
<?php if (TRUE === $menu_superadmin) : ?>
    <li class = "limenu">
        <a href = "<?php echo base_url() ?>cms">
            <img src = "<?php echo back_asset('img/icons/home.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Dashboard
            </b>
        </a>
    </li>
    <li class = "limenu">
        <a href = "<?php echo cms_url('admin/administradores') ?>">
            <img src = "<?php echo back_asset('img/icons/administrator.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Administradores
            </b>
        </a>
    </li>
<?php endif; ?>
<?php if (FALSE === $menu_superadmin) : ?>
    <!-- Aca el menu del usuario -->
    <li class = "limenu">
        <a href = "#">
            <img src = "<?php echo back_asset('img/icons/administrator.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Home
            </b>
        </a>
        <ul>  
            <li><a href="<?php echo cms_url('banner') ?>">Banner</a></li>   
            <li><a href="<?php echo cms_url('destacados') ?>">Destacado</a></li>
        </ul>
    </li>

    <li class = "limenu">
        <a href = "#">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>CATALOGO
            </b>
        </a>
        <ul>
            <li><a href="<?php echo cms_url('categorias') ?>">Categorias</a></li>
            <li><a href="<?php echo cms_url('modal') ?>">Articulos</a></li>
            <li><a href="<?php echo cms_url('zonas') ?>">Zonas</a></li>
            <li><a href="<?php echo cms_url('textos/edit/1') ?>">Terminos</a></li>
        </ul>
    </li>

    <!--<li class = "limenu">
        <a href = "<?php echo cms_url('categorias') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>CATEGORIAS
            </b>
        </a>
    </li>-->

    <li class = "limenu">
        <a href = "<?php echo cms_url('colores') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>COLORES
            </b>
        </a>
    </li>

    <li class = "limenu">
        <a href = "<?php echo cms_url('servicios') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>SERVICIOS
            </b>
        </a>
    </li>

    <li class = "limenu">
        <a href = "#">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Rentamuebles
            </b>
        </a>
        <ul>
            <li><a href="<?php echo cms_url('slider') ?>">Slider</a></li>
            <li><a href="<?php echo cms_url('clientes') ?>">Clientes</a></li>
            <li><a href="<?php echo cms_url('rentamuebles') ?>">RENTAMUEBLES</a></li>
        </ul>
    </li>

    <li class = "limenu">
        <a href = "<?php echo cms_url('contactenos') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>CONTACTENOS
            </b>
        </a>
    </li>

    <li class = "limenu">
        <a href = "#">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>FOOTER
            </b>
        </a>
        <ul>
            <li><a href="<?php echo cms_url('redes') ?>">Redes Sociales</a></li>
            <li><a href="<?php echo cms_url('footer') ?>">Información</a></li>
        </ul>
    </li>



<!--
    <li class = "limenu">
        <a href = "#">
            <img src = "<?php echo back_asset('img/icons/group.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Nosotros
            </b>
        </a>
        <ul>  
            <li><a href="<?php echo cms_url('quienes'); ?>">Descripcion</a></li>   
            <li><a href="<?php echo cms_url('lineas') ?>">Linea de Tiempo</a></li>   
            <li><a href="<?php echo cms_url('puntos') ?>">Cobertura</a></li>   
        </ul>
    </li>
    <li class = "limenu">
        <a href = "<?php  echo cms_url('servicios') ?>">
            <img src = "<?php echo back_asset('img/icons/stats_bars.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Servicios
            </b>
        </a>
    </li>
    <li class = "limenu">
        <a href = "<?php echo cms_url('antecedentes') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Antecedentes
            </b>
        </a>
    </li>    
    <li class = "limenu">
        <a href = "<?php echo cms_url('contactos') ?>">
            <img src = "<?php echo back_asset('img/icons/mail.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Contactos
            </b>
        </a>
    </li>    -->
<?php endif; ?>
