<?php

class Detalle extends DataMapper {

    public $model = 'detalle';
    public $table = 'detalle';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getDetalle($idcp){
        $this->where('cms_coloresproducto_id',$idcp);
        return $this->get();
    }

    public function getDetalle2($idp,$idc){

        return $this->query('SELECT * FROM cms_detalle d inner join cms_coloresproducto cp on d.cms_coloresproducto_id = cp.id where cp.cms_productos_id = "'.$idp.'" and cp.cms_colores_id = "'.$idc.'"');
    }

    public function saveDetalle($object = "") {
        $this->imagen = $object['imagen'];
        $this->cms_coloresproducto_id = $object['cms_coloresproducto_id'];
        return $this->save();
    }
    
    public function getDetalleById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateDetalle($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

