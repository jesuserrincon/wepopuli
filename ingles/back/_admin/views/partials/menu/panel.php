<!--------------- Menu para usuarios de rol admin -->
<?php if (TRUE === $menu_superadmin) : ?>
    <li class = "limenu">
        <a href = "<?php echo base_url() ?>cms">
            <img src = "<?php echo back_asset('img/icons/home.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Dashboard
            </b>
        </a>
    </li>
    <li class = "limenu">
        <a href = "<?php echo cms_url('admin/administradores') ?>">
            <img src = "<?php echo back_asset('img/icons/administrator.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Administradores
            </b>
        </a>
    </li>
<?php endif; ?>
<?php if (FALSE === $menu_superadmin) : ?>
    <!-- Aca el menu del usuario -->
    <li class = "limenu">
        <a href = "#">
            <img src = "<?php echo back_asset('img/icons/administrator.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Home
            </b>
        </a>
        <ul>  
            <li><a href="<?php echo cms_url('banner_home') ?>">Banner</a></li>   
        </ul>
    </li>
    
    <li class = "limenu">
        <a href = "#">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Textos
            </b>
        </a>
        <ul>  
            <li><a href="<?php echo cms_url('textos/edit/1') ?>">Busca apoyo</a></li> 
            <li><a href="<?php echo cms_url('textos/edit/2') ?>">Reglamento</a></li> 
            <li><a href="<?php echo cms_url('textos/edit/3') ?>">Revision</a></li> 
        </ul>
    </li>
    
    
    
    
            
    <li class = "limenu">
        <a href = "<?php echo cms_url('categorias') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Categorias
            </b>
        </a>
    </li>
    <li class = "limenu">
        <a href = "<?php echo cms_url('secciones') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Secciones
            </b>
        </a>
    </li>
      <li class = "limenu">
        <a href = "<?php echo cms_url('usuarios') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Usuarios
            </b>
        </a>
    </li>
    
      <li class = "limenu">
        <a href = "<?php echo cms_url('proyectos') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Proyectos
            </b>
        </a>
    </li>
 <li class = "limenu">
        <a href = "<?php echo cms_url('proyectos/cambiardias') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Dias Proyectos
            </b>
        </a>
    </li>
    
 <li class = "limenu">
        <a href = "<?php echo cms_url('newsletter') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Newsletter
            </b>
        </a>
    </li>

    



<!--
    <li class = "limenu">
        <a href = "#">
            <img src = "<?php echo back_asset('img/icons/group.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Nosotros
            </b>
        </a>
        <ul>  
            <li><a href="<?php echo cms_url('quienes'); ?>">Descripcion</a></li>   
            <li><a href="<?php echo cms_url('lineas') ?>">Linea de Tiempo</a></li>   
            <li><a href="<?php echo cms_url('puntos') ?>">Cobertura</a></li>   
        </ul>
    </li>
    <li class = "limenu">
        <a href = "<?php  echo cms_url('servicios') ?>">
            <img src = "<?php echo back_asset('img/icons/stats_bars.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Servicios
            </b>
        </a>
    </li>
    <li class = "limenu">
        <a href = "<?php echo cms_url('antecedentes') ?>">
            <img src = "<?php echo back_asset('img/icons/world.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Antecedentes
            </b>
        </a>
    </li>    
    <li class = "limenu">
        <a href = "<?php echo cms_url('contactos') ?>">
            <img src = "<?php echo back_asset('img/icons/mail.png') ?>" style = "margin-right: -20px">
            <span class = "ico gray shadow" ></span><b>Contactos
            </b>
        </a>
    </li>    -->
<?php endif; ?>
