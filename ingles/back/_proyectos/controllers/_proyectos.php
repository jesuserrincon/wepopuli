<?php

class _Proyectos extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "PROYECTOS";
        $this->load->model(array(
            CMSPREFIX . "proyectos/estados",
        ));
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Proyectos();
        $info = $b->getProyectos();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('proyectos');
    }

    
     public function destacar(){
         $idP = $this->input->post('id');
         $proyectosOBJ = new Proyectos();
         $proyecto = $proyectosOBJ->getProyectosById($idP);
         if($proyecto->destacado == 0){
              $datos = array(
                'id' => $idP,
                'destacado' => 1,
            );
         }else{
             $datos = array(
                'id' => $idP,
                'destacado' => 0,
            );
         }
         $proyectosOBJ->updateProyectos($datos);
    }
    
     public function reporte_especifico($id = 0){
         
        $this->load->library('excel');
        $objPHPExcel = new Excel();
        $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

        
        //Llamamos la consulta y obtenemos los usuarios registrados
        $b = new Proyectos();
        $info = $b->getReporteByProyecto($id);
        $infosus = $b->getSuscritosByProyecto($id);
        //Fin obtener datos
        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nombres');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Apellidos');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Proyecto');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Tipo');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Ayuda voluntariado');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Ayuda especie');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Monto de ayuda');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Fecha');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Suscritos');
        /* $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Pais');
         $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Departamento');
         $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Municipio');
         $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Dirección');*/
        
       
        
        $i = 3;
        foreach($info as $item){
            
               switch ($item->tipo){
                    case 0:
                        $tipo = 'Voluntariado';
                    break;
                    case 1:
                        $tipo = 'Especie';
                    break;
                    case 2:
                        $tipo = 'Monetario';
                    break;
                }
            
                
             $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $item->nombres);
             $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $item->apellidos);
             $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $item->nombreproyecto);
             $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $tipo);
             $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $item->titulo);
             $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '');
             $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, 0);
             $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $item->fecha);
             
             $i++;
        }
        $i = 3;
        foreach($infosus as $item){
          $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $item->email);
           $i++;
        }
         /*$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $item->pais);
         $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $item->departamento);
         $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $item->municipio);
         $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $item->direccion);*/
        
        // Rename sheet
        //echo date('H:i: s') . " Rename sheet\n";
        $objPHPExcel->getActiveSheet()->setTitle('Reporte ');


        // Save Excel 2007 file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte_usuario.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        // Save it as an excel 2003 file
        //$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter->save("nameoffile.xls");
    
     }
     public function reporte_general() {
         
        $this->load->library('excel');
        $objPHPExcel = new Excel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        
        //Llamamos la consulta y obtenemos los datos a exportar
        $proyectosOBJ =  new Proyectos();
        $reporte = $proyectosOBJ->getProyectosReportegeneral();
        //Fin obtener datos
        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nombre Proyecto');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Usuario Creador Proyecto');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Líder Proyecto');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Fundación');
         $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Pais');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Departamento');
         $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Municipio');
         $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Dirección');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Dirección fundación');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Teléfono');
        $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Fecha Inicio');
        $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Fecha Fin');
        $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Status');
        $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Apoyo Monetario Solicitado');
        $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Apoyo Voluntariado Solicitado');
        $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Apoyo en Especie Solicitado');
        $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Apoyo Monetario Conseguido');
        $objPHPExcel->getActiveSheet()->setCellValue('R1', 'Apoyo Voluntariado Conseguido');
        $objPHPExcel->getActiveSheet()->setCellValue('S1', 'Apoyo en Especie Conseguido');
        $objPHPExcel->getActiveSheet()->setCellValue('T1', '% Apoyo Monetario Conseguido');
        $objPHPExcel->getActiveSheet()->setCellValue('U1', '% Apoyo Voluntariado Conseguido');
        $objPHPExcel->getActiveSheet()->setCellValue('V1', '% Apoyo en Especie Conseguido');
        
        $i = 3;
        foreach($reporte as $item){
             $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $item->titulo);
             $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $item->nombres.' '.$item->apellidos);
             $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $item->nombreslider);
             $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $item->nombregestor);
             $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $item->pais);
             $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $item->departamento);
             $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $item->municipio);
             $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $item->direccion);
             $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $item->direccion);
             $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $item->telefono);
             $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $item->fechainicio);
             $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $item->fechafin);
             $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $item->estado);
             $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $item->valormonetario);
             $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $item->voluntariadosolicitado);
             $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $item->especiesolicitado);
             $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $item->monetarioconseguido);
             $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $item->voluntariadoconseguido);
             $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $item->especieconseguido);
             $objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $item->porcentaje_monetario);
             $objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $item->porcentaje_voluntariado);
             $objPHPExcel->getActiveSheet()->setCellValue('V'.$i, $item->porcentaje_especie);
             $i++;
        }
        
        // Rename sheet
        //echo date('H:i: s') . " Rename sheet\n";
        $objPHPExcel->getActiveSheet()->setTitle('Reporte general');


        // Save Excel 2007 file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte_general.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        // Save it as an excel 2003 file
        //$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter->save("nameoffile.xls");
    }
    
    public function urgente(){
         $idP = $this->input->post('id');
         $proyectosOBJ = new Proyectos();
         $proyecto = $proyectosOBJ->getProyectosById($idP);
         if($proyecto->urgente == 0){
              $datos = array(
                'id' => $idP,
                'urgente' => 1,
            );
         }else{
             $datos = array(
                'id' => $idP,
                'urgente' => 0,
            );
         }
         $proyectosOBJ->updateProyectos($datos);
    }
    public function exitoso(){
         $idP = $this->input->post('id');
         $proyectosOBJ = new Proyectos();
         $proyecto = $proyectosOBJ->getProyectosById($idP);
         if($proyecto->exitoso == 0){
              $datos = array(
                'id' => $idP,
                'exitoso' => 1,
            );
         }else{
             $datos = array(
                'id' => $idP,
                'exitoso' => 0,
            );
         }
         $proyectosOBJ->updateProyectos($datos);
    }
    
    
    
    
    
    public function quitarexitoso(){
         $idP = $this->input->post('id');
         $proyectosOBJ = new Proyectos();
         $proyecto = $proyectosOBJ->getProyectosById($idP);
         if($proyecto->quitarexitoso == 0){
              $datos = array(
                'id' => $idP,
                'quitarexitoso' => 1,
            );
         }else{
             $datos = array(
                'id' => $idP,
                'quitarexitoso' => 0,
            );
         }
         $proyectosOBJ->updateProyectos($datos);
    }
    
    
    
    
    
    public function add() {
        $this->_data['nombremodulo'] = "CATEGORIAS / New";
        $this->build('proyectos_new');
    }

    public function new_Proyectos() {
        $datos = array(
            'banner_superior' => $this->input->post('imagen'),
            'banner_inferior' => $this->input->post('imagen2'),
            'nombre' => $this->input->post('nombre'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'proyectos';
        $b = new Proyectos();
        if ($datos['banner_superior'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'proyectos_new';
            if ($b->saveProyectos($datos)){
                $build = 'proyectos';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getProyectos();
        $this->_data["info"] = $info;
        return redirect('cms/proyectos/index/'.$save);
        
    }

    
    
    
    
    
    
    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'proyectos/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."proyectos/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('proyectos/'.$data["data"]["file_name"], array('longside'=>'proyectos/new','width' => 1000, 'height' => 223,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $estados = new Proyectos();
        $estados->getEstados();
        $b = new Proyectos();
        $dat = $b->getProyectosById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        $this->_data["estados"] = $estados;
        return $this->build("proyectos_edit");
    }
    
    public function update_Proyectos(){


        $b = new Proyectos();
        if($this->input->post('actualizarfecha_2') == 'SI'){

            $fecha = date('Y-m-d');
            $fechafin = strtotime ( '+'.$this->input->post('dias_para_cerrar').' day' , strtotime ( $fecha ) ) ;
            $fechafin = date ( 'Y-m-d' , $fechafin );
            $datos = array(
                'id' => $this->input->post('id'),
                'fechafin' => $this->input->post('fechafin'),
                'porcentaje_total' => $this->input->post('porcentaje_total'),
                'estados_id' => $this->input->post('estados_id'),
                'fechainicio' => date('Y-m-d'),
                'fechafin' => $fechafin,
                'ordenhome' => $this->input->post('ordenhome')
            );
        }else{
           $datos = array(
            'id' => $this->input->post('id'),
            'fechafin' => $this->input->post('fechafin'),
            'porcentaje_total' => $this->input->post('porcentaje_total'),
            'estados_id' => $this->input->post('estados_id'),
               'ordenhome' => $this->input->post('ordenhome')

        ); 
        }
        
                        
        
        $update = $b->updateProyectos($datos);

        $proyecto = $b->getDatosAprobados($this->input->post('id'));
        //validamos si el proyecto esta en estado 1 aprobado enviamos un correo al usuario creador

        switch($this->input->post('estados_id')){
            case Estados::APROBADO:
                $titulo = 'Proyecto: '.$proyecto->titulo;
                $mensaje = 'Gracias por postular el proyecto,este ha sido aprobado puede estar pendiente de las donaciones que realicen día a día';
                //$this->enviarCorreo($proyecto->email,$asunto='Seguimiento proyecto.',$titulo,$mensaje);
                break;
            case Estados::RECHAZADO;
                $titulo = 'Proyecto: '.$proyecto->titulo;
                $mensaje = 'Su proyecto ha sido rechazado';
                //$this->enviarCorreo($proyecto->email,$asunto='Seguimiento proyecto.',$titulo,$mensaje);
                break;
            case Estados::CERRADO;
                $titulo = 'Proyecto: '.$proyecto->titulo;
                $mensaje = 'Su proyecto ha sido cerrado';
                //$this->enviarCorreo($proyecto->email,$asunto='Seguimiento proyecto.',$titulo,$mensaje);
                break;
            case Estados::CANCELADO;
                $titulo = 'Proyecto: '.$proyecto->titulo;
                $mensaje = 'Su proyecto ha sido cancelado';
                //$this->enviarCorreo($proyecto->email,$asunto='Seguimiento proyecto.',$titulo,$mensaje);
                break;
            case Estados::DESPUBLICADO;
                $titulo = 'Proyecto: '.$proyecto->titulo;
                $mensaje = 'Su proyecto ha sido despublicado';
                //$this->enviarCorreo($proyecto->email,$asunto='Seguimiento proyecto.',$titulo,$mensaje);
                break;
            case Estados::REVISADO;
                $titulo = 'Proyecto: '.$proyecto->titulo;
                $mensaje = 'Su proyecto ha sido revisado y es viable. Puede editarlo, siguiendo las indicaciones enviadas a su correo electrónico.';
                //$this->enviarCorreo($proyecto->email,$asunto='Seguimiento proyecto.',$titulo,$mensaje);
                break;
        }



        //Fin envio correo
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/proyectos/edit/".$this->input->post("id")."/".$a);
    }
    
    
    public function delete($id = ""){
        $b = new Proyectos();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/proyectos/index/".$return);
    }
    public function cambiardias(){
        $b = new Proyectos();
        $this->_data['dias'] = $b->getDefaultdiascerrar();
        return $this->build('actualizardias');
    }
    
    public function UpdateDefaultDias(){
         $b = new Proyectos();
         $b->UpdateDefaultDias($this->input->post('dias'));
         return redirect("cms/proyectos/cambiardias/1");
        
    }

}

