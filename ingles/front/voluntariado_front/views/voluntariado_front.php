<link href="<?php echo base_url(); ?>assets/css/colombia_incluyente_movil.css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/apply.js"></script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "09422a6d-4673-4a62-ac1c-160a92973882", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<!-- Add mousewheel plugin (this is optional) -->	
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancy.js"></script>
<div class="clear"></div>
<div class="txt_reglamento device_300 pad_bottom_0_mobile pad_5_mobile">
  <h2 class="h2_b" style="font-size:44px"><?php echo $proyecto->titulo; ?></h2>
  <h2 class="h2_des">Project by: <?php echo $gestor->nombre; ?></h2>
</div>

    
    
<div class="box_shadow_big clearfix device_320">

  <div class="col_e min_h_900 pad_int_20 device_300">
       <img src="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $proyecto->imagen_miniatura; ?>" class="rounded_img device_300" width="590" />
  	   <div class="spacer"></div>
       
       <h2 class="h2_c left"> Volunteer!</h2>
  		
        <div class="icon_apoya icon_apoya_3"></div>
        <div class="clear"></div>

        
    <h2 class="h2_azul device_320">It's time to support the change!</h2>
  		<div class="txt_14">Your support adds up to make a real difference!</div>
        
    	<h2 class="h2_c left">Select your volunteer work.</h2>
       <div class="clear"></div>
       <input type="hidden" name="idvoluntariado" id="idvoluntariado" value="0">
       <?php 
       $contador = 0;
       $contadorcompletos = 0;
       foreach ($voluntariados as $item){ $contador++; ?>
       	<div class="box_shadow3 relative">
        	<div class="pad_box_10">
                    <?php if($item->cantidad == $item->donados){ $contadorcompletos++;?>
                    Complete!!
                    <?php }else{ ?>
                    <a href="" onclick="javascript:checkvolintariado(<?php echo $item->id; ?>)" id="a<?php echo $item->id; ?>" class="no_apply2 left grupovol">Apply</a>
                    <?php } ?>
                    <div class="clear"></div>
            	<h2 class="h3_b"><?php echo $item->titulo;?></h2>
                <div class="txt_voluntario">
                <p><?php echo $item->descripcion;?></p>
                </div>
        	</div>
            <div  class="active_box_green grupovoldiv"></div>
        </div>
       <?php } ?>
        
       
        <div class="spacer pad_top_10_mobile clear" style="padding-top:20px;"></div>
       <?php if($contadorcompletos == $contador){?>
        <a href="<?php echo base_url();?>detalle_proyecto/index/<?php echo $proyecto->id; ?>" class="boton_a" style="display:block; float:left; margin-right:10px; margin-left:115px">Back</a>
       <?php }else{ ?>
        <a href="<?php echo base_url();?>detalle_proyecto/index/<?php echo $proyecto->id; ?>" class="boton_a" style="display:block; float:left; margin-right:10px; margin-left:115px">Back</a>
        <a href="javascript:validar_paso()" class="boton_a" style="display:block; float:left">Continue</a>
       <?php } ?>

  </div>
  
  
  
  
  
    <div class="col_f device_320">
    	
        <div class="logo_fundacion">
  <?php 
                    function image_exists($url) {
                        if(@getimagesize($url)){
                        return true;
                      }else{
                       return false;
                     }
                    }
                    $imagen = base_url().'uploads/proyectos/imagenes/'.$proyecto->imagen; 
                    $liderimagen = base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; 
                    $fundacionimagen = base_url().'uploads/proyectos/gestores/'.$gestor->imagen; 
                   ?>       
            <a href="#inline2" onclick="javascript:$('.fancybox').fancybox()"  class="fancybox">
                    
       <img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" width="200" /></a>
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>By: <br /><a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Leader: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>
        <div class="state box_min_sh" style="padding:13px 0px">
        	<div class="estado left"><div class="active_point left"><span>Status:</span> Activo</div></div>
            <div class="clear" style="margin-bottom:5px;"></div>
            <?php 
             function dameFecha($fecha,$dia)
                {   list($day,$mon,$year) = explode('/',$fecha);
                    return date('d/m/Y',mktime(0,0,0,$mon,$day+$dia,$year));        
                }
            ?>
            <div class="estado"><span>Due Date:</span> <?php echo dameFecha(date('d/m/Y'),$proyecto->dias_para_cerrar); ?></div>
        </div>
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Supported</center></div>
            <div class="barra_porcentaje2">
                <div class="bar2" style="width:<?php echo $proyecto->porcentaje_voluntariado;?>%"><?php echo $proyecto->porcentaje_voluntariado;?>%</div>
            </div>

		</div>
        <div class="spacer"></div>

        <div class="div_line"></div>
        
        <div class="spacer"></div>
        
        <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">IMPORTANT</h3>
            <div class="txt_box_min">
            	Wepopuli is a social projects and organizations search engine, a social crowdsourcing platform where you can find organizations and social projects from around the planet to help as a volunteer, --on-field or from your home or office--, based on sector, localization, objectives, due dates, required skills, etc., and according to your talents and interests.
			</div>
            <div class="line_color"></div>
		</div>
		
        <div class="spacer"></div>	
            
      <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">FAQs</h3>
            <div class="txt_box_min">
            	<h4 class="h4_pregunta"><a href="<?php echo base_url(); ?>seccion/index/5">How do I contribute to a project?</a></h4>
            	Each project has its own needs. To check them out and see how you can support them, you must register on our website and visit the project page. There, you can press the volunteer button, pick the kind of help you are willing to do and send the offering to get a response from the organization. 
			</div>
            <div class="line_color color2"></div>
		</div>


        <div class="spacer"></div>
        <div class="spacer"></div>

    </div>
    
    
    <div class="div_line clear dis_none_movil"></div>
    
    <div class="pad_int_20">
    
    
    </div>

</div>
    

<script>
    function checkvolintariado(id){
        $( "a" ).removeClass( "apply2" );
        $('.grupovol').html('Aplicar');
        $('.grupovoldiv').css("display", "none");
        $('#idvoluntariado').val(id);
    }
    function validar_paso(){
        var idvol = $('#idvoluntariado').val();
        if(idvol == 0){
            alert('You must choose at least one volunteer work to continue.');
            return false;
        }else{
            location.href = '<?php echo base_url().'voluntariado_paso2/index/'.$proyecto->id.'/';?>'+idvol;
        }
    }
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/aplication.js"></script>

<div id="inline2" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px; color:#1c75bc">Organization</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->nombre;?></h2>
    	<div class="spacer"></div>
    	<div class="txt_user">
            <p><?php echo $gestor->descripcion;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Project by: <a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Leader: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
	</div>
    
</div>

<div id="inline3" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px"><?php echo $gestor->nombreslider.' '.$gestor->apellidoslider;?></h2>
    	
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px"><?php echo $gestor->nombre;?></h3>
    
    	<div class="txt_user">
            <p><?php echo $gestor->acercadelider;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($liderimagen)){ echo base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; }else{ echo 'img/080.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Contact Information</span></div>
            <?php  if($gestor->emaillider != ''){ echo '<div class="dato"><b>Email: <a href="mailto:'.$gestor->emaillider.'" style="color:#602483">'.$gestor->emaillider.'</a></b></div>'; } ?>
            <?php  if($gestor->facebooklider != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebooklider.'</b></div>'; } ?>
            <?php  if($gestor->twitterlider != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitterlider.'</b></div>'; } ?>
        </div>
	</div>
    
</div>

    