<?php

class Opciones extends DataMapper {

    public $model = 'opciones';
    public $table = 'opciones';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getOpciones(){
        return $this->get();
    }
    
    public function saveOpciones($object = "") {
        $this->nombre = $object['nombre'];
        $this->precio = $object['precio'];
        $this->cms_modal_id = $object['cms_modal_id'];

        return $this->save();
    }
    
    public function getOpcionesById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateOpciones($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

