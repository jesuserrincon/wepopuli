<?php

class _Productos extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "PRODUCTOS";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Productos();
        $info = $b->getProductos($ids = '');
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('productos');
    }

    public function lista($ids = '', $value = '') {


           $sesion_subcategoria = array(
              'ids' => $ids
           );
            $this->session->set_userdata($sesion_subcategoria);

        $this->_data['save'] = $value;
        $b = new Productos();
        $info = $b->getProductos($this->session->userdata['ids']);
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('productos');
    }

    public function lista2($idc2 = '', $value = '') {


        $sesion_categoria2 = array(
            'idc2' => $idc2
        );
        $this->session->set_userdata($sesion_categoria2);

        $this->_data['save'] = $value;
        $b = new Productos();
        $info = $b->getProductos2($this->session->userdata['idc2']);
        $this->_data["info"] = $info;
        $this->_data["tipo"] = 2;
        $this->_data["count"] = $b->count();
        return $this->build('productos');
    }

    public function add($tipo = null) {
        $this->_data['nombremodulo'] = "PRODUCTOS / New";
        if($tipo == 1){
            $this->_data['tipo'] = $tipo;
        }
        $this->build('productos_new');
    }

    public function new_productos($ids = '') {
            $datos = array(
                'imagen' => $this->input->post('imagen'),
                'texto' => $this->input->post('texto'),
                'nombre' => $this->input->post('nombre'),
                'precio' => $this->input->post('precio'),
                'descripcion'=>$this->input->post('descripcion'),
                'cms_subcategorias_id' => $this->session->userdata['ids'],
            );



        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'productos';
        $b = new Productos();
        if ($datos['imagen'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'productos_new';
            if ($b->saveProductos($datos)){
                $build = 'productos';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getProductos($ids);
        $this->_data["info"] = $info;
        $ids = $this->session->userdata['ids'];
        return redirect('cms/productos/lista/'.$ids.'/'.$save);
        
    }

    public function new_productos2($idc2 = '') {
        $datos = array(
            'imagen' => $this->input->post('imagen'),
            'texto' => $this->input->post('texto'),
            'nombre' => $this->input->post('nombre'),
            'precio' => $this->input->post('precio'),
            'descripcion'=>$this->input->post('descripcion'),
            'cms_categorias_id' => $this->session->userdata['idc2'],
        );


        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'productos';
        $b = new Productos();
        if ($datos['imagen'] != ''){

            $this->_data['save'] = FALSE;
            $build = 'productos_new';
            if ($b->saveProductos2($datos)){
                $build = 'productos';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getProductos($idc2);
        $this->_data["info"] = $info;
        $ids = $this->session->userdata['ids'];
        return redirect('cms/productos/lista2/'.$this->session->userdata['idc2'].'/'.$save);

    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'productos/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."productos/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('productos/'.$data["data"]["file_name"], array('longside'=>'productos/new','width' => 211, 'height' => 227,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = "", $tipo = null){
        $b = new Productos();
        $dat = $b->getProductosById($id);
        if($tipo == 1){
            $this->_data['tipo'] = $tipo;
        }
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("productos_edit");
    }
    
    public function update_productos(){
        $b = new Productos();
        $datos = array(
            'id' => $this->input->post('id'),
            'nombre' => $this->input->post('nombre'),
            'texto' => $this->input->post('texto'),
            'precio' => $this->input->post('precio'),
            'descripcion_pdf'=>$this->input->post('descripcion')
        );


        $update = $b->updateProductos($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateProductos($datos);
        }
        if ($update){
            $a = TRUE;
        }

        if(isset($_POST['tipo'])){
        return redirect("cms/productos/edit/".$this->input->post("id")."/".$a."/1");
        }else{
            return redirect("cms/productos/edit/".$this->input->post("id")."/".$a);
        }
    }


    
    public function delete($id = "", $tipo = null){
        $b = new Productos();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        if($tipo == 1){
        return redirect("cms/productos/lista2/".$this->session->userdata['idc2']."/".$return);
        }else{
            return redirect("cms/productos/lista/".$this->session->userdata['ids']."/".$return);
        }
    }

}

