<?php

class _Destacados extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "Destacados";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {

        $this->_data['save'] = $value;
        $d = new Destacados();
        $info = $d->getDestacados();
        $this->_data["info"] = $info;
        $this->_data["count"] = $d->count();
        return $this->build('destacados');
    }

    public function add() {
        $this->_data['nombremodulo'] = "Destacados / New";
        $this->build('destacados_new');
    }

    public function new_Destacados() {
        $datos = array(
            'imagen' => $this->input->post('imagen'),
            'texto' => $this->input->post('texto'),
            'titulo' => $this->input->post('titulo'),
            'titulo2' => $this->input->post('titulo2'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'Destacados';
        $b = new Destacados();
        if ($datos['imagen'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'destacados_new';
            if ($b->saveDestacados($datos)){
                $build = 'Destacados';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getDestacados();
        $this->_data["info"] = $info;
        return redirect('cms/destacados/index/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'destacados/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."destacados/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('destacados/'.$data["data"]["file_name"], array('longside'=>'destacados/new','width' => 455, 'height' => 194,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Destacados();
        $dat = $b->getDestacadosById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("destacados_edit");
    }
    
    public function update_Destacados(){
        $b = new Destacados();
        $datos = array(
            'id' => $this->input->post('id'),
            'titulo1' => $this->input->post('titulo'),
            'texto' => $this->input->post('texto'),
            'titulo2' => $this->input->post('titulo2')

        );
        $update = $b->updateDestacados($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateDestacados($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/destacados/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Destacados();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/destacados/index/".$return);
    }

}

