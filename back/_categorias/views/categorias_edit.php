<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo $nombremodulo ?></span>
        <br /><?php echo anchor('cms/categorias/', 'Volver', 'class="uibutton icon special answer" style="float:right;position: relative;top: -5px"') ?>                 
    </div><!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div class="imu_info" id="info"></div>
                    <?php echo form_open_multipart('cms/categorias/update_Categorias/', 'id="form"') ?>
                        <input style="width:250px" type="hidden"  id="id" name="id" class="small" value="<?php echo $info->id; ?>" />

                    <div class="section">
                        <label>Nombre</label>
                        <div><input value="<?php echo $info->nombre; ?>"  class="large text" type="text"  id="nombre" name="nombre" /></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="nombre"></span></span>
                        <script type="text/javascript">
                            $("#nombre").limit("23",".nombre");
                        </script>
                    </div>

                    <div class="section">
                        <label>Imagen Superior</label>
                        <label>Subir nueva imagen (1000 px X 223 px)</label><br/><br/>
                        <div>
                            <img class="cuadro_edicion_fotos" id="img1"  src="<?php echo base_url()."uploads/categorias/new/".$info->banner_superior; ?>" width="223">
                            <div class="cuadro_edicion_fotos" id="divimg1"></div>
                            <br />
                            <input style="border-radius: 5px; " type="file" name="nombre" id="fileUpload1" class="fileUpload2" />    
                            <input type="hidden" name="imagen" id="imagen" />
                        </div>
                    </div>
                         <div class="section">
                            <label>Link Superior</label>
                            <div><input  class="large text" type="text" value="<?php echo $info->link_superior; ?>"  id="link_superior" name="link_superior" /></div>
                        </div>
                        
                           <div class="section">
                        <label>Imagen Inferior</label>
                        <label>Subir nueva imagen (1000 px X 223 px)</label><br/><br/>
                        <div>
                            <img class="cuadro_edicion_fotos" id="img2"  src="<?php echo base_url()."uploads/categorias/new/".$info->banner_inferior; ?>" width="223">
                            <div class="cuadro_edicion_fotos" id="divimg2"></div>
                            <br />
                            <input style="border-radius: 5px; " type="file" name="nombre2" id="fileUpload2" class="fileUpload2" />    
                            <input type="hidden" name="imagen2" id="imagen2" />
                        </div>
                    </div>
                  
                     <div class="section">
                            <label>Link Inferior</label>
                            <div><input  class="large text" type="text" value="<?php echo $info->link_inferior; ?>" id="link_inferior" name="link_inferior" /></div>
                        </div>
                        
                        
                    <div style="width: 100px;height: 30px;position: relative;top: 30px">
                        <input onclick="valida();" type="button" value="Guardar" class="uibutton confirm" />
                        
                    </div>
                    <?php echo form_close() ?>
                    <p>&nbsp;</p>
                 </fieldset>
                <br>
            </div>
        </div>
    </div>	
</div><!-- End content -->
<script type="text/javascript">
    function valida(){
        var titulo = $("#titulo").val();
        var texto = $("#texto").val();
        if (titulo == "" || texto == ""){
            $('#info').focus();
            showError('Complete todos los campos.',3000);
        }else{
            $('#form').submit();
        }
    }
    $(document).ready(function() {
        var texto = '';
        var nextinput = parseInt($("#inputTotal").val());
        $('#fileUpload1').uploadify({
                    'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                    'script'            : '<?php echo site_url(); ?>' + 'cms/categorias/upload/',
                    'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                    'auto'              : true,
                    'folder'            : '',
                    'queueSizeLimit'    : 3,
                    'multi'             : false,
                    'fileExt'           : '*.jpg;*.jpeg;*.png;*gif',
                    'auto'              : true,
                    'buttonText'        : 'Cargar imagen.',
                    'onComplete'  : function(event, queueId, fileObj, response, data) {
                var responseJson = $.parseJSON(response);
                
                if (responseJson.ok === true) {
                        $("#img1").attr('src','<?php echo base_url() ?>uploads/categorias/'+responseJson.data.file_name);
                        $("#img1").hide(800).delay(2000).show(800);
                        $("#divimg1").html('<h2>Cargando imagen...</h2>')
                        $("#divimg1").show(850).delay(2000).hide(750);
                        $("#imagen").val(responseJson.data.file_name)
                } else {
                        showError('Problemas al carga imagen.',3000);                            
                }
            }
                });   
                
                
                
                
                    
             //Subimos la segunda imagen
             
               $('#fileUpload2').uploadify({
                    'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                    'script'            : '<?php echo site_url(); ?>' + 'cms/categorias/upload/',
                    'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                    'auto'              : true,
                    'folder'            : '',
                    'queueSizeLimit'    : 3,
                    'multi'             : false,
                    'fileExt'           : '*.jpg;*.jpeg;*.png;*.gif',
                    'auto'              : true,
                    'buttonText'        : 'Cargar imagen.',
                    'onComplete'  : function(event, queueId, fileObj, response, data) {
                var responseJson = $.parseJSON(response);
                //alert(responseJson.ok);
                if (responseJson.ok === true) {
                        $("#img2").attr('src','<?php echo base_url()?>uploads/categorias/'+responseJson.data.file_name);
                        $("#img2").hide(800).delay(2000).show(800);
                        $("#divimg2").html('<h2>Cargando imagen...</h2>')
                        $("#divimg2").show(850).delay(2000).hide(750);
                        $("#imagen2").val(responseJson.data.file_name)
                } else {
                        showError('Problemas al carga imagen.',3000);                            
                }
            }
                });
                
                
                
    
    });
</script>
 <script>
    <?php if (isset($update)){
            if ($update ==  1){?>
            showSuccess('Actualización correcta.',3000);
            <?php } ?>
    <?php } ?>
</script>