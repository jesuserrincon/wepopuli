<?php

class Modal extends DataMapper {

    public $model = 'modal';
    public $table = 'modal';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getModal(){
        return $this->get();
    }
    
    public function saveModal($object = "") {
        $this->titulo = $object['titulo'];
        $this->texto = $object['texto'];
        return $this->save();
    }
    
    public function getModalById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateModal($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

