<?php

class Usuarios extends DataMapper {

    public $model = 'usuarios';
    public $table = 'usuarios';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getUsuarios(){
        return $this->get();
    }
    
    
    public function getReporteByUsuarios($id = 0){
        return $this->query('SELECT nombres,apellidos,tipo,vol.titulo titulo,nombre,fecha,pro.titulo nombreproyecto
                             FROM cms_ayuda_voluntariado ayu 
                             INNER JOIN cms_usuarios usu ON usu.id = ayu.usuarios_id
                             INNER JOIN cms_proyectos pro ON pro.id = ayu.proyectos_id
                             LEFT JOIN cms_voluntariado vol ON vol.id = ayu.tipo_voluntariado
                             LEFT JOIN cms_especie esp ON esp.id = ayu.tipo_voluntariado
                             WHERE ayu.usuarios_id = '.$id);
    }
    
    public function saveUsuarios($object = "") {
        $this->nombres = $object['nombres'];
        $this->apellidos = $object['apellidos'];
        $this->telefono = $object['telefono'];
        $this->telefono2 = $object['telefono2'];
        $this->cedula = $object['cedula'];
        $this->email = $object['email'];
        $this->pais = $object['pais'];
        $this->departamento = $object['departamento'];
        $this->direccion = $object['direccion'];
        $this->facebook = $object['facebook'];
        $this->twitter = $object['twitter'];
        $this->ciudad = $object['ciudad'];
        $this->sexo = $object['sexo'];
        $this->estado_civil = $object['estado_civil'];
        $this->clave = $object['clave'];
        $this->newsletter = $object['newsletter'];
        $this->recordarme = $object['recordarme'];
        $this->fecha_nacimiento = $object['fecha_nacimiento'];
        $this->pais_nacionalidad = $object['pais_nacionalidad'];
        $this->tipo_identificacion = $object['tipo_identificacion'];
        $this->cambio_correo = $object['cambio_correo'];
        return $this->save();
    }
    
    public function getUsuariosById($id = ""){
        
        return $this->get_by_id($id);
    }
    
   /* public function loginUsuario($email = "",$clave = ""){
        
        $usuario = $this->query('SELECT * FROM cms_usuarios WHERE email = "'.$email.'" AND clave ="'.$clave.'"');
        if($usuario->usuarios['stored']->id == ''){
            return 0;
        }else{
            return $usuario;
        }
    }*/
    
    public function loginUsuario($email,$clave){
        $array = array('email'=>$email,'clave'=>$clave,'estado'=>1);
        $count = $this->where($array)->count();
        if($count == 1){
            $usuario = $this->where($array)->get();
            $return['id'] = $usuario->id;
            $return['cedula'] = $usuario->cedula;
            $return['nombres'] = $usuario->nombres;
            $return['apellidos'] = $usuario->apellidos;
            $return['email'] = $usuario->email;
            $return['pais'] = $usuario->pais;
            $return['departamento'] = $usuario->departamento;
            $return['ciudad'] = $usuario->ciudad;
            $return['clave'] = $usuario->clave; 
            $return['tipo_identificacion'] = $usuario->tipo_identificacion; 
            $return['newsletter'] = $usuario->newsletter; 
            
            return $return;
        }else{
            return 0;
        }
    }
    
    public function loginUsuarioAdmin($id){
        $array = array('id'=>$id);
        $count = $this->where($array)->count();
        if($count == 1){
            $usuario = $this->where($array)->get();
            $return['id'] = $usuario->id;
            $return['cedula'] = $usuario->cedula;
            $return['nombres'] = $usuario->nombres;
            $return['apellidos'] = $usuario->apellidos;
            $return['email'] = $usuario->email;
            $return['pais'] = $usuario->pais;
            $return['departamento'] = $usuario->departamento;
            $return['ciudad'] = $usuario->ciudad;
            $return['clave'] = $usuario->clave;
            return $return;
        }else{
            return 0;
        }
    }
    
    public function updateUsuarios($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function updateUsuariosCC($datos = ""){
       return $this->where('cedula',$datos["cedula"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }
    public function usuarioByCc($cc){
        return $this->query("select email,id,cambio_correo from cms_usuarios where cedula = '$cc'");
    }

    public function usuarioByCorreo($correo){
        return $this->query("select * from cms_usuarios where email = '$correo'");
    }

    public function usuarioByCedula($cc, $tipo){
        
        return $this->query("select * from cms_usuarios where cedula = '$cc' And tipo_identificacion = '$tipo'");
    }
}

