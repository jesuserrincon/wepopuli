<?php

class Imagenescomplementarias extends DataMapper {

    public $model = 'imagenescomplementarias';
    public $table = 'imagenes_complementarias';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
   

    
    public function getImagenescomplementariasByProyecto($proyectoId = 0){
        return $this->query('SELECT *
                             FROM cms_imagenes_complementarias
                             WHERE proyectos_id = '.$proyectoId);
    }
    
    public function saveImagenescomplementarias($object = "") {
        $this->proyectos_id = $object['proyectos_id'];
        $this->imagen = $object['imagen'];
        return $this->save();
    }
    
    public function getLastId(){
        $usuario = $this->query('SELECT MAX(id) AS id FROM cms_imagenescomplementarias');
        return $usuario->id;
    }


    public function getImagenescomplementariasById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateImagenescomplementarias($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }


    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

