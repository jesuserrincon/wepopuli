<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo $nombremodulo ?></span>
    </div>
    <!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div style="position: absolute;float: left;height: 60px;">
                        <?php
                        //if ($count < 9) {
                            echo anchor(base_url()."cms/proyectos/reporte_general", "Reporte general", 'class="uibutton icon add" style="top: 100%; border-radius: 10px;"');
                        //}
                        ?>
                    </div>
                    <p>&nbsp;</p>

                    <div class="tableName toolbar">
                        <table class="display data_table2">
                            <thead>
                            <tr>
                                <th><div class='th_wrapp'>Id</div> </th>
                                <th><div class='th_wrapp'>Titulo</div> </th>
                                <th><div class='th_wrapp'>Estado</div> </th>
                                <th><div class='th_wrapp'>Destacar</div> </th>
                                <th><div class='th_wrapp'>Urgente</div> </th>
                                <th><div class='th_wrapp'>Exitoso</div> </th>
                                <th><div class='th_wrapp'>Quitar Exitosos</div> </th>
                                <th>
                                    <div class='th_wrapp'> Acciones</div>
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($info as $d) { ?>
                                <tr class="odd gradeX">

                                    <td width="25%" class="center"><?php echo $d->id ?></td>
                                    <td width="25%" class="center"><?php echo $d->titulo ?></td>
                                    <td width="25%" class="center"><?php echo $d->nombreestado ?></td>
                                    <td width="25%" class="center"><input type="checkbox" <?php if($d->destacado == 1){ echo 'CHECKED'; }?> value="<?php echo $d->id ?>"  onclick="destacar(<?php echo $d->id ?>)"></td>
                                    <td width="25%" class="center"><input type="checkbox" <?php if($d->urgente == 1){ echo 'CHECKED'; }?> value="<?php echo $d->id ?>"  onclick="urgente(<?php echo $d->id ?>)"></td>
                                    <td width="25%" class="center"><input type="checkbox" <?php if($d->exitoso == 1){ echo 'CHECKED'; }?> value="<?php echo $d->id ?>"  onclick="exitoso(<?php echo $d->id ?>)"></td>
                                    <td width="25%" class="center"><input type="checkbox" <?php if($d->quitarexitoso == 1){ echo 'CHECKED'; }?> value="<?php echo $d->id ?>"  onclick="quitarexitoso(<?php echo $d->id ?>)"></td>
                                   
                                    <td width="25%" class="center">
                                        <?php 
                                              
                                              echo anchor("cms/proyectos/reporte_especifico/". $d->id, "Reporte", "class='uibutton'"); 
                                              echo anchor("cms/usuarios/edit/" . $d->usuarios_id.'/1/' . $d->id, "Editar", "class='uibutton'  target='_blanck'");
                                              echo anchor("cms/proyectos/edit/" . $d->id, "Administrar", "class='uibutton'  target='_blanck'");
                                              echo anchor("cms/banner_proyecto/index/0/". $d->id, "Banners", "class='uibutton'  target='_blanck'");
                                        ?>
                                        <a onclick="return confirm('Seguro desea eliminar el item ?');" href="<?php echo base_url('cms/proyectos/delete') .'/'.$d->id ?>" class="uibutton  special" >Eliminar</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>
<script>
    function destacar(id){
            $.post("<?php base_url(); ?>proyectos/destacar", { id: id })
            .done(function( data ) {
                
            });
    }
        function urgente(id){
            $.post("<?php base_url(); ?>proyectos/urgente", { id: id })
            .done(function( data ) {
                
            });
    }
    
        function exitoso(id){
            $.post("<?php base_url(); ?>proyectos/exitoso", { id: id })
            .done(function( data ) {
                
            });
    }
    
        function quitarexitoso(id){
            $.post("<?php base_url(); ?>proyectos/quitarexitoso", { id: id })
            .done(function( data ) {
                
            });
    }
    
    
    function valida() {
        var titulo = $("#titulo").val();
        var texto = $("#texto").val();
        if (titulo == "" || texto == "") {
            $('#info').focus();
            showError('Complete todos los campos.', 3000);
        } else {
            $('#form').submit();
        }
    }
    <?php if (isset($save)){
            if ($save==1){?>
    showSuccess('Acción correcta.', 3000);
    <?php } ?>
    <?php } ?>
</script>