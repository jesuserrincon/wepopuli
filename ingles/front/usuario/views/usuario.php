<head>

<link rel="stylesheet" href="css/jquery-ui.css">
		  <script src="js/jquery-ui.js"></script>
          <link rel="stylesheet" href="/resources/demos/style.css">
          <script>
          $(function() {
			$(".datepicker").datepicker({
						changeMonth: true,
						changeYear: true,
						showButtonPanel: true,
						yearRange: '1930:2000',
						dateFormat: 'yy-mm-dd'
					});			
		
		          });
          </script>




</head>
<div class="txt_reglamento">
  <h2 class="h2_b" style="font-size:44px">My <span>profile</span></h2>
</div>

  <div class="box_shadow_big pad_box clearfix form_ax1">
        
        <div class="clearfix col_middle">
            
			<span style="font-size:12px"><span class="requiered">*</span> Indicates requierd</span>
            <div class="clear"></div>
            
			<?php if($usuario->cambio_correo != ''){ echo '<div class="msj_box">Puede habilitar el correo '.$usuario->cambio_correo.' haciendo click en el correo enviado. <a style="color:blue();" href="'.base_url().'usuario/eliminarcorreo"></a></div>'; } ?>
            
            
            <form method="post" action="<?php echo base_url().'usuario/UsuarioAct';?>" id="formularioedit" > 
            <div class="col_int_b_1 clearfix">
                <div class="label">Name <span class="requiered">*</span></div>
                <input type="text" tabindex="1" id="nombres" name="nombres" class="input_b" value="<?php echo $usuario->nombres ?>" />
                <div class="label">Type of identification <span class="requiered">*</span></div>
                <div class="data_x1"><?php echo $usuario->tipo_identificacion ?></div>
                
                <div class="label">Country of Nationality <span class="requiered">*</span></div>
                 <select class="select" tabindex="3"  name="pais_nacionalidad" id="pais_nacionalidad">
                    <option value="1">Colombia</option>
                </select>
                
                <div class="label">Province/State <span class="requiered">*</span></div>
                <input type="text"  tabindex="5"  class="input_b" />
                
				<div class="label">Address <span class="requiered">*</span></div>
                <input type="text"  tabindex="7" class="input_b" name="direccion" id="direccion"  value="<?php echo $usuario->direccion ?>" />


                <div class="label">Phone 2 (Opcional)</div>
                <input type="text" tabindex="9"  class="input_b" name="telefono2" id="telefono2"  value="<?php echo $usuario->telefono2 ?>" />
                
                <div class="label">Sex <span class="requiered">*</span></div>
                <select tabindex="11"  class="select" name="sexo" id="sexo">
                    <option value="Female" <?php if($usuario->sexo == 'Female'){ echo 'SELECTED'; }?>>Female</option>
                    <option value="masculino" <?php if($usuario->sexo == 'masculino'){ echo 'SELECTED'; }?>>Male</option>
                </select>
                
				<div class="label">Facebook (optional)</div>
                <input type="text" tabindex="13" class="input_b" name="facebook" id="facebook"  value="<?php echo $usuario->facebook ?>" />

                
				
				<div class="label">E-mail <span class="requiered">* </span> &nbsp &nbsp </div>
               <div class="data_x1" style="margin-bottom:0px"><?php echo $usuario->email ?></div>
               <a href="#cambiar_correo"  class="fancybox" style="color:#39C">Change E-mail</a><br /> 
               <a href="#cambiar_pass"  class="fancybox" style="color:#39C">Change Password</a>


				<!--<a class="verde_bt left <?php if($usuario->newsletter == 'si'){ echo 'active_recordarme'; }?>" id="newaletter">Deseo suscribirme al newsletter</a>-->
				<input type="hidden" tabindex="15" name="newsletter" id="newsletter" value="<?php echo $usuario->newsletter ?>">
                <div class="clear" style="margin-bottom:10px"></div>
                <div class="clear" style="margin-bottom:30px"></div>


            </div>
            
            <div class="col_int_b_1 clearfix right">
                <div class="label">Last Name <span class="requiered">*</span></div>
                <input type="text" tabindex="2" class="input_b" name="apellidos" id="apellidos" value="<?php echo $usuario->apellidos ?>" />

				<div class="label">Identification Number <span class="requiered">*</span></div>
                <div class="data_x1"><?php echo $usuario->cedula ?></div>
                
                 <div class="label">Residence Country <span class="requiered">*</span></div>
                 <select tabindex="4" class="select" name="pais" id="pais">
                    <option value="1">Colombia</option>
                </select>

                <div class="label">City/Town  <span class="requiered">*</span></div>
                <input tabindex="6" type="text" class="input_b" />

				<div class="label">Phone 1 <span class="requiered">*</span></div>
                <input tabindex="8" type="text" class="input_b" name="telefono" id="telefono"  value="<?php echo $usuario->telefono ?>" />

                <div class="label">Date of Birth <span class="requiered">*</span></div>
                <input tabindex="10" type="text" class="input_b datepicker"   name="fecha_nacimiento" id="fecha_nacimiento"  value="<?php echo $usuario->fecha_nacimiento ?>"  />
				
                <div class="label">Marital Status  <span class="requiered">*</span></div>
                <select tabindex="12" class="select" name="estado_civil" id="estado_civil">
                    <option value="Single" <?php if($usuario->estado_civil == 'Single'){ echo 'SELECTED'; }?>>Single</option>
                    <option value="Married" <?php if($usuario->estado_civil == 'Married'){ echo 'SELECTED'; }?>>Married</option>
                    <option value="Divorced" <?php if($usuario->estado_civil == 'Divorsiado'){ echo 'SELECTED'; }?>>Divorced</option>
                </select>

                <div class="label">Twitter (optional)</div>
                <input tabindex="14" type="text" class="input_b" name="twitter"  value="<?php echo $usuario->twitter ?>" />

               <!--<div class="label">Confirmar E-mail <span class="requiered">*</span></div>
                <input type="text" class="input_b" />-->
                
                
				

                
                <div class="change_pass">
                	  
                </div>
                
                

            </div>
            
          </form>  
          
        </div>
        

                    <div class="div_line clear" style="margin-bottom:0px; widows:100%"></div>
            <div class="pad_box_10 clearfix">
            <a href="javascript:validarformulario()" class="bt_green">Save	</a>	
			</div>

        
        
</div>
<script>
    
    function checkKey(key){
        var unicode;
        if (key.charCode)
        {unicode=key.charCode;}
        else
        {unicode=key.keyCode;}
        if (unicode == 13){
           cambiarcorreo();
        }
    }
    
    function checkKey2(key){
        var unicode;
        if (key.charCode)
        {unicode=key.charCode;}
        else
        {unicode=key.keyCode;}
        if (unicode == 13){
           cambiarclave();
        }
    }

    function validarformulario(){
        var captcha = $('#captchatext').val();
        
        var fecha_nacimiento = $('#fecha_nacimiento').val();
        var nombres = $('#nombres').val();
        var apellidos = $('#apellidos').val();
        var telefono = $('#telefono').val();
        var cedula = $('#cedula').val();
        var pais = $('#pais').val();
        var departamento = $('#departamento').val();
        var direccion = $('#direccion').val();
        var ciudad = $('#ciudad').val();
        var validacion = true;
       
        if(nombres == ''){
             $('#nombres').addClass("errorform");
             $('#nombres').attr('placeholder', '');
             $('#nombres').focus();
             validacion = false;
        }else{
             $('#nombres').removeClass("errorform");
            
        }
        if(apellidos == ''){
             $('#apellidos').addClass("errorform");
             $('#apellidos').attr('placeholder', '');
             $('#apellidos').focus();
             validacion = false;
        }else{
             $('#apellidos').removeClass("errorform");
            
        }
        if(telefono == ''){
             $('#telefono').addClass("errorform");
             $('#telefono').attr('placeholder', '');
             $('#telefono').focus();
             validacion = false;
        }else{
             $('#telefono').removeClass("errorform");
            
        }
       
        if(cedula == ''){
             $('#cedula').addClass("errorform");
             $('#cedula').attr('placeholder', '');
             $('#cedula').focus();
             validacion = false;
        }else{
             $('#cedula').removeClass("errorform");
            
        }
        
         if(direccion == ''){
             $('#direccion').addClass("errorform");
             $('#direccion').attr('placeholder', '');
             $('#direccion').focus();
             validacion = false;
        }else{
             $('#direccion').removeClass("errorform");
            
        }
        
         if(fecha_nacimiento == ''){
             $('#fecha_nacimiento').addClass("errorform");
             $('#fecha_nacimiento').attr('placeholder', '');
             $('#fecha_nacimiento').focus();
             validacion = false;
        }else{
             $('#fecha_nacimiento').removeClass("errorform");
            
        }
    
    
        if(ciudad == ''){
             $('#ciudad').addClass("errorform");
             $('#ciudad').attr('placeholder', '');
             $('#ciudad').focus();
             validacion = false;
        }else{
             $('#ciudad').removeClass("errorform");
            
        }
        if ($('#newaletter').hasClass('active_recordarme')){
                $('#newsletter').val('si');
            }else{
                 $('#newsletter').val('no');
            }
            
         if(validacion == false){
            return false;
        }
        
        
               $('#formularioedit').submit(); 
           
        
    }


    function cambiarclave(){
        var actual = $('#actual').val();
        var nuevaclave = $('#nuevaclave').val();
        var confirmaclave = $('#confirmaclave').val();
        var validacion = true;
       
        if(actual == ''){
            $('#actual').addClass("errorform");
            $('#actual').attr('placeholder', '');
            $('#actual').focus();
            validacion = false;
        }else{
             $('#actual').removeClass("errorform");
        }
        if(nuevaclave == ''){
            $('#nuevaclave').addClass("errorform");
            $('#nuevaclave').attr('placeholder', '');
            $('#nuevaclave').focus();
            validacion = false;
        }else{
             $('#nuevaclave').removeClass("errorform");
        }
        if(confirmaclave == ''){
            $('#confirmaclave').addClass("errorform");
            $('#confirmaclave').attr('placeholder', '');
            $('#confirmaclave').focus();
            validacion = false;
        }else{
             $('#confirmaclave').removeClass("errorform");
        }
        
        if(nuevaclave != confirmaclave){
            alert('The password you entered does not match.');
            return false;
        }
        
        if(validacion == false){
            alert('You must fill in all of the fields.');
            return false;
        }
    
        $.post( "<?php echo base_url(); ?>usuario/validarclave", { clave: actual, nuevaclave: nuevaclave })
          .done(function( data ) {
            if(data == 0){
                alert('The current password you entered is not valid.');
            }else{
                alert('Change has been saved.');
                $('#actual').val('');
                $('#nuevaclave').val('');
                $('#confirmaclave').val('');
                $('.fancybox-close').click();
            }
          });
        
        
    }
    
    
    function cambiarcorreo(){
        var correocambio = $('#correocambio').val();
        var validacion = true;
       
        if(correocambio == ''){
            $('#correocambio').addClass("errorform");
            $('#correocambio').attr('placeholder', 'New E-mail');
            $('#correocambio').focus();
            validacion = false;
        }else{
             $('#correocambio').removeClass("errorform");
        }
        if(validacion == false){
            alert('You must enter a valid E-mail address.');
            return false;
        }
    
        $.post( "<?php echo base_url(); ?>usuario/cambiarcorreo", { correocambio: correocambio })
          .done(function( data ) {
                if(data == 0){
                    alert('The email address is already registered.');
                    return false;
                }else{
                    alert('To complete the change of registered email you must click on the link sent to your new email.');
                    location.href = "<?php echo base_url().'usuario'; ?>";
                    
                }
           
          });
        
        
    }
</script>
<div id="cambiar_pass" style="width:265px;display:none; padding:0px !important">
	<div><h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">Change Password</h2></div>

	<div class="label">Current Password <span>*</span></div>
        <input type="password" class="input_a" id="actual" style="width:240px" />
	<div class="label">New Password <span>*</span></div>
        <input type="password" class="input_a" id="nuevaclave" style="width:240px"  />
	<div class="label">Re-enter New Password <span>*</span></div>
        <input type="password" class="input_a" id="confirmaclave" style="width:240px"  onkeypress="checkKey2(event);"/>

		<div style="font-size:12px; margin-top:-10px; margin-bottom:20px;"><span class="requiered">*</span> Indicates required</div>
	
    <input type="submit"  onclick="cambiarclave()" class="bt_green" value="Save" style="margin-bottom:0px !important; width:265px; background-size:100% 100%" />
</div>


<div id="cambiar_correo" style="width:350px;display:none;">
	<div><h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">Change E-mail</h2></div>
      <center>  
      	<input type="text" placeholder="New E-mail" class="input_a" id="correocambio" name="correocambio" onkeypress="checkKey(event);" style="width:326px; margin-bottom:20px" />
       
    	<input type="submit" onclick="cambiarcorreo()"   class="bt_green" value="Change E-mail" style="width:348px; background-size:100% 100%"/>
	</center>
</div>

