<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- Styles -->
<link href="assets/css/colombia_incluyente.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="assets/css/responsiveslides.css">
<link rel="stylesheet" media="screen, projection" href="assets/css/fancySelect.css">

<!-- Metadata -->
<title>Colombia Incluyente</title>
<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png" />
<meta http-equiv="content-language" content="es" />
<meta http-equiv="pragma" content="No-Cache" />
<meta name="viewport" content="width=device-width; initial-scale=0.9; target-densityDpi=device-dpi" />
<meta name="designer" content="romerorodrigo.com">

<style type="text/css">
@media only screen
and (min-width : 768px)
and (max-width : 1024px)
and (orientation : portrait) {
/* Styles */
body, html {
	width:1100px;
}
@viewport {
   width:1500px;
   zoom: 0.6;
}
}

</style>
<!-- JS -->
    <script type="text/javascript">
		function cargar_contenido(){
			$('#projects_show').append('<div class="project_box"><div class="imgLiquidFill imgLiquid liquid_a"><a href="#"  target="_blank"><img class="thumbnail_a" src="assets/img/024.jpg" /></a></div><div class="title">Por un país mejor</div><div class="des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum risus lacus, placerat nec bibendum sit amet, mollis consequat leo. Ut lacinia consequat nulla, ac elementum risus dapibus ut. In sodales justo vel mauris consectetur facilisis. Aenean sagittis ullamcorper tortor ut eleifend. Nam sit amet dolor non nisl lobortis faucibus. Praesent fringilla bibendum risus, a aliquam risus commodo hendrerit. Suspendisse a elit at nisi pretium adipiscing. Nunc viverra euismod dolor, ut ultricies mi egestas a. Nullam vehicula nisl neque.</div><div class="location">Rio Magdalena, Colombia</div><div class="line_div_project"></div><div class="percent_icon_box first_precent"><div class="money_icon_a icons"></div><div class="percent_circle">90%</div></div><div class="percent_icon_box"><div class="members_icon_a icons desactive"></div><div class="percent_circle">-</div></div><div class="percent_icon_box"><div class="shop_icon_a icons desactive"></div><div class="percent_circle">-</div></div><div class="clear"></div><div class="title_percent">Porcentaje total</div><div class="container"><div class="bar" id="progress12"></div></div></div>');
			 	$(".imgLiquidFill").imgLiquid({fill:true});
  
		
		}
	</script>

<script type="text/javascript">
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=293180007417759";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>

<script type="text/javascript">
if (window.matchMedia("(orientation: portrait)").matches) {
   // you're in PORTRAIT mode
            viewportmeta.content = 'width=device-width, initial-scale=0.65, maximum-scale=2';

}

if (window.matchMedia("(orientation: landscape)").matches) {
   // you're in LANDSCAPE mode
            viewportmeta.content = 'width=device-width, initial-scale=0.9';

}
</script>
<script src ="assets/js/imgLiquid.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
	$(".imgLiquidFill").imgLiquid({fill:true});
	$(".imgLiquidNoFill").imgLiquid({fill:false});
});
</script>

<script type="text/javascript">
$(document).ready(function () {
	$(".recordarme").click(function(){
	  $(".recordarme").toggleClass("active_recordarme");
	});
	
	$(".verde_bt").click(function(){
	  $(".verde_bt").toggleClass("active_recordarme");
	});
	$(".verde_bt2").click(function(){
	  $(".verde_bt2").toggleClass("active_recordarme");
	});
	$(".verde_bt3").click(function(){
	  $(".verde_bt3").toggleClass("active_recordarme");
	});
});
</script>
<script type="text/javascript" src="assets/js/responsiveslides.js"></script>
<script type="text/javascript">
$(function () {
  $("#slider2").responsiveSlides({
	auto: true,
	pager: true,
	speed: 300,
	maxwidth:1000
  });
  $("#slider3").responsiveSlides({
	auto: true,
	pager: true,
	speed: 300,
	maxwidth:1000
  });
});
</script>
<script src="assets/js/fancySelect.js"></script>
<script>
	$(document).ready(function() {
		$('.select').fancySelect();

		// Boilerplate
		var repoName = 'fancyselect'

		$.get('https://api.github.com/repos/octopuscreative/' + repoName, function(repo) {
			var el = $('#top').find('.repo');

			el.find('.stars').text(repo.watchers_count);
			el.find('.forks').text(repo.forks_count);
		});

		var menu = $('#top').find('menu');

		function positionMenuArrow() {
			var current = menu.find('.current');

			menu.find('.arrow').css('left', current.offset().left + (current.outerWidth() / 2));
		}

		$(window).on('resize', positionMenuArrow);

		menu.on('click', 'a', function(e) {
			var el = $(this),
				href = el.attr('href'),
				currentSection = $('#main').find('.current');

			e.preventDefault();

			menu.find('.current').removeClass('current');

			el.addClass('current');

			positionMenuArrow();

			if (currentSection.length) {
				currentSection.fadeOut(300).promise().done(function() {
					$(href).addClass('current').fadeIn(300);
				});
			} else {
				$(href).addClass('current').fadeIn(300);
			}
		});

		menu.find('a:first').trigger('click')
	});
</script>
<script src="assets/js/slides.min.jquery.js"></script>
<script>
	$(function(){
		$('.slides').slides({
			preload: true,
			generateNextPrev: true,
			generatePagination: false
		});
				
	});
</script>
<script type="text/javascript">
$(function(){
$.fn.customRadioCheck = function() {
 
  return this.each(function() {
 
    var $this = $(this);
    var $span = $('<span/>');
 
    $span.addClass('custom-'+ ($this.is(':checkbox') ? 'check' : 'radio'));
    $this.is(':checked') && $span.addClass('checked'); // init
    $span.insertAfter($this);
 
    $this.parent('label').addClass('custom-label')
      .attr('onclick', ''); // Fix clicking label in iOS
    // hide by shifting left
    $this.css({ position: 'absolute', left: '-9999px' });
 
    // Events
    $this.on({
      change: function() {
        if ($this.is(':radio')) {
          $this.parent().siblings('label')
            .find('.custom-radio').removeClass('checked');
        }
        $span.toggleClass('checked', $this.is(':checked'));
      },
      focus: function() { $span.addClass('focus'); },
      blur: function() { $span.removeClass('focus'); }
    });
  });
};
}());



;(function(){
$.fn.sexCheck = function() {
 
  return this.each(function() {
 
    var $this = $(this);
    var $span = $('<span/>');
 
    $span.addClass('custom2-'+ ($this.is(':checkbox') ? 'check' : 'radio'));
    $this.is(':checked') && $span.addClass('checked'); // init
    $span.insertAfter($this);
 
    $this.parent('label').addClass('custom2-label')
      .attr('onclick', ''); // Fix clicking label in iOS
    // hide by shifting left
    $this.css({ position: 'absolute', left: '-9999px' });
 
    // Events
    $this.on({
      change: function() {
        if ($this.is(':radio')) {
          $this.parent().siblings('label')
            .find('.custom2-radio').removeClass('checked');
        }
        $span.toggleClass('checked', $this.is(':checked'));
      },
      focus: function() { $span.addClass('focus'); },
      blur: function() { $span.removeClass('focus'); }
    });
  });
};
}());


;(function(){
$.fn.sexMCheck = function() {
 
  return this.each(function() {
 
    var $this = $(this);
    var $span = $('<span/>');
 
    $span.addClass('custom3-'+ ($this.is(':checkbox') ? 'check' : 'radio'));
    $this.is(':checked') && $span.addClass('checked'); // init
    $span.insertAfter($this);
 
    $this.parent('label').addClass('custom3-label')
      .attr('onclick', ''); // Fix clicking label in iOS
    // hide by shifting left
    $this.css({ position: 'absolute', left: '-9999px' });
 
    // Events
    $this.on({
      change: function() {
        if ($this.is(':radio')) {
          $this.parent().siblings('label')
            .find('.custom3-radio').removeClass('checked');
        }
        $span.toggleClass('checked', $this.is(':checked'));
      },
      focus: function() { $span.addClass('focus'); },
      blur: function() { $span.removeClass('focus'); }
    });
  });
};
}());
</script>

