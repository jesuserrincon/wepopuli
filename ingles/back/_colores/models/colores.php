<?php

class Colores extends DataMapper {

    public $model = 'colores';
    public $table = 'colores';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getColores(){
        return $this->get();
    }

    public function getColores2(){
        return $this->get();
}
    
    public function saveColores($object = "") {
        $this->color = $object['color'];
        $this->nombre = $object['nombre'];
        return $this->save();
    }
    
    public function getColoresById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateColores($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

