<style type="text/css">
#s4 {
	color:#fff;
	background-position:0px -100px;
}
#s3 {
	color:#fff;
	background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -50px;
}
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>

<link href="css/jquery-filestyle.css" rel="stylesheet" />
  	
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
<script src="js/jquery.sticky.js"></script>
<script>
  $(document).ready(function(){
    $(".sticker").sticky({topSpacing:30});
	
	
	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
		   $('.sticker').addClass('fixed_button_gestores');
		   }else{
			   $('.sticker').removeClass('fixed_button_history');
		   }
	});
  });
</script>
<div class="paso_paso clearfix">
    <a href="<?php echo base_url(); ?>reglamento/index/<?php echo $proyectoid; ?>" id="s1" class="step">Rules</a>
    <a href="<?php echo base_url(); ?>basicos/index/<?php echo $proyectoid; ?>" id="s2" class="step">Basics</a>
    <a href="<?php echo base_url(); ?>historia/index/<?php echo $proyectoid; ?>" id="s3" class="step"> Story </a>
    <a href="<?php echo base_url(); ?>gestores/index/<?php echo $proyectoid; ?>" id="s4" class="step">Gestores</a>
    <a href="<?php echo base_url(); ?>cuenta/index/<?php echo $proyectoid; ?>" id="s5" class="step">Cuenta</a>
    <a href="<?php echo base_url(); ?>revision/index/<?php echo $proyectoid; ?>" id="s6" class="step">Revisión</a>
    <a href="<?php echo base_url(); ?>previsualizar/index/<?php echo $proyectoid; ?>" id="s7" class="step">Previsualizar</a>
</div>

<div class="txt_reglamento">
	<h2 class="h2_b">Gestores del Proyecto </h2>
    Conocer a los gestores del proyecto genera confianza en el público y les da la certeza de que es una iniciativa legítima, respaldada por una organización que velará por la consecusión del proyecto.
</div>

<div class="box_a pad_box clearfix">
    
    <div class="col_c">
      <div class="box_shadow box_c" style="margin-bottom:30px;">
       	  <h6 style="color:#662d91">Información de la Organización que gestiona el proyecto.</h6>
          <div style="margin-bottom:10px;">Todos los proyectos deben pertenecer a una organización registrada, con minimo 3 años de antiguedad.</div>
		 
          <input type="text" class="input_c" name="nombre" id="nombre" />
          <div style="margin-bottom:50px"></div>          
          <div class="left" style="width:470px; margin-top:-40px;">
          	<h6>Imagen del Logotipo de la Organización</h6>
          	El Logotipo es la cara de la organización gestora del proyecto. Escoja la imagen correspondiente de su computador, en formato JPEG o PNG • Tamaño máximo: 2MB. • Dimensiones recomendadas: 200 x 200 pixeles.
          </div>
            <div class="right relative" style="width:111px; height:90px">
               <input value="" type="file" class="jfilestyle" data-theme="blue2">
            </div>
        
        <div class="div_line_2 clear spacer"></div>	
            
            <h6>Descripción de la organización a la que pertenece el proyecto</h6>
            <p>Háblenos de la organización, cuáles han sido sus logros y el impacto generado.</p>
          <textarea class="text_area_big"></textarea>
          <br />
          Maximum characters 0/1000
            <div class="spacer"></div>
		
        
            <div class="col_int_box_c left">
                    <div class="label">País originario</div>
                    <input type="text" class="input_a" />
                    <div class="label">Municipio</div>
                    <input type="text" class="input_a" />
                    <div class="label">Teléfono 1</div>
                    <input type="text" class="input_a" />
                    <div class="label">E-mail</div>
                    <input type="text" class="input_a" />
                    <div class="label">Twitter (Optional) </div>
                    <input type="text" class="input_a" placeholder="@ejemplo" />
            </div>
            <div class="col_int_box_c right">
           		<div class="label">Departamento / Región</div>
            	<input type="text" class="input_a" />
            	<div class="label">Dirección</div>
            	<input type="text" class="input_a" />
            	<div class="label">Teléfono 2 (opcional) </div>
            	<input type="text" class="input_a" />
            	<div class="label">Página web (opcional) </div>
            	<input type="text" class="input_a" />
            	<div class="label">Facebook (Optional)  </div>
            	<input type="text" class="input_a" placeholder="facebook.com/ejemplo" />
            </div>
            <div class="clear div_line_2 spacer"></div>
            
            
            
            <h6>Certificado de Cámara de Comercio <span style="color:#999; font-size:14px">(No mayor a 30 días)</span></h6>
            <p>¿Cuáles son los objetivos y metas del proyecto? Unos objetivos medibles ayudarán a definir las necesidades del proyecto y permitirán monitorear su desempeño.</p>
           	<input type="file" class="jfilestyle" data-theme="green">
            <h6 style="margin-bottom:5px">Nit / Rut</h6>
           	<input type="file" class="jfilestyle" data-theme="green">

            <div class="div_line_2 spacer"></div>


			<h6 style="color:#662d91">Información del líder del proyecto</h6>
            <div class="left" style="width:470px;">
              <p>Fotografía del líder del proyecto</p>
            	Muéstrele al mundo quién es el líder del proyecto, quién lo estará respaldando. Escoja la imagen correspondiente de su computador, en formato JPEG o PNG • Tamaño máximo: 2MB. • Dimensiones recomendadas: 200 x 200 pixeles.
            </div>
            <div class="right relative" style="width:111px; height:90px">
               <input value="" type="file" class="jfilestyle" data-theme="blue3">
            </div>
			<div class="spacer"></div>

        	<h6>Acerca del líder</h6>
            <p>Describa quién es el líder del proyecto, cuáles son las cualidades que lo hacen la persona ideal para liderarlo y en qué otros proyectos ha participado.</p>
			<textarea class="text_area_big"></textarea>
            <br />
            Maximum characters 0/1000
            
            
              <div class="spacer"></div>
		
        
            <div class="col_int_box_c left">
                    <div class="label">Nombres</div>
                    <input type="text" class="input_a" />
                    <div class="label">No. Identificación</div>
                    <input type="text" class="input_a" />
                    <div class="label">Teléfono 2 (opcional)</div>
                    <input type="text" class="input_a" />
                    <div class="label">Facebook (Optional) </div>
                    <input type="text" class="input_a" placeholder="facebook.com/ejemplo" />
            </div>
            <div class="col_int_box_c right">
           		<div class="label">Apellidos</div>
            	<input type="text" class="input_a" />
            	<div class="label">Teléfono 1</div>
            	<input type="text" class="input_a" />
            	<div class="label">E-mail</div>
            	<input type="text" class="input_a" />
            	<div class="label">Twitter (Optional)</div>
            	<input type="text" class="input_a" placeholder="@ejemplo" />
            </div>
            <div class="clear"></div>
        <div class="div_line_2"></div>
      </div>

        
	  
        
        
        
      
      
        <div class="clear" style="margin-bottom:0px"></div>
            <input type="submit" value="Delete Project" class="boton_c left" style="margin-right:10px;">
            <input type="submit" value="Save Changes"  class="boton_a left" style="margin-right:10px;">
            <a href="Rules.php" class="boton_b left" style="margin-right:10px;">Back</a>
            <a href="Rules.php" class="boton_b left">Continue</a>

        
    </div><!-- // columna C -->
    
    <div class="col_d mar_right">
       <div class="project_box sticker">
            <div class="imgLiquidFill imgLiquid liquid_a">
                <a href="#" >
                    <?php 
                    function image_exists($url) {
                        if(@getimagesize($url)){
                        return true;
                      }else{
                       return false;
                     }
                    }
                    $imagen = base_url().'uploads/proyectominiatura/'.$proyecto->imagen_miniatura; ?>
                    <img class="imgLiquidFill"  id="imagen_box" src="<?php if(image_exists($imagen)){ echo base_url().'uploads/proyectominiatura/'.$proyecto->imagen_miniatura; }else{ echo 'img/024.jpg'; }?>" />
                </a>
            </div>
           <div class="cat" id="categoria_box"><?php if($nomcategoria != ''){ echo $nomcategoria;}else{ echo 'Categoria'; }?></div> 
           <div class="title" id="titulo_box"><?php
            if($proyecto->titulo == ''){ echo 'Por un país mejor Colombia'; }else{ echo $proyecto->titulo; } ?></div>
            <div class="des" id="descripcion_box">
            <?php if($proyecto->descripcion_corta == ''){ echo 'Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.'; }else{ echo $proyecto->descripcion_corta; } ?>    
             
            </div>
            
            <div class="resumen">
                <ul class="data_project_min">                    <li class="by" id="by1">Organization pies descalsos</li>
                    <li class="by" id="by2">Rio Magdalena, Colombia </li>
                    <li class="by" id="by3"><span id="diasfaltantes_box"><?php if($proyecto->dias_para_cerrar == ''){ echo '30'; }else{ echo $proyecto->dias_para_cerrar; } ?></span> days to go</li></ul>
            </div>
            
            <div class="line_div_project"></div>
            
            <div class="percent_icon_box first_precent">
                <div class="money_icon_a icons"></div>
                <div class="percent_circle">0%</div>
            </div>
            <div class="percent_icon_box">
                <div class="members_icon_a icons"></div>
                <div class="percent_circle">0%</div>
            </div>
            <div class="percent_icon_box">
                <div class="shop_icon_a icons"></div>
                <div class="percent_circle">0%</div>
            </div>
            <div class="clear"></div>
            
            
            <div class="barra_porcentaje">
                <div class="bar" style="width:0%">0%</div>
            </div>
            
        </div><!-- // project box -->
    </div>
</div>
<script type="text/javascript" src="js/jquery-filestyle.js"></script>
<script type="text/javascript" src="js/aplication.js"></script>