<?php

class Estados extends DataMapper {

    const APROBADO = 1;
    const RECHAZADO = 2;
    const CERRADO = 3;
    const PENDIENTE = 4;
    const REVISADO = 5;
    const CANCELADO = 6;
    const SINTERMINAR =7;
    const DESPUBLICADO = 8;

    public $model = 'estados';
    public $table = 'estados';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getEstados(){
        return $this->query('SELECT titulo FROM cms_estados');
    }

    public function getEstados2(){
        return $this->query('SELECT * FROM cms_estados');
    }
    
    public function saveEstados($object = "") {
        $this->nombre = $object['nombre'];
        $this->banner_superior = $object['banner_superior'];
        $this->banner_inferior = $object['banner_inferior'];
        return $this->save();
    }
    
    public function getEstadosById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateEstados($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

