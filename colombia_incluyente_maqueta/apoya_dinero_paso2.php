<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<link href="assets/css/colombia_incluyente_movil.css" rel="stylesheet" />
<?php include"metadata.php" ?>  

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->

<script type="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "09422a6d-4673-4a62-ac1c-160a92973882", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<!-- Add mousewheel plugin (this is optional) -->	
<script type="text/javascript" src="assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>
</head>

<body class="device_320">
<?php include"header.php" ?>
<div class="clear"></div>
<div class="txt_reglamento device_300 pad_bottom_0_mobile pad_5_mobile">
  <h2 class="h2_b" style="font-size:44px">Proyecto X</h2>
  <h2 class="h2_des">Por: Fundación Píes descalzos</h2>
</div>

    
    
<div class="box_shadow_big clearfix device_320">

  <div class="col_e min_h_900 pad_int_20 device_300">
    <h2 class="h2_c left" style="padding-top:10px">Gracias por hacer realidad <br />
    una Colombia Incluyente!</h2>
  		
        <div class="icon_apoya"></div>
        <div class="clear"></div>
          
        
    <div class="div_line_2"></div>
        
        <div class="donacion_valor">
            <div class="div_span">Valor de la donación:</div> 
            <div class="div_span"><span>$1.000</span> <span class="span2">cop</span> <a href="apoya_dinero.php">Editar</a></div>
        </div>
        
    <div class="div_line_2"></div>
    <div class="spacer"></div>
    <h2 class="h2_b device_320">Completa el proceso. Diligencia el formulario.</h2>
  		<div class="txt_14">La siguiente información es la que aparecera en el certificado de donación, una vez este sea confirmada. La validación puede tardar unos días. </div>
  		<div class="spacer pad_top_10_mobile" style="padding-top:20px;"></div>
        
        <div class="col_g">
        	
            <div class="info_data">
            	<div class="label_data">Nombres</div>
                <div class="data_user">Rodrigo</div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Apellidos</div>
                <div class="data_user">Romero Castro</div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">No. Cédula</div>
                <div class="data_user">CC 1032408297</div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Teléfono 1</div>
                <div class="data_user">3106589845</div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Teléfono 2 (opcional)</div>
                <div class="data_user">3115896245</div>
            </div>
            
            
            <div class="info_data">
            	<div class="label_data">Email</div>
                <div class="data_user">bungomoss@hotmail.com</div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Páis</div>
                <div class="data_user">Colombia</div>
            </div>
            
            
            <div class="info_data">
            	<div class="label_data">Departamento (Región)</div>
                <div class="data_user">Cundinamarca</div>
            </div>
                    
            <div class="info_data">
            	<div class="label_data">Municipio</div>
                <div class="data_user">Bogotá</div>
            </div>
            
            
    
            <div class="info_data">
            	<div class="label_data">Dirección</div>
                <div class="data_user">Rodrigo Romero</div>
            </div>
            <div class="info_data">
            	<div class="label_data">Estado Civil</div>
                <div class="data_user">Soltero</div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Sexo</div>
                <div class="data_user">Hombre</div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Facebook (opcional)</div>
                <div class="data_user">facebook.com/sergioreyes</div>
            </div>
            
            
            <div class="info_data">
            	<div class="label_data">Twitter (opcional)</div>
                <div class="data_user">@sergioreyes</div>
            </div>
            
        </div>
        

		
        <div class="acept_box clear">
            <a class="verde_bt2">Acepto </a> 
            <a href="#terminos_cond"  class="fancybox terms">terminos y condiciones</a>
        </div>
        
        
        <a href="apoya_dinero_paso2.php" class="bt_green left mr_10">Atras</a>
        <a href="apoya_dinero_paso2.php" class="bt_green left mr_10_movil_500">Apoyar</a>
        
        
        
        <div class="div_line_2 visible_mobile"></div>
  </div>
  
  
  
  
  
    <div class="col_f device_320">
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Porcentaje total</center></div>
            <div class="barra_porcentaje2">
                <div class="bar2" style="width:25%">25%</div>
            </div>

		</div>
        <div class="spacer"></div>

        <div class="div_line"></div>
        <div class="spacer"></div>
        
        <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">IMPORTANT</h3>
            <div class="txt_box_min">
            	Colombia Incluyente does not guarantee projects or investigate a creator's ability to complete their project. It is the responsibility of the project creator to complete their project as promised, and the claims of this project are theirs alone.
			</div>
            <div class="line_color"></div>
		</div>
		
        <div class="spacer"></div>	
            
      <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Preguntas Frecuentes</h3>
            <div class="txt_box_min">
            	<h4 class="h4_pregunta"><a href="#">¿Cómo puedo apoyar un proyecto?</a></h4>
            	Colombia Incluyente does not guarantee projects or investigate a creator's ability to complete their project. It is the responsibility of the project creator to complete their project as promised, and the claims of this project are theirs alone.
			</div>
            <div class="line_color color2"></div>
		</div>


        <div class="spacer"></div>
        <div class="spacer"></div>

    </div>
    
    
    <div class="div_line clear dis_none_movil"></div>
    
    <div class="pad_int_20">
    
    
    </div>

</div>


<script type="text/javascript" src="assets/js/aplication.js"></script>

<?php include"footer.php" ?> 



<!-- //popup -->
<div id="inline2" style="width:650px;display:none;"  class="device_250">
	<div class="device_250">
        <div class="right" style="width:270px">    
            <div class="logo_fundacion">
                <img src="assets/img/080.png" class="rounded_img" width="200" />
            </div>
            <div class="data_fundacion">
                <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Contactame en:</span></div>
                <div class="dato"><b>Email:</b> <a href="mailto:info@miemail.es" style="color:#602483"> info@romerorodrigo.com</a></div>
                <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
                <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
                <div class="dato"><b>Bogotá, Colombia</div>
            </div>
        </div>
        
        <div class="left col_line_right device_230" style="width:350px;">
            <h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px">Lider</h2>
            <h2 class="h2_b" style="color:#662d91; margin-bottom:5px;">Rodrigo Romero Castro</h2>
            <h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px">Fundación pies descalzos</h3>
        
            <div class="txt_user">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent purus turpis, tincidunt in malesuada ut, mattis eget purus. Vivamus semper fringilla nunc, iaculis ultricies nibh scelerisque eget. Nam tellus orci, elementum et erat at, sollicitudin ultrices mauris. Maecenas id purus at nisi eleifend dignissim. Duis non mi id nisi venenatis ornare. Donec suscipit enim eget mauris pharetra, venenatis commodo elit congue. Maecenas non eleifend libero, non interdum nisi. Nunc a lorem ornare, volutpat nisi sed, molestie eros. Nam in interdum libero. Donec hendrerit vitae dui ut imperdiet.</p>
                <p>Vestibulum tempus id ante a eleifend. Morbi at elementum erat. Nam in nulla ac ante fermentum ultrices quis nec sem. Donec ultricies in est et dignissim. Donec sollicitudin tortor eget mauris pharetra, a congue metus feugiat. Integer velit risus, hendrerit et pulvinar eget, mollis a odio. Donec sed sodales nisi. Aenean accumsan tellus in est pretium euismod. Praesent eu risus eros. Donec pulvinar, eros vel tincidunt sagittis, eros velit tempus diam, et feugiat elit enim at augue.</p>
                <p>Sed adipiscing nisi ac justo suscipit lobortis. Suspendisse potenti. Integer a ullamcorper orci. Curabitur eget suscipit metus, et aliquam est. Praesent eget leo sed purus tempus eleifend sit amet a mauris. Suspendisse lacus dolor, rhoncus sed hendrerit at, laoreet eu turpis. Aliquam massa libero, pellentesque eget risus vel, vehicula tristique ipsum.</p>
            </div>
        </div>
	</div>
</div>
    
<!-- //popup -->
<div id="terminos_cond" style="display:none;" class="device_250">
	<div class="device_250">
        <h2 class="h2_b" style="margin-bottom:20px;">Terminos y Condiciones</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet libero viverra mi tristique ultrices. Fusce nec leo dapibus, pulvinar justo et, lacinia tortor. Duis a consequat magna. Cras sodales, lectus ut pellentesque congue, diam metus mattis enim, ac tincidunt nisi turpis ut metus. Ut in sapien at lacus semper facilisis vitae sed odio. Morbi facilisis faucibus eros vitae pharetra. Fusce eu ipsum congue, viverra velit a, mollis nunc. Nam mattis ligula eget metus aliquet, sed commodo justo aliquet.
        Donec consequat at sapien nec ullamcorper. <br />
        <br />
        Aliquam erat volutpat. Morbi dolor lacus, bibendum eu velit sit amet, cursus facilisis arcu. Fusce eu facilisis lorem. Pellentesque eu augue at mauris fringilla volutpat. Suspendisse tincidunt sit amet nisi id adipiscing. Integer porttitor nisl quis dignissim cursus. In suscipit sapien eros, ac vulputate quam iaculis vitae. Suspendisse justo odio, porttitor quis eros ut, adipiscing gravida nunc.
        <br /><br />
        Praesent accumsan eget nulla eget faucibus. Fusce eu nulla in odio pharetra gravida. Curabitur sollicitudin enim tellus, vitae commodo nunc dapibus vel. Ut rutrum quam id quam tempor, in pulvinar justo ultrices. Morbi ultricies, dolor eu tempus pretium, neque leo scelerisque neque, sit amet blandit ligula diam ac tortor. Cras eleifend scelerisque nibh, tempor vehicula ligula auctor at. Nullam arcu justo, egestas et libero non, placerat aliquam augue. Fusce elementum et sem nec malesuada. Suspendisse interdum vitae diam quis hendrerit. Etiam convallis malesuada porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
        <br /><br />
        Proin laoreet gravida eleifend. Ut varius semper metus vel consectetur. Integer ut massa sed ligula aliquam tincidunt. Aenean purus nunc, pulvinar eu scelerisque vel, commodo et erat. Sed consequat, diam sed lacinia congue, mi lectus porttitor nisi, ac fermentum justo erat a nisi. Vivamus ac rutrum felis, et commodo ante. Aliquam dignissim, neque vel volutpat rutrum, nisl ante sollicitudin lectus, at interdum nulla elit consectetur tellus. In commodo nisi quis sem euismod feugiat. Aenean accumsan porta tempor. Nullam porta faucibus risus, nec rhoncus orci feugiat in. Nunc hendrerit consequat laoreet. Aliquam congue vestibulum porta.</p>
	</div>
</div>
    
</div>

</body>
</html>