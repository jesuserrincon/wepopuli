<style type="text/css">
#s4 {
	color:#fff;
	background-position:0px -100px;
}
#s3 {
	color:#fff;
	background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -50px;
}
#s1 {
	color:#fff;
	background-position:0px -50px;
}
#s6 {
	left:551px !important;
}
#s7 {
	left:687px !important;
}
.fixed_button_basicos {
	margin-top:1870px !important;	
}
.box_a2 .col_d .first_precent {
	margin-left:85px;
}
</style>

<link href="css/jquery-filestyle.css" rel="stylesheet" />
  	
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
<script src="js/jquery.sticky.js"></script>
<script>
  $(document).ready(function(){
      
   $("#imagenlogo").bind('change', function(){
      if (this.files[0].size >= 2097152) {
      var tamanio = this.files[0].size/1048576;
      tamanio = tamanio.toFixed(2);
      alert('ATENCIÓN:\n\nSu archivo pesa '+(tamanio)+' Mb y supera el límite permitido de 2,0 MB.');
      $(this).val('');
      return false;
      }else{
          seleccionado();
      }
      });
      
      $("#archivo").bind('change', function(){
      if (this.files[0].size >= 2097152) {
      var tamanio = this.files[0].size/1048576;
      tamanio = tamanio.toFixed(2);
      alert('ATENCIÓN:\n\nSu archivo pesa '+(tamanio)+' Mb y supera el límite permitido de 2,0 MB.');
      $(this).val('');
      return false;
      }else{
          seleccionado();
      }
      });
      
      $("#archivo1").bind('change', function(){
      if (this.files[0].size >= 2097152) {
      var tamanio = this.files[0].size/1048576;
      tamanio = tamanio.toFixed(2);
      alert('ATENCIÓN:\n\nSu archivo pesa '+(tamanio)+' Mb y supera el límite permitido de 2,0 MB.');
      $(this).val('');
      return false;
      }else{
          seleccionado();
      }
      });
      
      $("#archivos2").bind('change', function(){
      if (this.files[0].size >= 2097152) {
      var tamanio = this.files[0].size/1048576;
      tamanio = tamanio.toFixed(2);
      alert('ATENCIÓN:\n\nSu archivo pesa '+(tamanio)+' Mb y supera el límite permitido de 2,0 MB.');
      $(this).val('');
      return false;
      }else{
          seleccionado();
      }
      });
      
   
  });
</script>
<script>
  $(document).ready(function(){
    $(".sticker").sticky({topSpacing:30});
	
	
	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
		   $('.sticker').addClass('fixed_button_gestores');
		   }else{
			   $('.sticker').removeClass('fixed_button_gestores');
		   }
	});
  });
  
  function confirm_click(){
return confirm("Si ya guardó cambios, o no lo desea hacer en este momento, presione Aceptar/Ok para continuar.");
} 
</script>
<div class="paso_paso clearfix">
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>reglamento_ci/index/<?php echo $proyectoid; ?>" id="s1" class="step">Reglamentos</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>basicos_ci/index/<?php echo $proyectoid; ?>" id="s2" class="step">Básicos</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>historia_ci/index/<?php echo $proyectoid; ?>" id="s3" class="step">Historia</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>gestores_front_ci/index/<?php echo $proyectoid; ?>" id="s4" class="step">Gestores</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>revision_ci/index/<?php echo $proyectoid; ?>" id="s6" class="step">Revisión</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>previsualizar_ci/index/<?php echo $proyectoid; ?>" id="s7" class="step">Previsualizar</a>
</div>

<div class="txt_reglamento">
	<h2 class="h2_b">Gestores del Proyecto </h2>
    Conocer a los gestores del proyecto genera confianza en el público y les da la certeza de que es una iniciativa legítima, respaldada por una organización que velará por la consecusión del proyecto.
</div>
<form id="form1" action="<?php echo base_url() ?>gestores_front_ci/add" method="post" enctype="multipart/form-data">
<div class="box_a2 pad_box clearfix">
    
    <div class="col_c">
      <div class="box_shadow box_c" style="margin-bottom:30px;">
       	  <h6 >Información de la Organización que gestiona el proyecto</h6>
          <div style="margin-bottom:10px;">
            <p>Los proyectos deben ser de fundaciones conformadas legalmente, con mínimo 3 años de constituidas.</p>
          </div>
		 
          <input type="text" class="input_c guardar" name="nombre" id="nombre" onkeyup="cambiartextoFundacion()" value="<?php echo $gestor->nombre ?>" />
          <div style="margin-bottom:50px"></div>          
          <div class="left" style="margin-top:-40px;">
          	<h6>Imagen del Logotipo de la Organización</h6>
            El Logotipo es la cara de la organización gestora del proyecto. Escoja la imagen correspondiente de su computador, en formato JPEG o PNG • Tamaño máximo: 2MB. • Dimensiones recomendadas: 200 x 200 pixeles.
            <div class="spacer"></div>

          </div>
            <div class="relative">
                <input type="hidden" name="imagen"  class="guardar" value="<?php echo $gestor->imagen;?>" />
            <input value="" name="archivos" id="imagenlogo" type="file" onchange="seleccionado();">
            </div>
             <?php if($gestor->imagen != ''){ echo '<span class="span_ver">Imagen subida correctamente - <a class="link_ver" target="_blank" href="'.base_url().'uploads/proyectos/gestores/'.$gestor->imagen.'"> Ver imagen actual</a></span>'; }?>
        
        <div class="div_line_2 clear"></div>	
            <div class="spacer"></div>
            <h6>Descripción de la organización a la que pertenece el proyecto</h6>
            <p>Háblenos de la organización, cuáles han sido sus logros y el impacto generado.</p>
            <textarea class="text_area_big guardar" name="descripcion" id="descripcion" ><?php echo $gestor->descripcion;?></textarea>
          <br />
          Máximo de caracteres <span id="descripcion-text">0/1000</span>
            <div class="spacer"></div>

        <table width="490" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>
            
      		<div class="col_int_box_c left">
                    <div class="label">País originario</div>
                    <input type="text" tabindex=1 class="input_a" />
                    <div class="label">Municipio</div>
                    <input type="text" tabindex=3 class="input_a" />
                    <div class="label">Teléfono 1</div>
                    <input type="text" tabindex=5 class="input_a guardar" onkeypress="return isNumberKey(event)" name="telefono" id="telefono" value="<?php echo $gestor->telefono;?>" />
                    <div class="label">E-mail</div>
                    <input type="text" tabindex=7 class="input_a guardar" name="email" id="email" value="<?php echo $gestor->email;?>" />
                    <div class="label">Twitter (opcional) </div>
                    <input type="text" tabindex=9 class="input_a guardar" name="twitter" id="twitter" value="<?php echo $gestor->twitter;?>" placeholder="@ejemplo" />
            </div>
            <div class="col_int_box_c right">
           		<div class="label">Departamento / Región</div>
                <input type="text" tabindex=2 class="input_a guardar" name="" id="" />
            	<div class="label">Dirección</div>
            	<input type="text" tabindex=4 class="input_a guardar" name="direccion" id="direccion"  value="<?php echo $gestor->direccion;?>" />
            	<div class="label">Teléfono 2 (opcional) </div>
            	<input type="text" tabindex=6 onkeypress="return isNumberKey(event)" class="input_a guardar" name="telefono2" id="telefono2" value="<?php echo $gestor->telefono2;?>" />
            	<div class="label">Página web (opcional) </div>
            	<input type="text" tabindex=8 class="input_a guardar" name="pagina" id="pagina"  value="<?php echo $gestor->pagina;?>" />
            	<div class="label">Facebook (opcional)  </div>
            	<input type="text" tabindex=10 class="input_a guardar" name="facebook" id="facebook"  value="<?php echo $gestor->facebook;?>" placeholder="facebook.com/ejemplo" />
            </div>
            
            </td>
          </tr>
        </table>
          
            <div class="clear div_line_2"></div>
            
			<div class="spacer"></div>
            <h6 style="margin-bottom:7px;">Certificado de Cámara de Comercio <span style="color:#999; font-size:14px">(No mayor a 30 días)</span></h6>

          
            <input tabindex=11 type="file" id="archivo" name="archivo" >
            <div></div>
			<?php if(!empty($gestor->certificado)){ ?>
              Documento subido correctamente <a style="color:#0CF; padding-left:5px; font-size:12px;" target="_blank" href="<?php echo base_url() ?>uploads/proyectos/gestores/<?php echo $gestor->certificado ?>">Ver Archivo Actual</a><br>
            <?php } ?>
          
          
		<div class="spacer"></div>
            <h6 style="margin-bottom:5px">Nit / Rut</h6>
            <input tabindex=12 type="file" id="archivo1" name="archivo1" ><!-- class="jfilestyle" data-theme="green" -->
			<div></div>
              <?php if(!empty($gestor->nit)){ ?>
               Documento subido correctamente  <a style="color:#0CF; padding-left:5px; font-size:12px;" target="_blank" href="<?php echo base_url() ?>uploads/proyectos/gestores/<?php echo $gestor->nit ?>">Ver Archivo Actual</a><br>
              <?php } ?>

            <div class="div_line_2"></div>
            <div class="spacer"></div>


			<h6>Información del líder del proyecto</h6>
                        <p>Fotografía del líder del proyecto</p>

            <div class="left">
           	Muéstrele al mundo quién es el líder del proyecto, quién lo estará respaldando. Escoja la imagen correspondiente de su computador, en formato JPEG o PNG • Tamaño máximo: 2MB. • Dimensiones recomendadas: 200 x 200 pixeles.
            <div class="spacer"></div>
            </div>
            <div style="clear:both"></div>
        <div class="relative">
                <input tabindex=13 type="hidden" name="liderimagen" id="liderimagen" class="guardar" value="<?php echo $gestor->liderimagen;?>" />
               <input tabindex=14 value="" name="archivos2" id="archivos2" type="file"  onchange="seleccionado2();">
            </div>
                        <?php if($gestor->liderimagen != ''){ echo '<span class="span_ver">Imagen subida correctamente - <a class="link_ver" target="_blank" href="'.base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen.'">Ver imagen actual</a></span>'; }?>
        
			<div class="spacer clear" style="margin-top:20px"></div>


        	<h6>Acerca del líder</h6>
            <p>Describa quién es el líder del proyecto, cuáles son las cualidades que lo hacen la persona ideal para liderarlo y en qué otros proyectos ha participado.</p>
            <textarea tabindex=15 class="text_area_big guardar" name="acercadelider" id="acercadelider"><?php echo $gestor->acercadelider;?></textarea>
            <br />
            Máximo de caracteres  <span id="acercadelider-text">0/1000</span>
            
            
              <div class="spacer"></div>
		
        <table width="490" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>
            
            <div class="col_int_box_c left">
                    <div class="label">Nombres</div>
                    <input tabindex=16 type="text" class="input_a guardar" name="nombreslider" id="nombreslider" value="<?php echo $gestor->nombreslider;?>" />
                    <div class="label">No. Identificación</div>
                    <input tabindex=18 type="text" onkeypress="return isNumberKey(event)" class="input_a guardar" name="cedulalider" id="cedulalider" value="<?php echo $gestor->cedulalider;?>" />
                    <div class="label">Teléfono 2 (opcional)</div>
                    <input tabindex=20 type="text" onkeypress="return isNumberKey(event)" class="input_a guardar" name="telefono2lider" id="telefono2lider" value="<?php echo $gestor->telefono2lider;?>" /> 
                    <div class="label">Facebook (opcional) </div>
                    <input tabindex=22 type="text" class="input_a guardar" name="facebooklider" id="facebooklider" value="<?php echo $gestor->facebooklider;?>" placeholder="facebook.com/ejemplo" />
            </div>
            <div class="col_int_box_c right">
           		<div class="label">Apellidos</div>
            	<input tabindex=17 type="text" class="input_a guardar" name="apellidoslider" id="apellidoslider" value="<?php echo $gestor->apellidoslider;?>" />
            	<div class="label">Teléfono 1</div>
            	<input tabindex=19 type="text" onkeypress="return isNumberKey(event)" class="input_a guardar" name="telefonolider" id="telefonolider" value="<?php echo $gestor->telefonolider;?>" />
            	<div class="label">E-mail</div>
            	<input tabindex=21 type="text" class="input_a guardar" name="emaillider" id="emaillider" value="<?php echo $gestor->emaillider;?>" />
            	<div class="label">Twitter (opcional)</div>
            	<input tabindex=23 type="text" class="input_a guardar" name="twitterlider" id="twitterlider" value="<?php echo $gestor->twitterlider;?>" placeholder="@ejemplo" />
            </div>
            
            </td>
            </tr>
            </table>
            <div class="clear"></div>
        
      </div>

        
	  
        
        
        
      
      
        <div class="clear" style="margin-bottom:0px"></div>
            <input type="hidden" id="redirec" name="redirec" class="guardar">
            <input type="hidden" name="proyectoid" class="guardar" value="<?php echo $proyectoid ?>">
            <input type="hidden" name="id" class="guardar" value="<?php echo $gestor->id ?>">
            <input type="button" onclick="javascript:borrarproyecto()" value="Borrar Proyecto" class="boton_c left" style="margin-right:10px;">
           <input type="button" onclick="javascript:guardarproyecto(1)" value="Guardar Cambios" class="boton_a left" style="margin-right:10px;">
           <a href="<?php echo base_url() ?>historia_ci/index/<?php echo $proyectoid?>" class="boton_b left" style="margin-right:10px;">Atrás</a>
           <a href="javascript:guardarproyecto(2)"  class="boton_b left">Siguiente</a>

        
    </div><!-- // columna C -->
    
    <div class="col_d mar_right">

        <?php echo $vista; ?>

    </div>
</div>

</form>

<script type="text/javascript" src="js/jquery-filestyle.js"></script>
<script type="text/javascript" src="js/aplication.js"></script>

<?php if(isset($tipo)){ ?>
<script>
    alert('Se han guardado los cambios realizados.');
</script>
<?php } ?>

<script>
function isNumberKey(e){
	var key = window.Event ? e.which : e.keyCode;
	return (key >= 48 && key <= 57);
}
init_contadorTa("acercadelider","acercadelider-text", 1000);
init_contadorTa("descripcion","descripcion-text", 1000);



function init_contadorTa(idtextarea, idcontador,max)
{
    $("#"+idtextarea).keyup(function()
            {
                updateContadorTa(idtextarea, idcontador,max);
            });
    
    $("#"+idtextarea).change(function()
    {
            updateContadorTa(idtextarea, idcontador,max);
    });
    
}

function updateContadorTa(idtextarea, idcontador,max)
{
    var contador = $("#"+idcontador);
    var ta =     $("#"+idtextarea);
    contador.html("0/"+max);
    
    contador.html(ta.val().length+"/"+max);
    if(parseInt(ta.val().length)>max)
    {
        ta.val(ta.val().substring(0,max-1));
        contador.html(max+"/"+max);
    }

}

    function cambiartextoFundacion(){
        var nombre = $('#nombre').val();
        $('#by1').html(nombre);
    }

function validaEmail(email) {
    var re  = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
    if (!re.test(email)) {
        alert ("Dirección de e-mail inválida");
        return false;
    }
    return true;
}

    function guardarproyecto(tipo){


        var email2 = $('#emaillider').val();
        if(email2 != ''){
        if(validaEmail(email2) == false){
            $('#emaillider').focus();
            return false;
        }
        }

        $('#redirec').val(tipo);
        if(tipo == 2){
            var nombre = $('#nombre').val();
            var descripcion = $('#descripcion').val();
            var direccion = $('#direccion').val();
            var telefono = $('#telefono').val();
            var email = $('#email').val();
            var acercadelider = $('#acercadelider').val();
            var nombreslider = $('#nombreslider').val();
            var apellidoslider = $('#apellidoslider').val();
            var cedulalider = $('#cedulalider').val();
            var telefonolider = $('#telefonolider').val();
            var emaillider = $('#emaillider').val();

        }

        var archivo = $('#archivo').val();
        var archivo1 = $('#archivo1').val();
        var archivo2 = $('#archivos2').val();
        var imagen = $('#imagenlogo').val();

        if(archivo1 != ''){
            $('#form1').submit();
            return false;
        }

        if(archivo != ''){
            $('#form1').submit();
            return false;
        }

        if(imagen != ''){
            $('#form1').submit();
            return false;
        }

        if(archivo2 != ''){
            $('#form1').submit();
            return false;
        }

        var data = $('.guardar').serialize();
        $.ajax({
            url: "<?php echo base_url()?>gestores_front/add",
            type: "post",
            dataType: "text",
            data: data
        }).done(function (data) {

                if(data == 2){
                    location.href = '<?php echo base_url(); ?>revision_ci/index/<?php echo $proyectoid; ?>';
                }else{
                alert('Se han guardado los cambios realizados.');
                }
            });

    }

function borrarproyecto(){
        if(confirm('¿Desea eliminar el proyecto?')){
            location.href = '<?php echo base_url(); ?>basicos/eliminarproyecto/<?php echo $proyectoid; ?>';
        }else{

        }
    }

/*function seleccionado(){

var archivos = document.getElementById("archivos");
var archivo = archivos.files;

 if(window.XMLHttpRequest) { 
 var Req = new XMLHttpRequest(); 
 }else if(window.ActiveXObject) { 
 var Req = new ActiveXObject("Microsoft.XMLHTTP"); 
 }


var data = new FormData();


for(i=0; i<archivo.length; i++){
   data.append('archivo'+i,archivo[i]);
}


Req.open("POST", "<?php //echo base_url(); ?>gestores_front/uploadimg", true);


Req.onload = function(Event) {
if (Req.status == 200) { 
  var msg = Req.responseText;
  $('#imagen').val(msg);
} else { 
  console.log(Req.status); 
} 
};

 Req.send(data); 
}*/
/*
function seleccionado2(){ 

var archivos = document.getElementById("archivos2");
var archivo = archivos.files;

 if(window.XMLHttpRequest) { 
 var Req = new XMLHttpRequest(); 
 }else if(window.ActiveXObject) { 
 var Req = new ActiveXObject("Microsoft.XMLHTTP"); 
 }


var data = new FormData();


for(i=0; i<archivo.length; i++){
   data.append('archivo'+i,archivo[i]);
}


Req.open("POST", "<?php //echo base_url(); ?>gestores_front/uploadimg", true);


Req.onload = function(Event) {
if (Req.status == 200) { 
  var msg = Req.responseText;
  $('#liderimagen').val(msg);
} else { 
  console.log(Req.status); 
} 
};

 Req.send(data); 
}*/
</script>

<!-- BACKUP validacion
var validacion = true;
            
            if($('#imagen').val() == ''){
                alert('Imagen Organizacion vacia');
                validacion =  false;
            }
            if($('#liderimagen').val() == ''){
                alert('Imagen lider vacia');
                validacion =  false;
            }
             if(nombre == ''){
                    $('#nombre').addClass("errorform");
                    $('#nombre').attr('placeholder', 'Campo obligatorio.');
                    $('#nombre').focus();
                    validacion = false;
            }else{
                    $('#nombre').removeClass("errorform");
            }
            
             if(descripcion == ''){
                    $('#descripcion').addClass("errorform");
                    $('#descripcion').attr('placeholder', 'Campo obligatorio.');
                    $('#descripcion').focus();
                    validacion = false;
            }else{
                    $('#descripcion').removeClass("errorform");
            }
            
             if(direccion == ''){
                    $('#direccion').addClass("errorform");
                    $('#direccion').attr('placeholder', 'Campo obligatorio.');
                    $('#direccion').focus();
                    validacion = false;
            }else{
                    $('#direccion').removeClass("errorform");
            }
             if(telefono == ''){
                    $('#telefono').addClass("errorform");
                    $('#telefono').attr('placeholder', 'Campo obligatorio.');
                    $('#telefono').focus();
                    validacion = false;
            }else{
                    $('#telefono').removeClass("errorform");
            }
            
             if(email == ''){
                    $('#email').addClass("errorform");
                    $('#email').attr('placeholder', 'Campo obligatorio.');
                    $('#email').focus();
                    validacion = false;
            }else{
                    $('#email').removeClass("errorform");
            }
            
            
              if(acercadelider == ''){
                    $('#acercadelider').addClass("errorform");
                    $('#acercadelider').attr('placeholder', 'Campo obligatorio.');
                    $('#acercadelider').focus();
                    validacion = false;
            }else{
                    $('#acercadelider').removeClass("errorform");
            }
              if(nombreslider == ''){
                    $('#nombreslider').addClass("errorform");
                    $('#nombreslider').attr('placeholder', 'Campo obligatorio.');
                    $('#nombreslider').focus();
                    validacion = false;
            }else{
                    $('#nombreslider').removeClass("errorform");
            }
              if(apellidoslider == ''){
                    $('#apellidoslider').addClass("errorform");
                    $('#apellidoslider').attr('placeholder', 'Campo obligatorio.');
                    $('#apellidoslider').focus();
                    validacion = false;
            }else{
                    $('#apellidoslider').removeClass("errorform");
            }
              if(cedulalider == ''){
                    $('#cedulalider').addClass("errorform");
                    $('#cedulalider').attr('placeholder', 'Campo obligatorio.');
                    $('#cedulalider').focus();
                    validacion = false;
            }else{
                    $('#cedulalider').removeClass("errorform");
            }
              if(telefonolider == ''){
                    $('#telefonolider').addClass("errorform");
                    $('#telefonolider').attr('placeholder', 'Campo obligatorio.');
                    $('#telefonolider').focus();
                    validacion = false;
            }else{
                    $('#telefonolider').removeClass("errorform");
            }
              if(emaillider == ''){
                    $('#emaillider').addClass("errorform");
                    $('#emaillider').attr('placeholder', 'Campo obligatorio.');
                    $('#emaillider').focus();
                    validacion = false;
            }else{
                    $('#emaillider').removeClass("errorform");
            }
            
            if(validacion == false){
                alert('Algunos de los campos obligatorios estan vacios.');
                return false;
            }
-->