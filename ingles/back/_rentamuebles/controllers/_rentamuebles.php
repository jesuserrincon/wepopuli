<?php

class _Rentamuebles extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "RENTAMUEBLES";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {

        $this->_data['save'] = $value;
        $b = new Rentamuebles();
        $info = $b->getRentamuebles();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('rentamuebles');

    }

    public function add() {
        $this->_data['nombremodulo'] = "COLORES / New";
        $this->build('rentamuebles_new');
    }

    public function new_rentamuebles() {
        $datos = array(
            'titulomision' => $this->input->post('titulomision'),
            'mision' => $this->input->post('mision'),
            'titulovision' => $this->input->post('titulovision'),
            'vision' => $this->input->post('vision'),
            //'titulohistotia' => $this->input->post('titulohistotia'),
            //'historia' => $this->input->post('historia'),
            //'titulotexto' => $this->input->post('titulotexto'),
            'texto' => $this->input->post('texto'),
        );

        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'rentamuebles';
        $b = new Rentamuebles();

        if ($datos['mision'] != ''){

            $this->_data['save'] = FALSE;
            $build = 'rentamuebles_new';
            if ($b->saveRentamuebles($datos)){
                $build = 'rentamuebles';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getRentamuebles();
        $this->_data["info"] = $info;
        return redirect('cms/rentamuebles/index/'.$save);

    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'rentamuebles/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."rentamuebles/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;
            $resize['width']	 =300;
            $resize['height']	= 160;
            $resize['new_image'] = UPLOADSFOLDER."rentamuebles/new/".$data["data"]["file_name"];

            $this->load->library('image_lib', $resize);

            if ( ! $this->image_lib->resize())
            {
                $data = array(
                    'ok' => false,
                    'resize' => $this->image_lib->display_errors()
                );
            }else{
                $data = array(
                    'ok' => true,
                    'resize' => true,
                    'data' => $this->upload->data()
                );
            }
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Rentamuebles();
        $dat = $b->getRentamueblesById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("rentamuebles_edit");
    }
    
    public function update_Rentamuebles(){
        $b = new Rentamuebles();
        $datos = array(
            'id' => $this->input->post('id'),
            'titulomision' => $this->input->post('titulomision'),
            'mision' => $this->input->post('mision'),
            'titulovision' => $this->input->post('titulovision'),
            'vision' => $this->input->post('vision'),
            //'titulohistotia' => $this->input->post('titulohistotia'),
            //'historia' => $this->input->post('historia'),
            //'titulotexto' => $this->input->post('titulotexto'),
            'texto' => $this->input->post('texto'),

        );
        $update = $b->updateRentamuebles($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateRentamuebles($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/rentamuebles/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Rentamuebles();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/rentamuebles/index/".$return);
    }

}

