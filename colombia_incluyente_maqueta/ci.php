<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  
<style type="text/css">
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>
</head>

<body>
<?php include"header.php" ?>  


<div class="box_a pad_box clearfix">

<div class="wrapper relative">
    <ul class="rslides" id="slider2">
      <li><a href="#"><img src="assets/img/009.jpg" alt=""></a></li>
      <li><a href="#"><img src="assets/img/025.png" alt="" style="height:223px"></a></li>
      <li><a href="#"><img src="assets/img/009.jpg" alt=""></a></li>
    </ul>
  </div>
  
  <div class="big_box">
    	<h2 class="h2_b"><span style="font-family:Arial; font-weight:bold; color:#75796f">¿</span>Qué es CI?</h2>
    		<div class="txt">
            <div class="clear" style="margin-bottom:30px;"></div>
            
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vel convallis augue, vel ornare libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac faucibus odio. Nullam eget urna congue, placerat odio ac, dictum tellus. Duis venenatis ante lacus, quis ullamcorper risus porttitor sed. Ut vel orci volutpat, porta eros id, varius metus. Sed non mattis nisl. Quisque bibendum ante pharetra pharetra consequat. Proin congue massa aliquet enim imperdiet pharetra. Aenean malesuada iaculis diam vel varius. Pellentesque volutpat porttitor urna consectetur rhoncus.</p>
            <p>Aliquam erat volutpat. Donec vitae est pharetra, luctus nibh non, adipiscing sem. Duis bibendum ante non risus pulvinar, id scelerisque dui semper. Duis eget erat sem. Vestibulum placerat, metus sit amet ultrices scelerisque, metus diam gravida enim, ac lobortis orci turpis in elit. Etiam est enim, convallis ut lacus quis, egestas porttitor quam. Vestibulum a erat vehicula, pharetra mi in, vehicula purus. Vestibulum suscipit est ut metus laoreet, eget molestie arcu mollis. Nullam neque nunc, laoreet non odio in, rutrum suscipit purus. Ut eget lorem scelerisque, facilisis est at, varius purus. Fusce egestas nec eros vitae suscipit. Maecenas ornare fermentum sodales. Quisque ante libero, vehicula et pellentesque pretium, congue id neque. Nam fringilla nibh quis commodo suscipit. Nulla mollis massa nec ante sollicitudin malesuada.</p>
            <p>Donec eget turpis ac diam dignissim consectetur. Aliquam a nisl ut leo placerat tristique. Aliquam erat volutpat. Quisque rhoncus ipsum eu metus luctus aliquet. Sed enim magna, pharetra vitae viverra nec, posuere vel sapien. Nam est tortor, faucibus at imperdiet vitae, bibendum quis dolor. Nam euismod sem vehicula, laoreet nisl accumsan, consectetur urna. Phasellus et tincidunt mi. Duis enim est, placerat id rhoncus et, lobortis vel ligula. Phasellus posuere est sit amet risus aliquet sollicitudin. Aliquam ut urna vel velit placerat suscipit sit amet sed ante. Nunc eleifend eu augue vel ultricies. Cras magna mauris, egestas id sem sit amet, fringilla tincidunt ipsum.</p>
            <p>Vivamus interdum ac nibh eu pretium. Nulla faucibus risus convallis eros iaculis aliquam. Ut congue ipsum vitae odio lobortis, nec rhoncus neque molestie. Maecenas adipiscing, sem sed tempus condimentum, purus enim mattis diam, ac lobortis odio nunc at urna. Curabitur scelerisque eget diam sed rhoncus. Quisque condimentum, massa in aliquet tempus, mauris sapien dapibus metus, in placerat lacus leo nec felis. Etiam luctus neque sed condimentum hendrerit. Duis magna nisl, mattis sed condimentum commodo, varius a arcu. Duis sit amet porttitor odio, eget dapibus mi. Duis ultricies ante quis mattis ultrices.</p>
            <center></center>
            <div class="clear" style="margin-top:30px;"></div>
        </div>
        
  </div>
</div>

<div id="inline1" style="width:550px;display:none;">
    <h4>Antes, debes saber que:<br />
      <br />
    </h4>
    <p>Lo que se publique debe ser un proyecto concreto y debe
      poder medirse el impacto y verse el resultado de manera tangible.</p>
    <p>El proyecto debe estár localizado en Colombia.</p>
    <p>El proyecto lo debe gestionar una organización debidamente registrada en la camara de comercio Colombiana, con fecha de constitución no menor a 3 años y un minimo de 3 proyectos realizados que cumplan con criterios reservados de Colombia Incluyente.</p>


    <h4>¿Dónde se encuentra el proyecto?<br />
      <br />
    </h4>
  <div class="form_pop_box">
   	  <div class="label">País</div>
        <input type="text" disabled="disabled" class="input_a" value="Colombia" />
    	<div class="label">Departamento/Región <span>*</span></div>
        <input type="text" class="input_a" />
    	<div class="label">Municipio <span>*</span></div>
        <input type="text" class="input_a" />
    </div>
    
    <div class="div_line clear" style="margin-bottom:20px; margin-top:20px;"></div>
  <input type="submit" class="bt_green" value="continuar" />
</div>



<div id="inline2" style="width:400px;display:none;">
    <h4>¿Eres Usuario de Colombia Incluyente?<br /><br /></h4>
    <p>Para postular un proyecto debes ser ususario registrado.
	Ingresa o de lo contario puedes <a href="" style="color:#0091e9">registrarte gratis.</a>
  <div class="form_pop_box"><br />
    	<div class="label">Usuario <span>*</span></div>
        <input type="text" class="input_a" />
    	<div class="label">Contraseña <span>*</span></div>
        <input type="password" class="input_a" />
    </div>
    
    <div class="div_line clear" style="margin-bottom:20px; margin-top:20px;"></div>
  	<a href="reglamentos.php"><input type="submit" class="bt_green" value="ingresa" /></a>
</div>

<?php include"footer.php" ?>  
</body>
</html>