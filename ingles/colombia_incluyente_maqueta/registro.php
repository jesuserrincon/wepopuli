<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  

</head>

<body>
<?php include"header.php" ?>  

<div class="box_a pad_box clearfix">
	<div class="col_a">
        <h2 class="h2_b">Ingresar/ Registro</h2>
        <div class="ingresar">
        	<div class="int_pad font_12">
                <span class="h5">Ingresar</span><br />
                Por favor ingresa tus datos
            </div>
            <div class="div_line" style="margin-bottom:10px;"></div>

        	<div class="int_pad font_12">
                <div class="label">E-mail</div>
                <input type="text" class="input_a" />
                
                <div class="label">Password</div>
                <input type="password" class="input_a" />
                
                <a href="#" class="right link_a">Olvide mi contraseña</a>
                <div class="clear"></div>
                <a class="recordarme right">Recordarme</a>
            </div>
            <div class="div_line" style="margin:10px 0px;"></div>
			<a href="#" class="bt_green">Ingresar</a>	

        </div>
    
    </div>
    
    <div class="col_b">
    	<h3 class="h3_b">¿Nuevo en Colombia Incluyente?</h3>
        
        <div class="box_shadow clearfix">
            <div class="col_int_b_1 clearfix">
                <div class="label">Nombres <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">No. Cédula <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">Teléfono 2 (Opcional)</div>
                <input type="text" class="input_b" />
                <div class="label">Pais <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">Departamento (Región) <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">Dirección <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">Facebook (opcional)</div>
                <input type="text" class="input_b" />
                <div class="label">Twitter (opcional)</div>
                <input type="text" class="input_b" />
                <div class="clear" style="margin-bottom:40px"></div>
                <a class="verde_bt right">Deseo suscribirme al newsletter</a>

            </div>
            
            <div class="col_int_b_1 clearfix right">
                <div class="label">Apellidos <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

                <div class="label">Télefono 1 <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

                <div class="label">E-mail <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

                <div class="label">Confirmar E-mail <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

                <div class="label">Municipio <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

              <div class="label">Sexo <span class="requiered">*</span></div>
               
                <label><input class="sex_f" type="radio" name="n" title="femenino"/></label>
                <label class="rr"><input class="sex_m" type="radio" name="n" title="masculino"/></label>
				<div class="clear" style="margin-bottom:20px"></div>
                
                <div class="label">Estado Civil <span class="requiered">*</span></div>
                <select class="select">
                    <option value="">Soltero</option>
                    <option>Casado</option>
                    <option>Divorciado</option>
                </select>
				<div class="clear m_20_bottom"></div>

                <div class="label">Contraseña <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

                <div class="label">Confirmar Contraseña <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

            </div>
            
            <div class="div_line clear" style="margin-bottom:0px"></div>
            <div class="pad_box_10 clearfix">
            <a href="reglamentos.php" class="bt_green right">Registrarme!</a>	
            <div class="right" style="padding:5px 20px">
                <a class="verde_bt2 right">Acepto <span>terminos y condiciones</span></a><br />
                <a class="verde_bt3 right">Recordarme</a>
            </div>
			</div>
        </div>
        
        
    </div>
</div>
<script type="text/javascript">
	$('.checkbox-style, .radio-style').customRadioCheck();
	$('.sex_f').sexCheck();
	$('.sex_m').sexCheck();
</script>
<?php include"footer.php" ?>  
</body>
</html>