<?php

class Slider extends DataMapper {

    public $model = 'slider';
    public $table = 'slider';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getSlider(){
        return $this->get();
    }
    
    public function saveSlider($object = "") {
        $this->imagen = $object['imagen'];
        return $this->save();
    }
    
    public function getSliderById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateSlider($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

