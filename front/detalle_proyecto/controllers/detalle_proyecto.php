<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class detalle_proyecto extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."textos/textos",
               CMSPREFIX."gestores/gestores",
               CMSPREFIX."imagenescomplementarias/imagenescomplementarias",
               CMSPREFIX."proyectos/estados",
               CMSPREFIX."banner_proyecto/banner_proyecto",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0,$validacion = 0) {
        parent::index($proyectoid);
        $this->set_title('Bienvenidos a ' . SITENAME, true);

        $e = new Estados();
        $estados = $e->getEstados2();
        $arrayEstados = array();
        foreach($estados as $data){
            $arrayEstados[$data->id]=$data->nombre;
            $arrayClases[$data->id]=$data->clase;
        }
        
        $banner_proyecto = new Banner_proyecto();
        $bannerspro = $banner_proyecto->getBanner_proyecto_by_seccion($proyectoid);
        $this->_data['bannerspro'] = $bannerspro;
        $this->_data['estados'] = $arrayEstados;
        $this->_data['clases'] = $arrayClases;
        $proyectoOBJ = new Proyectos();
        $gestorOBJ = new Gestores();
        //seleccionamos el proyecto a mostrar
        $this->_data["proyecto"] = $proyectoOBJ->getProyectosById($proyectoid);
        //Fin mostrar el proyecto 
        //traemos la informacion de la fundacion y lider
        $this->_data["gestor"] = $gestorOBJ->getGestoresByIdProyecto($proyectoid);
        //Fin informacion fundacion
        //enviamos una variable para validar si el usuario esta conectado 
        if($this->nativesession->get('usuario') == ''){
            $this->_data["login"] = 'no';
        }else{
            $this->_data["login"] = 'si';
        }
        $voluntariado = new Voluntariado();
        
        $this->_data["cantidadvoluntariados"] = $voluntariado->getVoluntariadoByProyectoCOUNT($proyectoid);
        
        
        $especie = new Especie();
        $this->_data["cantidadespecie"] = $especie->getEspecieByProyectoCOUNT($proyectoid);
        
        $this->_data['validacion'] = $validacion;
        //fin validacion usuario conectado
        $imagenesComplementarias = new Imagenescomplementarias();
        $imgComplementarias = $imagenesComplementarias->getImagenescomplementariasByProyecto($proyectoid);
        $this->_data['imgComplementarias'] = $imgComplementarias;
        return $this->build('detalle_proyecto');
    }
    
  
    
}
