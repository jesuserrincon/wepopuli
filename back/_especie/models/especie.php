<?php

class Especie extends DataMapper {

    public $model = 'especie';
    public $table = 'especie';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getEspecie(){
        return $this->query('SELECT id,nombre  FROM cms_especie');
    }
    
    public function getEspecieByProyecto($idproyecto = 0){
        return $this->query('SELECT id,nombre,valor FROM cms_especie WHERE proyectos_id = '.$idproyecto);
    }
     public function getEspecieByProyectoCOUNT($idproyecto = 0){
        $consulta =  $this->query('SELECT COUNT(*) cantidad FROM cms_especie WHERE proyectos_id = '.$idproyecto);
        return $consulta->cantidad;
    }
    
    public function saveEspecie($object = "") {
        $this->nombre = $object['nombre'];
        $this->cantidad_paquetes = $object['cantidad_paquetes'];
        $this->valor = $object['valor'];
        $this->proyectos_id = $object['proyectos_id'];
        return $this->save();
    }
    
    public function getLastId(){
        $usuario = $this->query('SELECT MAX(id) AS id FROM cms_especie');
        return $usuario->id;
    }
    
    public function getEspecieById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateEspecie($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

