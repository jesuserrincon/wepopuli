<style type="text/css">
#s6 {
	color:#fff;
	background-position:0px -100px;
}
#s5 {
	color:#fff;
	background-position:0px -50px;
}
#s4 {
	color:#fff;
	background-position:0px -50px;
}
#s3 {
	color:#fff;
	background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -50px;
}
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>

<link href="assets/css/jquery-filestyle.css" rel="stylesheet" />
  	
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
<div class="paso_paso clearfix">
    <a href="<?php echo base_url(); ?>reglamento/index/<?php echo $proyectoid; ?>" id="s1" class="step">Rules</a>
    <a href="<?php echo base_url(); ?>basicos/index/<?php echo $proyectoid; ?>" id="s2" class="step">Basics</a>
    <a href="<?php echo base_url(); ?>historia/index/<?php echo $proyectoid; ?>" id="s3" class="step"> Story </a>
    <a href="<?php echo base_url(); ?>gestores_front/index/<?php echo $proyectoid; ?>" id="s4" class="step">Gestores</a>
    <a href="<?php echo base_url(); ?>cuenta/index/<?php echo $proyectoid; ?>" id="s5" class="step">Cuenta</a>
    <a href="<?php echo base_url(); ?>revision/index/<?php echo $proyectoid; ?>" id="s6" class="step">Revisión</a>
    <a href="<?php echo base_url(); ?>previsualizar/index/<?php echo $proyectoid; ?>" id="s7" class="step">Previsualizar</a>
</div>
<div class="txt_reglamento">
  <h2 class="h2_b">Revisión</h2>
</div>

<div class="box_a2 clearfix">
    
    
    <div class="col_c">
    	<div class="box_shadow box_c">
    	<?php 
      $texto2 = str_replace('/colombiaincluyente/back/assets/js/kcfinder-2.51/upload/files/', base_url().'back/assets/js/kcfinder-2.51/upload/files/',$texto);
     echo str_replace('<ul>','<ul class="list_2">', $texto2);
       ?>
        </div>
        
              <div class="clear spacer"></div>

            <input type="hidden" id="redirec" name="redirec" class="guardar">
            <input type="hidden" name="id" class="guardar" value="<?php echo $proyectoid ?>">
            <input type="button" onclick="javascript:borrarproyecto()" value="Delete Project" class="boton_c left" style="margin-right:10px;">
            <a href="<?php echo base_url(); ?>cuenta/index/<?php echo $proyectoid; ?>" class="boton_b left" style="margin-right:10px;">Back</a>
            <a href="<?php echo base_url(); ?>previsualizar/index/<?php echo $proyectoid; ?>"  class="boton_b left">Continue</a>

    </div>
    <!-- // columna C -->
    
     <div class="col_d mar_right">

         <?php echo $vista; ?>

    </div>
</div>
<script type="text/javascript" src="js/jquery-filestyle.js"></script>
<script type="text/javascript" src="js/aplication.js"></script>
<script>
        function borrarproyecto(){
        if(confirm('Are you sure you want to delete this project?')){
            location.href = '<?php echo base_url(); ?>basicos/eliminarproyecto/<?php echo $proyectoid; ?>';
        }else{

        }
    }
</script>
        