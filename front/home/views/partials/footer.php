<div class="div_line" style="margin-top:10px"></div>
<div class="footer clearfix device_320">
    <div class="box_a device_320">
        <div class="device_300">
        <div class="col_footer">
            <h3>Proyectos</h3>
            <a href="<?php echo base_url().'resultados_busqueda/index/0/3';?>">Más apoyados</a>
            <a href="<?php echo base_url().'resultados_busqueda/index/0/4';?>">Exitosos</a>
            <a href="<?php echo base_url().'resultados_busqueda/index/0/5';?>">Urgentes</a>
            <?php
            foreach($s_proyectos as $data){ ?>
                <a href="<?php echo base_url().'seccion/index/'.$data->id ?>"><?php echo $data->titulo ?></a>
            <?php } ?>
        </div>
        <div class="col_footer">
            <h3>Aprende</h3>
            <?php
            foreach($s_aprende as $data){ ?>
                <a href="<?php echo base_url().'seccion/index/'.$data->id ?>"><?php echo $data->titulo ?></a>
            <?php } ?>
        </div>
        </div>


        <div class="device_300 clear_mobile">
        <div class="col_footer">
            <h3>De nosotros</h3>
            <a href="<?php echo base_url().'seccion/index/3' ?>">¿Quiénes somos?</a>
            <a href="<?php echo base_url().'seccion/index/4' ?>">¿Cómo funciona?</a>
            <?php foreach($s_acerca as $data){ ?>
                <a href="<?php echo base_url().'seccion/index/'.$data->id ?>"><?php echo $data->titulo ?></a>
            <?php } ?>
        </div>
        <div class="col_footer">
            <h3>Social</h3>
            <!--a href="#">Newsfeed</a>
            <a href="#">Blog</a>
                        <a href="#">Tumblr</a-->

            <a href="#newsletter_box" class="fancybox">Newsletter</a>
            <a href="https://twitter.com/wepopuli" target="_blank">Twitter</a>
            <a href="https://facebook.com/wepopuli" target="_blank">Facebook</a>
            <?php
            foreach($s_social as $data){ ?>
                <a href="<?php echo base_url().'seccion/index/'.$data->id ?>"><?php echo $data->titulo ?></a>
            <?php } ?>
        </div>
        </div>
        
        <div class="clear pad_top_mobile device_320"></div>


        <div class="device_300">
            <div class="foter_social ft_1" style="margin-left:370px">
                <a href="https://facebook.com/wepopuli" target="_blank" title="facebook"><img src="img/026.png" /></a>
                Like
            </div>
            <div class="foter_social">
                <a href="https://twitter.com/wepopuli" target="_blank" title="twitter"><img src="img/027.png" /></a>
                Tweet
            </div>
            <div class="foter_social">
                <a href="https://www.youtube.com/user/wepopuli" target="_blank" title="youtube"><img src="img/028.png" /></a>
                Follow us!
            </div>
        </div>
       
        
    </div>
</div>

<div id="newsletter_box" style="display:none; width:350px; overflow:hidden; height:156px;">
    <h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">Suscribirme al newsletter:</h2>
    <input type="text" placeholder="E-mail" id="emailsuscrito" class="input_j" />
    <input type="submit" value="Suscribirme" onclick="suscribirse()" class="bt_green left" style=" width:349px; background-size:100% 100%"  />
</div>
<script>
    function validaEmailNews(email) { 
    var re  = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/; 
    if (!re.test(email)) { 
        return false; 
    } 
    return true; 
    }
    
    function suscribirse(){
        var email = $('#emailsuscrito').val();
        if(validaEmailNews(email)){
            
            $.post( "<?php echo base_url(); ?>registro/suscribirsenewsletter", { email : email })
            .done(function( data ) {
                if(data == 0){
                     alert('El usuario ya se ha suscrito al newsletter.');
                     return false;
                }
                if(data == 1){
                     alert('Gracias por suscribirse a nuestro newsletter.');
                     $('.fancybox-close').click();
                     return false;
                }
            });
            
        }else{
             alert ("Debe ingresar un E-mail válido"); 
            return false;
        }
        
    }
</script>
    