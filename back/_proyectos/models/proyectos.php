<?php

class Proyectos extends DataMapper {

    public $model = 'proyectos';
    public $table = 'proyectos';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    
    
    public function getEstados(){
        return $this->query('SELECT id,nombre FROM cms_estados');
    }
    
    public function UpdateDefaultDias($dias = 0){
        $this->query_void('alter table cms_proyectos alter dias_cerrado set default '.$dias.';');
    }
    
    public function getDefaultdiascerrar(){
        $registro = $this->query('SHOW FULL COLUMNS FROM cms_proyectos  WHERE Field = "dias_cerrado"');
        return $registro->Default;
    }
    
    public function getProyectos(){
        return $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,est.nombre nombreestado,destacado,urgente,exitoso,quitarexitoso,usuarios_id
                             FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                             INNER JOIN cms_estados est ON pro.estados_id = est.id ');
    }
    
    public function getProyectosReportegeneral(){
        return $this->query('SELECT titulo, usu.nombres nombres ,usu.apellidos apellidos,nombreslider,ges.nombre nombregestor, ges.direccion direccion
                             ,ges.telefono telefono,fechainicio,fechafin,est.nombre estado,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,valormonetario
                             ,(SELECT SUM(cantidad) FROM cms_voluntariado WHERE proyectos_id = pro.id ) voluntariadosolicitado
                             ,(SELECT SUM(valor) FROM cms_especie WHERE proyectos_id = pro.id ) especiesolicitado
                             ,(SELECT COUNT(*) FROM cms_ayuda_voluntariado WHERE tipo = 0 AND proyectos_id = pro.id ) voluntariadoconseguido
                             FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                             INNER JOIN cms_estados est ON pro.estados_id = est.id
                             INNER JOIN cms_usuarios usu ON usu.id = pro.usuarios_id
                             LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id');
    }
    public function getReporteByProyecto($id = 0){
        return $this->query('SELECT nombres,apellidos,tipo,vol.titulo titulo,nombre,fecha,pro.titulo nombreproyecto
                             FROM cms_ayuda_voluntariado ayu 
                             INNER JOIN cms_usuarios usu ON usu.id = ayu.usuarios_id
                             INNER JOIN cms_proyectos pro ON pro.id = ayu.proyectos_id
                             LEFT JOIN cms_voluntariado vol ON vol.id = ayu.tipo_voluntariado
                             LEFT JOIN cms_especie esp ON esp.id = ayu.tipo_voluntariado
                             WHERE ayu.proyectos_id = '.$id);
    }
    
    
    public function getProyectosDestacados(){
        return $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,dias_para_cerrar,
                             ges.nombre fundacion,exitoso,urgente,estados_id,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado
                             FROM cms_proyectos pro 
                             LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                             INNER JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                             WHERE destacado = 1 AND estados_id = 1 OR estados_id = 3');
    }
    
     public function getProyectosByPalabra($palabra,$desde){
        $return = $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,dias_para_cerrar,ges.nombre nombre_fundacion,urgente,exitoso,estados_id,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                                WHERE estados_id = 3  AND titulo like "%'.$palabra.'%" OR estados_id = 3  AND descripcion_corta like "%'.$palabra.'%" OR estados_id = 3 
                                AND tags_busqueda like "%'.$palabra.'%" OR estados_id = 1  AND titulo like "%'.$palabra.'%" OR  estados_id = 1  
                                AND descripcion_corta like "%'.$palabra.'%" OR  estados_id = 1  AND tags_busqueda like "%'.$palabra.'%" LIMIT '.$desde.',8
                                ');
        return $return;
    }
    
     public function getProyectosByPalabraCOUNT($palabra){
        $return = $this->query('SELECT COUNT(*) cantidad
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                                WHERE estados_id = 3  AND titulo like "%'.$palabra.'%" OR estados_id = 3  AND descripcion_corta like "%'.$palabra.'%" OR estados_id = 3 
                                AND tags_busqueda like "%'.$palabra.'%" OR estados_id = 1  AND titulo like "%'.$palabra.'%" OR  estados_id = 1  
                                AND descripcion_corta like "%'.$palabra.'%" OR  estados_id = 1  AND tags_busqueda like "%'.$palabra.'%" 
                                ');
        return $return;
    }

    
    
     public function getProyectosByTags($palabra,$desde){
        $return = $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,dias_para_cerrar,ges.nombre nombre_fundacion,urgente,exitoso,estados_id,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                                WHERE  estados_id = 3  AND tags_busqueda like "%'.$palabra.'%" OR estados_id = 1  AND tags_busqueda like "%'.$palabra.'%" LIMIT '.$desde.',8
                                ');
        return $return;
    }
    
     public function getProyectosByTagsCOUNT($palabra){
        $return = $this->query('SELECT COUNT(*) cantidad
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                                WHERE  estados_id = 3  AND tags_busqueda like "%'.$palabra.'%" OR estados_id = 1  AND tags_busqueda like "%'.$palabra.'%"
                                ');
        return $return;
    }
    
    
    
    public function getProyectosMasapoyados($desde){
        $return = $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,dias_para_cerrar,ges.nombre nombre_fundacion,urgente,exitoso,estados_id,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id 
                                WHERE estados_id = 1 AND porcentaje_total > 0 ORDER BY porcentaje_total DESC LIMIT '.$desde.',8');
        return $return;
    }
    
    public function getProyectosMasapoyadosCOUNT(){
        $return = $this->query('SELECT COUNT(*) cantidad
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id 
                                WHERE estados_id = 1 AND porcentaje_total > 0 ORDER BY porcentaje_total DESC ');
        return $return;
    }
    
    public function getProyectosExitosos($desde){
        $return = $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,dias_para_cerrar,ges.nombre nombre_fundacion,urgente,exitoso,estados_id,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id 
                                WHERE quitarexitoso = 0 AND porcentaje_total >= 100 OR quitarexitoso = 0 AND exitoso = 1  LIMIT '.$desde.',8
                                ');
        return $return;
    }
    public function getProyectosExitososCOUNT(){
        $return = $this->query('SELECT COUNT(*) cantidad
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id 
                                WHERE quitarexitoso = 0 AND porcentaje_total >= 100 OR quitarexitoso = 0 AND exitoso = 1 
                                ');
        return $return;
    }
    
    public function getProyectosUrgentes($desde){
        $return = $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,dias_para_cerrar,ges.nombre nombre_fundacion,urgente,exitoso,estados_id,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                                WHERE urgente = 1 LIMIT '.$desde.',8');
        return $return;
    }
    
    public function getProyectosUrgentesCOUNT(){
        $return = $this->query('SELECT COUNT(*) cantidad
                                FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                                LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                                WHERE urgente = 1');
        return $return;
    }
    
    
    public function getProyectosByCategoria($categoria,$desde){
        return $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,dias_para_cerrar,urgente,exitoso,estados_id,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado,ges.nombre nombre_fundacion
                             FROM cms_proyectos pro INNER JOIN cms_categorias cat ON pro.categorias_id = cat.id
                             LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                             WHERE estados_id = 1  AND categorias_id  = '.$categoria.' OR estados_id = 3  AND categorias_id  = '.$categoria.' LIMIT '.$desde.',8');
    }
    
     public function getProyectosByCategoriaCOUNT($categoria){
        return $this->query('SELECT COUNT(*) cantidad
                             FROM cms_proyectos pro INNER JOIN cms_categorias cat ON pro.categorias_id = cat.id
                             LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                             WHERE estados_id = 1  AND categorias_id  = '.$categoria.' OR estados_id = 3  AND categorias_id  = '.$categoria);
    }
    
    public function getProyectosApoyadosByUsuario($idusu,$desde){
        return $this->query('SELECT pro.id id,pro.titulo titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,dias_para_cerrar,est.nombre nombreestado
                            ,ges.nombre nombreges,ges.nombreslider nombreslider,ges.pagina paginages,ges.facebook facebookges,ges.twitter twitterges,ges.imagen imagenges
                            ,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,COUNT(*),vol.titulo tituloayuda,ges.facebook facebook,ges.pagina pagina,ges.twitter twitter
                            ,ges.descripcion descripcionges,acercadelider,apellidoslider,ges.liderimagen imagenlider,facebooklider,twitterlider,emaillider,clase,textotooltip,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado
                            FROM cms_ayuda_voluntariado ayu 
                            INNER JOIN cms_voluntariado vol ON vol.id = ayu.tipo_voluntariado
                            INNER JOIN cms_proyectos pro ON pro.id = ayu.proyectos_id
                            LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                            LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                            LEFT JOIN cms_estados est ON est.id = pro.estados_id
                            WHERE ayu.usuarios_id = '.$idusu.' GROUP BY pro.id ORDER BY pro.id DESC LIMIT '.$desde.',10');
    }
    
    
     public function getProyectosApoyadosByUsuarioCOUNT($idusu){
        $consulta = $this->query('SELECT pro.id id
                            FROM cms_ayuda_voluntariado ayu 
                            INNER JOIN cms_voluntariado vol ON vol.id = ayu.tipo_voluntariado
                            INNER JOIN cms_proyectos pro ON pro.id = ayu.proyectos_id
                            LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                            LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                            LEFT JOIN cms_estados est ON est.id = pro.estados_id
                            WHERE ayu.usuarios_id = '.$idusu.' GROUP BY pro.id ORDER BY pro.id DESC ');
        return $consulta;
    }
    
    public function getVoluntariadoAyudaUsuario($idusu = 0,$proyectosid = 0){
        return $this->query('SELECT titulo AS tituloayuda,COUNT(*) cantidad
FROM cms_ayuda_voluntariado ayu INNER JOIN cms_voluntariado vol ON vol.id = ayu.tipo_voluntariado
WHERE usuarios_id = '.$idusu.' AND ayu.proyectos_id = '.$proyectosid.' GROUP BY titulo' );
    }
    
     public function getVoluntariadoAyudaTotal($proyectosid = 0){
        return $this->query('SELECT titulo AS tituloayuda,COUNT(*) cantidad
                             FROM cms_ayuda_voluntariado ayu INNER JOIN cms_voluntariado vol ON vol.id = ayu.tipo_voluntariado
                             WHERE  ayu.proyectos_id = '.$proyectosid.' GROUP BY titulo' );
    }
    
    public function getProyectosByUsuario($idusu,$desde){
        return $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,dias_para_cerrar
                             ,est.nombre nombreestado
                             ,ges.nombre nombreges,ges.nombreslider nombreslider,ges.pagina paginages,ges.facebook facebookges,ges.twitter twitterges,ges.imagen imagenges
                             ,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,ges.facebook facebook,ges.pagina pagina,ges.twitter twitter
                            ,ges.descripcion descripcionges,acercadelider,apellidoslider,ges.liderimagen imagenlider,facebooklider,twitterlider,emaillider,clase,textotooltip,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado
                             FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                             LEFT JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                             LEFT JOIN cms_estados est ON est.id = pro.estados_id
                             WHERE usuarios_id  = '.$idusu.' ORDER BY pro.id DESC LIMIT '.$desde.',10');
    }
    
     public function getProyectosByUsuarioCOUNT($idusu){
        $cantidad = $this->query('SELECT COUNT(*) cantidad
                             FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                             INNER JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                             INNER JOIN cms_estados est ON est.id = pro.estados_id
                             WHERE usuarios_id  = '.$idusu.' ORDER BY pro.id DESC');
       return $cantidad->cantidad; 
    }
    
    public function getProyectosLimit($desde = 0,$hasta = 0){
        return $this->query('SELECT pro.id id,titulo,descripcion_corta,imagen_miniatura,cat.nombre nombrecateglria,porcentaje_voluntariado,porcentaje_especie,porcentaje_monetario,porcentaje_total,dias_para_cerrar,
                             ges.nombre fundacion,urgente,exitoso,estados_id,valormonetario,(SELECT COUNT(*) FROM cms_especie WHERE proyectos_id = pro.id) AS cantidadespecie
                             ,(SELECT COUNT(*) FROM cms_voluntariado WHERE proyectos_id = pro.id) AS cantidadvoluntariado
                             FROM cms_proyectos pro 
                             INNER JOIN cms_categorias cat ON pro.categorias_id = cat.id
                             INNER JOIN cms_gestores ges ON ges.proyectos_id = pro.id
                             WHERE estados_id = 1 OR estados_id = 3 ORDER BY pro.ordenhome DESC LIMIT '.$desde.',8');
    }
    
    public function getProyectosByEstado($idestado = 0){
        return $this->query('SELECT COUNT(*) as cantidad
                             FROM cms_proyectos pro LEFT JOIN cms_categorias cat ON pro.categorias_id = cat.id
                             WHERE estados_id = '.$idestado);
    }
    
    public function getProyectosByEstadoCron($idestado = 0){
        return $this->query('SELECT id,dias_para_cerrar,dias_cerrado
                             FROM cms_proyectos 
                             WHERE estados_id = '.$idestado);
    }
    
    public function saveProyectos($object = "") {
        $this->pais = $object['pais'];
        $this->departamento = $object['departamento'];
        $this->municipio = $object['municipio'];
        $this->usuarios_id = $object['usuarios_id'];
        return $this->save();
    }
    
    public function getLastId(){
        $usuario = $this->query('SELECT MAX(id) AS id FROM cms_proyectos');
        return $usuario->id;
    }
    
     public function getMaxOrden(){
        $usuario = $this->query('SELECT MAX(ordenhome) AS max FROM cms_proyectos');
        return ($usuario->max +1);
    }
    
    public function getCantidadVolByProyecto($idpro = 0){
        $voluntariados = $this->query('SELECT ROUND((SELECT COUNT(*) FROM cms_ayuda_voluntariado WHERE proyectos_id = '.$idpro.') * 100 / SUM(cantidad)) total FROM cms_voluntariado WHERE proyectos_id = '.$idpro);
        return $voluntariados->total;
    }
    public function getDatosAprobados($idpro = 0){
        $voluntariados = $this->query('SELECT pro.titulo titulo, ges.nombre fundacion, usu.email email FROM cms_proyectos pro INNER JOIN cms_gestores ges ON pro.id = ges.proyectos_id INNER JOIN cms_usuarios usu ON usu.id = pro.usuarios_id WHERE pro.id = '.$idpro);
        return $voluntariados;
    }

    public function getProyectosById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateProyectos($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    public function ActualizarTotalayudas($idpro = 0,$porcentaje = 0){
        $tot = $this->query('SELECT ROUND((porcentaje_voluntariado+porcentaje_monetario+porcentaje_especie)*100/'.$porcentaje.') total  FROM cms_proyectos WHERE id = '.$idpro);
        $datos = array(
                    'id' => $idpro,
                    'porcentaje_total' => $tot->total,
        );
        $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

