-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 01-10-2013 a las 10:33:59
-- Versión del servidor: 5.5.32-cll
-- Versión de PHP: 5.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `estrateg_pag`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_antecedentes`
--

CREATE TABLE IF NOT EXISTS `cms_antecedentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_antecedentes_cms_idiomas` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cms_antecedentes`
--

INSERT INTO `cms_antecedentes` (`id`, `idioma_id`, `titulo`, `imagen`) VALUES
(2, 1, 'Kimberly Clark', '3bcca63725a81f2fdb924b0b2ec1aa8b.png'),
(3, 1, 'Casa Luker ', '7f112d60032ee421eaba0c4b518f1a33.png'),
(4, 1, 'Colgate Palmolive', '87bf4a11b1746625beb9ed4571f1bf7f.png'),
(5, 1, 'Alimentos Polar', 'a6bd123b1c1d92236d80ed1a1c8512e6.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_banners`
--

CREATE TABLE IF NOT EXISTS `cms_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_banners_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `cms_banners`
--

INSERT INTO `cms_banners` (`id`, `idioma_id`, `imagen`, `link`) VALUES
(7, 2, 'a376ab5b85ec871c1708bb433274065e.jpg', ''),
(8, 1, '2fb06735f4320cd47b64e470f45ca2ec.jpg', 'http://www.estrategiasymercadeo.com'),
(9, 1, 'ed336ca6fcc84b5f1b8847ff4d1f0b51.jpg', 'http://www.estrategiasymercadeo.com'),
(10, 1, '71b4508c479ec2f585b7be877cff9df9.jpg', 'http://www.estrategiasymercadeo.com'),
(11, 1, 'd760e26f01824f64d700e2a1ee3d6f40.jpg', 'http://estrategiasymercadeo.com/'),
(13, 1, '9a66516c3cde09d771583196fd7d366b.jpg', 'http://www.estrategiasymercadeo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_campanas`
--

CREATE TABLE IF NOT EXISTS `cms_campanas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `antecedente_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_campanas_cms_antecedentes1` (`antecedente_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `cms_campanas`
--

INSERT INTO `cms_campanas` (`id`, `antecedente_id`, `titulo`, `texto`) VALUES
(6, 2, 'Regreso a Clases', '<div><div style="text-align: left;"><span style="font-size: 10pt;"><b>Actividad:</b> Regreso a Clases con Huggies, Juega y Gana</span></div><span style="font-size: 10pt;"><div style="text-align: left;"><span style="font-size: 10pt;"><b>Marca:</b> Huggies&nbsp;</span></div></span><span style="font-size: 10pt;"><div style="text-align: left;"><span style="font-size: 10pt;"><b>Canal:</b> Pañaleras</span></div></span><span style="font-size: 10pt;"><div style="text-align: left;"><span style="font-size: 10pt;"><b>Cubrimiento:</b> Bogotá, Eje Cafetero, Cali, Medellín, Bucaramanga, Cúcuta, Villavicencio , Neiva, Ibagué, Girardot, Fusa.</span></div></span></div><div style="font-weight: normal;"><br></div>\n\n<img src="http://dayone.ca/resource/logo-15.png" height="60px">\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_contactos`
--

CREATE TABLE IF NOT EXISTS `cms_contactos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_contactos_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_contactos`
--

INSERT INTO `cms_contactos` (`id`, `idioma_id`, `titulo`, `texto`, `imagen`) VALUES
(1, 1, 'Contactos', '<p><br>\n</p>\n<p><br>\n</p>', 'ade6bae2df6b5a16c507e965a7d15a70.png'),
(2, 2, 'Contact Us', '<br>', '95e686c628087467283fd25f9060cb01.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_directorios`
--

CREATE TABLE IF NOT EXISTS `cms_directorios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_directorios`
--

INSERT INTO `cms_directorios` (`id`, `ciudad`, `direccion`, `telefono`, `email`) VALUES
(1, 'Bogotá', 'Calle  97 No. 10-39 Of 306 \nEdificio Comercial 97', '(57+1) 7443925 ', 'info@estrategiasymercadeo.com'),
(2, 'Cali', 'Av.6 A Bis No. 35N-100 Of 604 \n', '(57+2) 6594084', 'info@estrategiasymercadeo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_groups`
--

CREATE TABLE IF NOT EXISTS `cms_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cms_groups`
--

INSERT INTO `cms_groups` (`id`, `name`, `description`) VALUES
(1, 'superadmin', 'Super Administrador'),
(2, 'admin', 'Administrador'),
(3, 'usuarios', 'Usuarios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_idiomas`
--

CREATE TABLE IF NOT EXISTS `cms_idiomas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_idiomas`
--

INSERT INTO `cms_idiomas` (`id`, `titulo`) VALUES
(1, 'Español'),
(2, 'Ingles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_imgcampanas`
--

CREATE TABLE IF NOT EXISTS `cms_imgcampanas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campana_id` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_imgcampanas_cms_campanas1` (`campana_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `cms_imgcampanas`
--

INSERT INTO `cms_imgcampanas` (`id`, `campana_id`, `imagen`) VALUES
(7, 6, 'fa21ad4b126ce0e905def6ea1ddaf0d9.JPG'),
(8, 6, '9c3cbf90d6eb1384504cd23ffd4194e7.JPG'),
(9, 6, '9ae21aebbabd946453de11aad3a28625.JPG'),
(10, 6, '5e2211dcbc4c4af0aabe28c5f3884b15.JPG');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_imgservicios`
--

CREATE TABLE IF NOT EXISTS `cms_imgservicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servicio_id` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_lineas`
--

CREATE TABLE IF NOT EXISTS `cms_lineas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `fecha` date NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `imagen_dos` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_login_attempts`
--

CREATE TABLE IF NOT EXISTS `cms_login_attempts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_logos`
--

CREATE TABLE IF NOT EXISTS `cms_logos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_logos_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `cms_logos`
--

INSERT INTO `cms_logos` (`id`, `idioma_id`, `imagen`, `link`) VALUES
(8, 2, '623fb33375a100ac7ac107ef1a27b6bf.png', 'http://www.google.com'),
(9, 2, 'bf6cb65071bf543fee1b1f997478324c.png', 'http://www.google.com'),
(10, 2, 'e195519859268f78a09bfb7a667a7fd1.png', 'http://www.google.com'),
(12, 1, '521077f1955c7de5377abda473184d37.png', 'http://www.google.com'),
(23, 1, 'f173936e9a6cbc0b90e6ce1d1313abd8.png', 'http://www.google.com'),
(24, 1, '0f00796cd37eb8d574b97ce980190eca.png', 'http://www.estrategiasymercadeo.com'),
(25, 1, '2d6907b1c233d599f4fed52d338d1c46.png', 'http://www.estrategiasymercadeo.com'),
(26, 1, 'f9043b72977bcc01e98d7fd789295994.png', 'http://www.estrategiasymercadeo.com'),
(27, 1, 'f7c603c7881f9707994be71be0118a80.png', 'http://www.estrategiasymercadeo.com'),
(28, 1, '6cf11b08d83ae2964c9a86713609237c.png', 'http://www.estrategiasymercadeo.com'),
(29, 1, '4acb06ad942fe1ac10f08051ea66cbfb.png', 'http://estrategiasymercadeo.com/home/index'),
(30, 1, 'cec6eae9112422737fa7bc0b60123913.png', 'http://estrategiasymercadeo.com/home/index');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menu`
--

CREATE TABLE IF NOT EXISTS `cms_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` text,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `cms_menu`
--

INSERT INTO `cms_menu` (`id`, `title`, `url`, `icon`) VALUES
(12, 'Destacados', 'cms/destacados', 'pictures_folder');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_newsletters`
--

CREATE TABLE IF NOT EXISTS `cms_newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cms_newsletters`
--

INSERT INTO `cms_newsletters` (`id`, `email`) VALUES
(2, 'jucasecu_92@hotmail.com'),
(3, 'danielvillareal@gmail.com'),
(4, '0'),
(5, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_puntos`
--

CREATE TABLE IF NOT EXISTS `cms_puntos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `latitud` varchar(100) NOT NULL,
  `longitud` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_puntos`
--

INSERT INTO `cms_puntos` (`id`, `titulo`, `latitud`, `longitud`) VALUES
(1, 'Oficina Bogotá', '4.624335270608115', '-74.07394409179688'),
(2, 'Oficina Cali', '3.4736963886287544', '-76.52681350708008');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_quienes`
--

CREATE TABLE IF NOT EXISTS `cms_quienes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_quienes_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_quienes`
--

INSERT INTO `cms_quienes` (`id`, `idioma_id`, `titulo`, `texto`, `imagen`) VALUES
(1, 1, 'Nosotros', '<div style="text-align: justify;">Estrategias y Mercadeo es una compañía Colombiana especializada que nació, creció y se ha desarrollado en el Canal Tradicional (TAT) por más de 18 años. &nbsp;Presta servicios integrales en estrategia, táctica, logística e implementación con cubrimiento nacional , buscando aumentar la Colocación y Rotación de &nbsp;P/S.</div><div style="text-align: justify;">&nbsp;</div><div style="text-align: justify;">El conocimiento del Canal (Tiendas ,Mayoristas y Distribuidores) y de el Shopper en el país nos permite diseñar e implementar campañas que permitan a su marca estar presente en el momento de verdad (compra) mejorando los resultados (Ventas) mediante una optima rotación (Consumidor).</div><div style="text-align: justify;">&nbsp;</div><div style="text-align: justify;">Nuestro equipo está liderado por coordinadores de campo (nomina directa y permanente) con gran experiencia, capacidad, &nbsp;habilidad, conocimiento en la ejecución y orientación de campañas que soportan los resultados y efectividad de las mismas.</div><div style="text-align: justify;">&nbsp;</div><div style="text-align: justify;">Entrega diaria, semanal y mensual de reportes de actividad que permitan detectar oportunidades y un seguimiento completo de la campaña.</div><div style="text-align: justify;">&nbsp;</div><div style="text-align: justify;">Pólizas de cumplimiento respaldan y ratifican el éxito de nuestro trabajo.</div><div><br></div>', '34faad76d14361dc31e9a10f90c7ef52.jpg'),
(2, 2, 'About Us', 'afasdf engdfghgfgfnvnb', '4fa47a6873d47353eddf7c61e91111e2.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_redes`
--

CREATE TABLE IF NOT EXISTS `cms_redes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `twitter` text NOT NULL,
  `facebook` text NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_redes`
--

INSERT INTO `cms_redes` (`id`, `twitter`, `facebook`, `email`) VALUES
(1, 'https://www.google.com.co/', 'http://www.facebook.com/estrategiasymercadeo', ' https://www.google.com/a/estrategiasymercadeo.com/ServiceLogin?service=mail&passive=true&rm=false&continue=http%3A%2F%2Fmail.google.com%2Fa%2Festrategiasymercadeo.com&ltmpl=default&ltmplcache=2&hl=es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_servicios`
--

CREATE TABLE IF NOT EXISTS `cms_servicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `link` text NOT NULL,
  `Contactos` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_servicios_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Volcado de datos para la tabla `cms_servicios`
--

INSERT INTO `cms_servicios` (`id`, `idioma_id`, `titulo`, `texto`, `imagen`, `link`, `Contactos`) VALUES
(7, 1, 'Ruta Ganadora', '<div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;"><b style="font-size: 10pt;"><font size="4">La Ruta Ganadora</font></b></div><div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;"><br></div><div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;">Campaña cooperativa para máximo (3) marcas de categorías diferentes que visita un número determinado de tiendas donde se genera siembra y rotación de producto mediante actividad promocional.</div><div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;"><br></div><div style="font-size: 10pt;"><div><font face="Arial, Verdana" size="2"><b>Objetivos</b>&nbsp;</font></div><div style="font-weight: normal;"><font face="Arial, Verdana" size="2"><br></font></div><div style="font-weight: normal;"><ul><li><span style="font-size: small;">Apoyar la labor de su fuerza de ventas y distribuidor (es) en los diferentes circuitos del país, mediante la siembra de producto.</span></li></ul></div><div style="font-weight: normal;"><font face="Arial, Verdana" size="2"><br></font></div><div style="font-weight: normal;"><ul><li><span style="font-size: small;">Impulsar la rotación del producto codificado para disipar los temores del tendero</span></li></ul></div><div style="font-weight: normal;"><font face="Arial, Verdana" size="2"><br></font></div><div style="font-weight: normal;"><ul><li><span style="font-size: small;">Generar bases de datos de las tiendas visitadas sean manejantes o no.</span></li></ul></div><div style="font-weight: normal;"><font face="Arial, Verdana" size="2"><br></font></div><div style="font-weight: normal;"><ul><li><span style="font-size: small;">Incentivar la fidelidad del consumidor mediante redención de empaques vacíos por premios.</span></li></ul></div></div><div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;"><br></div>\n\n', '07a34bd2a3f6ba094c9ca7e78e4f770e.png', '', '0'),
(8, 1, 'Aguinaldo de las Marcas ', 'Lorem ipsum prueba<div><br></div>', '3ded0f65cadb9dae90e9ae4e42bc32bd.png', '', '0'),
(9, 1, 'Estrategias & POP', 'asdf', '1f891304c7a44b1802e6d378b870edf1.png', '', 'a'),
(10, 1, 'SIM', 'asd', 'da596d4ed9d1fe00562a7a918114928d.png', '', 'asd'),
(13, 1, 'Investigaciones', 'aasd', 'a3c1fde01aa7e5b22ed76532b096f9a6.png', '', 'asd'),
(14, 1, 'Eventos', 'asd', 'd8fbab305c3296e7fa8a85ff4c89b85c.png', '', 'asd'),
(15, 1, 'Conciertos', 'asd', '05680692c28b528d8e5d84d6eb39eeb5.png', '', 'asd'),
(17, 1, 'Marketing Car', 'asad', '1107aba96c378ddca8819a90317d9480.png', '', 'asd'),
(18, 1, 'Mobiliario Corabastos', 'asd', '8e27d2e1686fab24f7f1cb244870bf0a.png', '', 'asd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_sessions`
--

CREATE TABLE IF NOT EXISTS `cms_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_sessions`
--

INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('006b9ebc03bfdab1652217b9e48fcaf9', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380320705, ''),
('00eb9b25411ce5ece51d854c08509111', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22', 1380579380, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('01b991e04cdc2c50f0c15c97a15dcdfa', '186.180.46.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380145797, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0200c0627479c60aee4ebce85ca8a595', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380320706, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('020bf1546b5a21edc1c5097477bbc4fd', '131.253.24.56', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380395261, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0257081fc2135e17f5cadb71ebba0a47', '186.115.58.26', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)', 1380638724, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('033ab5e1e935fc565476ba1e1a12b887', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376590576, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('03468a95949c5e804e69e804f92d913b', '131.253.24.81', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2)', 1380574704, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0397e8fb42e4280328d27ff304762b46', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380331443, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('03bbf7e51f825a0b18734875d72e7dcc', '131.253.36.197', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380300896, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('04729aed82b455e61abb55d83a663e1e', '131.253.24.143', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380113801, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('04c4d2f0c11359476b8a90ae0691613f', '131.253.24.55', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380462171, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('05471b64b755b0d53697153c9fc99da4', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379352692, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('059e86da712f94c2ae8f5a7820bd9e5a', '131.253.24.51', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380464908, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('05e11cf787d4f3f74c8fe08a7d015b76', '208.80.194.127', 'Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/5.0', 1380256843, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0643e90b908e852f55b293ab7e16abbb', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377119169, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('067329fc0b8a5e88a0f1d7ca011f9e8e', '131.253.24.97', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380472546, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('067aca693a9806cb7a34104480e48abf', '131.253.24.61', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380476732, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('06b174f4a7e0f25409da2a05a6fa83e5', '186.181.245.240', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380145619, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:1:"1";}'),
('070abc5dae3467dd8fa6cb4237861734', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377117417, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('07772469633e1a9d16b3fdb8bcb658b9', '131.253.24.150', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380463975, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('07a3cff66263f3ef08d653085835644a', '181.48.71.245', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380058188, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0837dab23677249758eb438002430858', '131.253.24.134', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380464081, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('087de05680963de293cb43b887c45bb1', '181.50.85.47', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380301603, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0914b975e6291170d91e004726350111', '131.253.26.254', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380560014, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('093ca8e58612a699e9c4e9fc1105ac63', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380576809, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('09d3424e170981a9337cf89e614a8846', '198.27.82.156', 'Mozilla/5.0 (compatible; meanpathbot/1.0; +http://www.meanpath.com/meanpathbot.html)', 1380088575, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0a1661c57d8da357257f2209b25fd16e', '131.253.24.89', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380391219, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0afd3990223e44c59c308d6cd4f268ae', '131.253.24.55', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380229750, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0b5cee064ba89be0df32712594f52061', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439443, ''),
('0b9b3691a6ee0055bae0970ebc065ad7', '157.56.93.93', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380451767, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0bee5cbc43464119cc30b1548119d18e', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36', 1373481507, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('0c8457740f1ce2c98a6c4bc7da4ffd36', '65.55.218.170', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;  WOW64;  Trident/5.0)', 1380586003, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0cd401767ef2575f832e47c26e46c730', '198.27.82.149', 'Mozilla/5.0 (compatible; meanpathbot/1.0; +http://www.meanpath.com/meanpathbot.html)', 1380096247, ''),
('0ce7a0e88279908243e859a07013ffdb', '131.253.24.61', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322)', 1380476603, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0d2822a5b99feb9bfd25d8b6e55440c4', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377198676, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('0d5c9d8ab8bd591cc3595400d11d883e', '65.52.100.214', '0', 1373508576, ''),
('0d694546f0dd8328b10b841d1c2a7442', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483342, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0dfc609c5f18a67b4efcfbd53e457ba0', '69.84.207.246', 'Mozilla/4.0 (compatible; MSIE 7.0;Windows NT 5.1;.NET CLR 1.1.4322;.NET CLR 2.0.50727;.NET CLR 3.0.04506.30) Lightspeeds', 1380502232, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0e087b290041324ae3cfd782e90019fd', '131.253.24.53', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380256609, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0ebb35543f5e10afe8ba7b5692af8ef0', '131.253.24.61', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380476096, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0edf0683d582fdb7ba37045c58c6b5e8', '131.253.24.49', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380300566, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('0f39b943e4c98e6b789ba0e08b9ee7ff', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377532966, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";}'),
('0ffe27480f9d04f0edb834794a3efc17', '131.253.24.68', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729)', 1380301026, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('115488eeb5b7165a06399a0ac5c382ce', '176.31.14.28', 'Opera/9.80 (Windows NT 5.1; U; en) Presto/2.9.168 Version/11.51', 1380265733, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1195ef106903606d439d37bb8a44e67b', '208.80.194.26', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/534.52.7 (KHTML, like Gecko) Version/5.1 Safari/534.50', 1380306405, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('11ba400a581649ac4fbd377440c9f7b8', '144.76.85.195', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380083748, ''),
('1202691701b63e50594dd3f280a4b4dd', '181.54.113.54', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36', 1373511291, ''),
('124c9e6963a3a79dbbf291b8c4faeb8a', '190.28.129.236', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380636705, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('12e5f44e49abf7cd5e07fc254abef401', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483321, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('137e3236da08cf29d3535ef1b9a6d367', '208.80.194.127', 'Mozilla/4.0(compatible;MSIE 7.0;Windows NT 6.1;SV1;.NET CLR 1.0.3705;.NET CLR 3.0.30618)', 1380066226, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1449311c73232a5e3628e08ad8c7eee3', '131.253.24.34', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380253885, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('14542e15fc0b76d499fa74e1f50fa6bc', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380155761, ''),
('14c5c4196af90c309d8fabfef2bd0a53', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378834699, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('14c7924348ec4a52664c04de0fa39d72', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380156751, ''),
('1637fb78be2d576245f76d55c791c94c', '190.60.239.146', 'Shockwave Flash', 1378848611, ''),
('1649ddd882ac26aeb4e394ff90720f73', '87.117.189.100', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)', 1380394663, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1662dbf42937b7a9c6c9f46251f2cd6d', '208.80.194.30', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.18) Gecko/20110614 Firefox/3.6.18', 1380534168, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('176a08a8664c5c46c38dd02ddd487000', '208.80.194.26', 'Mozilla/5.0 (Windows NT 5.1; U; en) Opera 8.01', 1380364765, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('177b91d1019b2c2833e5dd27c484edf5', '131.253.24.49', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380308655, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('17bb67067277b711f24000c821f27df4', '131.253.24.73', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380066920, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('17d8dc4f2093d3a6ac5a2defb19a7fe9', '65.55.218.169', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380514048, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('17ea61ceb43eeda1a0252bb279bbc605', '131.253.24.72', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380231330, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('181e6e00a9dadb9d18cc7a639c17cebf', '199.30.24.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b', 1380452700, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('186d1ca2f65c92aeefcd53efc5965f7d', '208.80.194.29', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)', 1380188943, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('191d7602be32ec9b0a8e428946652e02', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377199367, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('192604e0daf63e24507aa245c8ced1d9', '131.253.24.68', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607)', 1380473476, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('194037a4b2214affe26817cadee01ff0', '131.253.24.49', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380112948, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('194b073349dbbc5e5e4f94a8cbdce0e3', '131.253.24.39', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380311173, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('19a959df2889f359c51eb6918c8d646a', '131.253.24.101', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380427478, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('19fbd23993df408f0a41ab524a2567a2', '144.76.85.195', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380083749, ''),
('1a094448105853bf619408a4bbb71c38', '65.55.217.55', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380538631, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1bb8ebbe1634565014b057283925430b', '181.48.71.245', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378934809, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1bc331359e9f4d17d61e35d541270dfe', '131.253.24.45', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380559033, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1bc96706e18ece22d068654aaed67286', '131.253.24.58', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380121715, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1bef503a8441f1e6f52b46d9ff43cf78', '131.253.24.117', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b', 1380108822, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1c3eeb39f1018cdcffbd78226cb5571e', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380161458, ''),
('1d0de944574479b165fdc7de43389af8', '131.253.26.225', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380464360, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1f0890a8bf0a87763b204ba0ba70b899', '123.125.71.109', 'Mozilla/5.0 (Linux;u;Android 2.3.7;zh-cn;) AppleWebKit/533.1 (KHTML,like Gecko) Version/4.0 Mobile Safari/533.1 (compati', 1380170875, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('1f64e9aaeb34872d0e21dfdd0d5745b0', '131.253.24.89', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380402798, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('20de810bb7bcd62eff1bb637bdc08f61', '157.56.93.93', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380451679, ''),
('20e80c79d91e11f7a344d747e285504c', '144.76.71.140', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380514398, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2156a6dfdba64ba3162c228fad2b3c63', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380146550, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('21d947d201ad7222cc895047cf66e1f6', '181.135.215.106', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376345843, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:37:"gerencia.general@blpconstructores.com";s:8:"username";s:16:"Campo Elias Leal";s:5:"email";s:37:"gerencia.general@blpconstructores.com";s:7:"user_id";s:1:"7";s:14:"old_last_login";s:4:"2013";}'),
('233c8885677d1dc6161ef013f8f6fa4c', '157.55.34.93', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380622533, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('238d5e0d09fb44a121fe2e4025a87b0e', '180.76.5.169', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1380608397, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2413c26cc642b78601150ed91f46bdf6', '131.253.26.225', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380196115, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('244fefa9449607a56affc82221ffa697', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380316345, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('25a223d6bd24de26ad4e53583e97c557', '131.253.24.46', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380381300, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('26a30143f8b3460f2e7f81de8ed96eb4', '131.253.24.48', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380120270, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('26f9929c625e2bc759c98465d77ad499', '157.55.33.20', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380344910, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2756ccbe134d96e2d4ced56d122754db', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380580186, ''),
('277f0d73d8febef35186a6644e2e4ab5', '131.253.24.73', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380388946, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('27d3815f3846077a8e45ee80e0f104ee', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378736932, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('29071de0e4c163c9c6a90f93b9d59e56', '204.101.161.159', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4', 1380128259, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2926f5565bf5069fcd8f55064f8fed93', '186.103.122.10', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380495149, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2998c42fe28b0da5274ff1d0574499ce', '157.55.33.84', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380638887, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('29f5d7dbe7cdbd397dced27d5c22d49a', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380235942, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('29f78a3939c94ac325b1d35ab4759aaa', '131.253.26.231', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2)', 1380514645, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2a398db3b699542397b05789e93fda9a', '131.253.24.41', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380120882, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2a8d96566d5a4f3cb0c828022be02029', '131.253.24.59', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380466607, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2b3ff93cd921982f3f41d945f7284837', '186.98.190.144', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380146553, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2b7974213684644c8c524eaf22476c33', '157.55.35.85', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380600213, ''),
('2c7cdd93c03f940616a3286af766675a', '131.253.24.69', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322)', 1380299480, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2de3e7341fc8ff69df0deb900d8ef939', '181.48.71.245', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1380637263, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2e16506b71ceb497efdb436e478b9b1a', '131.253.24.94', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380113262, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2e1f66ee43273785249a54d28effe818', '157.55.33.20', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380248424, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2e24048da95b5df375a04b99dc92d69e', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378789335, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2e8302aa0d835b81703b2c0337b84d77', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377525111, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('2e96b0769ce6520c7f350fc1aab9f15d', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380155683, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2eb1358016d7792038ee6c8678c2a6ca', '190.25.134.232', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)', 1380586378, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2ebdaa796fd432d625aee42d89c2381b', '131.253.24.104', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380366038, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2ef8595bf086945c9e6a53bffe8af410', '131.253.24.43', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380381479, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2f29c832bf2afe03a0973a28739dfd1a', '131.253.26.227', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380366916, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2fbdf2fcb3287b199cb329c0cc106563', '200.118.73.32', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380052172, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2fc62e074c507d37ce7bf31bb292d617', '131.253.26.228', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380463877, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('307e794c62e4513429097432908df5ad', '210.14.133.202', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)', 1380520764, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('308493fbece54ee38c54c0e34a52bf1e', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378478751, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('30c65a3051b9e06200b2ca8c3b6bbb1c', '131.253.24.56', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380261619, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('30ce1f9dd1eb90367f113738bad781c1', '181.68.209.250', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375508411, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('3236273ea21e01246de83a83a657b22c', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377033792, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('32bef4c1ab9507b0ff7ce35797a36224', '65.55.218.168', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;  WOW64;  Trident/5.0)', 1380219188, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('350b6ae06c936f48c8829d2e1588a57e', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483357, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('353276f95af99f186870c554789d30eb', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380432888, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('36a4c6f4b9bf05e4e71f6a2956c59477', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380409829, ''),
('3728870bed8cd1ee8dd1ab281d11f6b4', '199.30.16.121', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b', 1380108818, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('37979e1343c0beb195a04d31bd2fbade', '157.55.32.109', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380068614, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('37c4245ecf585dd6feba322dc7f18fe2', '131.253.26.254', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380298130, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3836ea0edb8f7cd329efa4f6a226b2e1', '37.228.105.89', 'Opera/9.80 (J2ME/MIDP; Opera Mini/7.1.32249/31.1475; U; en) Presto/2.8.119 Version/11.10', 1380333735, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('383ca2f1df363a25b028c25e7f388183', '131.253.24.72', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380391279, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('384cd39e2f71e4557d9057a80f913324', '131.253.24.86', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380135460, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('38a7ddf504d35c61def72422bee3706c', '131.253.36.203', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380199420, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('38bcdd009ffb061e5bca48a63fa9aee0', '208.80.194.30', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; ru)', 1380222062, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('39f66beb78403b9ce50a37bc73d6426e', '131.253.24.32', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380157365, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3a5cc01ab8a49e51e13c95ce8f50b36e', '190.182.79.8', 'NokiaC3-00/5.0 (04.45) Profile/MIDP-2.1 Configuration/CLDC-1.1 Mozilla/5.0 AppleWebKit/420+ (KHTML, like Gecko) Safari/4', 1380571314, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3ab3ef04410499e11a5affd53145a209', '181.48.15.194', 'Mozilla/5.0 (Windows NT 5.1; rv:24.0) Gecko/20100101 Firefox/24.0', 1380122776, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3add52fe0074eaef466c69a1a660411f', '186.103.113.119', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380159067, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3b7a823ea379000172a26163173b89e7', '198.27.82.156', 'Mozilla/5.0 (compatible; meanpathbot/1.0; +http://www.meanpath.com/meanpathbot.html)', 1380088575, ''),
('3b922d3c56ca7814c266dcbde4adda5f', '199.21.99.84', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1380577919, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3d2c14d86b4b58909b7b3c86b3af128b', '199.30.16.114', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b', 1380108867, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3db6e0b0071d8bbc1990a916758c9908', '117.78.13.18', 'nutch-1.4/Nutch-1.4', 1380576435, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3db8b74679c326ef438efda20b94b8a3', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380531683, ''),
('3dbaced8dfa5caaf91edc645f8a57709', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377199484, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('3e519b9494e2cb99357bb9e86f19032e', '65.55.218.60', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380122839, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3e5543f7ffeac443d3acc9f9dbede08a', '131.253.24.55', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380261871, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3e817ed01afadbf894fe595fd47b7911', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483321, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3e897bd67fdbac8101735de3fea29baa', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377101112, 'a:5:{s:8:"identity";s:21:"prueba@imaginamos.com";s:8:"username";s:6:"Prueba";s:5:"email";s:21:"prueba@imaginamos.com";s:7:"user_id";s:1:"9";s:14:"old_last_login";s:4:"2013";}'),
('3f379e96dd14b2136715977e10d7e08b', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380367634, ''),
('3fb976d35fa9aa8e6976e50f092bde1f', '131.253.24.60', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380466374, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('3fe231d9dea7a13df46e86a67a14af00', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380430154, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('400c78900713f49fff0eaa302d016811', '131.253.24.32', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380210214, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('412104b73bbf9d476ef5138d1c552f65', '131.253.24.156', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380388490, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('421b3aee3a4c501748acc25b7ca78487', '65.55.217.53', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380169605, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4311817dc093a4e1aedc0dde49f87c89', '131.253.24.62', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380287020, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('440e4781b262103ae1b5e3d462856475', '199.21.99.84', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1380609610, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('442f0b369c895e76d5ccbc04ca36dcc4', '131.253.24.46', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380315326, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('460cb279de41235e909d2834d8a06985', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380432613, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('466b35d2bcdee6c64cd60f1446ee3734', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377208623, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('4671cbb1492e9df6d3276c28267fa795', '157.55.34.93', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380622290, ''),
('46c0de1362c154e27b1e8212758613da', '131.253.24.36', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380309528, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('47068793703109329154402ce47ef211', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380575416, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('475922322b6a4c5ce66f311f0e4c5876', '199.30.16.109', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b', 1380178768, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('47a4d8497156b1c03d63b05349ac5a9e', '131.253.24.61', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380476700, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('48622a241b4ef96125d8f60c5ae07ebb', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380489250, ''),
('48805ec7e175971aed21ca6f65eb35e7', '207.102.138.158', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows 98)', 1380128276, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('49280b7c6b134387e874e57ac524574f', '131.253.24.84', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380471280, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4965055a9bb40e27e8fde506db857d9f', '186.82.235.100', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0', 1380403864, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4adba7e5c17c422757b1301e655d8ea6', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376077379, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:21:"prueba@imaginamos.com";s:8:"username";s:17:"Prueba Imaginamos";s:5:"email";s:21:"prueba@imaginamos.com";s:7:"user_id";s:1:"6";s:14:"old_last_login";s:4:"2013";}'),
('4b6cdaee9890d07ffe8fad642d5e8a50', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380504728, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4b7a440507bfabf2b6ae059ed1cdfd5f', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380504729, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4b97c862e8e4c405087988c4b01fb7a4', '198.27.66.35', 'Mozilla/5.0 (compatible; meanpathbot/1.0; +http://www.meanpath.com/meanpathbot.html)', 1380092636, ''),
('4c24303661486ae7bc60c586ef397961', '157.55.35.85', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380580597, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4d1b0fc2c60b85312b593fd52d4f6b9e', '131.253.24.56', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380393041, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4d3be568cf1b36a6d87d470f8e5f8189', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379362508, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('4d5cd684b5af4c8740c8fccbaf584f8c', '131.253.24.102', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380336434, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4dccfb56c66468a7eb94fb60205e0586', '131.253.26.234', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1)', 1380464739, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4de1412cfe32e94ac4cde3222059bd5e', '131.253.24.68', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380361931, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4e4f451b44eafa8a2a69dd3b3c7e5785', '181.236.21.151', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380068004, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4e51548ddc5a9a25e1a8ccebf28d9ad7', '37.9.53.50', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows 98; Win 9x 4.90)', 1380051949, ''),
('4e7492ac172a3c9a822264503299e71e', '131.253.24.82', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380117350, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4ed89f2b0a4ccbd693055a92252816c9', '131.253.24.80', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380091794, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('4f72f6861d2a55c43be7675b6e2ccad0', '131.253.24.68', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380318823, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('50412ef7b388bbe7aa426e5a327bcad4', '131.253.26.230', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380118014, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5125bea56a4637d590acd7b9861f2d83', '190.242.126.59', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378942569, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('512ac4f145ceec50da8c3cd57a8c2b74', '131.253.24.101', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380394816, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('517db2959bd4580347dfa5fc32e54d0d', '94.120.254.79', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380087566, ''),
('51d6c3e11933efde01faa4e535ad63a0', '144.76.85.195', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380083745, ''),
('522c028a6ade6e1c9e804cdc5bdf0e66', '208.80.194.30', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_6) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.202 Safari/535.1', 1380515361, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('52ab11e472b16dcd557116e1a4ebfb6a', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380258596, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('532229848f9faaa0980877c1e04d54a8', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376593249, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('536cf2db1031ad58b3251a31572e10c7', '131.253.24.157', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380378753, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('549d9c37430008dd774aa2d010318241', '209.114.36.166', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows 98)', 1380518111, ''),
('54b2b61e7643ad9f254fbfe3600d0610', '179.14.238.69', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380251137, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5512374296039f1545ae4791dece473f', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380162037, ''),
('5540761329e9af147d68c5f3fcca8d3a', '131.253.26.246', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380119918, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('55f5fb0265afe7b18c509f488b8ca3a3', '131.253.24.67', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380567278, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('569c20ee3da462b7748bdb5867c30e59', '131.253.24.67', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380563698, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('569d2062d836697be01d599311b4a4f0', '131.253.24.61', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380556091, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('56a3822b63d5f107b5b394fb8764832f', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380641094, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('56ac92f2a3f360ec36cd5520b1c9a39d', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377012514, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('572b698b94a76e61e385a6124005b30e', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376077498, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('57a8e30c7f0be8bad006646bbc5cdcf9', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377199325, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('57c818bfed077e0b5513cef33c7bd93d', '202.46.57.147', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100101 Firefox/6.0.2', 1380405835, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('57ef6885f6ff5d59cdd14de08d68570e', '208.80.194.127', 'Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/7.0.1', 1380087988, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('580efdacdc82a6e1ecafbde9cc18a97f', '186.168.32.173', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378772730, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5832599e776ead0bf3274e016ff05ba5', '157.55.32.107', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380056294, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5866242aa1115a8c7285e18c4856a5ab', '181.68.209.250', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375504918, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('59152108ae60a6ac42a03892fa0941dd', '131.253.24.37', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380162447, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('59243b7f265be49fdb62a46bd2eb990b', '131.253.24.39', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380382840, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5985bffef4044f90157d35a9e937c77e', '144.76.85.195', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380083746, ''),
('59914e2dec56335e62a93428e2ef57b3', '186.80.87.225', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380126273, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5a3acab321f0d9d1a598d518d40191a3', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380255443, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5a810fb446ee996827552e83d2bb485e', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380211257, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5af24d8111d999344f954e0a34e6b8c1', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380314638, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5c24e3a7fcb2a2fa006e1aa648213fcb', '190.84.190.192', 'Mozilla/5.0 (Windows NT 5.1; rv:22.0) Gecko/20100101 Firefox/22.0', 1380332227, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5c2aecde67a7621e31fc207ceeb16de3', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380369735, ''),
('5dae07f56e3b5fd20d78fb9e20f01fac', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380316346, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5e01d0468041c8b2cf033c4ef645658f', '131.253.26.254', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380197479, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5e4dd23280793e18b937c7cd09c1eefc', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380084080, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5f0becbf20a6833be6c47df6aef7d8e6', '54.243.177.120', '0', 1380290180, ''),
('5f7caa2427823c653430bac9416b0c08', '131.253.38.67', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0)', 1380304192, ''),
('5f99b54ee7724cc1d42f3bdd42273d4f', '198.27.82.146', 'Mozilla/5.0 (compatible; meanpathbot/1.0; +http://www.meanpath.com/meanpathbot.html)', 1380076306, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5faf6ef35dbace9e971ae5a1ff967ef2', '131.253.24.49', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380232937, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5faff168fb7dcb0a5d100f757a6a14d4', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380155729, ''),
('607379c1b5add2b67e3ac375542510d5', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483321, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('613a5426f29cc83ea0ec9efd159fefe9', '131.253.24.35', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380130703, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('619eb359e089b6f2a28fdc37036955bf', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380367638, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('625b00587469b142376113e45ad8c5a9', '131.253.24.73', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380639594, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}');
INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('63aa9bd8a4818e2102ada7f5a3af5262', '199.21.99.84', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1380387992, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('641108a6a563be38adb94ca2748f83bd', '131.253.24.80', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380640990, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('641fae7c97da0227c74e2f1d3e15937a', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380409829, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('64282ac66a5693c1cc9f06a05b627554', '131.253.24.35', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607)', 1380367493, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('6489493ce588b87dfbc928df4321fa5f', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483398, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('64d2755e53e91b43ba6e5438562ba6c8', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378830710, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('6559b732e3575a8e4e0b0fc8b0b0c448', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377528052, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('65da542c8adc8b56a8906179f58bab0b', '131.253.24.61', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380476740, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('65eb5eab1b5e21715c114b6ab8c948c1', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380316345, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('65ef5bf637e8828da92964c4be118a37', '131.253.24.42', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380317010, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('66718f2764ca88b587047f266f52d155', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380157789, ''),
('66950fe47b219d83ae05bc7f9fbd4aa0', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377525217, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('682ef7d489314fec71a34a7fe3e5419b', '131.253.24.64', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380469041, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('68937e595aaf60ddc5c5533ccf9848d7', '131.253.26.251', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380463970, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('6a92530c70bb17e5f13fcf2f00934d9f', '208.80.194.28', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US) Gecko/20101012 Firefox/3.6.11', 1380039272, ''),
('6ab40f0b6ea07c04046db48ee15e124e', '131.253.24.33', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380303613, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('6abbbbe9083ac79aff8c5fed280fbbff', '131.253.24.68', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380466612, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('6bcbb2d3593b9fe27602fd7d04d25b29', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380155683, ''),
('6bfac1e6c2e8add1b5d04314a1cc0df2', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380052272, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('6c55cddec6f2cceab46f3fce8ac4029e', '131.253.24.101', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380399664, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('6c8940b48d5289d68e9318ce9da4f45d', '208.80.194.26', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.187 Safari/535.1', 1380181521, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('6cc697f54842b2b3f8a7dc63b2318fe6', '131.253.24.59', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380471501, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('6d7dc875bcac297fef5d21ba7fc1a818', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380155763, ''),
('6d86348e0eae8e46c0cdc87aba8ab11f', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439441, ''),
('6e5df7c72c431fc6c072c8d0b30f961f', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375742013, 'a:5:{s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";}'),
('6fcc526377b1c265f96ddbf171833cb2', '196.20.73.110', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380261518, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('70746272bf483c99e88843a666f04350', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377028848, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('707c0ff42a75c124cf99583ecba894d9', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378829750, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('71d1fe33c8a6936a17e5b0801d13b13d', '208.80.194.30', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.7) Gecko/20100713 Firefox/3.6.7', 1380122730, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('71f561e56eb73a24d827e614534379a0', '131.253.24.61', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380476847, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7249424153fd5102944a68fb665ae52c', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380583182, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('732730e078854ebfab5fece2e7d1f8fa', '131.253.24.130', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380306153, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('744c02639d845d7544d1f0324ddc00a8', '199.21.99.84', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1380609574, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('74780381b493f870c1301e4b08b432b0', '208.80.194.127', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.100 Safari/534.30', 1380361322, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('749e3233c6b8388651d8e188394e9a93', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1378819629, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('755d1b6c6802cbc4a75406f79db24a9c', '190.24.187.240', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.59.8 (KHTML, like Gecko) Version/5.1.9 Safari/534.59.8', 1378760604, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:1:"1";}'),
('75749b01d4e127b23fabf42975a6cb9b', '131.253.24.43', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380337484, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('767281c8015a844829378f70ad09aaf5', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380156932, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";b:0;}'),
('77a8a387601c708374470bd6e0d95a9e', '208.80.194.127', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.23) Gecko/20110920 Firefox/3.6.23', 1380063065, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('77b7efb415d549a70fa3ba4fba902344', '131.253.24.84', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380466634, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('783681967d1d41a0cc0edf4f9b40524f', '207.102.138.158', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7', 1380128281, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('797d049b4b5fd374a5e7fd46041e9581', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379352693, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('7a396216f92b754cc36538fedcd50763', '181.48.71.245', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378747627, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7a77cd0c58b7f34bc71532e5632d86ba', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380323875, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7b71d6c3d9a64f6453677d94270b3a50', '208.80.194.26', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.30 (KHTML, like Gecko) Ubuntu/11.04 Chromium/12.0.742.112 Chrome/12.0.74', 1380075941, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7b72c4a579e4e6e2ff8905dad782e0d0', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380250623, ''),
('7bd9ef906da393a1074ae928468bcfbd', '131.253.24.73', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380303150, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7c1f95cc944f6717a9d0a9f4cf04413f', '190.28.129.236', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380552830, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7c3c838a9528b342c1abb9edd60889c0', '131.253.24.34', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380260988, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7d3d470b3902cb4008beb8da1d6d1455', '181.192.163.132', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0 AlexaToolbar/alxf-2.19', 1380554648, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7d6a09bb968557eb15d6c359e6b369e7', '181.48.71.245', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1380574188, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7e33cc1d1e468b06a81bda10d9450b6a', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1378853115, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('7e897024e9d7af41da4b99c79e538dfd', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380638530, 'a:1:{s:4:"lang";s:1:"1";}'),
('7eb5de361887736284ede89adffab5e8', '131.253.24.35', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325)', 1380260288, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7fbb826e5c469eb2b4143f04f9563f34', '131.253.24.53', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380199420, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('80c0f33f9b02412429fd76e4fdcf2dbe', '190.147.153.73', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380583461, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:1:"1";}'),
('80c9c57ddf1ef0bd4aae814672c1aa08', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439443, ''),
('812a20ba6ce24e59c350dcf27b6d256b', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1378746845, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('814b1069fec5e8c198e88c6b43cdc413', '207.102.138.158', 'Browserlet', 1380128265, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('818bbd2e770694371d8a7df7ab94abb1', '186.168.32.173', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378788637, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('81c9dd97c32afbea03bbb8954d02ddff', '208.80.194.28', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_6) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.202 Safari/535.1', 1380600213, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('81e35197248d602e6c55cda91079b827', '131.253.24.35', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380269455, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('824b8ac50f37f6da76b74243e882a781', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727)', 1380432622, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('826bd62d2adb485c427b9b759a5fba8d', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380303511, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('82f87db33c0d448e8247826b070bf103', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380527002, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('83163ccaa237ead8eb9d346c41214fc2', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380638822, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('84fab7291d9124dfc8d626dba92b113b', '181.48.71.245', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1379362730, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"2";}'),
('850bc1847ee4723649f4142cef907b35', '208.80.194.28', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2.11) Gecko/20101013 Ubuntu/10.04 (lucid) Firefox/3.6.11', 1380321718, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('851c9006958f039b7b4ff885dc503162', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380607610, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('862403c4e54deb9d96de7a0af8ac360d', '208.80.194.32', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/4.0)', 1380413137, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('86a356d5d5f0ea708b1ecc95906531c4', '208.80.194.35', 'Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/7.0.1', 1380107081, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('86c1831b67b3a573e1fc1b51829e23e1', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380312198, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('874f22ebb22f669a90d8f09514c94311', '131.253.24.72', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380358785, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('87fc6551124a1f2f2d8211f596108dd3', '131.253.24.35', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380403877, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('880e5e898fa64d7b8bad0d8e999fd912', '131.253.24.59', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380059721, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8823b473b51e4e417df3cb322586dcc5', '157.55.32.189', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380130921, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('88b0c2b57f6e9cf648d16a5b7c201ccc', '131.253.38.67', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0)', 1380304192, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8a31d9e98b75b793fd8e88b137e7c3cd', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380160743, ''),
('8add041a739724515644920d0e159512', '186.98.190.144', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380146551, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8b69974d3340bb5ba6fa13432a317977', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380159493, ''),
('8b9eb6e14ec2e0d5301479e834f2eb44', '208.80.194.30', 'Mozilla/5.0 (Windows NT 5.1; U; en) Opera 8.01', 1380162402, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8bcb8611d2e6e41cfaccf757612cc26e', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380160047, ''),
('8c3c84311ec395c2fc8ccd73bf3a2001', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1379106819, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8c4ded8dcbb31975177eb2e314eb6c8d', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380155727, ''),
('8d830a02f11eff60c35290d397b688a5', '131.253.24.76', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380462584, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8d86f9ac8e86480f9868b910b95718f5', '144.76.85.195', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380083728, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8eb8f21f379ea1a8e2eee91d1f813dee', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378398241, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('8f4a2f4a8f7d5669481eaa45f31885d8', '186.181.245.240', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380130313, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8f6bffe6b096319ab138a964b6d4df3f', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375743078, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('8f90082f1facb27b058af62f61e3f641', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439442, ''),
('8fcc2d269904c683564eeda30818ab53', '190.147.153.73', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380146551, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8fd7b2594661009a83b75cb843ff7716', '98.137.206.90', 'Mozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp) NOT Firefox/3.5', 1380429534, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9012cbc7cd697c3bd11bc0261db6f735', '199.30.16.109', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b', 1380178768, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('903678f0b89996d74c3d7d1d76a37e57', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380029347, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('90c8764a033bf27358a7d5ac6f16a9bd', '131.253.24.58', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325)', 1380118937, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('91946b16feaad34f923d61bb7908067a', '131.253.24.43', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380328849, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('923e74c0ff0e3aa0c17b9bc2da2307c5', '131.253.24.102', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380641039, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('92714ef466deabc9202e310440eee86b', '186.30.75.235', 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:21.0) Gecko/20100101 Firefox/21.0', 1380290080, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('92885a1ea2e3a8b2db9c0fa3913b080f', '131.253.24.77', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380209737, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('934b580d236681e1975dc2a8c3f566e3', '186.29.119.248', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.59.8 (KHTML, like Gecko) Version/5.1.9 Safari/534.59.8', 1376325122, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:21:"prueba@imaginamos.com";s:8:"username";s:6:"Prueba";s:5:"email";s:21:"prueba@imaginamos.com";s:7:"user_id";s:1:"9";s:14:"old_last_login";s:4:"2013";}'),
('938b8bbaa5cdd7413202f340768a0b27', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380372743, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('944d5ff0eabe6cbef449db48a89c616c', '190.147.153.73', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380238443, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('95108fc7ffc969c5ce7a8e19225e3448', '199.21.99.84', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1380220769, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('95ee19a600b0ca5ef7701ad75cee61ca', '131.253.24.51', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325)', 1380462054, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('97af7ca9bfa48a50592a61b87725aa8c', '208.80.194.127', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US) Gecko/20101012 Firefox/3.6.11', 1380442317, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('97e5bdc733a903bd3be50fc54a0afca1', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380354690, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('97ee21ba52440411508877e916ad533b', '14.98.72.142', 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0', 1380264666, ''),
('98043d8d2b5d61f515925ccd4d11a6e1', '131.253.24.141', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380463548, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9805cf9fe7df625087a95235b7e65e55', '199.21.99.84', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1380220779, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('98ab739eae834aa5050be631bfac79ce', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379361899, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('99bbdd6fc4dc8fd5b5e3389a117f67ec', '131.253.24.75', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380206141, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9a50096f27009d1cdd0cf1cc20460ac3', '123.125.71.19', 'Mozilla/5.0 (Linux;u;Android 2.3.7;zh-cn;) AppleWebKit/533.1 (KHTML,like Gecko) Version/4.0 Mobile Safari/533.1 (compati', 1380273649, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9a7adb387ac56a22282aaba727f42cec', '65.55.218.168', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;  WOW64;  Trident/5.0)', 1380067269, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9a82f0ce99b090a623ccd87edaa4291d', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1378839642, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9a87d65f7302dee325202d3249793aa1', '181.48.71.245', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1379106820, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9afaee7e1aeed0bf5918b61f8b2d8720', '131.253.26.246', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380306042, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9b1bc13b7747fcf6cb3136ad148e880d', '131.253.24.56', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380404793, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9b561726a506a8756e471b1b3dfb63c6', '131.253.24.97', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)', 1380201545, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9b7aaab06770156fe380f1f0e8e8cf3d', '131.253.38.67', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0)', 1380304192, ''),
('9bd75e2a88bc2b50f6a4e7bc2521f74d', '66.96.183.7', '0', 1380291903, ''),
('9bd908b148cc0ebc46cf3224abf6faba', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439443, ''),
('9c0b74f804b902414af01f9d4705e0df', '131.253.24.134', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380303005, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9c84eeb1d7df40155245a26964e8178a', '70.40.220.102', '0', 1380293736, ''),
('9ca1c5e89fd5a438faa849ba58e0a5da', '208.80.194.29', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.100 Safari/534.30', 1380378891, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9d5fba5b2e379a2a65794bc06a950ffa', '208.80.194.127', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50', 1380295548, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9f3ddf6265b5cc4e00b1852ef5e9d11b', '65.55.218.169', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380386590, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9f878d534019f4ee63bd9d1fe024015c', '131.253.24.36', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380325021, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9fc4b4c879a77fbdb778c568337a8724', '131.253.24.35', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380250385, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9fcd782ae6d23952d348b6ff5187add8', '208.80.194.127', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_6) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.202 Safari/535.1', 1380147133, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9fec3877f795755264bf419959e6de9c', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380527001, ''),
('a056cd0a1c3319d40415211a033332f4', '131.253.24.151', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380463768, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a0a43d1be5665f69afc0dc5a9eb8a960', '188.143.234.127', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)', 1380520779, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a0e0bf29e3dc8bed240a2361ef4f47c8', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380316345, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a1477b7c32e6a7f21f2cbc63e099d498', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1380486892, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a201b0141d4f1459efe2033f584a4428', '131.253.24.55', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380414330, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a231b4049c71db90ea0929ba48bfe507', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22', 1380052443, 'a:7:{s:9:"user_data";s:0:"";s:4:"lang";i:1;s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";}'),
('a30cf356a5fd0eaafaae45317be015dd', '131.253.26.242', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380555700, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a34289d25f8182e7265ad2c5d01bda01', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378841680, 'a:7:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";s:4:"lang";i:1;}'),
('a3efdadf8f9a20ec71a46510c524873a', '131.253.24.43', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380387640, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a4094aa33ff736fa24d08fca1b5324f0', '190.24.67.76', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)', 1380302756, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a42b7d1b7ddfa836aea34c5654862427', '131.253.24.35', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380640580, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a441a376508c4ed0822916a0c97a8b57', '194.187.148.223', 'Mozilla/5.0 (Windows NT 6.1; rv:19.0) Gecko/20100101 Firefox/19.0', 1380487856, ''),
('a46ef5dd3f62a2b3d07dfc60694883e6', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483321, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a4be13cad991e8e094f890a8437b2baf', '157.55.35.85', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380600344, ''),
('a56f3cf479549d981da64e5c5844e7ed', '208.80.194.34', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0)', 1380274254, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a5b3639b9c483702cab584a4d67567ee', '199.21.99.84', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1380577894, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a5df82169309044afb1dcc973c27215c', '199.85.212.225', '0', 1380295037, ''),
('a68db0b1760af068c34bb629942a04ec', '181.69.217.146', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377143187, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('a760999e279dce4e1535380127de050d', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380537968, ''),
('a78c4782f7573800543ee2f73ee893d7', '176.31.14.28', 'Opera/9.80 (X11; Linux i686; U; en) Presto/2.7.62 Version/11.00Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; en-US) ', 1380265735, ''),
('a802081998bc27ff08193fcd6e655e68', '69.84.207.246', 'Mozilla/4.0 (compatible; MSIE 7.0;Windows NT 5.1;.NET CLR 1.1.4322;.NET CLR 2.0.50727;.NET CLR 3.0.04506.30) Lightspeeds', 1380497674, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a87b5412422e745b8ca4ace540eff5f6', '131.253.24.41', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380270817, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a92b88ff8a68ba378ebc883a03097003', '131.253.24.53', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380183379, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ab5721f6db24bae80b59d99733de9700', '131.253.24.38', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380366278, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ab68812013cbb93ebc0f6162c316cb06', '131.253.24.40', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380256915, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ac942fb780178075facfc7dd56a149dd', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379352693, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('ad3d6d2bb0c3783aa110535fdd760108', '198.27.82.149', 'Mozilla/5.0 (compatible; meanpathbot/1.0; +http://www.meanpath.com/meanpathbot.html)', 1380096247, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ad586722fa8c5e75006f69af6544daa6', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439444, ''),
('ad608b239928333358f6c8ac2fab9387', '78.129.250.17', 'Mozilla/5.0 (compatible; aiHitBot/2.7; +http://www.aihit.com/)', 1380414790, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('adf25c427d1684b13ce8bc849b876100', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1378751130, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('aec605889706d851655f91666c0186c7', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1378500741, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('aecd8cdadd4993bea0a06fe57c8da7f7', '144.76.85.195', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380083750, ''),
('af5bd7fe76129654e108a52e7c5026ef', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380480049, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b068e1face5d289b8e82d9b758cb133c', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377198730, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('b078abc1862139997772b5a4ab7da68e', '144.76.85.195', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380083751, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b13cd6f3a037398de7818153898482d9', '190.66.80.112', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375940978, ''),
('b1411c7d1aa767864bd2664961ab03b5', '176.31.14.28', 'Lynx/2.8.6rel.4 libwww-FM/2.14 SSL-MM/1.4.1 OpenSSL/0.9.8g', 1380265734, ''),
('b1b17e1b095db8e5e8f0989968b62c98', '131.253.24.92', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380206391, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b1cef98de9c30439e4db71fed1e8c94e', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483377, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b2e4000b2e45a9e7935fbafd305653d5', '194.187.148.223', 'Mozilla/5.0 (Windows NT 6.1; rv:19.0) Gecko/20100101 Firefox/19.0', 1380487857, ''),
('b35674f3c06d4f44dadfa45dfbb12fc3', '131.253.24.128', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380464254, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b3b81a9922c3940b2feb2b5ea33074b1', '131.253.24.81', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2)', 1380246372, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b3cdeee87a3dc92f1b2f841e713cefb7', '131.253.24.64', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380463954, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b46c54a0ebd7a272c44eb09122dde29c', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483324, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b62524f81355a0ca32d68ca11e37ca1f', '131.253.26.231', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380117089, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b62ceddec383b62a451b160e945dc406', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380369772, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b69a4ca7505f23ea28b43539eadc67e1', '131.253.24.94', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1)', 1380564138, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b705bd75848bfd9475040c65a7698351', '208.80.194.26', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.68 Safari/534.24', 1380237853, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b73de278ad87f5db36909a3e313abd4e', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22', 1379968374, 'a:7:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";s:4:"lang";s:1:"1";}'),
('b7b2c16f83485c2e67da2d7067f24c48', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439444, ''),
('b876b0e17186cf30b101b46aa2767bc3', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377115918, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('b8f4c06b5fed57db2df52fecd9fd42ea', '181.236.21.151', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380252406, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b961783c3d570124d7fc1e845d76e5be', '131.253.26.242', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607)', 1380300253, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('b9bb61c98c5454899fa7968c251a20bd', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380374905, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ba404f03bd8511c22dc18705a57b7347', '190.85.98.185', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380053406, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ba494b9c997c6f2d18f9035213862f84', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376324643, ''),
('ba8e9f9ada8d3f290ec8c1f1bc371d79', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377116463, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('bac9ce37173788e90183197f37f1b91a', '96.227.124.38', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)', 1380162218, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('bace01c34536934b100bea9385045f6f', '131.253.24.35', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380381653, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('bc1fcb90f75e2e136e653d5c2543ba62', '131.253.24.51', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325)', 1380470584, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('bc8c5534e8d36671ee1a8b9d1fe86864', '208.80.194.127', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.7) Gecko/20100713 Firefox/3.6.7', 1380050168, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('bcb5c31a5f5aeeaba12631f9aa8df4cc', '131.253.24.95', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380475751, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('bd5fd34082380895abdacfbc030fd35a', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380029420, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"2";}'),
('bd7f7f45d1d0d544ddf48472f71a2100', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380374847, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('be3c70cbd6a6011d588c8aa1a2b16d85', '131.253.26.242', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727)', 1380560426, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('bee5647abe1bf9d8e5dc0fe005819150', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377204305, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('bef9d45f6e0c6626ced634b24a279911', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378419902, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('bfd930a954c95444766258c54622c691', '131.253.24.56', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729)', 1380399676, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c012c64f25533f92ca353cfa43cfcd2a', '181.130.216.13', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11', 1380165986, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c049d77bdf33868262d39cecca5c8f66', '185.10.104.131', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1380215087, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c0b9980527064b3b3541e1df9f656f7f', '186.115.95.191', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380056708, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c0de4236b44116b12f3b6296a2dfe66e', '131.253.24.63', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380577110, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c14b4ab88dfb4dcbe05942768fe8e837', '131.253.26.253', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727)', 1380196788, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c2099d033cfca8676de4181f923c52d9', '131.253.24.134', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380304943, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c326f8e31d81b4ce8e44e293dd4b151a', '208.80.194.127', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0)', 1380131805, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c3d763daed6424c37977ce82a6ebc2eb', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377119317, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('c41f8ca01d33c750856ed59d9dc122b4', '181.159.10.148', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380574713, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c46b347583d4bb674256bc721be8456b', '131.253.24.58', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380081129, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c482f5538c9bc88814697995f8777024', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439442, ''),
('c57a5c1e66b1a849bb1be2a99a6838e6', '131.253.24.58', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380227137, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c5ba549890fe501e16b8704f0a9bd2ad', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380337337, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c5f27c10dc46d38044ed1587f731386d', '190.147.153.73', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B350', 1379279292, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c6a133b7d7078a1eccd30f2926e72f23', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439445, ''),
('c753e1c72d7eb75388ebfee3e1848aa0', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377209349, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('c756a8e4d5f648e0424939520ddbc05c', '131.253.24.92', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380232255, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c8bdb52d5ee30b789d655d29fcbbc303', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376593249, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('c95c79bb8fd77a44d281bb4f194117d6', '198.27.82.146', 'Mozilla/5.0 (compatible; meanpathbot/1.0; +http://www.meanpath.com/meanpathbot.html)', 1380076306, ''),
('c9a9c7b815944ee3f137de350d44d15a', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377119443, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('c9e622825b6f7913c7b9d8aa05ded8db', '198.27.80.33', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)', 1380493284, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ca26c35d1a6ec0b68bce671e35d45917', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380357304, ''),
('cacfddf83f13490639d313ba5278c6e3', '199.21.99.84', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1380388003, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('cadc339eb35afb8065596137b62bef06', '176.31.14.28', 'Mozilla/5.0 (X11; U; FreeBSD amd64; en-us) AppleWebKit/531.2+ (KHTML, like Gecko) Safari/531.2+ Epiphany/2.30.0', 1380265736, ''),
('cb48eddf12c17a6d6489d2f9dad43b9f', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439443, ''),
('cb79659f6bfb4eedbc6097bacb587ad2', '131.253.24.128', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380464255, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('cc61a15f75dbcbb319a401eb752e7e36', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1378483413, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}');
INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('cc89653a4804df3f0d4931b835525a14', '208.80.194.26', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.91 Safari/534.30', 1380280145, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ce1651e6f708687273694d6b90d0d742', '198.27.66.35', 'Mozilla/5.0 (compatible; meanpathbot/1.0; +http://www.meanpath.com/meanpathbot.html)', 1380092636, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ce1e2cc62776861e87441ab7dd628a9e', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380545610, ''),
('cf2e542d78bc7cb1933fc36cef3969e3', '131.253.24.45', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380557271, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('cf65d24ebc90dc6b2e1982305f833c6b', '131.253.24.54', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380126950, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d084c40da530ef8a3bb275dc0d64a327', '190.147.153.73', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1379631965, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d262e59a19bfa2eab75f8b385b36dc65', '181.48.71.245', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1379955357, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d28755a52358b939beeb1d06fb49ac5d', '131.253.24.56', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380380184, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d3345e638e57ae6c9c496be4cd4db9a9', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)', 1380057115, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d334c6ed9f8709f556858bc6bbf9934f', '144.76.85.195', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380083730, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d42dfd124c356ea6a52c02c0a072b418', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729)', 1380374203, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d48507d3c841281255f77dee54e9f1b0', '208.80.194.26', 'Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/4.0.1', 1380397452, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d4c2d2ae505284cc26a34f157e69de12', '144.76.85.195', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380083749, ''),
('d4e0f336b0b7482d3d9f213a5faf200f', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380375012, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d4e65f0180cdbb9b133d9154db63d1c0', '131.253.24.76', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380640764, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d519f8b3c878a5506bec12d6925ff257', '198.27.80.33', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)', 1380262802, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d54e0ea3e6d961dec17e8b035bf6e847', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379350853, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('d6307c05119ae9f80ac95c80bca42756', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378846765, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"2";}'),
('d71cdd43b4592b8dd7ed6b17cac48c87', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377120361, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('d7329f284a4824b6ced395ac7617b622', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377118779, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('d7c8672351ec82a58ef65f57d4a36595', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483407, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d8c02573ad96bd787b69053db9396237', '208.80.194.127', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.68 Safari/534.24', 1380336546, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d8cbec7390396a5c46add0bfcc981534', '194.187.148.223', 'Mozilla/5.0 (Windows NT 6.1; rv:19.0) Gecko/20100101 Firefox/19.0', 1380487857, ''),
('d8ee224335d273d094d93a2c82d942d5', '131.253.24.104', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380640366, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d952d0680732a48bff70f4d5663340d1', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380432703, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d9814884ce1ca562308e37919c129ffc', '173.193.219.168', 'Aboundex/0.3 (http://www.aboundex.com/crawler/)', 1380483422, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d9d2a5d1f0549dedcb7b1cd94dab19df', '208.80.194.27', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)', 1380430510, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d9de851ccacbb3a75151dd9a4d909ff9', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380432611, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('daddff7621e936c63fbb2078ba99b9ba', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0', 1378848592, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('db0b5c3ee6466a10141d33308b89927b', '131.253.24.59', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;  ', 1380640639, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('db5eb810796603dcf587b6ff6d73fca6', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380227731, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('dbb8cdb2905d2351707d394e768ee205', '208.80.194.26', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.68 Safari/534.24', 1380617466, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('dbc1a592f3ef65ed8f8b5f1086ca3a7e', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380484152, ''),
('dbf2d3ac964757591cee9d4b5fe5b5aa', '181.69.217.146', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377145681, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('dc20c12f9c0dcf1aa2bd42229f1a9b5f', '188.143.232.103', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1380439444, ''),
('dc4fa3149e8cc6ac2b9fa1b8066a1cc3', '131.253.26.227', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380115463, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('dd7f7d5bddb4e83a0e5260e1d02f31f7', '131.253.24.53', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380361333, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('de0276ff4e0fdf778ad2bae25cbd0f42', '131.253.24.135', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380464799, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('de79f04a6e1cf765e2f1d1fee61702e8', '207.102.138.158', 'Browserlet', 1380128269, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('de7c57ccf2c055c5928f1250c898d294', '131.253.24.105', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380462048, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('df330fa4a693c82649f0ff42a93dc4f3', '131.253.24.46', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380405141, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('df592c497b861d9bce924d23628c7026', '131.253.24.96', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380560098, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('dfa32642ec287eae7d8880091a2c1a3f', '131.253.24.45', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380059349, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('e5262946aee9089b0441232ba6d2fec2', '208.80.194.127', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.23) Gecko/20110920 Firefox/3.6.23', 1380203939, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('e5e5687dfb19db23214b1a92662aa354', '207.102.138.158', 'Browserlet', 1380128261, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('e64f683f74d40beef5b9a70310d4619f', '131.253.24.45', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380566472, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('e6aa4521bd54ff491db72f0d7970180c', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380363141, ''),
('e7038a99f48778abd04d5e77594ab5c9', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380433549, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('e78f577145e1f33e5f970fea256f86fd', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377119785, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('e899cd239c62a3b5544bc82928573954', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380406815, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('e8b013a30bb41cf9b8aa7d98c58a7618', '181.192.157.84', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380226787, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('e99fb418e28c76ea18c0bdb1cd94e03d', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380082238, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('e9d109eeaf089447f2a4d37e2b4e6e94', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380617494, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ec23ea218c47dbac5477eef1afa8af06', '131.253.24.97', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380413456, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ec306360d694369e78d1bacb1d06bb66', '131.253.24.84', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322)', 1380099073, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ecb8b8e0a513ec6829f046dd928217a8', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380328548, ''),
('ecce03462ed330367abe673ef1186b52', '80.77.95.46', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.6) Gecko/20100625 Firefox/3.6.6', 1380580391, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ecf116ebbbb558999da7c286654e5c47', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1380257617, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('edef81a63af22280ae774e52021b8e94', '208.115.113.88', 'Mozilla/5.0 (compatible; Ezooms/1.0; ezooms.bot@gmail.com)', 1380053723, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('edf0b5f20f1a0f3941be284959b6fbbd', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378873958, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ee2d0a271bcd6b80f4ce68f2992388eb', '178.255.87.242', 'Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20100101 Firefox/15.0.1', 1380578177, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ee485c979180410a7481c7d483677a5d', '131.253.24.140', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380197850, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ee9472bd18c546721083c94017b4b65d', '190.147.153.73', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1380234925, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ef047160bc22aedc5507441822579259', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380495766, ''),
('ef266789a4d7bed369506ab079154743', '131.253.24.72', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380261116, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ef4876e8b0c86b5df99e2fb204cc6567', '65.55.218.169', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380128579, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('ef781ac5d3882ca7f7b14e9860206cbb', '131.253.24.81', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.64', 1380576836, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('eff6e6a883b41c9b388ddb556f88ad19', '190.27.71.197', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_2 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B146', 1379245071, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f046135c684dcb0be7973b5ff91b0f13', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380373475, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f0863f3cfa0b4d877ef1319ed0611193', '131.253.24.56', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.04506.', 1380378771, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f0ff33601756d4812e8e3cd4c7d1e399', '131.253.24.36', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1)', 1380256638, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f13ff313b029d10627f3c257e7c1e6b4', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377269455, ''),
('f1dcfda0b940fac915f7fee5eb03fb1a', '131.253.24.81', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380567426, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f495ca9b05cde4970ebf95c927621db3', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380432744, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f4f50f33eea661652e8b88856b2da74c', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380618103, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f5b744068645a3b33fdc041f3927c8ab', '180.76.6.37', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1380580011, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f5cdf7e7c5346dc0cb1625399745d483', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377118033, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('f5d72e59b9b91cbb76a139975790c9fc', '131.253.24.82', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729)', 1380279057, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f60a980529eabe2606d08f86fd6e0f5f', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380155034, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f6e715d7e7fb3b5f5924e217d2046ae6', '199.30.24.135', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b', 1380355877, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f769e7ec7d4831de7bf0c0f1fb6028e5', '66.249.75.109', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1380158542, ''),
('f76e60b2cd18ffe6a253cc8886f1f385', '131.253.24.105', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.30729;', 1380205931, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f77e5403302094e33ec6c7a417db4445', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22', 1380140975, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('f7ca798f72fbf6d1d73a80acb025c7cf', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377113005, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('f7fef4569f300af5e07befcbd6681976', '181.48.71.245', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1378747944, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f84578712267fdc9cd5996fd8c2a8f53', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22', 1380126917, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('f8955d3e2a990aaecde1d239db8500dc', '181.54.113.54', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379429811, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:1:"1";}'),
('f8ae99be04285371eae080d18b35809a', '131.253.24.57', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380374870, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f91b6d3c1ef0312adea8a3991f49b299', '176.31.14.28', 'Opera/9.80 (X11; Linux i686; U; en) Presto/2.9.168 Version/11.52', 1380265734, ''),
('f95e5a502d060c760b03ed0fb1effe7c', '157.55.35.85', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1380600431, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f98ce7a71e728baab98183be571caae3', '199.30.25.63', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b', 1380339765, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f9a8c02816c0ef25b088efd04410b680', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377204200, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('f9c78f775ff18cb7b9a8c0a9bdd2cc2f', '131.253.24.100', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.', 1380305522, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f9e20066740dbba25a32190dd5f9134e', '131.253.24.51', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;  ', 1380201281, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('fade32886767479eb5b6b716a6f3d49c', '54.227.175.13', 'Mozilla', 1380153077, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('fb454cd8dc2f5d80878401ef39a7d3c2', '66.249.75.109', 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobi', 1380313366, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";b:0;}'),
('fb867c73be6cc157663200db9dbef738', '181.48.71.245', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET4.0C; .NET4.0E; .N', 1380573142, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('fbdf79f0e6a1e05d5776ca3f8a724817', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377115848, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";}'),
('fcb7ab6047010564ebd21b3146102b5f', '5.9.112.68', 'Mozilla/5.0 (compatible; SISTRIX Crawler; http://crawler.sistrix.net/)', 1380561674, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('fcd344603109be5f33b9e212f79cb808', '207.102.138.158', 'Browserlet', 1380128254, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('fcef2c4a94119631038878c821dd86e1', '180.76.5.193', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1380556505, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('fd995350a9b99cdd3b14e18106a64851', '131.253.24.55', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;  SV1;  .NET CLR 1.1.4325;  .NET CLR 2.0.40607;  .NET CLR 3.0.04506.64', 1380143556, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('fe66fa73ca1e80b25f2a43104e6abf6e', '131.253.24.128', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2;  SLCC1;  .NET CLR 1.1.4322;  .NET CLR 2.0.50727;  .NET CLR 3.0.30729;', 1380393390, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_users`
--

CREATE TABLE IF NOT EXISTS `cms_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `cms_users`
--

INSERT INTO `cms_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(5, '\0\0', 'administrator', '092e624ccaf41c1b9c0dd32a1041043a82507bc7', 'e0efe63787', 'cms@imaginamos.com', NULL, NULL, NULL, '1218e83c71363e71c292b071dace76d3f56b47af', 1343253917, 2013, 1, NULL, NULL, NULL, NULL),
(9, '¾<ï’', 'Prueba', '092e624ccaf41c1b9c0dd32a1041043a82507bc7', 'e0efe63787', 'prueba@imaginamos.com', NULL, NULL, NULL, '1218e83c71363e71c292b071dace76d3f56b47af', 2013, 2013, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_users_groups`
--

CREATE TABLE IF NOT EXISTS `cms_users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_users_groups` (`user_id`),
  KEY `group_users_groups` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `cms_users_groups`
--

INSERT INTO `cms_users_groups` (`id`, `user_id`, `group_id`) VALUES
(5, 5, 1),
(9, 9, 2);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cms_antecedentes`
--
ALTER TABLE `cms_antecedentes`
  ADD CONSTRAINT `fk_cms_antecedentes_cms_idiomas` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_banners`
--
ALTER TABLE `cms_banners`
  ADD CONSTRAINT `fk_cms_banners_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_campanas`
--
ALTER TABLE `cms_campanas`
  ADD CONSTRAINT `fk_cms_campanas_cms_antecedentes1` FOREIGN KEY (`antecedente_id`) REFERENCES `cms_antecedentes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_contactos`
--
ALTER TABLE `cms_contactos`
  ADD CONSTRAINT `fk_cms_contactos_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_imgcampanas`
--
ALTER TABLE `cms_imgcampanas`
  ADD CONSTRAINT `fk_cms_imgcampanas_cms_campanas1` FOREIGN KEY (`campana_id`) REFERENCES `cms_campanas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_imgservicios`
--
ALTER TABLE `cms_imgservicios`
  ADD CONSTRAINT `fk_cms_imgservicios_cms_servicios1` FOREIGN KEY (`id`) REFERENCES `cms_servicios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_logos`
--
ALTER TABLE `cms_logos`
  ADD CONSTRAINT `fk_cms_logos_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_quienes`
--
ALTER TABLE `cms_quienes`
  ADD CONSTRAINT `fk_cms_quienes_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_servicios`
--
ALTER TABLE `cms_servicios`
  ADD CONSTRAINT `fk_cms_servicios_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_users_groups`
--
ALTER TABLE `cms_users_groups`
  ADD CONSTRAINT `group_users_groups` FOREIGN KEY (`group_id`) REFERENCES `cms_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_users_groups` FOREIGN KEY (`user_id`) REFERENCES `cms_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
         