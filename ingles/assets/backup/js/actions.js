// Nombre del proyecto: Rentamuebles
// Nombre del archivo: actions.js
// Descripción: Funciones globales
// Fecha de creación: Noviembre del 2013
// Autor: Stive Zambrano

$(window).on("load", function(){$("#preload").stop().fadeOut(600);});
$(window).on("load", function(){timeOut = window.setTimeout(function(){$(".check-bt").children().animate({width: 0});}, 3000);});
$(document).ready(function(){
  $(".over-bw").hover(function(){$(this).children(".icon-bw").stop().animate({marginTop: -10}, 400, "easeInOutExpo");}, function(){$(this).children(".icon-bw").stop().animate({marginTop: 0}, 400, "easeInOutExpo");}); $(".over-bt-bw").click(function(){$(".con-bw").stop().fadeOut(200);});
	if($(".home-slider").size()>0){$(".home-slider").royalSlider({arrowsNav: true, loop: true, keyboardNavEnabled: true, controlsInside: false, imageScaleMode: "fill", arrowsNavAutoHide: false, autoScaleSlider: true, autoScaleSliderWidth: 940, autoScaleSliderHeight: 486, controlNavigation: "bullets", thumbsFitInViewport: false, navigateByClick: true, startSlideId: 0, autoPlay: {enabled: true}, transitionType: "move", globalCaption: true});};
	$(".bt-subnav-dv").click(function(){$(".subnav-lr-dv").delay().slideDown(200, "easeInOutExpo"); $(".bt-subnav-dv").children("span").css({"background-position": "-168px -90px"});}); $("html").click(function(){if($(".subnav-lr-dv").is(":visible")){$(".subnav-lr-dv").slideUp(200, "easeInOutExpo"); $(".bt-subnav-dv").children("span").css({"background-position": "-168px -60px"});}});
	$(".clients-slider").bxSlider({auto: true, maxSlides: 5, minSlides: 2, pager: false, pause: 7000, slideMargin: 8, slideWidth: 180, speed: 600});
	setInterval(function(){$(".check-bt").animate({backgroundColor: "#f7b400"}, 1000).delay(4000).animate({backgroundColor: "#e3402e"}, 1000).delay(4000);});
	var timeOut = null;
	$(".check-bt").hover(function(){$(this).children().stop().animate({width: "100%"}, 400);}, function(){$(this).children().stop().animate({width: 0}, 400);});
	$(".item-list").hover(function(){$(".item-list").stop().animate({opacity: 0.5}); $(this).stop().animate({opacity: 1});}, function(){$(".item-list").stop().animate({opacity: 1});});
	$(".con-gal-info .con-gal-grl").first().css({display: "block"}); $(".big-gal").first().css({display: "block"}); /*$(".con-gal-grl").children(".gal-minis").children("img").removeClass("mini-act");*/ $(".con-gal-grl").children(".gal-minis").children("img").first().addClass("mini-act");
	$(".mini-gal").click(function(){$(".mini-gal").removeClass("mini-act"); $(this).addClass("mini-act"); $(".big-gal").hide(); var ver_contenido = $(this).attr("data-id"); $("."+ver_contenido).stop().fadeIn(200); $(this).children(".big-gal:first").css({display: "block"});});
	$(".color").click(function(){$(".con-gal-grl").hide(); var ver_contenido = $(this).attr("data-id"); $("."+ver_contenido).stop().fadeIn(200); $(this).children(".big-gal:first").css({display: "block"}); $("."+ver_contenido).children(".gal-img-b").children("img").css({display: "none"}); $("."+ver_contenido).children(".gal-img-b").children("img:first").css({display: "block"}); $("."+ver_contenido).children(".gal-minis").children("img").removeClass("mini-act"); $("."+ver_contenido).children(".gal-minis").children("img:first").addClass("mini-act"); /*$("."+ver_contenido).children(".gal-minis").children("img").removeClass("mini-act"); $(".con-gal-grl").children(".gal-minis").children("img").first().addClass("mini-act");*/});
	$(function(){$(".an-din").on("click",function(event){var $anchor = $(this); $("html, body").stop().animate({scrollTop: $($anchor.attr("href")).offset().top}, 800, "easeInOutExpo"); event.preventDefault();});});	
	$(".con-service").hover(function(){$(".con-service").stop().animate({opacity: 0.5}); $(this).stop().animate({opacity: 1});}, function(){$(".con-service").stop().animate({opacity: 1});});
	$(".con-service:odd .service-info img").removeClass("fl").addClass("fr").css({margin: "0 0 1em 2em"}); $(".con-service:odd .con-bts-list .bt-list-more").css({left: 0, right: "inherit"});
	//if($(".grl-form").size()>0){$(".grl-form").validationEngine({scroll:false});};
	if($(".modal-box").size()>0){$(".modal-box").modalbox({CloseHandle : $(".close")});};
	if($.browser.msie&&$.browser.version==10){$("html").addClass("ie10");};
	if($.browser.msie){$("input[placeholder], textarea[placeholder]").each(function(){var input = $(this); $(input).val(input.attr("placeholder")); $(input).focus(function(){if (input.val() == input.attr("placeholder")){input.val("");}}); $(input).blur(function(){if (input.val() == "" || input.val() == input.attr("placeholder")){input.val(input.attr("placeholder"));}});});};
	if($(".footer-ahorranito").size()>0){$(".footer-ahorranito").ahorranito({width: 210});};
});
function numbs(evt){if(window.event){keynum = evt.keyCode;}else{keynum = evt.which;}if(keynum>47 && keynum<58){return true;}else{return false;}};
(function($, window){var dev = window.location.hash.indexOf("dev") > -1 ? ".dev": ""; window.applyValidation = function(){$.validate({onModulesLoaded: function($form){console.log();}, onValidate: function(){var $callbackInput = $("#callback"); if( $callbackInput.val() == 1 ){return {element: $callbackInput, message: ""};}}, onError: function(){if(!$.formUtils.haltValidation){/*alert('Invalid');*/}}, onSuccess: function(){/*alert('Valid');*/ form.submit(); return false;}});}; window.applyValidation(); /*Load one module outside $.validate() even though you do not have to*/ $.formUtils.loadModules("date"+dev+".js", false, false); /*Add a new validator*/ $.formUtils.addValidator({name: "even_number", validatorFunction : function(value, $el, config, language, $form){return parseInt(value, 10) % 2 === 0;}, borderColorOnError: "", errorMessage: "", errorMessageKey: ""});})(jQuery, window);