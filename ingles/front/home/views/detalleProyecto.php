<script src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets');?>/js/jquery.tagsinput.js"></script>
<div class="project_box sticker">
    <div class="relative">
        <div class="imgLiquidFill imgLiquid liquid_a">
            <a href="#" >
                <?php
                function image_exists($url) {
                    if(@getimagesize($url)){
                        return true;
                    }else{
                        return false;
                    }
                }
                $imagen = base_url().'uploads/proyectominiatura/'.$proyecto->imagen_miniatura; ?>
                <img class="imgLiquidFill"  id="imagen_box" src="<?php if(image_exists($imagen)){ echo base_url().'uploads/proyectominiatura/'.$proyecto->imagen_miniatura; }else{ echo 'img/024.jpg'; }?>" />
            </a>
        </div>
        <div class="cat" id="categoria_box"><?php if($nomcategoria != ' '){ echo $nomcategoria;}else{ echo 'Categoria proyecto'; }?></div>
        <div class="title" id="titulo_box"><?php
            if($proyecto->titulo == ''){ echo 'Project Title'; }else{ echo $proyecto->titulo; } ?></div>
        <div class="des" id="descripcion_box">
            <?php if($proyecto->descripcion_corta == ''){ echo 'Short Description'; }else{ echo $proyecto->descripcion_corta; } ?>

        </div>

        <div class="resumen">
            <ul class="data_project_min">
                <li class="by" id="by1">
                    <?php if($gestor->nombre == ''){ echo 'Organization'; }else{ echo $gestor->nombre; } ?>

                </li>
                <li class="by" id="by2">Municipio, País </li>
                <li class="by" id="by3"><span id="diasfaltantes_box"><?php if($proyecto->dias_para_cerrar == ''){ echo ''; }else{ echo $proyecto->dias_para_cerrar; } ?></span> days to go</li></ul>
        </div>
        <div class="pos_data">
            <div class="line_div_project"></div>

          <!--  <div class="percent_icon_box first_precent">
                <div class="money_icon_a icons"></div>
                <div class="percent_circle">0%</div>
            </div>-->

            <div class="percent_icon_box first_precent">
                <div class="members_icon_a icons"></div>
                <div class="percent_circle">0%</div>
            </div>
            <!--<div class="percent_icon_box">
                <div class="shop_icon_a icons"></div>
                <div class="percent_circle">0%</div>
            </div>-->
            <div class="clear"></div>

            <center><div style="font-size:12px; margin-bottom:4px">Supported</div></center>
            <?php if($proyecto->ocutlar_barra_porcentaje == 1){?>
                <div style="text-align: center;"><span style="font-size:12px; margin-bottom:4px;">This project requires of constant support.</span></div>
            <?php }else{ ?>
                <div class="barra_porcentaje">
                    <div class="bar" style="width:0%">0%</div>
                </div>
            <?php }?>
        </div>
    </div>       <!-- // Relative -->
</div><!-- // project box -->