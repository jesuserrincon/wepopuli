<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class apoyoespecie extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
         $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."textos/textos",
               CMSPREFIX."gestores/gestores",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0) {

        parent::index($proyectoid);
        $this->set_title('Bienvenidos a ' . SITENAME, true);


        $especies = new Especie();

        $especie = $especies->getEspecieByProyecto($proyectoid);

        $hatml = '';

        foreach($especie as $item){

        $hatml .='<div class="box_shadow3 relative">
        	<div class="pad_box_10">

                <a href="#" id="'.$item->id.'" class="no_apply2 left">Agregar</a>

                <div class="clear"></div>
            	<h2 class="h3_b">'.$item->nombre.'</h2>';

            $detalleEspecie = new Especie_detalle();
            $detalle = $detalleEspecie->getEspecie_detalleByEspecie($item->id);



            $hatml .=' <div class="table_ayuda_div">
                  <table class="table_ayuda" width="100%" border="0" cellspacing="0" cellpadding="0">
				   <tr class="title_tab">
						<td align="left" valign="middle">Cant.</td>
						<td align="left" valign="middle">Cont/Med x und.</td>
						<td valign="middle">Elemento</td>
						<td valign="middle">Marca</td>

				  </tr>';
            $i=1;
            foreach($detalle as $datos){

                        $hatml .='<tr>
                            <td align="left" valign="middle">'.$datos->cantidad.'</td>
                            <td align="left" valign="middle">'.$datos->contenido.'</td>
                            <td valign="middle">'.$datos->elemento.'</td>
                            <td valign="middle">'.$datos->marca.'</td>

                      </tr>';
                $i++;
            }

				$hatml .='</table>
              </div>';

              $hatml .='<div class="paquete_valor">
              	<h3 class="h3_purpure">Valor del paquete:</h3>
                <h2 class="h2_verde">$'. number_format($item->valor, 0, '', '.').'<span>(cop)</span></h2>
              </div>
              <div class="clear"></div>

        	</div>

            <div class="active_box_green"></div>
        </div>';

        }


        $this->_data['data'] = $hatml;
        $this->_data['id'] = $proyectoid;
        //enviamos una variable para validar si el usuario esta conectado 
        if($this->nativesession->get('usuario') == ''){
            $this->_data["login"] = 'no';
        }else{
            $this->_data["login"] = 'si';
        }
        //Fin envio voluntariado
        return $this->build('apoya_especie');
    }

    public function paso2(){

        if(isset($_POST['ids'])){
            $this->nativesession->set('apoyo',$_POST);
        }

        $datos = $usuario = $this->nativesession->get('apoyo');

        parent::index($datos['id']);

        $html = '';
        $valortotal = 0;
        foreach($datos['ids'] as $item){
          $especie = new Especie();
          $dataEspecie = $especie->getEspecieById($item);

            $html .='<div class="relative">
          
        	<div class="pad_box_10 over_box">
                <div class="clear"></div>

                <a href="#" onclick="setTimeout(function(){sumar()}, 500);" class="delete" title="Eliminar ayuda"></a>

            	<h2 class="h3_b">'.$dataEspecie->nombre.'</h2>
                <div class="table_ayuda_div">
                  <table class="table_ayuda" width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr class="title_tab">
						<td align="left" valign="middle">Cant.</td>
						<td align="left" valign="middle">Cont/Med x und.</td>
						<td valign="middle">Elemento</td>
						<td valign="middle">Marca</td>
				  </tr>
				  ';
					
            $detalleEspecie = new Especie_detalle();
            $detalle = $detalleEspecie->getEspecie_detalleByEspecie($dataEspecie->id);


            $i=1;
            foreach($detalle as $datos){

                $html .='<tr>
                            <td align="left" valign="middle">'.$datos->cantidad.'</td>
                            <td align="left" valign="middle">'.$datos->contenido.'</td>
                            <td valign="middle">'.$datos->elemento.'</td>
                            <td valign="middle">'.$datos->marca.'</td>

                      </tr>';
                $i++;
            }



				$html .='</table>
              </div>
		
		 		<div class="right cantidad_paquetes" style="margin-top:20px; width:178px">
			   	<h3 class="h3_purpure">Cantidad de paquetes:</h3<br />
				<select class="select sumaselect" style="width:120px" onchange="sumar()">
					<option value="'.($dataEspecie->valor * 1).'">1</option>
					<option value="'.($dataEspecie->valor * 2).'">2</option>
					<option value="'.($dataEspecie->valor * 3).'">3</option>
					<option value="'.($dataEspecie->valor * 4).'">4</option>
				</select>
			 </div>
		 
              <div class="paquete_valor right" style="margin-right:20px">
              	<h3 class="h3_purpure">Valor del paquete:</h3>
                <h2 class="h2_verde">$'.number_format($dataEspecie->valor, 0, '', '.').'<span>(cop)</span></h2>
              </div>
			  
			   
		 
              <div class="clear"></div>

        	</div>

            <div class="active_box_green"></div>
        </div>

        <div class="spacer"></div>';


          $valortotal = $valortotal + $dataEspecie->valor;
        }
        //enviamos una variable para validar si el usuario esta conectado 
        if($this->nativesession->get('usuario') == ''){
            $this->_data["login"] = 'no';
        }else{
            $this->_data["login"] = 'si';
        }
         $usuario = $this->nativesession->get('usuario');
        $usuarioOBJ = new Usuarios();
        $this->_data["usuario"] =  $usuarioOBJ->getUsuariosById($usuario['id']);
        $this->_data['html']=$html;
        $this->_data['valortotal']= $valortotal;
        return $this->build('apoya_especie_paso2');
    }


    // ----------------------------------------------------------------------


} ?>