<?php

class _Logos_dos extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "LOGOS";
         $this->load->model(CMSPREFIX."banner/idioma");
         $this->load->model(CMSPREFIX."logos/logos");
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {
        $cLogos = new Logos();
        $logos = $cLogos->where('tipo_id',1)->get();
        $cIdioma = new Idioma();
        $idioma = $cIdioma->getIdioma();
        $this->_data['info'] = $logos;       
        $this->_data['idioma'] = $idioma;       
        $this->_data['save'] = $value;       
        return $this->build('logos');
    }
    public function add($value = '') {
        $cIdioma = new Idioma();
        $idioma = $cIdioma->getIdioma();
        $this->_data['nombremodulo'] = "LOGOS / NUEVO";
        $this->_data['idioma'] = $idioma;  
        $this->_data['save'] = $value;  
        return $this->build('logos_new');
    }
    public function new_Logos() {
        $datos = array(
            'idioma_id' => $this->input->post('idioma_id'),
            'tipo_id' => 1,
            'imagen' => $this->input->post('imagen'),
            'link' => $this->input->post('link'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $b = new Logos();
        if ($datos['idioma_id'] != ''){
            
            $this->_data['save'] = FALSE;
            if ($b->saveLogos($datos)){
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        return redirect('cms/logos_dos/index/'.$save);        
    }
     public function edit($id = "",$update = ""){
        $cIdioma = new Idioma();        
        $b = new Logos();
        $dat = $b->getLogosById($id);
        $idioma = $cIdioma->getIdiomaById($dat->idioma_id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        $this->_data["idioma"] = $idioma;
        return $this->build("logos_edit");
    }
    
    public function update_Logos(){
        $b = new Logos();
        $datos = array(
            'id' => $this->input->post('id'),
            'link' => $this->input->post('link'),                
        );
        $update = $b->updateLogos($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateLogos($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/logos_dos/edit/".$this->input->post("id")."/".$a);
    }
    public function upload($width = '' , $height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'logos/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {
            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );
            
        } else {
                $data = array(
                        'data' => $this->upload->data()
                );
                $resize['image_library'] = 'GD2';
                $resize['source_image']	= UPLOADSFOLDER."logos/".$data["data"]["file_name"];
                $resize['maintain_ratio'] = TRUE;
                $resize['width']	 = $width;
                $resize['height']	= $height;
                $resize['new_image'] = UPLOADSFOLDER."logos/new/".$data["data"]["file_name"];

                $this->load->library('image_lib', $resize); 

                if ( ! $this->image_lib->resize())
                    {
                    $data = array(
                            'ok' => false,
                            'resize' => $this->image_lib->display_errors()
                        );    
                    }else{
                        $data = array(
                            'ok' => true,
                            'resize' => true,
                            'data' => $this->upload->data()
                        );
                    }
        }
        echo json_encode($data);
    }   
    public function delete_Imagen($id = ""){
        $b = new Logos();
        $return = FALSE;
        if ($b->deleteLogos($id)){
            $return = TRUE;
        };
        return redirect("cms/logos_dos/index/".$return);
    } 

}

