<?php

class Banner_proyecto extends DataMapper {

    public $model = 'banner_proyecto';
    public $table = 'banner_proyecto';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getBanner_proyecto(){
        return $this->get();
    }
    
    public function getBanner_proyecto_by_seccion($id){
        return $this->query('SELECT * FROM cms_banner_proyecto WHERE seccion = '.$id);
    }
    
    public function getBanner_proyecto_by_tipo($id){
        return $this->query('SELECT * FROM cms_banner_proyecto WHERE tipo = '.$id);
    }
    
    
    public function saveBanner_proyecto($object = "") {
        $this->link = $object['link'];
        $this->texto = $object['texto'];
        $this->imagen = $object['imagen'];
        $this->titulo = $object['titulo'];
        $this->pie_de_pagina = $object['pie_de_pagina'];
        $this->seccion = $object['seccion'];
        $this->tipo = $object['tipo'];
        return $this->save();
    }
    
    public function getBanner_proyectoById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateBanner_proyecto($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

