<div class="widget">
    <div class="header"><span><span class="ico gray window"></span><?php echo $nombremodulo ?></span>
        <br /><?php echo anchor('cms/logos_dos/', 'Volver', 'class="uibutton icon special answer" style="float:right;position: relative;top: -5px"') ?>                 
    </div><!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div class="imu_info" id="info"></div>
                    <?php echo form_open_multipart('cms/logos_dos/update_Logos/', 'id="form"') ?>
                     <input style="width:250px" type="hidden"  id="id" name="id" class="small" value="<?php echo $info->id; ?>" /> 
                    <div class="section">
                         <label>Idioma</label>
                        <div>
                            <?php echo $idioma->titulo;  ?>
                        </div>
                    </div>
                    <div class="section">
                         <label>Link </label>
                        <div>
                            <input type="text"  id="link" name="link" class="medium" value="<?php echo $info->link; ?>"/> 
                        </div>
                    </div>
                    <div>
                        <div class="section">
                            <label>Imagen
                        </label>
                        <label>Subir nueva imagen (400px x 300px)</label><br/><br/>
                            <div>
                                <img class="cuadro_edicion_fotos" id="img1"  src="<?php echo base_url() . "uploads/logos/new/" . $info->imagen; ?>" width="150">
                                <div class="cuadro_edicion_fotos" id="divimg1"></div>
                                <br />
                                <input type="file" name="nombre" id="fileUpload1" class="fileUpload2" />    
                                <input type="hidden" name="imagen" id="imagen" />
                            </div>
                        </div>
                    </div>
                    <div style="width: 100px;height: 30px;position: relative;top: 30px">
                        <input onclick="valida();" type="button" value="Guardar" class="uibutton confirm" />
                        
                    </div>
                    <?php echo form_close() ?>
                    <p>&nbsp;</p>
                 </fieldset>
                <br>
            </div>
        </div>
    </div>	
</div><!-- End content -->
<script type="text/javascript">
var url = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i ;                 
    function valida(){
        var titulo = $("#titulo").val();
        var link = $("#link").val();
        if (titulo == "" || link == "" ){
                $('#info').focus();
                showError('Complete todos los campos.',3000);
            }else{
                if(!url.exec(link)){
                    showError('Ext. url invalida',8000);
                    return false;
                }else{
                    $('#form').submit();
                }   
            }
   }                    
     $(document).ready(function() {
        var texto = '';
        var nextinput = parseInt($("#inputTotal").val());
        $('#fileUpload1').uploadify({
                'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                'script'            : '<?php echo site_url(); ?>' + 'cms/logos_dos/upload/400/300',
                'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                'auto'              : true,
                'folder'            : '',
                'queueSizeLimit'    : 3,
                'multi'             : false,
                'fileExt'           : '*.jpg;*.jpeg;*.png;*gif',
                'auto'              : true,
                'buttonText'        : 'Cargar imagen.',
                'onComplete'  : function(event, queueId, fileObj, response, data) {
            var responseJson = $.parseJSON(response);
            if (responseJson.ok === true) {
                    $("#img1").attr('src','<?php echo base_url() ?>uploads/logos/'+responseJson.data.file_name);
                    $("#img1").hide(800).delay(2000).show(800);
                    $("#divimg1").html('<h2>Cargando imagen...</h2>');
                    $("#divimg1").show(850).delay(2000).hide(750);
                    $("#imagen").val(responseJson.data.file_name);
            } else {
                    showError('Problemas al carga imagen.',3000);                            
            }
        }
    });
   }); 
<?php if (isset($update)){
        if ($update ==  1){?>
        showSuccess('Actualización correcta.',3000);
        <?php } ?>
<?php } ?>
</script>