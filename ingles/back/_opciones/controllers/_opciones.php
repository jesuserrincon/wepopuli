<?php

class _Opciones extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "REDES SOCIALES";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Opciones();
        $info = $b->getOpciones();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('opciones');
    }

    public function add() {
        $this->_data['nombremodulo'] = "OPCIONES / New";
        $this->build('opciones_new');
    }

    public function new_opciones() {
        $datos = array(
            'nombre' => $this->input->post('nombre'),
            'precio' => $this->input->post('precio'),
            'cms_modal_id' => 1,
        );

        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'opciones';
        $b = new Opciones();
        if ($datos['nombre'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'opciones_new';
            if ($b->saveOpciones($datos)){
                $build = 'opciones';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getOpciones();
        $this->_data["info"] = $info;
        return redirect('cms/opciones/index/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'opciones/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."opciones/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('opciones/'.$data["data"]["file_name"], array('longside'=>'opciones/new','width' => 272, 'height' => 304,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Opciones();
        $dat = $b->getOpcionesById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("opciones_edit");
    }
    
    public function update_opciones(){
        $b = new Opciones();
            $datos = array(
                'nombre' => $this->input->post('nombre'),
                'precio' => $this->input->post('precio'),
                'cms_modal_id' => 1,
            'id' => $this->input->post('id')
        );

                    $update = $b->updateOpciones($datos);

        if ($update){
            $a = TRUE;
        }
        return redirect("cms/opciones/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Opciones();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/opciones/index/".$return);
    }

}

