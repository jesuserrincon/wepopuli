<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  
<style type="text/css">
#s2 {
	color:#fff;
	background-position:0px -50px;
}
</style>

<link href="assets/css/jquery-filestyle.css" rel="stylesheet" />
<link href="assets/css/jquery.tagsinput.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/css/jquery-ui.css">
  <script src="assets/js/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( ".datepicker" ).datepicker({ minDate: +7, maxDate: "+10M +1D" });
  });
  </script>
  
  	
<script type="text/javascript" src="assets/js/jquery.tagsinput.js"></script>
<script type="text/javascript">
		function onAddTag(tag) {
			alert("Added a tag: " + tag);
		}
		function onRemoveTag(tag) {
			alert("Removed a tag: " + tag);
		}
		
		function onChangeTag(input,tag) {
			alert("Changed a tag: " + tag);
		}
		
		$(function() {

			$('#tags_1').tagsInput({width:'auto'});
			$('#tags_2').tagsInput({
				width: 'auto',
				onChange: function(elem, elem_tags)
				{
					var languages = ['php','ruby','javascript'];
					$('.tag', elem_tags).each(function()
					{
						if($(this).text().search(new RegExp('\\b(' + languages.join('|') + ')\\b')) >= 0)
							$(this).css('background-color', 'yellow');
					});
				}
			});
			$('#tags_3').tagsInput({
				width: 'auto',

				//autocomplete_url:'test/fake_plaintext_endpoint.html' //jquery.autocomplete (not jquery ui)
				autocomplete_url:'test/fake_json_endpoint.html' // jquery ui autocomplete requires a json endpoint
			});
		// Uncomment this line to see the callback functions in action
		//			$('input.tags').tagsInput({onAddTag:onAddTag,onRemoveTag:onRemoveTag,onChange: onChangeTag});		
		
		// Uncomment this line to see an input with no interface for adding new tags.
		//			$('input.tags').tagsInput({interactive:false});
		});
	
	</script>
    <script TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</script>
</head>

<body>
<?php include"header.php" ?>  
<?php include"paso_paso.php" ?>

<div class="txt_reglamento">
	<h2 class="h2_b">Creación del proyecto</h2>
    Es hora de empezar a describir el proyecto, siendo lo más específico posible. Entre más clara sea la descripción del proyecto, más altas serán las probabilidades de conseguir el apoyo necesario para llevarlo a cabo.
</div>

<div class="box_a pad_box clearfix">
    <div class="col_d mar_right">
        <div class="project_box sticker">
            <div class="imgLiquidFill imgLiquid liquid_a">
                <a href="#"  target="_blank">
                    <img class="thumbnail_a" src="assets/img/024.jpg" />
                </a>
            </div>
            <div class="title">Por un país mejor <br />Colombia</div>
            <div class="des">
             Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
            </div>
            
            <div class="resumen">
                <ul class="data_project_min"> 
                    <li class="by">Fundación pies descalsos</li>
                    <li class="by">Rio Magdalena, Colombia </li>
                    <li class="by">6 dias faltantes</li>
                </ul>
            </div>
            
            <div class="line_div_project"></div>
            
            <div class="percent_icon_box first_precent">
                <div class="money_icon_a icons"></div>
                <div class="percent_circle">100%</div>
            </div>
            <div class="percent_icon_box">
                <div class="members_icon_a icons"></div>
                <div class="percent_circle">50%</div>
            </div>
            <div class="percent_icon_box">
                <div class="shop_icon_a icons"></div>
                <div class="percent_circle">20%</div>
            </div>
            <div class="clear"></div>
            
            
            <div class="barra_porcentaje">
                <div class="bar" style="width:45%">45%</div>
            </div>
            
        </div><!-- // project box -->
    </div>
    
    <div class="col_c">
        <div class="box_shadow box_c" style="margin-bottom:30px;">
            <h6>Imagen miniatura del proyecto <span>*</span> </h6>
            <div class="left" style="width:470px">
                Escoja una imagen de su computador, en
                formato JPEG, PNG, GIF, o BMP • Tamaño máximo: 2MB.
                Dimensiones mínimas: 640x480 pixeles • 4:3 relación radio.
            </div>
            <div class="right relative" style="width:111px; height:90px">
               <input type="file" class="jfilestyle" data-theme="blue">
            </div>
        </div>
        
        <div class="box_shadow box_c">
            <h6>Título del Proyecto <span>*</span> </h6>
            <p>El título del proyecto debe ser simple, específico y fácil de recordar. 
            Evite palabras como “ayuda”, “apoyo” o “patrocine”.</p>
            <input type="text" class="input_c" style="margin-bottom:10px" /><br />Máximo de caracteres 0/60
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c">
            <h6>Categoría<span>*</span> </h6>
            <select class="select">
                <option value="">Categorías</option>
                <option>Lorem Ipsum</option>
                <option>Dolor Sit</option>
                <option>Vehicula Ornare</option>
                <option>Foo</option>
                <option>Bar</option>
                <option>Baz</option>
                <option>Qux</option>
                <option>Zoobie</option>
                <option>Frang</option>
            </select><br />
            Seleccione la categoría que más se ajuste al área de ejecución del proyecto.
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
		<div class="box_shadow box_c">
            <h6>Tags<span>*</span> </h6>
            <input id="tags_1" type="text" class="tags input_c"/></p>
            Seleccione la categoría que más se ajuste al área de ejecución del proyecto.
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>

        <div class="box_shadow box_c">
            <h6>Descripción corta <span>*</span> </h6>
            <p>¿Si se utilizara una frase para describir el proyecto, ¿cuál sería?</p>
            <textarea class="texarea_1"></textarea>
            
            <br />Máximo de caracteres 0/135
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Localización del proyecto <span>*</span> </h6>
            <p>¿Dónde se desarrollará el proyecto?</p>
            <div class="col_int_box_c left">
            	<div class="label">País</div>
            	<input type="text" class="input_a" />
            	<div class="label">Municipio</div>
            	<input type="text" class="input_a" />
            </div>
            <div class="col_int_box_c right">
           		<div class="label">Departamento/Región</div>
            	<input type="text" class="input_a" />
            	<div class="label">Dirección (Si aplica)</div>
            	<input type="text" class="input_a" />
            </div>
            <div class="clear"></div>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Tiempo de Publicación<span>*</span> </h6>
            <p>Recomendamos que los proyectos estén publicados 30 días o menos. Tiempos de publicación cortos tienen tasas de éxito más altas, y ayudarán a crear un sentido de urgencia que ayudará a conseguir más apoyo para el proyecto. </p>
            <b>Fecha Límite</b><br />
            <input type="text" class="datepicker input_a">
            <div class="clear"></div>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Clases de apoyo<span>*</span> </h6>
            <p>Diligencia las clases de apoyo segun requiera el proyecto. Puedes escoger una, dos o todas las clases de apoyo si el proyecto lo necesita.</p>           
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Apoyo de voluntariado</h6>
            <input type="text" class="input_b" placeholder="Ej. Personal para pintar casas" style="margin-right:20px;" />
             Máximo de caracteres 0/135<p></p>
            <p>Describa la clase de voluntariado que necesita. ¿Por qué y para qué?</p>           
            <textarea class="texarea_1"></textarea>
            <br />Máximo de caracteres 0/135
            <div class="clear" style="margin-bottom:10px;"></div>
            <div class="left">
            	<a href="#" class="add_2">Añadir otro voluntariado</a>
            	<a href="#" class="less_2">Personal para pintar casas</a>

            </div>
			<div class="right">
            	Cantidad de Voluntarios:
            	<input type="text" class="input_d" onKeyPress="return numbersonly(this, event)"/>
            </div>

        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Apoyo monetario</h6>
            <p>Explique por qué y para qué necesita apoyo monetario.</p>           
            <textarea class="texarea_1"></textarea>
            <br />Máximo de caracteres 1000/1000
            <div class="clear" style="margin-bottom:10px;"></div>
			<div class="right">
            	Cuánto requiere para su proyecto:
            	<input  type="text" class="input_e" value="$" onKeyPress="return numbersonly(this, event)" />

            </div>

        </div>
        <div class="div_green clear"></div>        
        <div class="box_shadow box_c clearfix">
            <h6>Apoyo en Especie</h6>
            <p>Ingrese los elementos que requiera su proyecto.</p>           
            <input type="text" class="input_f" placeholder="Nombre del paquete. Ej.Materiales de construcción" style="margin-right:20px" />
            Máximo de caracteres 0/30
            <div class="clear" style="margin-bottom:30px"></div>
            
            <div>
            <div class="col_a_especie left">
            	<center>Elemento</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_g" placeholder="Ej: Atún" />
                <input type="text" class="input_g" placeholder="Ej: Baldosas" />
                <input type="text" class="input_g" placeholder="Ej: Leche"/>
                <input type="text" class="input_g" />
                <input type="text" class="input_g" />
                <input type="text" class="input_g" />
                <input type="text" class="input_g" />
            </div>
            <div class="col_a_especie left">
            	<center>Marca</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_g" placeholder="Ej: Van camps" />
                <input type="text" class="input_g" placeholder="Ej: Corona (Ref. tauro)" />
                <input type="text" class="input_g" placeholder="Ej: Alpina deslactosada" />
                <input type="text" class="input_g" />
                <input type="text" class="input_g" />
                <input type="text" class="input_g" />
                <input type="text" class="input_g" />
            </div>
            <div class="col_b_especie left">
            	<center>Cantidad</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_h" onKeyPress="return numbersonly(this, event)" placeholder="Ej: 450" />
                <input type="text" class="input_h" onKeyPress="return numbersonly(this, event)" placeholder="Ej: 150" />
                <input type="text" class="input_h" onKeyPress="return numbersonly(this, event)" placeholder="Ej: 1000" />
                <input type="text" class="input_h" onKeyPress="return numbersonly(this, event)" />
                <input type="text" class="input_h" onKeyPress="return numbersonly(this, event)" />
                <input type="text" class="input_h" onKeyPress="return numbersonly(this, event)" />
                <input type="text" class="input_h" onKeyPress="return numbersonly(this, event)" />
            </div>
            <div class="col_b_especie left" style="margin-right:0px; width:108px;">
            	<center>Cont/Med x und.</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_h" placeholder="Ej: 180gr" />
                <input type="text" class="input_h" placeholder="Ej: 45cm x 45cm" />
                <input type="text" class="input_h" placeholder="Ej: 1 Litro" />
                <input type="text" class="input_h" />
                <input type="text" class="input_h" />
                <input type="text" class="input_h" />
                <input type="text" class="input_h" />
            </div>
            <div class="clear" style="margin-bottom:20px;"></div>
            
            </div><!-- // CONTENEDOR inputs -->
            <div class="left" style="width:270px">
            	<a href="#" class="add_2" title="Agregar">Añadir nuevo item</a>
            	<a href="#" class="add_2" title="Agregar">Añadir nuevo paquete</a>
            	<a href="#" class="less_2" title="eliminar">Eliminar <span style="line-height:13px">"Materiales de construcción</span></a>
            </div>
			<div class="right" style="width:330px;">
                <div class="right">Cantidad de paquetes necesarios: <input type="text" class="input_d" onKeyPress="return numbersonly(this, event)" /></div>
                <div class="clear"></div>

            	<div class="right" style="font-size:18px; margin-bottom:10px;">Valor paquete de especie:</div><div class="clear"></div>
                <input type="text" class="input_e right" value="$" onKeyPress="return numbersonly(this, event)" style="margin-bottom:4px;" />
                <div class="right" style="text-align:right; font-size:12px;">El valor mínimo de un paquete<br /> debe ser de 20.000 pesos.</div>
            </div>

        </div>
        
        	<div class="clear" style="margin-bottom:30px"></div>
            <input type="submit" value="Borrar Proyecto" class="boton_c left" style="margin-right:10px;">
            <input type="submit" value="Guardar Cambios" class="boton_a left" style="margin-right:10px;">
            <a href="reglamentos.php" class="boton_b left" style="margin-right:10px;">Atrás</a>
            <a href="reglamentos.php" class="boton_b left">Siguiente</a>

        
    </div><!-- // columna C -->
    
</div>



<div id="inline2" style="width:400px;display:none;">
    <h4>¿Eres Usuario de Colombia Incluyente?<br /><br /></h4>
    <p>Para postular un proyecto debes ser ususario registrado.
	Ingresa o de lo contario puedes <a href="" style="color:#0091e9">registrarte gratis.</a>
  <div class="form_pop_box"><br />
    	<div class="label">Usuario <span>*</span></div>
        <input type="text" class="input_a" />
    	<div class="label">Contraseña <span>*</span></div>
        <input type="password" class="input_a" />
    </div>
    
    <div class="div_line clear" style="margin-bottom:20px; margin-top:20px;"></div>
  	<a href="reglamentos.php"><input type="submit" class="bt_green" value="ingresa" /></a>
</div>
<script type="text/javascript" src="assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="assets/js/aplication.js"></script>

<?php include"footer.php" ?>  
</body>
</html>