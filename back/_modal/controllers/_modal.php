<?php

class _Modal extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "REDES SOCIALES";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Modal();
        $info = $b->getModal();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('modal');
    }

    public function add() {
        $this->_data['nombremodulo'] = "MODAL / New";
        $this->build('modal_new');
    }

    public function new_Modal() {
        $datos = array(
            'titulo' => $this->input->post('titulo'),
            'texto' => $this->input->post('texto'),
        );

        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'modal';
        $b = new Modal();
        if ($datos['texto'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'modal_new';
            if ($b->saveModal($datos)){
                $build = 'modal';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getModal();
        $this->_data["info"] = $info;
        return redirect('cms/modal/index/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'modal/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."modal/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('modal/'.$data["data"]["file_name"], array('longside'=>'modal/new','width' => 272, 'height' => 304,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Modal();
        $dat = $b->getModalById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("modal_edit");
    }
    
    public function update_modal(){
        $b = new Modal();
        $datos = array(
            'titulo' => $this->input->post('titulo'),
            'texto' => $this->input->post('texto'),
            'id' => $this->input->post('id')

        );

                    $update = $b->updateModal($datos);

        if ($update){
            $a = TRUE;
        }
        return redirect("cms/modal/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Modal();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/modal/index/".$return);
    }

}

