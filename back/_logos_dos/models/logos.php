<?php

class Logos extends DataMapper {

    public $model = 'logos';
    public $table = 'logos';
    
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getLogos(){
        return $this->get();
    }
    
    public function saveLogos($object = "") {
        
        $this->idioma_id = $object['idioma_id'];        
        $this->imagen = $object['imagen'];        
        $this->link = $object['link'];        
        return $this->save();
    }
    public function getLogosById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateLogos($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    public function deleteLogos($id) {
        $this->where('id',$id)->get();
        return $this->delete();
    }
    public function getLogosByIdioma($idioma = ""){
       $array = array('idioma_id' => $idioma);
        return $this->where($array)->get();
    }
}

