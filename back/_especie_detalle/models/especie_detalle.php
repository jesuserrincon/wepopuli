<?php

class Especie_detalle extends DataMapper {

    public $model = 'especie_detalle';
    public $table = 'especie_detalle';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getEspecie_detalle(){
        return $this->query('SELECT id,titulo FROM cms_especie_detalle');
    }
    
    public function getEspecie_detalleByEspecie($idproyecto = 0){
        return $this->query('SELECT id,elemento,marca,cantidad,contenido,especie_id FROM cms_especie_detalle WHERE especie_id = '.$idproyecto);
    }
    
    public function saveEspecie_detalle($object = "") {
        $this->elemento = $object['elemento'];
        $this->marca = $object['marca'];
        $this->cantidad = $object['cantidad'];
        $this->contenido = $object['contenido'];
        $this->especie_id = $object['especie_id'];
        return $this->save();
    }
    
    public function getLastId(){
        $usuario = $this->query('SELECT MAX(id) AS id FROM cms_especie_detalle');
        return $usuario->id;
    }
    
    public function getEspecie_detalleById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateEspecie_detalle($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

