<?php

class _Newsletter extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "NEWSLETTER";
        //$this->load->model("banner");
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    
    public function index($value = '') {

        $this->_data['save'] = $value;
        $cNewsletter = new News();
        $newsletter = $cNewsletter->getNewsletter();
        $this->_data["info"] = $newsletter;
        return $this->build('newsletter');
    }
    public function csv($value = '') {
        header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Newsletter.xls");
header("Pragma: no-cache");
header("Expires: 0");
        $this->_data['save'] = $value;
        $cNewsletter = new News();
        $newsletter = $cNewsletter->getNewsletter();
        $this->_data["info"] = $newsletter;
        
        echo '
            <table>
    <tr>
        <th>Email</th>
    </tr>
    <tbody> ';
       foreach ($newsletter as $data){
            
           echo ' <tr>
                <td valign="top">'.
                     $data->email
                .'</td>';
       }
        echo ' </tr>
    </tbody>
</table>
            ';
    }
    
    
    
    public function deleteNews($id = ""){
        $b = new News();
        $return = FALSE;
         if ($b->deleteNewsletter($id)){
            $return = TRUE;
        };
        
        return redirect("cms/newsletter/index/".$return);
    }
}

