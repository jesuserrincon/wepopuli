<?php

class Zonas extends DataMapper {

    public $model = 'zonas';
    public $table = 'zonas';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getZonas(){
        return $this->get();
    }
    
    public function saveZonas($object = "") {
        $this->nombre = $object['nombre'];
        $this->precio = $object['precio'];
        return $this->save();
    }
    
    public function getZonasById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateZonas($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

