<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class registro extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('encrypt');
        $this->load->model(array(
            CMSPREFIX . "usuarios/usuarios",
            CMSPREFIX . "news/news",
        ));

        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($accion = 0, $idUser = '') {
        $this->set_title('Bienvenidos a ' . SITENAME, true);
        $this->_data["accion"] = $accion;
        $this->_data["idUser"] = $idUser;
        return $this->build('registro');
    }

    public function activaremail($cc) {

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        $this->load->model(array(
            CMSPREFIX . "usuarios/usuarios",
        ));

        $datos = array(
            'cedula' => $cc,
            'estado' => 1,
        );


        $usuarioOBJ = new Usuarios();
        $usuarioOBJ->updateUsuariosCC($datos);

        $email = $usuarioOBJ->usuarioByCc($cc);

        $email = $email->email;
        $asunto = 'Bienvenida';
        $titulo = 'Bienvenido';
        $mensaje = 'Bienvenido a Colombia Incluyente.';

        $this->enviarCorreo($email,$asunto,$titulo,$mensaje);

        redirect('registro/index/2', 'refresh');



        //return $this->build('registro');
    }

    public function login() {

        $this->load->model(array(
            CMSPREFIX . "usuarios/usuarios",
        ));

        $emaillog = $this->input->post('emaillog');
        $password = md5($this->input->post('password'));

        $usuarioOBJ = new Usuarios();
        $datosusu = $usuarioOBJ->loginUsuario($emaillog, $password);
        if ($datosusu == 0) {
            echo 'Error en los datos suministrados';
        } else {
            $this->nativesession->set('usuario', $datosusu);
            //print_r($datosusu);
            //redirect('registro/index/3','refresh');
        }
    }

    public function crearproyecto() {
       
        
        $this->load->model(array(
            CMSPREFIX . "proyectos/proyectos",
            CMSPREFIX . "gestores/gestores",
            CMSPREFIX . "cuentas/cuentas",
        ));


        $g = new Gestores();
        $c = new Cuentas();
        
        $datos = array(
            'pais' => $this->input->post('pais'),
            'departamento' => $this->input->post('departamento'),
            'municipio' => $this->input->post('ciudad'),
            'usuarios_id' => $_SESSION['usuario']['id'],
        );

        $proyectoOBJ = new Proyectos();
        $proyectoOBJ->saveProyectos($datos);
        $id = $proyectoOBJ->getLastId();

        $data = array(
            'proyectos_id' => $id,
        );
        $g->saveGestores($data);
        $c->saveCuentas($data);
        echo $id;
    }

    public function logout() {
        $this->nativesession->delete('usuario');
        redirect('home/index/2', 'refresh');
    }

    
     
    
    
    public function UsuarioAdd() {


        $this->load->model(array(
            CMSPREFIX . "usuarios/usuarios",
        ));

        $datos = array(
            'nombres' => $this->input->post('nombres'),
            'apellidos' => $this->input->post('apellidos'),
            'telefono' => $this->input->post('telefono'),
            'telefono2' => $this->input->post('telefono2'),
            'cedula' => $this->input->post('cedula'),
            'email' => $this->input->post('email'),
            'pais' => $this->input->post('pais'),
            'departamento' => $this->input->post('departamento'),
            'direccion' => $this->input->post('direccion'),
            'facebook' => $this->input->post('facebook'),
            'twitter' => $this->input->post('twitter'),
            'ciudad' => $this->input->post('ciudad'),
            'sexo' => $this->input->post('sexo'),
            'estado_civil' => $this->input->post('estado_civil'),
            'clave' => md5($this->input->post('clave')),
            'newsletter' => $this->input->post('newsletter'),
            'recordarme' => $this->input->post('recordarme'),
            'fecha_nacimiento' => $this->input->post('fecha_nacimiento'),
            'pais_nacionalidad' => $this->input->post('pais_nacionalidad'),
            'tipo_identificacion' => $this->input->post('tipo_identificacion'),
        );

        $usuariosOBJ = new Usuarios();
        $usuariosOBJ->saveUsuarios($datos);

        $emali = $this->input->post('email');
        $asunto = 'Activa tu cuenta en Colombia Incluyente';
        $titulo = 'Activacion de cuenta';
        $mensaje = 'Gracias por registrarte en Colombia Incluyente. Haz clic en el siguiente botón para activar tu cuenta y empezar a conocer las iniciativas.
        <a href="'.base_url().'registro/activaremail/' . $this->input->post('cedula') . '">Activar cuenta</a>';
        $this->enviarCorreo($emali,$asunto,$titulo,$mensaje);

        redirect('registro/index/1', 'refresh');
    }

    public function recuperarClave(){


        $this->email->from('info@colombiaincluyente.com', 'Colombia incluyente');
        $this->email->to($this->input->post('email'));
        $idUser = $this->input->post('id');


        $this->email->subject('Recuperar clave');

        $message = '
<div class="deviceWidth_b">
        <!-- Wrapper -->
  <table class="deviceWidth_b" width="100%" bgcolor="#f0f0f0" style="font-family:Arial, Helvetica, sans-serif; color:#707070; " align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
		 <thead>
         	<tr>
            	<td>
                	<img src="'.base_url().'mail/img/005.png" width="682" height="80" alt="Colombia Incluyente Logo">
                </td>
            </tr>
         </thead>
        </table>


        <table class="deviceWidth_b"  style="font-family:Arial, Helvetica, sans-serif; color:#707070" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
        	<tbody>
        	<tr>
            	<td style="padding-left:20px; padding-top:20px">
                	<div style="font-size:20px;">Recuperar clave</div>
                </td>
            </tr>

        </tbody></table>

        <table class="deviceWidth_b" width="100%" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; color:#707070" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
        	<tbody><tr>
				<td style="padding:20px">
                para recuperar su clave de cick <a href="'.base_url().'registro/index/4/'.$idUser.'">Aqui</a>
                </td>
			</tr>
        </tbody></table>



        <table class="deviceWidth_b" width="100%" bgcolor="#3e3e3e" style="font-family:Arial, Helvetica, sans-serif; color:#707070" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
		 <tfoot>
         	<tr>
            	<td valign="middle" style="height:74px">
                <img src="'.base_url().'mail/img/001.png" width="478" height="74" alt="Ir a la pagina" style="float:left;">

                <a href="#"><img src="'.base_url().'mail/img/002.png" style="float:right; margin-top:10px; margin-left:15px; margin-right:20px" alt="facebook"></a>
                <a href="#"><img src="'.base_url().'mail/img/003.png" style="float:right; margin-top:10px; margin-left:15px;" alt="twitter"></a>
                <a href="#"><img src="'.base_url().'mail/img/004.png" style="float:right; margin-top:10px; margin-left:15px;" alt="youtube"></a>
                </td>
            </tr>
         </tfoot>
        </table>
</div>';


        $this->email->message($message);


        $this->email->send();

        redirect('registro/index/3', 'refresh');

    }

    public function verificarCorreo(){

        $correo = $this->input->post('correo');
        $usuario = new Usuarios();
        $datosUsuario = $usuario->usuarioByCorreo($correo);

        if(!empty($datosUsuario->id)){
        echo $datosUsuario->id;
        }else{
            echo 0;
        }
    }
    
    
    public function suscribirsenewsletter(){

            $email = $this->input->post('email');
            $news = new News();
            $newsresult = $news->SuscritoNews($email);
            if(empty($newsresult->id)){
                    $datos = array(
                   'email' => $this->input->post('email')
                   );
            $news->saveNewsletter($datos);
                echo 1;
            }else{
                echo 0;
            }
        }
     
    
    public function verificarCedula(){

            $cedula = $this->input->post('cedula');
            $tipo = $this->input->post('tipo');
            $usuario = new Usuarios();
            $datosUsuario = $usuario->usuarioByCedula($cedula,$tipo);

            if(!empty($datosUsuario->id)){
            echo $datosUsuario->id;
            }else{
                echo 0;
            }
        }

        public function nuevaClave(){

            $usuario = new Usuarios();
            $datos = array(
            'id' => $this->input->post('id'),
            'clave' => md5($this->input->post('clave'))
            );

            $usuario->updateUsuarios($datos);

            redirect('registro/index/5', 'refresh');

        }


    // ----------------------------------------------------------------------
}
