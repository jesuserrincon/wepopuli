<?php

class Voluntariado extends DataMapper {

    public $model = 'voluntariado';
    public $table = 'voluntariado';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getVoluntariado(){
        return $this->query('SELECT id,titulo FROM cms_voluntariado');
    }
    
    public function getVoluntariadoByProyecto($idproyecto = 0){
        return $this->query('SELECT id,titulo,descripcion,cantidad,
(SELECT COUNT(*) FROM cms_ayuda_voluntariado WHERE proyectos_id = '.$idproyecto.' AND tipo_voluntariado = vol.id) donados 
FROM cms_voluntariado vol WHERE proyectos_id = '.$idproyecto);
    }
    public function getVoluntariadoByProyectoCOUNT($idproyecto = 0){
         $consulta = $this->query('SELECT COUNT(*) AS cantidad
                        FROM cms_voluntariado vol WHERE proyectos_id = '.$idproyecto);
         return $consulta->cantidad;
    }
    
    public function saveVoluntariado($object = "") {
        $this->titulo = $object['titulo'];
        $this->descripcion = $object['descripcion'];
        $this->cantidad = $object['cantidad'];
        $this->proyectos_id = $object['proyectos_id'];
        return $this->save();
    }
    
    public function getLastId(){
        $usuario = $this->query('SELECT MAX(id) AS id FROM cms_voluntariado');
        return $usuario->id;
    }
    
    public function getVoluntariadoById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateVoluntariado($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

