<div class="box_a">
    <div class="info_txt">
        <h2>¿De dónde vienen los proyectos?</h2>
        <p style="width:870px; text-align: justify">Vienen de las necesidades del pueblo, de la gente, de iniciativas sociales concretas que necesiten del apoyo de todos, a través de aportes económicos, paquetes de ayuda y voluntariado.</p>
    </div>
</div>

  <div class="wrapper relative">
    <ul class="rslides" id="slider2">
      <?php foreach ($banners_superior as $item){?>
        <li><a href="<?php echo $item->link; ?>" target="_blanck"><img src="../uploads/banner_home/new/<?php echo $item->imagen; ?>" alt=""></a></li>
       <?php }?>
    </ul>
  </div>


<div class="box_a">
    <div class="div_line"></div>
    <div class="info_txt">
        <h2>¿De dónde viene el apoyo?</h2>
      	<p style="width:870px; text-align:justify">De todos los que queremos optimizar las condiciones de las personas vulnerables, proteger el medio ambiente, concientizar y hacer un mundo mejor.</p>
    </div>
    <div class="div_line"></div>
    <a href="<?php echo base_url().'buscaapoyo'; ?>" class="bt_a">Busca Apoyo</a>
</div>

<div class="box_b search_area clearfix" style="width:875px">
    <h2 class="h2_b search_h2" style="line-height:33px; margin-left:20px; margin-right:0px">Descubre y Apoya Proyectos</h2>
    <div class="search_box left" style="height:38px; margin-left:20px">
        <form id="formulariobusqueda2" action="<?php echo base_url() ?>resultados_busqueda/index/0/2" method="post">
            <input type="submit" class="go" onclick="buscar2()" value="" title="Buscar" />
            <input type="text" id="palabrabuscar2" name="palabrabuscar" class="search" placeholder="Buscar" onkeypress="keydown2(event,'funcion')"  />
        </form>
    </div>
    
    <div class="right">
        <select id="basic-usage-demo" class="select" onchange="filtrarcategorias()" style="height:38px">
       <option value="0">Selecciona Categoría</option>
        <?php foreach ($categorias as $item){?>
            <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
        <?php } ?>	
    </select>
    </div>    
</div>

    <h3 class="h3_line">Destacados</h3>
    <div class="box_a relative">
    
        <div id="cargador" style="text-align:center; margin-top:20px; margin-bottom:20px"><img src="<?php echo base_url(); ?>assets/img/load.gif" width="32" /></div>

        <div class="slides">
            <div class="slides_container">
              <div class="content clearfix">  
               <?php
               $cantidaddes = 0;
               foreach ($proyectosdestacados as $item){
               $cantidaddes++;    
               }
               $j=1;
               foreach ($proyectosdestacados as $item){?>
                  <div class="project_box">
                  		
                        <!-- BANDAS -->
                        <?php if($item->exitoso == 1){ echo '<div class="banda bnd1">Exitoso</div>';} ?>
                        <?php if($item->urgente == 1){ echo '<div class="banda bnd2">Urgente</div>';} ?>
                        <?php if($item->estados_id == 3){ echo '<div class="banda bnd3">Cerrado</div>';} ?>
                        
                        
                        
                  
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="<?php echo base_url().'detalle_proyecto/index/'.$item->id ?>">
                                <img class="thumbnail_a" src="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $item->imagen_miniatura; ?>" />
                            </a>
                        </div>
                        <div class="cat"><?php echo $item->nombrecateglria;?></div>
                        <div class="title"> <a href="<?php echo base_url().'detalle_proyecto/index/'.$item->id ?>"><?php echo $item->titulo; ?></a></div>
                        <div class="des">
                         <?php echo $item->descripcion_corta; ?>
                        </div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by"><?php echo $item->fundacion; ?></li>
                                <li class="by">Departamento, País </li>
                                <li class="by"><?php echo $item->dias_para_cerrar;?> dias restantes</li>
                            </ul>
                        </div>
                        
                        <div class="pos_data">
                            <div class="line_div_project"></div>
                            
                            <div class="percent_icon_box first_precent">
                                <div class="money_icon_a <?php if($item->valormonetario < 20000){ echo 'ico_desactivado'; }?> icons"></div>
                                <div class="percent_circle <?php if($item->valormonetario < 20000){ echo 'ico_desactivado'; }?> "><?php echo $item->porcentaje_monetario;?>%</div>
                            </div>
                            <div class="percent_icon_box">
                                <div class="members_icon_a <?php if($item->cantidadvoluntariado == 0){ echo 'ico_desactivado'; }?> icons"></div>
                                <div class="percent_circle <?php if($item->cantidadvoluntariado == 0){ echo 'ico_desactivado'; }?>"><?php echo $item->porcentaje_voluntariado;?>%</div>
                            </div>
                            <div class="percent_icon_box">
                                <div class="shop_icon_a  <?php if($item->cantidadespecie == 0){ echo 'ico_desactivado'; }?> icons"></div>
                                <div class="percent_circle  <?php if($item->cantidadespecie == 0){ echo 'ico_desactivado'; }?>"><?php echo $item->porcentaje_especie;?>%</div>
                            </div>
                            <div class="clear"></div>
                            
                            <center><div style="font-size:12px; margin-bottom:4px">Porcentaje recaudado total</div></center>
                            <div class="barra_porcentaje">
                                <div class="bar" style="width:<?php echo $item->porcentaje_total;?>%"><?php echo $item->porcentaje_total;?>%</div>
                            </div>
                        </div>
                    </div>
               <?php 
               if($j % 4 == 0 && $cantidaddes >$j ){
                   echo '</div><div class="content clearfix">'; 
               }
               $j++;
               }?>
                
                </div><!-- // content  -->
                
            </div>
        </div>
    </div>
    
    <h3 class="h3_line">Todos</h3>
    <div class="box_a pad_box clearfix" style="width:960px">
    
    <div id="cargador" style="text-align:center; margin-top:0px; margin-bottom:20px"><img src="<?php echo base_url(); ?>assets/img/load.gif" width="32" /></div>
        
        <div id="projects_show">
                               
                  

            <?php 
               foreach ($proyectos as $item){?>
                  <div class="project_box">
                   		
                        <!-- BANDAS -->
                         <?php if($item->exitoso == 1){ echo '<div class="banda bnd1">Exitoso</div>';} ?>
                        <?php if($item->urgente == 1){ echo '<div class="banda bnd2">Urgente</div>';} ?>
                        <?php if($item->estados_id == 3){ echo '<div class="banda bnd3">Cerrado</div>';} ?>
                        
                        
                        
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="<?php echo base_url().'detalle_proyecto/index/'.$item->id ?>">
                                <img class="thumbnail_a" src="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $item->imagen_miniatura; ?>" />
                            </a>
                        </div>
                        <div class="cat"><?php echo $item->nombrecateglria;?></div>
                        <div class="title"> <a href="<?php echo base_url().'detalle_proyecto/index/'.$item->id ?>"><?php echo $item->titulo; ?></a></div>
                        <div class="des">
                         <?php echo $item->descripcion_corta; ?>
                        </div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by"><?php echo $item->fundacion; ?></li>
                                <li class="by">Departamento, País </li>
                                <li class="by"><?php echo $item->dias_para_cerrar;?> dias restantes</li>
                            </ul>
                        </div>
                        <div class="pos_data">
                        <div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                            <div class="money_icon_a  <?php if($item->valormonetario < 20000){ echo 'ico_desactivado'; }?> icons"></div>
                            <div class="percent_circle  <?php if($item->valormonetario < 20000){ echo 'ico_desactivado'; }?>"><?php echo $item->porcentaje_monetario;?>%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a icons <?php if($item->cantidadvoluntariado == 0){ echo 'ico_desactivado'; }?>"></div>
                            <div class="percent_circle <?php if($item->cantidadvoluntariado == 0){ echo 'ico_desactivado'; }?>"><?php echo $item->porcentaje_voluntariado;?>%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a <?php if($item->cantidadespecie == 0){ echo 'ico_desactivado'; }?> icons"></div>
                            <div class="percent_circle <?php if($item->cantidadespecie == 0){ echo 'ico_desactivado'; }?>"><?php echo $item->porcentaje_especie;?>%</div>
                        </div>
                        <div class="clear"></div>
                        
                            <center><div style="font-size:12px; margin-bottom:4px">Porcentaje recaudado total</div></center>
                        <div class="barra_porcentaje">
                            <div class="bar" style="width:<?php echo $item->porcentaje_total;?>%"><?php echo $item->porcentaje_total;?>%</div>
                        </div>
                        </div>
                        
                    </div>
               <?php } ?>
        </div>
        
        
        
        <div class="clear"></div>
        
        <h3 onclick="javascript:cargar_contenido()" id="btnvermas" class="h3_line more_projects">Ver más proyectos</h3>
        
        
        <div id="cargador" style="text-align:center; margin-top:20px;display:none;"><img src="<?php echo base_url(); ?>assets/img/load.gif" width="32" /></div>
        <input type="hidden" name="cantidadpro" id="cantidadpro" value="<?php echo $cantidad->cantidad; ?>">
        <input type="hidden" name="cantidadactual" id="cantidadactual" value="8">
        <div class="hidden_projects">
        
        </div>
        
    </div>
    
    
    <div class="wrapper relative">
        <ul class="rslides" id="slider3">
           <?php foreach ($banners_inferior as $item2){?>
        	<li><a href="<?php echo $item2->link; ?>" target="_blanck"><img src="../uploads/banner_home/new/<?php echo $item2->imagen; ?>" alt=""></a></li>
       	 <?php }?>
        </ul>
    </div>
    <script>
     function keydown2(e,s){
    if (!e) var e = window.event;
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    if (code==13){
        busca2r();	
    }
    }
    
    function buscar2(){
        var palabrabuscar = $('#palabrabuscar2').val();
        $('#formulariobusqueda2').submit();
        
    }
    
    
    function filtrarcategorias(){
        var idcategoria = $('#basic-usage-demo').val();
        location.href = '<?php echo base_url(); ?>resultados_busqueda/index/'+idcategoria+'/1';
    }
    
    function cargar_contenido(){
        $('#cargador').css('display','block');
        $('#btnvermas').css('display','none');
        var cantidadpro = $('#cantidadpro').val();
        var cantidadactual = $('#cantidadactual').val();
        $.post( "<?php echo base_url().'home/cargarproyectos'; ?>", { cantidadactual: cantidadactual })
        .done(function( data ) {
            $('#projects_show').append(data);
            $('#cantidadactual').val(parseInt(cantidadactual)+parseInt(8));
            $('#btnvermas').css('display','block');
            if(parseInt(cantidadpro) < parseInt($('#cantidadactual').val())){
                $('#btnvermas').css('display','none');
            }
             $(".imgLiquidFill").imgLiquid();
             $('#cargador').css('display','none');
        });
        
    }
    </script>
    <?php if(isset($cambiodecorreo)){?>
    <script>alert('Su correo ha sido cambiado satisfactoriamente. Puede ingresar con su nuevo correo y su clave.');</script>
    <?php } ?>
