<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo $nombremodulo ?></span>
        <br /><?php echo anchor('cms/footer/', 'Volver', 'class="uibutton icon special answer" style="float:right;position: relative;top: -5px"') ?>                 
    </div><!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div class="imu_info" id="info"></div>
                    <?php echo form_open_multipart('cms/footer/update_footer/', 'id="form"') ?>
                        <input style="width:250px" type="hidden"  id="id" name="id" class="small" value="<?php echo $info->id; ?>" />

                    <div class="section">
                        <label>Telefono 1</label>
                        <div><input value="<?php echo $info->telefono1; ?>"  class="large text" type="text"  id="telefono1" name="telefono1" /></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="telefono1"></span></span>
                        <script type="text/javascript">
                            $("#telefono1").limit("37",".telefono1");
                        </script>
                    </div>

                    <div class="section">
                        <label>Telefono 2</label>
                        <div><input value="<?php echo $info->telefono2; ?>"  class="large text" type="text"  id="telefono2" name="telefono2" /></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="telefono2"></span></span>
                        <script type="text/javascript">
                            $("#telefono2").limit("37",".telefono2");
                        </script>
                    </div>

                    <div class="section">
                        <label>Dirección</label>
                        <div><input value="<?php echo $info->direccion; ?>"  class="large text" type="text"  id="direccion" name="direccion" /></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="direccion"></span></span>
                        <script type="text/javascript">
                            $("#direccion").limit("37",".direccion");
                        </script>
                    </div>


                    <div class="section">
                        <label>Ciudad</label>
                        <div><input value="<?php echo $info->ciudad; ?>"  class="large text" type="text"  id="ciudad" name="ciudad" /></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="ciudad"></span></span>
                        <script type="text/javascript">
                            $("#ciudad").limit("37",".ciudad");
                        </script>
                    </div>

                    <div class="section">
                        <label>Pbx</label>
                        <div><input value="<?php echo $info->pbx; ?>"  class="large text" type="text"  id="pbx" name="pbx" /></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="pbx"></span></span>
                        <script type="text/javascript">
                            $("#pbx").limit("37",".pbx");
                        </script>
                    </div>

                    <div class="section">
                        <label>Correo</label>
                        <div><input value="<?php echo $info->correo; ?>"  class="large text" type="text"  id="correo" name="correo" /></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="correo"></span></span>
                        <script type="text/javascript">
                            $("#correo").limit("37",".correo");
                        </script>
                    </div>

                    <div class="section">
                        <label>Logo cort</label>
                        <label>Subir nueva imagen (86 px X 186 px)</label><br/><br/>
                        <div>
                            <img class="cuadro_edicion_fotos" id="img1"  src="<?php echo base_url()."uploads/footer/new/".$info->logocort; ?>" width="450">
                            <div class="cuadro_edicion_fotos" id="divimg1"></div>
                            <br />
                            <input style="border-radius: 5px; " type="file" name="nombre" id="fileUpload1" class="fileUpload2" />
                            <input type="hidden" name="imagen" id="imagen" />
                        </div>
                    </div>
                  
                    
                    <div style="width: 100px;height: 30px;position: relative;top: 30px">
                        <input onclick="valida();" type="button" value="Guardar" class="uibutton confirm" />
                        
                    </div>
                    <?php echo form_close() ?>
                    <p>&nbsp;</p>
                 </fieldset>
                <br>
            </div>
        </div>
    </div>	
</div><!-- End content -->
<script type="text/javascript">
    function valida(){
        var pinterest = $("#pinterest").val();
        var twitter = $("#twitter").val();
        var facebook = $("#facebook").val();
        var youtube = $("#youtube").val();

        if (youtube == "" || pinterest == "" || twitter == "" || facebook == ""){
            $('#info').focus();
            showError('Complete todos los campos.',3000);
        }else{
            $('#form').submit();
        }
    }
    $(document).ready(function() {
        var texto = '';
        var nextinput = parseInt($("#inputTotal").val());
        $('#fileUpload1').uploadify({
                    'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                    'script'            : '<?php echo site_url(); ?>' + 'cms/footer/upload/',
                    'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                    'auto'              : true,
                    'folder'            : '',
                    'queueSizeLimit'    : 3,
                    'multi'             : false,
                    'fileExt'           : '*.jpg;*.jpeg;*.png;*gif',
                    'auto'              : true,
                    'buttonText'        : 'Cargar imagen.',
                    'onComplete'  : function(event, queueId, fileObj, response, data) {
                var responseJson = $.parseJSON(response);
                
                if (responseJson.ok === true) {
                        $("#img1").attr('src','<?php echo base_url() ?>uploads/footer/'+responseJson.data.file_name);
                        $("#img1").hide(800).delay(2000).show(800);
                        $("#divimg1").html('<h2>Cargando imagen...</h2>')
                        $("#divimg1").show(850).delay(2000).hide(750);
                        $("#imagen").val(responseJson.data.file_name)
                } else {
                        showError('Problemas al carga imagen.',3000);                            
                }
            }
                });   
    
    });
</script>
 <script>
    <?php if (isset($update)){
            if ($update ==  1){?>
            showSuccess('Actualización correcta.',3000);
            <?php } ?>
    <?php } ?>
</script>