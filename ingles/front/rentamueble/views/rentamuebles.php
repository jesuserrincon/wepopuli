
	<style type="text/css">#nav-bt1 {color: #464646;} #nav-bt1 span {background-position:-48px -60px;}</style>


  <section>
    <div class="con-section">
      <div class="mg-section cfx">
        <h1 class="int-tt cfx">Rentamuebles</h1>
        <h2><?php echo $info->titulovision ?></h2>
        <p align="justify">
            <?php echo nl2br($info->vision) ?>
        </p>
        <div class="mg-renta-slider fl">
          <div class="sliderContainer fullWidth fr cfx">
            <div class="home-slider royalSlider heroSlider rsMinW">
            <?php foreach($slider as $item){ ?>
              <!--Slide-->
              <div class="rsContent" data-rsDelay="7000">
                <img class="rsImg" src="<?php echo base_url() ?>uploads/slider/new/<?php echo $item->imagen ?>" alt="" />
              </div>
                <?php } ?>
            </div>
          </div>
        </div>
        <h2><?php echo $info->titulomision ?></h2>
        <p align="justify">

            <?php echo nl2br($info->mision) ?>
        </p>

          <?php echo $info->texto ?>

          <!--<h2>Historía</h2>
        <p align="justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.</p>
        <h2>Lorem ipsum</h2>
        <p align="justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.</p>
        -->


          <h2>Nuestros clientes</h2>
        <div class="con-clients-slider">
          <ul class="clients-slider">
              <?php foreach($clientes as $item){ ?>
            <li><img src="<?php echo base_url() ?>uploads/clientes/new/<?php echo $item->imagen ?>" alt=""></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </section>
    