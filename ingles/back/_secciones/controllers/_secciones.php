<?php

class _Secciones extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "SECCIONES";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Secciones();
        $info = $b->getSecciones();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('secciones');
    }

    public function add() {
        $this->_data['nombremodulo'] = "CATEGORIAS / New";
        $this->build('secciones_new');
    }

    public function new_Secciones() {
        $datos = array(
            'titulo' => $this->input->post('titulo'),
            'texto' => $this->input->post('texto'),
            'imagen' => $this->input->post('imagen'),
            'tipo_video' => $this->input->post('tipo_video'),
            'video' => $this->input->post('video'),
            'ubicacion' => $this->input->post('ubicacion'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'secciones';
        $b = new Secciones();
            
            $this->_data['save'] = FALSE;
            $build = 'secciones_new';
            if ($b->saveSecciones($datos)){
                $build = 'secciones';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        
        $info = $b->getSecciones();
        $this->_data["info"] = $info;
        return redirect('cms/secciones/index/'.$save);
        
    }

    
    
    
    
    
    
    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'secciones/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."secciones/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('secciones/'.$data["data"]["file_name"], array('longside'=>'secciones/new','width' => 800, 'height' => 500,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Secciones();
        $dat = $b->getSeccionesById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("secciones_edit");
    }
    
    public function updateSecciones(){
        $b = new Secciones();
        //$texto2 = $this->input->post('texto');
        $datos = array(
            'id' => $this->input->post('id'),
            'titulo' => $this->input->post('titulo'),
            'texto' => $this->input->post('texto'),
            'tipo_video' => $this->input->post('tipo_video'),
            'video' => $this->input->post('video'),
            'ubicacion' => $this->input->post('ubicacion'),

        );

        $update = $b->updateSecciones($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateSecciones($datos);
        }
        
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/secciones/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Secciones();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/secciones/index/".$return);
    }

}

