<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo $nombremodulo ?></span>
        <br /><?php echo anchor('cms/rentamuebles/', 'Volver', 'class="uibutton icon special answer" style="float:right;position: relative;top: -5px"') ?>
    </div><!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div class="imu_info" id="info"></div>
                    <?php echo form_open_multipart('cms/rentamuebles/update_rentamuebles/', 'id="form"') ?>
                        <input style="width:250px" type="hidden"  id="id" name="id" class="small" value="<?php echo $info->id; ?>" />


                    <div class="section">
                        <label>Titulo Misión</label>
                        <div><input  class="large text" type="text" value="<?php echo $info->titulomision; ?>"  id="titulomision" name="titulomision" /></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="titulomision"></span></span>
                        <script type="text/javascript">
                            $("#titulomision").limit("55",".titulomision");
                        </script>
                    </div>

                    <div class="section">
                        <label>Misión</label>
                        <div><textarea  class="large text" type="text"  id="mision" name="mision" /><?php echo $info->mision; ?></textarea></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="mision"></span></span>
                        <script type="text/javascript">
                            $("#mision").limit("370",".mision");
                        </script>
                    </div>

                    <div class="section">
                        <label>Titulo Visión</label>
                        <div><input  class="large text" type="text" value="<?php echo $info->titulovision; ?>"  id="titulovision" name="titulovision" /></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="titulovision"></span></span>
                        <script type="text/javascript">
                            $("#titulovision").limit("111",".titulovision");
                        </script>
                    </div>

                    <div class="section">
                        <label>Visión</label>
                        <div><textarea  class="large text" type="text"  id="vision" name="vision" /><?php echo $info->vision; ?></textarea></div>
                        <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="vision"></span></span>
                        <script type="text/javascript">
                            $("#vision").limit("930",".vision");
                        </script>
                    </div>

                    <div class="section">
                        <label>Texto</label>
                        <div><textarea  class="large text" type="text"  id="texto" name="texto" /><?php echo $info->texto; ?></textarea></div>
                    </div>
                    
                    <div style="width: 100px;height: 30px;position: relative;top: 30px">
                        <input onclick="valida();" type="button" value="Guardar" class="uibutton confirm" />
                        
                    </div>
                    <?php echo form_close() ?>
                    <p>&nbsp;</p>
                 </fieldset>
                <br>
            </div>
        </div>
    </div>	
</div><!-- End content -->
<script type="text/javascript">
    $('#texto').cleditor();
    function valida(){
        var titulomision = $("#titulomision").val();
        var mision = $("#mision").val();
        var titulovision = $("#titulovision").val();
        var vision = $("#vision").val();

        if (titulomision == "" || mision == "" || titulovision == "" || vision == ""){
            $('#info').focus();
            showError('Complete todos los campos.',3000);
        }else{
            $('#form').submit();
        }
    }

    $(document).ready(function() {
        var texto = '';
        var nextinput = parseInt($("#inputTotal").val());
        $('#fileUpload1').uploadify({
                    'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                    'script'            : '<?php echo site_url(); ?>' + 'cms/rentamuebles/upload/',
                    'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                    'auto'              : true,
                    'folder'            : '',
                    'queueSizeLimit'    : 3,
                    'multi'             : false,
                    'fileExt'           : '*.jpg;*.jpeg;*.png;*gif',
                    'auto'              : true,
                    'buttonText'        : 'Cargar imagen.',
                    'onComplete'  : function(event, queueId, fileObj, response, data) {
                var responseJson = $.parseJSON(response);
                
                if (responseJson.ok === true) {
                        $("#img1").attr('src','<?php echo base_url() ?>uploads/rentamuebles/'+responseJson.data.file_name);
                        $("#img1").hide(800).delay(2000).show(800);
                        $("#divimg1").html('<h2>Cargando imagen...</h2>')
                        $("#divimg1").show(850).delay(2000).hide(750);
                        $("#imagen").val(responseJson.data.file_name)
                } else {
                        showError('Problemas al carga imagen.',3000);                            
                }
            }
                });   
    
    });
</script>
 <script>
    <?php if (isset($update)){
            if ($update ==  1){?>
            showSuccess('Actualización correcta.',3000);
            <?php } ?>
    <?php } ?>
</script>