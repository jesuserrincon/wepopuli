

<link href="<?php echo base_url(); ?>assets/css/jquery-filestyle.css" rel="stylesheet" />



<script src="<?php echo base_url(); ?>assets/js/jquery.tools.min.js"></script>
<script>
$(function() {
	$(".tool").tooltip();
});
</script>



<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
		$(document).ready(function() {
			

			$('.fancybox').fancybox();

			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			
			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

		
			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});
		});
</script>



<div class="txt_reglamento" style="margin-top:40px; padding-left:40px;">
  <h2 class="h2_b" style="font-size:44px">Mis <span>Proyectos</span></h2>
</div>

    
    
<div class="box_shadow_big clearfix">
  <div class="clear" style="height:30px"></div>
 
 <table border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td><div class="pg" style="margin-top:0px">
                            <?php 
                            $pagina = ($desde / 10)+1;
                            if($desde > 0){
                            ?>
                        <div class="left"><a href="<?php echo base_url().'misproyectos/index/'.($desde-10); ?>" class="prev2"></a></div>
                            <?php }?>
                            <div class="left num">
                            <?php for($i=0;$i<($cantidad/10);$i++){ 
                            	if($pagina == $i+1){
                            		$class = 'class="activo"';
                            	}else{
                            		$class = '';
                            	}
                                echo '<a href="'.base_url().'misproyectos/index/'.($i*10).'" '.$class.'>'.($i+1).'</a>';
                            }?>
                                </div>
                            <?php if(($desde+10)<$cantidad){?>
                            <div class="left"><a href="<?php echo base_url().'misproyectos/index/'.($desde+10); ?>" class="next2"></a></div>
                            <?php }?>
                        <div class="clear"></div>
                    </div></td>
              </tr>
            </table>
 
        <div class="clear" style="margin-bottom:20px; margin-top:20px"></div>
  <?php echo $html;?>

        <div class="pad_int_20">
    <div class="div_line_2"></div>
    <a href="<?php echo base_url(); ?>buscaapoyo" class="bt_green2" style="margin-top:20px">Crear nuevo Proyecto</a>
        <div class="div_line_2"></div>
       
       <table border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td><div class="pg">
                            <?php 
                            if($desde > 0){?>
                        <div class="left"><a href="<?php echo base_url().'misproyectos/index/'.($desde-10); ?>" class="prev2"></a></div>
                            <?php }?>
                            <div class="left num">
                            <?php for($i=0;$i<($cantidad/10);$i++){ 
                            	if($pagina == $i+1){
                            		$class = 'class="activo"';
                            	}else{
                            		$class = '';
                            	}
                                echo '<a href="'.base_url().'misproyectos/index/'.($i*10).'" '.$class.'>'.($i+1).'</a>';
                            }?>
                                </div>
                            <?php if(($desde+10)<$cantidad){?>
                            <div class="left"><a href="<?php echo base_url().'misproyectos/index/'.($desde+10); ?>" class="next2"></a></div>
                            <?php }?>
                        <div class="clear"></div>
                    </div></td>
              </tr>
            </table>
       
       
       
       
       <div class="spacer"></div>
	</div>	
    
</div>
    
    
<script type="text/javascript" src="<?php echo base_url(); ?><?php echo base_url(); ?>assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?><?php echo base_url(); ?>assets/js/aplication.js"></script>



<!-- //popup -->
<?php echo $inline;?>

<?php if($eliminar ==1){?>
<script>alert('Proyecto eliminado correctamente.');</script>
<?php } ?>
