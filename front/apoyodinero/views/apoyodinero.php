<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<link href="<?php echo base_url(); ?>assets/css/colombia_incluyente_movil.css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/apply.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/just_numbers_input.js"></script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "09422a6d-4673-4a62-ac1c-160a92973882", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancys.js"></script>
</head>

<body class="device_320">

<div class="clear"></div>
<div class="txt_reglamento device_300 pad_bottom_0_mobile pad_5_mobile">
  <h2 class="h2_b" style="font-size:44px"><?php echo $proyecto->titulo; ?></h2>
  <h2 class="h2_des">Proyecto por: <?php echo $gestor->nombre; ?></h2>
</div>

    
    
<div class="box_shadow_big clearfix device_320">

  <div class="col_e min_h_900 pad_int_20 device_300">
       <img src="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $proyecto->imagen_miniatura; ?>" class="rounded_img device_300" width="590" />
  	   <div class="spacer"></div>


	    <div class="icon_apoya" style="margin:0px auto; float:none"></div>
        
    <center>
        <h2 class="h2_b" style="margin-bottom:0px; color:#7f3f98">Gracias</h2>        
        <div class="clear"></div>
      <h3 class="h3_c" style="font-size:22px">Realiza tu contribución</h3>
         
         </center>

	<table border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center">
        <div class="contribuir_box">
            <div class="boxes_3">
            
            	<div class="cantidad" style="margin-bottom:5px">
                    <center><span style="line-height:none">Pesos Colombianos</span></center>
                </div>
                <div style="width:250px; margin:0px auto">
                    <div class="money left" style="margin-left:-18px">$</div>
                    <input type="text" id="cantidadadonar" class="input_a left" style="margin-bottom:0px" />
                </div>
                
                
            </div>
            <div class="clear" style="padding-left:30px; font-size:12px">
            Donación mínima: 20.000 mil pesos
            </div>
        </div>
        </td>
        </tr>
        <tr>
        <td style="padding:0px 30px; text-align:center">
		<div class="clear"></div>
                
        <h2 class="h2_azul device_320">¡Es hora de apoyar el cambio!</h2>
  		<div class="txt_14">Su contribución se sumará a la de muchas más personas que también creen y quieren un mundo mejor, más humano e inclusivo.</div>
		<p></p>
        <h2 class="h2_azul device_320">La donación se utilizará para:</h2>
  		<div class="txt_14"><?php echo $proyecto->comentarios_monetario; ?> </div>

        
       </td>
  </tr>
</table>
               <br /><br />
                <a class="boton_a" href="<?php echo base_url().'detalle_proyecto/index/'.$proyecto->id ;?>" style="display:block; float:left; margin-right:10px; margin-left:150px">Atrás</a>
                <a class="boton_a" onclick="aplica()" style="display:block; float:left;">Continuar</a>
        
    <div class="div_line_2 visible_mobile"></div>
  </div>
<div id="divId">

</div>
    
  
  
  
  
  
   <div class="col_f device_320">
    	
        <div class="logo_fundacion">
  <?php 
                    function image_exists($url) {
                        if(@getimagesize($url)){
                        return true;
                      }else{
                       return false;
                     }
                    }
                    $imagen = base_url().'uploads/proyectos/imagenes/'.$proyecto->imagen; 
                    $fundacionimagen = base_url().'uploads/proyectos/gestores/'.$gestor->imagen; 
                    $liderimagen = base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; 
                   ?>       
            <a href="#inline2" onclick="javascript:$('.fancybox').fancybox()"  class="fancybox">
                <img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/060.jpg'; }?>" width="200" />
            </a>
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Proyecto por:<br /> <a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Liderado por: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>
        
        <div class="state box_min_sh" style="padding:13px 0px">
        	<div class="estado left"><div class="active_point left"><span>Status:</span> Activo</div></div>
            <div class="clear" style="margin-bottom:5px;"></div>
            <?php 
             function dameFecha($fecha,$dia)
                {   list($day,$mon,$year) = explode('/',$fecha);
                    return date('d/m/Y',mktime(0,0,0,$mon,$day+$dia,$year));        
                }
            ?>
            <div class="estado"><span>Hasta:</span> <?php echo dameFecha(date('d/m/Y'),$proyecto->dias_para_cerrar); ?></div>
        </div>
        
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Porcentaje total</center></div>
            <div class="barra_porcentaje2">
                <div class="bar2" style="width:<?php echo $proyecto->porcentaje_total;?>%"><?php echo $proyecto->porcentaje_total;?>%</div>
            </div>

		</div>


        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Importante</h3>
            <div class="txt_box_min">
            	Wepopuli es un buscador de organizaciones y proyectos sociales, una plataforma de crowdsource social donde puedes encontrar las organizaciones y proyectos a nivel mundial que mejor se adapten a tus talentos y necesidades, teniendo en cuenta criterios como sector, localización y objetivos del proyecto, fechas, aptitudes solicitadas, etc., a las que podrás ayudar como voluntario de campo o desde tu casa u oficina, dependiendo de sus requerimientos.
			</div>
            <div class="line_color"></div>
		</div>
		
        <div class="spacer"></div>	
            
      <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Preguntas Frecuentes</h3>
            <div class="txt_box_min">
            	¿Cómo contribuyo a un proyecto? Cada proyecto tiene sus necesidades puntuales. Para verlas, ingresa como usuario registrado a la web y revisa las necesidades desplegadas por la organización que lidera el proyecto.</div>
            <div class="line_color color2"></div>
		</div>
    	<div class="spacer visible_mobile"></div>

    </div>
    
    
    <div class="div_line clear dis_none_movil"></div>
    
    <div class="pad_int_20">
    
    
    </div>

</div>
    


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/aplication.js"></script>



<!-- //popup -->

<div id="inline2" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px; color:#1c75bc">Fundación</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->nombre;?></h2>
    	<div class="spacer"></div>
    	<div class="txt_user">
            <p><?php echo $gestor->descripcion;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/060.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Proyecto por: <br /><a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Liderado por: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
	</div>
    
</div>

<div id="inline3" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px"><?php echo $gestor->nombreslider.' '.$gestor->apellidoslider;?></h2>
    	<!--<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->acercadelider;?></h2>-->
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px"><?php echo $gestor->nombre;?></h3>
    
    	<div class="txt_user">
            <p><?php echo $gestor->acercadelider;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($liderimagen)){ echo base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; }else{ echo 'img/080.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Información de contacto:</span></div>
            <?php  if($gestor->emaillider != ''){ echo '<div class="dato"><b>Email: <a href="mailto:'.$gestor->emaillider.'" style="color:#602483">'.$gestor->emaillider.'</a></b></div>'; } ?>
            <?php  if($gestor->facebooklider != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebooklider.'</b></div>'; } ?>
            <?php  if($gestor->twitterlider != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitterlider.'</b></div>'; } ?>
        </div>
	</div>
    
</div>
    
<script>
    function aplica(){
        var cantidadadonar = $('#cantidadadonar').val();
        if(cantidadadonar == ''){
            alert('Debe ingresar el valor de su donación.');
            return false;
        }
        if(cantidadadonar < 20000){
            alert('El monto ingresado debe ser mayor a $20.000 pesos.');
            return false;
        }
        location.href = '<?php echo base_url();?>apoyodinero/paso2/'+cantidadadonar+'/<?php echo $proyecto->id?>';
    }
</script>
</body>
</html>
