<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class revision extends Front_Controller {

    public function __construct() {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."textos/textos",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0) {
        $this->set_title('Bienvenidos a ' . SITENAME, true);

        parent::index($proyectoid);
        $vista = $this->build2('../../home/views/detalleProyectoCompleto');
        $this->_data["vista"] = $vista;

        $textosOBJ = new Textos();
        $texto = $textosOBJ->getTextosById(3);
        $this->_data["texto"] = $texto->texto;


        
        $this->_data["proyectoid"] = $proyectoid;
        return $this->build('revision');
    }
    
  
    
}
