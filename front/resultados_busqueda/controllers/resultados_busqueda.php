<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class resultados_busqueda extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."textos/textos",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($categoriaid = 0,$filtro = 0) {
        $this->set_title('Bienvenidos a ' . SITENAME, true);
        //Enviamos las categorias
             $categoriasOBJ = new Categorias();
             $results = $categoriasOBJ->getCategorias();
             $this->_data['categorias'] = $results;
             $this->_data['filtro'] = $filtro;
                $this->_data['busquedafiltro'] = 0;
        //Fin envio categorias
        switch ($filtro){
            case 1:
                //Hacemos la busqueda por palabra si existe
                $proyectosOBJ = new Proyectos();
                $this->_data['proyectos'] = $proyectosOBJ->getProyectosByCategoria($categoriaid,0);
                 $proyectosOBJ2 = new Proyectos();
                 $this->_data['cantidad'] = $proyectosOBJ2->getProyectosByCategoriaCOUNT($categoriaid);
                $categoriaOBJ = new Categorias();
                $nombrecat = $categoriaOBJ->getCategoriasById($categoriaid);
                //Fin busqueda por palabra
                $this->_data['tituloresultado'] = 'Categoría: '.$nombrecat->nombre;
                $this->_data['categoria'] = $nombrecat;
                $this->_data['busquedafiltro'] = $categoriaid;
            break;
            case 2:
                 $palabrabuscar = $this->input->post('palabrabuscar');
                 if($this->input->post('palabrabuscar') == ''){
                        redirect('home/index/');
                 }
                 //Hacemos la busqueda por palabra si existe
                 $proyectosOBJ = new Proyectos();
                 $this->_data['proyectos'] = $proyectosOBJ->getProyectosByPalabra($palabrabuscar,0);
                 $proyectosOBJ2 = new Proyectos();
                 $this->_data['cantidad'] = $proyectosOBJ2->getProyectosByPalabraCOUNT($palabrabuscar);
                 //Fin busqueda por palabra
                 $this->_data['tituloresultado'] = 'Resultados para: '.$palabrabuscar;
                $this->_data['busquedafiltro'] = $palabrabuscar;
            break;
            
            case 3:
                 //Hacemos la consulta de los 12 proyectos más apoyados
                 $proyectosOBJ = new Proyectos();
                 $this->_data['proyectos'] = $proyectosOBJ->getProyectosMasapoyados(0);
                 $proyectosOBJ2 = new Proyectos();
                 $this->_data['cantidad'] = $proyectosOBJ2->getProyectosMasapoyadosCOUNT();
                 //Fin busqueda por palabra
                 $this->_data['tituloresultado'] = 'Más apoyados';
            break;
            case 4:
                 //Hacemos la busqueda de los proyectos exitosos
                 $proyectosOBJ = new Proyectos();
                 $this->_data['proyectos'] = $proyectosOBJ->getProyectosExitosos(0);
                 $proyectosOBJ2 = new Proyectos();
                 $this->_data['cantidad'] = $proyectosOBJ2->getProyectosExitososCOUNT();
                 //Fin busqueda proyectos exitosos
                 $this->_data['tituloresultado'] = 'Exitosos';
            break;
            case 5:
                 $palabrabuscar = $this->input->post('palabrabuscar');
                 //Hacemos la busqueda por palabra si existe
                 $proyectosOBJ = new Proyectos();
                 $this->_data['proyectos'] = $proyectosOBJ->getProyectosUrgentes(0);
                 $proyectosOBJ2 = new Proyectos();
                 $this->_data['cantidad'] = $proyectosOBJ2->getProyectosUrgentesCOUNT();
                 //Fin busqueda por palabra
                 $this->_data['tituloresultado'] = 'Urgentes';
            break;
            case 6:
                 $tags = $this->input->post('tags');
                 //Hacemos la busqueda por palabra si existe
                 $proyectosOBJ = new Proyectos();
                 $this->_data['proyectos'] = $proyectosOBJ->getProyectosByTags($tags,0);
                 $proyectosOBJ2 = new Proyectos();
                 $this->_data['cantidad'] = $proyectosOBJ2->getProyectosByTagsCOUNT($tags);
                 //Fin busqueda por palabra
                 $this->_data['tituloresultado'] = $tags;
                $this->_data['busquedafiltro'] = $tags;
            break;
            
        }
        return $this->build('resultados_busqueda');
    }
    
    public function cargarproyectosPALABRA(){
           $palabra = $this->input->post('busquedafiltro');
           $cantidadactual = $this->input->post('cantidadactual');
           //Creamos el objeto y cargamos los proyectos 
           $proyectoOBJ = new Proyectos();
           $proyectos = $proyectoOBJ->getProyectosByPalabra($palabra,$cantidadactual);
           //Fin proyecyos 
            $html = '';
            foreach ($proyectos as $item){
                $exitoso = '';
                $urgente = '';
                $cerrado = '';
                if($item->exitoso == 1){ $exitoso = '<div class="banda bnd1">Exitoso</div>';}
                if($item->urgente == 1){ $urgente = '<div class="banda bnd2">Urgente</div>';}
                if($item->estados_id == 3){ $cerrado = '<div class="banda bnd3">Cerrado</div>';}
                
                if($item->valormonetario < 20000){ $desmon = 'ico_desactivado'; }else{ $desmon = ''; }
                if($item->cantidadvoluntariado == 0){ $desvol = 'ico_desactivado'; }else{ $desvol = ''; }
                if($item->cantidadespecie == 0){ $desesp = 'ico_desactivado'; }else{ $desesp = ''; }
                 
                $html .= ' <div class="project_box">
                         '.$exitoso.'
                         '.$urgente.'
                         '.$cerrado.'    
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">
                                <img class="thumbnail_a" src="'.base_url().'uploads/proyectominiatura/'.$item->imagen_miniatura.'" />
                            </a>
                        </div>
                        <div class="cat">'.$item->nombrecateglria.'</div>
                        <div class="title"><a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">'.$item->titulo.' </a></div>
                        <div class="des">'.$item->descripcion_corta.'</div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by">'.$item->fundacion.' </li>
                                <li class="by">Departamento, País </li>
                                <li class="by">'.$item->dias_para_cerrar.' dias restantes</li>
                            </ul>
                        </div>
                        <div class="pos_data">
                        <div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                            <div class="money_icon_a '.$desmon.' icons"></div>
                            <div class="percent_circle '.$desmon.'">'.$item->porcentaje_monetario.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a '.$desvol.' icons"></div>
                            <div class="percent_circle '.$desvol.'">'.$item->porcentaje_voluntariado.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a '.$desesp.' icons"></div>
                            <div class="percent_circle '.$desesp.'">'.$item->porcentaje_especie.'%</div>
                        </div>
                        <div class="clear"></div>
                        
                            <center><div style="font-size:12px; margin-bottom:4px">Total recibido</div></center>
                        <div class="barra_porcentaje">
                            <div class="bar" style="width:'.$item->porcentaje_total.'%">'.$item->porcentaje_total.'%</div>
                        </div>
                        </div>
                        
                    </div>';
              }
            echo $html;
    }
    
    public function cargarproyectosCATEGORIA(){
           $cantidadactual = $this->input->post('cantidadactual');
           $idcategoria = $this->input->post('busquedafiltro');
           //Creamos el objeto y cargamos los proyectos 
           $proyectoOBJ = new Proyectos();
           $proyectos = $proyectoOBJ->getProyectosByCategoria($idcategoria,$cantidadactual);
           //Fin proyecyos 
            $html = '';
            foreach ($proyectos as $item){
                $exitoso = '';
                $urgente = '';
                $cerrado = '';
                if($item->exitoso == 1){ $exitoso = '<div class="banda bnd1">Exitoso</div>';}
                if($item->urgente == 1){ $urgente = '<div class="banda bnd2">Urgente</div>';}
                if($item->estados_id == 3){ $cerrado = '<div class="banda bnd3">Cerrado</div>';}
                
                if($item->valormonetario < 20000){ $desmon = 'ico_desactivado'; }else{ $desmon = ''; }
                if($item->cantidadvoluntariado == 0){ $desvol = 'ico_desactivado'; }else{ $desvol = ''; }
                if($item->cantidadespecie == 0){ $desesp = 'ico_desactivado'; }else{ $desesp = ''; }
                 
                $html .= ' <div class="project_box">
                         '.$exitoso.'
                         '.$urgente.'
                         '.$cerrado.'    
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">
                                <img class="thumbnail_a" src="'.base_url().'uploads/proyectominiatura/'.$item->imagen_miniatura.'" />
                            </a>
                        </div>
                        <div class="cat">'.$item->nombrecateglria.'</div>
                        <div class="title"><a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">'.$item->titulo.' </a></div>
                        <div class="des">'.$item->descripcion_corta.'</div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by">'.$item->fundacion.' </li>
                                <li class="by">Departamento, País </li>
                                <li class="by">'.$item->dias_para_cerrar.' dias restantes</li>
                            </ul>
                        </div>
                        <div class="pos_data">
                        <div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                            <div class="money_icon_a '.$desmon.' icons"></div>
                            <div class="percent_circle '.$desmon.'">'.$item->porcentaje_monetario.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a '.$desvol.' icons"></div>
                            <div class="percent_circle '.$desvol.'">'.$item->porcentaje_voluntariado.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a '.$desesp.' icons"></div>
                            <div class="percent_circle '.$desesp.'">'.$item->porcentaje_especie.'%</div>
                        </div>
                        <div class="clear"></div>
                        
                            <center><div style="font-size:12px; margin-bottom:4px">Total recibido</div></center>
                        <div class="barra_porcentaje">
                            <div class="bar" style="width:'.$item->porcentaje_total.'%">'.$item->porcentaje_total.'%</div>
                        </div>
                        </div>
                        
                    </div>';
              }
            echo $html;
    }
    public function cargarproyectosAPOYADOS(){
           $cantidadactual = $this->input->post('cantidadactual');
           //Creamos el objeto y cargamos los proyectos 
           $proyectoOBJ = new Proyectos();
           $proyectos = $proyectoOBJ->getProyectosMasapoyados($cantidadactual);
           //Fin proyecyos 
            $html = '';
            foreach ($proyectos as $item){
                $exitoso = '';
                $urgente = '';
                $cerrado = '';
                if($item->exitoso == 1){ $exitoso = '<div class="banda bnd1">Exitoso</div>';}
                if($item->urgente == 1){ $urgente = '<div class="banda bnd2">Urgente</div>';}
                if($item->estados_id == 3){ $cerrado = '<div class="banda bnd3">Cerrado</div>';}
                
                if($item->valormonetario < 20000){ $desmon = 'ico_desactivado'; }else{ $desmon = ''; }
                if($item->cantidadvoluntariado == 0){ $desvol = 'ico_desactivado'; }else{ $desvol = ''; }
                if($item->cantidadespecie == 0){ $desesp = 'ico_desactivado'; }else{ $desesp = ''; }
                 
                $html .= ' <div class="project_box">
                         '.$exitoso.'
                         '.$urgente.'
                         '.$cerrado.'    
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">
                                <img class="thumbnail_a" src="'.base_url().'uploads/proyectominiatura/'.$item->imagen_miniatura.'" />
                            </a>
                        </div>
                        <div class="cat">'.$item->nombrecateglria.'</div>
                        <div class="title"><a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">'.$item->titulo.' </a></div>
                        <div class="des">'.$item->descripcion_corta.'</div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by">'.$item->fundacion.' </li>
                                <li class="by">Departamento, País </li>
                                <li class="by">'.$item->dias_para_cerrar.' dias restantes</li>
                            </ul>
                        </div>
                        <div class="pos_data">
                        <div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                            <div class="money_icon_a '.$desmon.' icons"></div>
                            <div class="percent_circle '.$desmon.'">'.$item->porcentaje_monetario.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a '.$desvol.' icons"></div>
                            <div class="percent_circle '.$desvol.'">'.$item->porcentaje_voluntariado.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a '.$desesp.' icons"></div>
                            <div class="percent_circle '.$desesp.'">'.$item->porcentaje_especie.'%</div>
                        </div>
                        <div class="clear"></div>
                        
                            <center><div style="font-size:12px; margin-bottom:4px">Total recibido</div></center>
                        <div class="barra_porcentaje">
                            <div class="bar" style="width:'.$item->porcentaje_total.'%">'.$item->porcentaje_total.'%</div>
                        </div>
                        </div>
                        
                    </div>';
              }
            echo $html;
    }
    
    
    public function cargarproyectosEXITOSOS(){
           $cantidadactual = $this->input->post('cantidadactual');
           //Creamos el objeto y cargamos los proyectos 
           $proyectoOBJ = new Proyectos();
           $proyectos = $proyectoOBJ->getProyectosUrgentes($cantidadactual);
           //Fin proyecyos 
            $html = '';
            foreach ($proyectos as $item){
                $exitoso = '';
                $urgente = '';
                $cerrado = '';
                if($item->exitoso == 1){ $exitoso = '<div class="banda bnd1">Exitoso</div>';}
                if($item->urgente == 1){ $urgente = '<div class="banda bnd2">Urgente</div>';}
                if($item->estados_id == 3){ $cerrado = '<div class="banda bnd3">Cerrado</div>';}
                
                if($item->valormonetario < 20000){ $desmon = 'ico_desactivado'; }else{ $desmon = ''; }
                if($item->cantidadvoluntariado == 0){ $desvol = 'ico_desactivado'; }else{ $desvol = ''; }
                if($item->cantidadespecie == 0){ $desesp = 'ico_desactivado'; }else{ $desesp = ''; }
                 
                $html .= ' <div class="project_box">
                         '.$exitoso.'
                         '.$urgente.'
                         '.$cerrado.'    
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">
                                <img class="thumbnail_a" src="'.base_url().'uploads/proyectominiatura/'.$item->imagen_miniatura.'" />
                            </a>
                        </div>
                        <div class="cat">'.$item->nombrecateglria.'</div>
                        <div class="title"><a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">'.$item->titulo.' </a></div>
                        <div class="des">'.$item->descripcion_corta.'</div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by">'.$item->fundacion.' </li>
                                <li class="by">Departamento, Pais </li>
                                <li class="by">'.$item->dias_para_cerrar.' dias restantes</li>
                            </ul>
                        </div>
                        <div class="pos_data">
                        <div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                            <div class="money_icon_a '.$desmon.' icons"></div>
                            <div class="percent_circle '.$desmon.'">'.$item->porcentaje_monetario.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a '.$desvol.' icons"></div>
                            <div class="percent_circle '.$desvol.'">'.$item->porcentaje_voluntariado.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a '.$desesp.' icons"></div>
                            <div class="percent_circle '.$desesp.'">'.$item->porcentaje_especie.'%</div>
                        </div>
                        <div class="clear"></div>
                        
                            <center><div style="font-size:12px; margin-bottom:4px">Total recibido</div></center>
                        <div class="barra_porcentaje">
                            <div class="bar" style="width:'.$item->porcentaje_total.'%">'.$item->porcentaje_total.'%</div>
                        </div>
                        </div>
                        
                    </div>';
              }
            echo $html;
    }
    
    
    public function cargarproyectosURGENTES(){
           $cantidadactual = $this->input->post('cantidadactual');
           //Creamos el objeto y cargamos los proyectos 
           $proyectoOBJ = new Proyectos();
           $proyectos = $proyectoOBJ->getProyectosMasapoyados($cantidadactual);
           //Fin proyecyos 
            $html = '';
            foreach ($proyectos as $item){
                $exitoso = '';
                $urgente = '';
                $cerrado = '';
                if($item->exitoso == 1){ $exitoso = '<div class="banda bnd1">Exitoso</div>';}
                if($item->urgente == 1){ $urgente = '<div class="banda bnd2">Urgente</div>';}
                if($item->estados_id == 3){ $cerrado = '<div class="banda bnd3">Cerrado</div>';}
                
                if($item->valormonetario < 20000){ $desmon = 'ico_desactivado'; }else{ $desmon = ''; }
                if($item->cantidadvoluntariado == 0){ $desvol = 'ico_desactivado'; }else{ $desvol = ''; }
                if($item->cantidadespecie == 0){ $desesp = 'ico_desactivado'; }else{ $desesp = ''; }
                 
                $html .= ' <div class="project_box">
                         '.$exitoso.'
                         '.$urgente.'
                         '.$cerrado.'    
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">
                                <img class="thumbnail_a" src="'.base_url().'uploads/proyectominiatura/'.$item->imagen_miniatura.'" />
                            </a>
                        </div>
                        <div class="cat">'.$item->nombrecateglria.'</div>
                        <div class="title"><a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">'.$item->titulo.' </a></div>
                        <div class="des">'.$item->descripcion_corta.'</div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by">'.$item->fundacion.' </li>
                                <li class="by">Departamento, Pais </li>
                                <li class="by">'.$item->dias_para_cerrar.' dias restantes</li>
                            </ul>
                        </div>
                        <div class="pos_data">
                        <div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                            <div class="money_icon_a '.$desmon.' icons"></div>
                            <div class="percent_circle '.$desmon.'">'.$item->porcentaje_monetario.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a '.$desvol.' icons"></div>
                            <div class="percent_circle '.$desvol.'">'.$item->porcentaje_voluntariado.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a '.$desesp.' icons"></div>
                            <div class="percent_circle '.$desesp.'">'.$item->porcentaje_especie.'%</div>
                        </div>
                        <div class="clear"></div>
                        
                            <center><div style="font-size:12px; margin-bottom:4px">Total recibido</div></center>
                        <div class="barra_porcentaje">
                            <div class="bar" style="width:'.$item->porcentaje_total.'%">'.$item->porcentaje_total.'%</div>
                        </div>
                        </div>
                        
                    </div>';
              }
            echo $html;
    }

    public function cargarproyectosTAGS(){
        
           $tags = $this->input->post('busquedafiltro');
           $cantidadactual = $this->input->post('cantidadactual');
           //Creamos el objeto y cargamos los proyectos 
           $proyectoOBJ = new Proyectos();
           $proyectos = $proyectoOBJ->getProyectosByTags($tags,$cantidadactual);
           //Fin proyecyos 
            $html = '';
            foreach ($proyectos as $item){
                $exitoso = '';
                $urgente = '';
                $cerrado = '';
                if($item->exitoso == 1){ $exitoso = '<div class="banda bnd1">Exitoso</div>';}
                if($item->urgente == 1){ $urgente = '<div class="banda bnd2">Urgente</div>';}
                if($item->estados_id == 3){ $cerrado = '<div class="banda bnd3">Cerrado</div>';}
                
                if($item->valormonetario < 20000){ $desmon = 'ico_desactivado'; }else{ $desmon = ''; }
                if($item->cantidadvoluntariado == 0){ $desvol = 'ico_desactivado'; }else{ $desvol = ''; }
                if($item->cantidadespecie == 0){ $desesp = 'ico_desactivado'; }else{ $desesp = ''; }
                 
                $html .= ' <div class="project_box">
                         '.$exitoso.'
                         '.$urgente.'
                         '.$cerrado.'    
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">
                                <img class="thumbnail_a" src="'.base_url().'uploads/proyectominiatura/'.$item->imagen_miniatura.'" />
                            </a>
                        </div>
                        <div class="cat">'.$item->nombrecateglria.'</div>
                        <div class="title"><a href="'. base_url().'detalle_proyecto/index/'.$item->id.' ">'.$item->titulo.' </a></div>
                        <div class="des">'.$item->descripcion_corta.'</div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by">'.$item->fundacion.' </li>
                                <li class="by">Departamento, Pais </li>
                                <li class="by">'.$item->dias_para_cerrar.' dias restantes</li>
                            </ul>
                        </div>
                        <div class="pos_data">
                        <div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                            <div class="money_icon_a '.$desmon.' icons"></div>
                            <div class="percent_circle '.$desmon.'">'.$item->porcentaje_monetario.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a '.$desvol.' icons"></div>
                            <div class="percent_circle '.$desvol.'">'.$item->porcentaje_voluntariado.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a '.$desesp.' icons"></div>
                            <div class="percent_circle '.$desesp.'">'.$item->porcentaje_especie.'%</div>
                        </div>
                        <div class="clear"></div>
                        
                            <center><div style="font-size:12px; margin-bottom:4px">Total recibido</div></center>
                        <div class="barra_porcentaje">
                            <div class="bar" style="width:'.$item->porcentaje_total.'%">'.$item->porcentaje_total.'%</div>
                        </div>
                        </div>
                        
                    </div>';
              }
            echo $html;
    }
  
    
}
