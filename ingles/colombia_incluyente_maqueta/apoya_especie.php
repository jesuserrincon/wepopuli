<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<link href="assets/css/colombia_incluyente_movil.css" rel="stylesheet" />
<?php include"metadata.php" ?>  

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
<script type="text/javascript" src="assets/js/apply.js"></script>
<script type="text/javascript" src="assets/js/just_numbers_input.js"></script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "09422a6d-4673-4a62-ac1c-160a92973882", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<script type="text/javascript" src="assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="assets/js/fancys.js"></script>
</head>

<body class="device_320">
<?php include"header.php" ?>
<div class="clear"></div>
<div class="txt_reglamento device_300 pad_bottom_0_mobile pad_5_mobile">
  <h2 class="h2_b" style="font-size:44px">Proyecto X</h2>
  <h2 class="h2_des">Por: Fundación Píes descalzos</h2>
</div>

    
    
<div class="box_shadow_big clearfix device_320">

  <div class="col_e min_h_900 pad_int_20 device_300">
       <img src="assets/img/058.jpg" class="rounded_img device_300" width="590" />
  	   <div class="spacer"></div>
       
       <h2 class="h2_c left">Adelante! <br /><span class="h2_c_span">Realice su contribución en Especie</span></h2>
  		
        <div class="icon_apoya icon_apoya_2"></div>
        <div class="clear"></div>
        
        <div class="div_line_2 margin_bottom_20_mobile visible_mobile"></div>
        
        <h2 class="h2_azul device_320">Es hora de apoyar el cambio en Colombia</h2>
  		<div class="txt_14">Su contribución se sumará a la de muchos colombianos más que también creen y quieren una Colombia mejor, más humana e inclusiva.</div>
  		<div class="spacer pad_top_10_mobile" style="padding-top:20px;"></div>
       
        <div class="div_line_2"></div>
        
        <div class="spacer"></div>
        
    	<h2 class="h2_c left">Ayudas:</h2>
       <div class="clear"></div>
       
       	<div class="box_shadow3 relative">
        	<div class="pad_box_10">
            	
                <a href="#" class="no_apply left">Aplicar</a>
                
                <div class="clear"></div>
            	<h2 class="h3_b">Materiales de construcción</h2>
                <div class="table_ayuda_div">
                  <table class="table_ayuda" width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="6%" align="center" valign="middle">1#</td>
                            <td width="4%" align="center" valign="middle">3</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                          </tr>


                        <tr>
                            <td width="6%" align="center" valign="middle">2#</td>
                            <td width="4%" align="center" valign="middle">5</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                            <td width="6%" align="center" valign="middle">6#</td>
                            <td width="4%" align="center" valign="middle">3</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                            <td width="6%" align="center" valign="middle">1#</td>
                            <td width="4%" align="center" valign="middle">3</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                                                    <td width="6%" align="center" valign="middle">1#</td>
                                                    <td width="4%" align="center" valign="middle">3</td>
                                                    <td width="9%" align="center" valign="middle">TN</td>
                                                    <td width="65%" valign="middle">Arroz Blanco</td>
                                                    <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                                                    <td width="6%" align="center" valign="middle">1#</td>
                                                    <td width="4%" align="center" valign="middle">3</td>
                                                    <td width="9%" align="center" valign="middle">TN</td>
                                                    <td width="65%" valign="middle">Arroz Blanco</td>
                                                    <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                                                    <td width="6%" align="center" valign="middle">1#</td>
                                                    <td width="4%" align="center" valign="middle">3</td>
                                                    <td width="9%" align="center" valign="middle">TN</td>
                                                    <td width="65%" valign="middle">Arroz Blanco</td>
                                                    <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  
				</table>
              </div>
              
              <div class="paquete_valor">
              	<h3 class="h3_purpure">Valor del paquete:</h3>
                <h2 class="h2_verde">$1.000.000 <span>(cop)</span></h2>
              </div>
              <div class="clear"></div>
              
        	</div>
            
            <div class="active_box_green"></div>
        </div>
        
        <div class="box_shadow3 relative">
        	<div class="pad_box_10">
            	
                <a href="#" class="no_apply left">Aplicar</a>
                
                <div class="clear"></div>
            	<h2 class="h3_b">Materiales de construcción</h2>
                <div class="table_ayuda_div">
                  <table class="table_ayuda" width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="6%" align="center" valign="middle">1#</td>
                            <td width="4%" align="center" valign="middle">3</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                          </tr>


                        <tr>
                            <td width="6%" align="center" valign="middle">2#</td>
                            <td width="4%" align="center" valign="middle">5</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                            <td width="6%" align="center" valign="middle">6#</td>
                            <td width="4%" align="center" valign="middle">3</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                            <td width="6%" align="center" valign="middle">1#</td>
                            <td width="4%" align="center" valign="middle">3</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                                                    <td width="6%" align="center" valign="middle">1#</td>
                                                    <td width="4%" align="center" valign="middle">3</td>
                                                    <td width="9%" align="center" valign="middle">TN</td>
                                                    <td width="65%" valign="middle">Arroz Blanco</td>
                                                    <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                                                    <td width="6%" align="center" valign="middle">1#</td>
                                                    <td width="4%" align="center" valign="middle">3</td>
                                                    <td width="9%" align="center" valign="middle">TN</td>
                                                    <td width="65%" valign="middle">Arroz Blanco</td>
                                                    <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                                                    <td width="6%" align="center" valign="middle">1#</td>
                                                    <td width="4%" align="center" valign="middle">3</td>
                                                    <td width="9%" align="center" valign="middle">TN</td>
                                                    <td width="65%" valign="middle">Arroz Blanco</td>
                                                    <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  
				</table>
              </div>
              
              <div class="paquete_valor">
              	<h3 class="h3_purpure">Valor del paquete:</h3>
                <h2 class="h2_verde">$1.000.000 <span>(cop)</span></h2>
              </div>
              <div class="clear"></div>
              
        	</div>
            
            <div class="active_box_green"></div>
        </div>
        
        <div class="box_shadow3 relative">
        	<div class="pad_box_10">
            	
                <a href="#" class="no_apply left">Aplicar</a>
                
                <div class="clear"></div>
            	<h2 class="h3_b">Materiales de construcción</h2>
                <div class="table_ayuda_div">
                  <table class="table_ayuda" width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="6%" align="center" valign="middle">1#</td>
                            <td width="4%" align="center" valign="middle">3</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                          </tr>


                        <tr>
                            <td width="6%" align="center" valign="middle">2#</td>
                            <td width="4%" align="center" valign="middle">5</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                            <td width="6%" align="center" valign="middle">6#</td>
                            <td width="4%" align="center" valign="middle">3</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                            <td width="6%" align="center" valign="middle">1#</td>
                            <td width="4%" align="center" valign="middle">3</td>
                            <td width="9%" align="center" valign="middle">TN</td>
                            <td width="65%" valign="middle">Arroz Blanco</td>
                            <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                                                    <td width="6%" align="center" valign="middle">1#</td>
                                                    <td width="4%" align="center" valign="middle">3</td>
                                                    <td width="9%" align="center" valign="middle">TN</td>
                                                    <td width="65%" valign="middle">Arroz Blanco</td>
                                                    <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                                                    <td width="6%" align="center" valign="middle">1#</td>
                                                    <td width="4%" align="center" valign="middle">3</td>
                                                    <td width="9%" align="center" valign="middle">TN</td>
                                                    <td width="65%" valign="middle">Arroz Blanco</td>
                                                    <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  

                        <tr>
                                                    <td width="6%" align="center" valign="middle">1#</td>
                                                    <td width="4%" align="center" valign="middle">3</td>
                                                    <td width="9%" align="center" valign="middle">TN</td>
                                                    <td width="65%" valign="middle">Arroz Blanco</td>
                                                    <td width="16%" valign="middle">Roa</td>
                    </tr>
                                                  
				</table>
              </div>
              
              <div class="paquete_valor">
              	<h3 class="h3_purpure">Valor del paquete:</h3>
                <h2 class="h2_verde">$1.000.000 <span>(cop)</span></h2>
              </div>
              <div class="clear"></div>
              
        	</div>
            
            <div class="active_box_green"></div>
        </div>
       
        <a href="apoya_especie_paso2.php" class="bt_green">Continuar</a>
        
        <div class="div_line_2 visible_mobile"></div>
  </div>
  
  
  
  
  
    <div class="col_f device_320">
    	
        <div class="logo_fundacion">
        	<img src="assets/img/060.jpg" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato"><b>Por:</b> Fundación Píes descalzos</div>
            <div class="dato"><b>Liderado por:</b> <a href="#inline2"  class="fancybox"  style="color:#27aae1">Rodrigo Romero  Castro</a></div>
            <div class="dato"><b>www.website1.com</div>
            <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
            <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
            <div class="dato"><b>Bogotá, Colombia</div>
        </div>
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Porcentaje total</center></div>
            <div class="barra_porcentaje2">
                <div class="bar2" style="width:25%">25%</div>
            </div>

		</div>
        <div class="spacer"></div>

        <div class="div_line"></div>
        <div class="spacer"></div>
        
        <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">IMPORTANT</h3>
            <div class="txt_box_min">
            	Colombia Incluyente does not guarantee projects or investigate a creator's ability to complete their project. It is the responsibility of the project creator to complete their project as promised, and the claims of this project are theirs alone.
			</div>
            <div class="line_color"></div>
		</div>
		
        <div class="spacer"></div>	
            
      <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Preguntas Frecuentes</h3>
            <div class="txt_box_min">
            	<h4 class="h4_pregunta"><a href="#">¿Cómo puedo apoyar un proyecto?</a></h4>
            	Colombia Incluyente does not guarantee projects or investigate a creator's ability to complete their project. It is the responsibility of the project creator to complete their project as promised, and the claims of this project are theirs alone.
			</div>
            <div class="line_color color2"></div>
		</div>


        <div class="spacer"></div>
        <div class="spacer"></div>

    </div>
    
    
    <div class="div_line clear dis_none_movil"></div>
    
    <div class="pad_int_20">
    
    
    </div>

</div>
    


<script type="text/javascript" src="assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="assets/js/aplication.js"></script>

<?php include"footer.php" ?> 

<!-- //popup -->
<div id="inline2" style="width:650px;display:none;"  class="device_250">
	<div class="device_250">
        <div class="right" style="width:270px">    
            <div class="logo_fundacion">
                <img src="assets/img/080.png" class="rounded_img" width="200" />
            </div>
            <div class="data_fundacion">
                <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Contactame en:</span></div>
                <div class="dato"><b>Email:</b> <a href="mailto:info@miemail.es" style="color:#602483"> info@romerorodrigo.com</a></div>
                <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
                <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
                <div class="dato"><b>Bogotá, Colombia</div>
            </div>
        </div>
        
        <div class="left col_line_right device_230" style="width:350px;">
            <h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px">Lider</h2>
            <h2 class="h2_b" style="color:#662d91; margin-bottom:5px;">Rodrigo Romero Castro</h2>
            <h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px">Fundación pies descalzos</h3>
        
            <div class="txt_user">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent purus turpis, tincidunt in malesuada ut, mattis eget purus. Vivamus semper fringilla nunc, iaculis ultricies nibh scelerisque eget. Nam tellus orci, elementum et erat at, sollicitudin ultrices mauris. Maecenas id purus at nisi eleifend dignissim. Duis non mi id nisi venenatis ornare. Donec suscipit enim eget mauris pharetra, venenatis commodo elit congue. Maecenas non eleifend libero, non interdum nisi. Nunc a lorem ornare, volutpat nisi sed, molestie eros. Nam in interdum libero. Donec hendrerit vitae dui ut imperdiet.</p>
                <p>Vestibulum tempus id ante a eleifend. Morbi at elementum erat. Nam in nulla ac ante fermentum ultrices quis nec sem. Donec ultricies in est et dignissim. Donec sollicitudin tortor eget mauris pharetra, a congue metus feugiat. Integer velit risus, hendrerit et pulvinar eget, mollis a odio. Donec sed sodales nisi. Aenean accumsan tellus in est pretium euismod. Praesent eu risus eros. Donec pulvinar, eros vel tincidunt sagittis, eros velit tempus diam, et feugiat elit enim at augue.</p>
                <p>Sed adipiscing nisi ac justo suscipit lobortis. Suspendisse potenti. Integer a ullamcorper orci. Curabitur eget suscipit metus, et aliquam est. Praesent eget leo sed purus tempus eleifend sit amet a mauris. Suspendisse lacus dolor, rhoncus sed hendrerit at, laoreet eu turpis. Aliquam massa libero, pellentesque eget risus vel, vehicula tristique ipsum.</p>
            </div>
        </div>
	</div>
</div>
    
<!-- //popup -->
<div id="terminos_cond" style="display:none;" class="device_250">
	<div class="device_250">
        <h2 class="h2_b" style="margin-bottom:20px;">Terminos y Condiciones</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet libero viverra mi tristique ultrices. Fusce nec leo dapibus, pulvinar justo et, lacinia tortor. Duis a consequat magna. Cras sodales, lectus ut pellentesque congue, diam metus mattis enim, ac tincidunt nisi turpis ut metus. Ut in sapien at lacus semper facilisis vitae sed odio. Morbi facilisis faucibus eros vitae pharetra. Fusce eu ipsum congue, viverra velit a, mollis nunc. Nam mattis ligula eget metus aliquet, sed commodo justo aliquet.
        Donec consequat at sapien nec ullamcorper. <br />
        <br />
        Aliquam erat volutpat. Morbi dolor lacus, bibendum eu velit sit amet, cursus facilisis arcu. Fusce eu facilisis lorem. Pellentesque eu augue at mauris fringilla volutpat. Suspendisse tincidunt sit amet nisi id adipiscing. Integer porttitor nisl quis dignissim cursus. In suscipit sapien eros, ac vulputate quam iaculis vitae. Suspendisse justo odio, porttitor quis eros ut, adipiscing gravida nunc.
        <br /><br />
        Praesent accumsan eget nulla eget faucibus. Fusce eu nulla in odio pharetra gravida. Curabitur sollicitudin enim tellus, vitae commodo nunc dapibus vel. Ut rutrum quam id quam tempor, in pulvinar justo ultrices. Morbi ultricies, dolor eu tempus pretium, neque leo scelerisque neque, sit amet blandit ligula diam ac tortor. Cras eleifend scelerisque nibh, tempor vehicula ligula auctor at. Nullam arcu justo, egestas et libero non, placerat aliquam augue. Fusce elementum et sem nec malesuada. Suspendisse interdum vitae diam quis hendrerit. Etiam convallis malesuada porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
        <br /><br />
        Proin laoreet gravida eleifend. Ut varius semper metus vel consectetur. Integer ut massa sed ligula aliquam tincidunt. Aenean purus nunc, pulvinar eu scelerisque vel, commodo et erat. Sed consequat, diam sed lacinia congue, mi lectus porttitor nisi, ac fermentum justo erat a nisi. Vivamus ac rutrum felis, et commodo ante. Aliquam dignissim, neque vel volutpat rutrum, nisl ante sollicitudin lectus, at interdum nulla elit consectetur tellus. In commodo nisi quis sem euismod feugiat. Aenean accumsan porta tempor. Nullam porta faucibus risus, nec rhoncus orci feugiat in. Nunc hendrerit consequat laoreet. Aliquam congue vestibulum porta.</p>
	</div>
</div>
    
</body>
</html>