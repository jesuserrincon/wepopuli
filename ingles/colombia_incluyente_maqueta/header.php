
<div class="back_top device_320">
    <div class="header">
    	<a href="index.php" id="logo"></a>
        <div class="social_header_links">
        	<!--TWITTER-->
            <a href="https://twitter.com/colombiaincluyente" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @colombiaincluyente</a>
		 	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        	
            <!--FACEBOOK-->
            <fb:follow href="https://www.facebook.com/pages/Colombia-incluyente/1455517308010333" colorscheme="light" layout="button_count" show_faces="true"></fb:follow>
      	    <div id="fb-root"></div>
        </div>
    </div>
</div>
<div class="div_line"></div>
<div class="buttons clearfix device_320">
	
    <div class="device_320 left">
        <a href="#" class="button_a">¿Qué es Ci?</a>
        <div class="div_buttons"></div>
        <a href="como_funciona.php" class="button_a">¿Cómo funciona?</a>
        <div class="div_buttons"></div>
        <a href="#" class="button_a">Preguntas Frecuentes</a>
    </div>
    
    <div class="div_buttons dis_none_movil"></div>
    
    <div class="under_line clearfix device_320">
        <div class="search_box">
            <a href="resultados_busqueda.php"><input type="submit" class="go" value="" title="Buscar" /></a>
            <input type="text" class="search" placeholder="Buscar" />
        </div>
        <div class="login_button">
        	<a href="perfil.php">Login</a> / <a href="registro.php">Registrate</a>
        </div>
    </div>
</div>
<div class="div_line"></div>