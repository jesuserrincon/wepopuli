<?php

class _Proyectos extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "PROYECTOS";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Proyectos();
        $info = $b->getProyectos();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('proyectos');
    }

    public function add() {
        $this->_data['nombremodulo'] = "CATEGORIAS / New";
        $this->build('proyectos_new');
    }

    public function new_Proyectos() {
        $datos = array(
            'banner_superior' => $this->input->post('imagen'),
            'banner_inferior' => $this->input->post('imagen2'),
            'nombre' => $this->input->post('nombre'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'proyectos';
        $b = new Proyectos();
        if ($datos['banner_superior'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'proyectos_new';
            if ($b->saveProyectos($datos)){
                $build = 'proyectos';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getProyectos();
        $this->_data["info"] = $info;
        return redirect('cms/proyectos/index/'.$save);
        
    }

    
    
    
    
    
    
    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'proyectos/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."proyectos/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('proyectos/'.$data["data"]["file_name"], array('longside'=>'proyectos/new','width' => 1000, 'height' => 223,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $estados = new Estados();
        $estados->get();
        $b = new Proyectos();
        $dat = $b->getProyectosById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        $this->_data["estados"] = $estados;
        return $this->build("proyectos_edit");
    }
    
    public function update_Proyectos(){
        $b = new Proyectos();
        $datos = array(
            'id' => $this->input->post('id'),
            'dias_para_cerrar' => $this->input->post('dias_para_cerrar'),
            'estados_id' => $this->input->post('estados_id'),

        );

        $update = $b->updateProyectos($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'banner_superior' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateProyectos($datos);
        }
        if($this->input->post('imagen2') != ''){
            $datos = array(
            'banner_inferior' => $this->input->post('imagen2'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateProyectos($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/proyectos/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Proyectos();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/proyectos/index/".$return);
    }

}

