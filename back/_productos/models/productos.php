<?php

class Productos extends DataMapper {

    public $model = 'productos';
    public $table = 'productos';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getProductos($idc){
        $this->where('cms_subcategorias_id',$idc);
        return $this->get();
    }
    
    public function getProductos3($idc ,$pagination, $segment){
        $this->where('cms_subcategorias_id',$idc);
        $this->limit($pagination, $segment);
        return $this->get();
    }

    public function getProductos2($idc2){
        $this->where('cms_categorias_id',$idc2);
        return $this->get();
    }
    
    public function saveProductos($object = "") {


        $this->descripcion_pdf = $object['descripcion'];
        $this->nombre = $object['nombre'];
        $this->precio = $object['precio'];
        $this->texto = $object['texto'];
        $this->imagen = $object['imagen'];
        $this->cms_subcategorias_id = $object['cms_subcategorias_id'];
        return $this->save();
    }

    public function saveProductos2($object = "") {

        $this->descripcion_pdf = $object['descripcion'];
        $this->nombre = $object['nombre'];
        $this->precio = $object['precio'];
        $this->texto = $object['texto'];
        $this->imagen = $object['imagen'];
        $this->cms_categorias_id = $object['cms_categorias_id'];
        return $this->save();
    }
    
    public function getProductosById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateProductos($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

