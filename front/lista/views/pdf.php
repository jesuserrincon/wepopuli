<img src="<?php echo base_url() ?>assets/img/header-logo.png"><br><br>

<h3>Señores:</h3>
<?php echo nl2br($nombre."<br>".$correo)?><br>


<p>Atendiendo su amable solicitud sometemos a su consideración la siguiente propuesta para la adquisisción en arrendamiento por parte de ustedes, muebles y electrodomésticos los cuales relacionamos a continuación:
</p>



<table cellpadding="0" cellspacing="0" style="width:100%">
  <tr>
    <td colspan="3">
      <table cellpadding="0" cellspacing="0" style="width:100%;">
        <tr>
          <td bgcolor="#ad370a" style="width:2%"></td>
          <td height="30" bgcolor="#ad370a" align="center" style="width:25%; color:#fff;">
          Producto
          </td>
          <td bgcolor="#ad370a" align="center" style="width:40%; color:#fff;">
          Descripción
          </td>
          <td bgcolor="#ad370a" align="center" style="width:15%; color:#fff;">
          Cantidad 
          </td>
          <td bgcolor="#ad370a" align="center" style="color:#fff;" >
          Valor
          </td>
          <td bgcolor="#ad370a" style="width:2%"></td>
        </tr>
          <?php
          $valorArrendamiento = 0;
          foreach ($subcategorias as $key => $val) {
          $subtotal = 0;
          $i = 0;

          foreach ($val as $item) {

          ?>
        <tr>
          <td width="10" style="border-bottom:1px solid #ddd"></td>
          <td style="border-right:1px solid #ddd; padding:10px 0; border-bottom:1px solid #ddd">
              <?php echo $item['name'] ?>          </td>
          <td style="border-right:1px solid #ddd; padding:10px 0; border-bottom:1px solid #ddd">
           <p style="padding:0 10px;">
               <?php if(isset($item['options']['descripcion']) && !empty($item['options']['descripcion'])) {
               if($item['options']['descripcion'] != 'null'){

                  echo nl2br($item['options']['descripcion']);

               }
              }else{
                   echo '---';
               }
               ?>
           </p>
          </td>
          <td align="center" valign="middle"  style="border-right:1px solid #ddd; padding:10px 0; border-bottom:1px solid #ddd">
              <?php echo $item['qty']." " ?>
          </td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd" align="right">
              <?php echo '$'.number_format($item['subtotal'],0,'.','.');
              $subtotal += $item['subtotal'];
              ?>
          </td>
          <td style="border-bottom:1px solid #ddd" ></td>
        </tr>
          <?php
          }
              $valorArrendamiento += $subtotal;
        }
          ?>

          <?php
          $adicionales = array();
          foreach ($categorias as $key => $val) {
              $subtotal = 0;
              $i = 0;
              foreach ($val as $item) {

                  ?>
                  <tr>
                      <td width="10" style="border-bottom:1px solid #ddd"></td>
                      <td style="border-right:1px solid #ddd; padding:10px 0; border-bottom:1px solid #ddd">
                          <?php echo $item['name'] ?>          </td>
                      <td style="border-right:1px solid #ddd; padding:10px 0; border-bottom:1px solid #ddd">
                          <p style="padding:0 10px;">
                              <?php if(isset($item['options']['descripcion']) && !empty($item['options']['descripcion'])) {
                                  if($item['options']['descripcion'] != 'null'){

                                      echo nl2br($item['options']['descripcion']);

                                  }
                              }else{
                                  echo '---';
                              }
                              ?>
                          </p>
                      </td>
                      <td align="center" valign="middle"  style="border-right:1px solid #ddd; padding:10px 0; border-bottom:1px solid #ddd">
                          <?php echo $item['qty']." " ?>
                      </td>
                      <td style="padding:10px 0; border-bottom:1px solid #ddd" align="right">
                          <?php echo '$'.number_format($item['subtotal'],0,'.','.');
                          $subtotal += $item['subtotal'];
                          ?>
                      </td>
                      <td style="border-bottom:1px solid #ddd" ></td>
                  </tr>
          <?php

          for($i=0; $i<5;$i++){
          if(isset($item['options'][$i])){
              $pieces = explode("-", $item['options'][$i]);
              //echo $pieces[1];
              //$pieces = explode("-", $item['options'][$i]);
              $subtotal+=$pieces[0];
              // echo number_format($pieces[0],0,'.','.');
              $dato = array('nombre'=>$pieces[1],'precio'=>$pieces[0]);
              array_push($adicionales, $dato);
            }
          }
              ?>

              <?php
              }
              $valorArrendamiento += $subtotal;
          }
          ?>

      </table>
    </td>
  </tr>
  <tr>
    <td height="30"></td>
  </tr>
  <tr>
    <td colspan="3" style="font-size:18px">ADICIONALES</td>
  </tr>
  <tr>
    <td height="15"></td>
  </tr>
  <tr>
    <td colspan="3">
      <table cellpadding="0" cellspacing="0" style="width:100%">
        <tr>
          <td bgcolor="#f09500" style="width:2%" height="30">
          </td>
          <td bgcolor="#f09500" style="width:80%; color:#fff;" align="center">
            Producto
          </td>
          <td bgcolor="#f09500" style="color:#fff; width:16%" align="center">
            Valor
          </td>
          <td style="width:2%" bgcolor="#f09500"></td>
        </tr>
<?php

          foreach ($adicionales as $key => $val) {

?>
        <tr>
          <td style="width:2%; border-bottom:1px solid #ddd;" >
          </td>
          <td style="border-right:1px solid #ddd; padding:10px 0; border-bottom:1px solid #ddd">
            <?php echo $val['nombre'] ?>
          </td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd" align="right">
              <?php echo number_format($val['precio'],0,'.','.'); ?>
          </td>
          <td style="width:2%; border-bottom:1px solid #ddd;" ></td>
        </tr>
 <?php

          }
  ?>

      </table>
    </td>
  </tr>
  <tr>
    <td height="30"></td>
  </tr>
  <tr>
    <td style="width:48%; border:1px solid #ddd" valign="top">
      <table cellpadding="0" cellspacing="0" style="width:100%" >
        <tr>
          <td colspan="4" align="center" style="color:#fff;" bgcolor="#ad370a" height="30">
            Alquiler primer mes
          </td>
        </tr>
        <tr>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd; border-right:1px solid #ddd; width:60%; ">
          Alquiler mensual
          </td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd; width:32%" align="right">
              <?php echo '$'.number_format($valorArrendamiento,0,'.','.') ?>
          </td>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
        </tr>
        
        <tr>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd; border-right:1px solid #ddd; ">
          Transporte ida y regreso
          </td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd;" align="right">
              <?php
              $trasporte = $zona;
              echo '$'.number_format($trasporte,0,'.','.') ?>
          </td>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
        </tr>
        
        <tr>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd; border-right:1px solid #ddd; ">
          IVA 16%
          </td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd;" align="right">
              <?php
              $iva = ($valorArrendamiento + $trasporte)* 0.16;
              echo '$'.number_format($iva,0,'.','.') ?>
          </td>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
        </tr>
        
        <tr>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd; border-right:1px solid #ddd; ">
          <b>TOTAL A PAGAR PRIMER MES</b>
          </td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd;" align="right">
              <?php
              $valorPrimerMes = ($valorArrendamiento + $trasporte)+ $iva;
              echo '$'.number_format($valorPrimerMes,0,'.','.') ?>
          </td>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
        </tr>
      </table>
    </td>
    <td style="width:4%">
    </td>
    <td style="width:48%; border:1px solid #ddd" valign="top">
      <table cellpadding="0" cellspacing="0" style="width:100%" >
        <tr>
          <td colspan="4" align="center" style="color:#fff;" bgcolor="#ad370a" height="30">
            Alquiler otros meses
          </td>
        </tr>
        <tr>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd; border-right:1px solid #ddd; width:60% ">
          Alquiler mensual
          </td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd; width:32%" align="right">
              <?php echo '$'.number_format($valorArrendamiento,0,'.','.') ?>
          </td>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
        </tr>
        
        <tr>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd; border-right:1px solid #ddd; ">
          IVA 16%
          </td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd;" align="right">
              <?php
              $iva = $valorArrendamiento * 0.16;
              echo '$'.number_format($iva,0,'.','.') ?>
          </td>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
        </tr>
        
        <tr>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd; border-right:1px solid #ddd; ">
          <b>TOTAL A PAGAR PRIMER MES</b>
          </td>
          <td style="padding:10px 0; border-bottom:1px solid #ddd;" align="right">
              <?php
              $total = $valorArrendamiento + $iva;
              echo '$'.number_format($total,0,'.','.') ?>
          </td>
          <td style="width:4%; border-bottom:1px solid #ddd;"></td>
        </tr>
      </table>
    </td>
  </tr>-
</table>




<h4>CLÁUSULAS INFORMATIVAS</h4>
<p>
    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.   Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.
</p>


<h5>
    Atentamente,
</h5>
<h4>SC RENTAMUEBLES SAS</h4>