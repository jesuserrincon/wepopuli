<?php

class _Banner_home extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->load->helper('url');
        $this->_data['nombremodulo'] = "BANNER";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '',$idseccion = 0) {
        $this->_data['save'] = $value;
        $b = new Banner_home();
        $info = $b->getBanner_home();
        $this->_data["idseccion"] = $idseccion;
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('banner_home');
    }

    public function add($idseccion = 0) {
        $this->_data["idseccion"] = $idseccion;
        $this->_data['nombremodulo'] = "BANNER / New";
        $this->build('banner_home_new');
    }

    public function new_Banner_home() {
        $datos = array(
            'imagen' => $this->input->post('imagen'),
            'titulo' => $this->input->post('titulo'),
            'texto' => $this->input->post('texto'),
            'link' => $this->input->post('link'),
            'tipo' => $this->input->post('tipo'),
            'pie_de_pagina' => $this->input->post('pie_de_pagina'),
            'seccion' => $this->input->post('seccion')
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'banner_home';
        $b = new Banner_home();
        if ($datos['imagen'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'banner_home_new';
            if ($b->saveBanner_home($datos)){
                $build = 'banner_home';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getBanner_home();
        $this->_data["info"] = $info;
        return redirect('cms/banner_home/index/'.$save.'/'.$this->input->post('seccion'));
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'banner_home/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."banner_home/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

                $data = array(
                    'ok' => true,
                    'resize' => true,
                    'data' => $this->upload->data()
                );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('banner_home/'.$data["data"]["file_name"], array('longside'=>'banner_home/new','width' => 1000, 'height' => 223,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = "",$idseccion = 0){
        $b = new Banner_home();
        $dat = $b->getBanner_homeById($id);
        $this->_data["idseccion"] = $idseccion;
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("banner_home_edit");
    }
    
    public function update_Banner_home(){
        $b = new Banner_home();
        $datos = array(
            'id' => $this->input->post('id'),
            'link' => $this->input->post('link'),
            'texto' => $this->input->post('texto'),
            'titulo' => $this->input->post('titulo'),
            'pie_de_pagina' => $this->input->post('pie_de_pagina'),
            'tipo' => $this->input->post('tipo')

        );
        $update = $b->updateBanner_home($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateBanner_home($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/banner_home/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Banner_home();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/banner_home/index/".$return);
    }

}

