<?php

class Redes extends DataMapper {

    public $model = 'redes';
    public $table = 'redes';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getRedes(){
        return $this->get();
    }
    
    public function saveRedes($object = "") {
        $this->youtube = $object['youtube'];
        $this->facebook = $object['facebook'];
        $this->twitter = $object['twitter'];
        $this->pinterest = $object['pinterest'];
        return $this->save();
    }
    
    public function getRedesById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateRedes($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

