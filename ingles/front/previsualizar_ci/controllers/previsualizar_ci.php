<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class previsualizar_ci extends Front_Controller {

    public function __construct() {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."gestores/gestores",
               CMSPREFIX."cuentas/cuentas",
               CMSPREFIX."imagenescomplementarias/imagenescomplementarias",
            CMSPREFIX."proyectos/estados",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0) {
        parent::index($proyectoid);
        $this->set_title('Bienvenidos a ' . SITENAME, true);

        $e = new Estados();
        $estados = $e->getEstados2();
        $arrayEstados = array();
        foreach($estados as $data){
            $arrayEstados[$data->id]=$data->nombre;
        }
        $this->_data['estados'] = $arrayEstados;

           //enviamos el registro del proyecto
           $proyectoOBJ = new Proyectos();
           $this->_data["proyecto"] = $proyectoOBJ->getProyectosById($proyectoid); 
           //Fin envio registro del proyecto

        $imagenesComplementarias = new Imagenescomplementarias();
        $imgComplementarias = $imagenesComplementarias->getImagenescomplementariasByProyecto($proyectoid);
        $this->_data['imgComplementarias'] = $imgComplementarias;
           
        $gestoresOBJ = new Gestores();
        $this->_data["gestor"] = $gestoresOBJ->getGestoresByIdProyecto($proyectoid);
        $this->_data["proyectoid"] = $proyectoid;
        return $this->build('previsualizar');
    }
    
    public function validar(){
           $proyectoid = $this->input->post('id');
           $proyectoOBJ = new Proyectos();
           $proyecto = $proyectoOBJ->getProyectosById($proyectoid); 
           $validacion = TRUE;
           $htmlvalidacion = 'Debe completar todos los campos obligatorios en cada paso, verifique los siguientes pasos:';
        //validamos los datos de basicos
        if($proyecto->titulo == '' || $proyecto->imagen_miniatura == ''  || $proyecto->tags_busqueda == ''  || $proyecto->descripcion_corta == ''){
            $validacion = FALSE;
            $htmlvalidacion .= '<br/><br/><a href="'.base_url().'basicos_ci/index/'.$proyectoid.'">BÁSICOS</a>';
        }
        //fin validacion basicos
        //traemos las ayudas para validar que almenos tenga una   
        $voluntariadoOBJ = new Voluntariado();
        $voluntariados = $voluntariadoOBJ->getVoluntariadoByProyecto($proyectoid);
        $ayudavol = 0;
        foreach ($voluntariados as $item){
            $ayudavol ++;
        }
        $especieOBJ = new Especie();
        $especies = $especieOBJ->getEspecieByProyecto($proyectoid);
        $ayudaesp = 0;
        foreach ($especies as $item){
            $ayudaesp ++;
        }
        if($proyecto->valormonetario == 0 && $ayudavol == 0 &&  $ayudaesp == 0 ){
               $validacion = FALSE;
               $htmlvalidacion .= '<br/><br/><a href="'.base_url().'basicos_ci/index/'.$proyectoid.'">AYUDA VOLUNTARIADO</a>';
        }
        //fin validacion ayudas    
        //validamos los datos de historia
        if( $proyecto->descripcion == ''  || $proyecto->objetivosymetas == ''  || $proyecto->comoselleva == ''  || $proyecto->riesgos == ''   || $proyecto->preguntas == ''   || $proyecto->video == ''){
            $validacion = FALSE;
            $htmlvalidacion .= '<br/><br/><a href="'.base_url().'historia_ci/index/'.$proyectoid.'">HISTORIA</a>';
        }
        //fin validacion historia
        //validamos los datos de gestores
        $gestoresOBJ = new Gestores();
        $gestores = $gestoresOBJ->getGestoresByIdProyecto($proyectoid);
        if($gestores->nombre == '' || $gestores->descripcion == ''  || $gestores->direccion == ''  || $gestores->telefono == ''  || $gestores->email == ''   || $gestores->acercadelider == ''   || $gestores->apellidoslider == '' || $gestores->cedulalider == '' || $gestores->telefonolider == '' || $gestores->emaillider == ''){
            $validacion = FALSE;
            $htmlvalidacion .= '<br/><br/><a href="'.base_url().'gestores_front_ci/index/'.$proyectoid.'">GESTORES</a>';
        }
        //fin validacion gestores
        //validamos los datos de cuentas
        /*$cuentaOBJ = new Cuentas();
        $cuenta = $cuentaOBJ->getCuentaProyectosById($proyectoid);
        if($cuenta->titular == '' || $cuenta->banco == ''  || $cuenta->numero_de_cuenta == ''  || $cuenta->tipo_de_cuenta == ''  || $cuenta->nombres == ''   || $cuenta->cargo == ''   || $cuenta->email == '' || $cuenta->apellidos == '' || $cuenta->cedula == '' || $cuenta->telefono == '' || $cuenta->direccion == ''){
            $validacion = FALSE;
            $htmlvalidacion .= '<br/><br/><a href="'.base_url().'cuentas/index/'.$proyectoid.'">CUENTA</a>';
        }*/
        //fin validacion cuentas

           if($validacion == FALSE){
               echo $htmlvalidacion;
           }else{

               //Enviamos el correo al usuario 
               
         $usuario = $this->nativesession->get('usuario');
        $usuarioOBJ = new Usuarios();
        $usuarioobj =  $usuarioOBJ->getUsuariosById($usuario['id']);

        $asunto = 'Postulación exitosa.';
        $titulo = 'Proyecto: '.$proyecto->titulo;
        $mensaje = 'Su proyecto ha sido recibido y será revisado.';

        $this->enviarCorreo($usuarioobj->email,$asunto,$titulo,$mensaje);

               //Fin envio correo
                $datos = array(
                        'id' => $proyectoid,
                        'estados_id' => 4,
                 );
               $proyectoOBJ->updateProyectos($datos);
               echo 1;
           }
           
    }
    
  
    
}
