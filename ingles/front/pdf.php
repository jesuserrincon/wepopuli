<img src="<?php echo base_url()  ?>assets/img/header-logo.png">

<h3>señores:</h3>
<p>
    Atendiendo su amable solicitud sometemos a su consideración la siguiente propuesta para la adquisisción en arrendamiento por parte de ustedes, muebles y electrodomésticos los cuales relacionamos a continuación:
</p>

<table style="width: 100%">

    <?php
   $valorArrendamiento = 0;
    foreach ($subcategorias as $key => $val) {
        $subtotal = 0;
        $i = 0;
        ?>
        <tr>
        <td style="height: 50px" colspan="3">
            </td>
        </tr>
        <?php
        foreach ($val as $item) {
                if($i== 0){
            ?>
        <tr>
            <td colspan="2">
                <?php echo $item['options']['subcategoria'] ?>
            </td>
        </tr>
                    <?php } ?>
            <tr>
                <td style="width: 5%">

                </td>
                <td style="width: 1%">
                    <?php echo $item['qty'] ?>
                </td>
                <td style="width: 10%">
                    <?php echo $item['name'] ?>
                </td>
                <td style="width: 10%">
                    <?php echo '$'.number_format($item['subtotal'],0,'.','.');
                    $subtotal += $item['subtotal'];
                    ?>
                </td>
            </tr>
            <?php if(isset($item['options']['descripcion']) && !empty($item['options']['descripcion'])) {?>
            <tr>
                <td></td>
                <td></td>
                <td><?php echo $item['options']['descripcion'] ?></td>
            </tr>
                <?php } ?>
        <?php
            $i++;
        }
        $valorArrendamiento += $subtotal;
        ?>
        <tr>
        <td colspan="3">
            <b>VALOR ARRENDAMIENTO MENSUAL</b>
        </td>
        <td>

            <b><?php echo '$'.number_format($subtotal,0,'.','.') ?></b>
        </td>
        </tr>
    <?php
    }
    ?>

    <?php

    foreach ($categorias as $key => $val) {
        $subtotal = 0;
        $i = 0;
        ?>
        <tr>
            <td style="height: 50px" colspan="3">
            </td>
        </tr>
        <?php
        foreach ($val as $item) {
            ?>

            <tr>
                <td style="width: 5%">

                </td>
                <td style="width: 1%">
                    <?php echo $item['qty'] ?>
                </td>
                <td style="width: 10%">
                    <b><?php echo $item['name'] ?></b>
                </td>
                <td style="width: 10%">
                    <?php echo '$'.number_format($item['subtotal'],0,'.','.');
                    $subtotal += $item['subtotal'];
                    ?>
                </td>
            </tr>
            <?php if(isset($item['options']['descripcion']) && !empty($item['options']['descripcion'])) {?>
                <tr>
                    <td></td>
                    <td></td>
                    <td><?php echo nl2br($item['options']['descripcion']) ?></td>
                </tr>
            <?php } ?>

            <tr>
                <td>
                    <b>OTROS</b>
                </td>
            </tr>
           <?php for($i=0; $i<5;$i++){
                if(isset($item['options'][$i])){
                ?>
            <tr>
                <td>

                </td>
                <td>

                </td>
                <td>
                    <?php
                    $pieces = explode("-", $item['options'][$i]);
                    echo $pieces[1] ?>
                </td>
                <td>
                    <?php
                    $pieces = explode("-", $item['options'][$i]);
                    $subtotal+=$item['options'][$i];
                    echo number_format($pieces[0],0,'.','.') ?>
                </td>
            </tr>

            <?php } } ?>

            <?php
            $i++;
        }
        ?>

        <tr>
            <td colspan="3">
                <b>VALOR ARRENDAMIENTO MENSUAL</b>
            </td>
            <td>

                <b><?php echo '$'.number_format($subtotal,0,'.','.') ?></b>
            </td>
        </tr>

    <?php
        $valorArrendamiento += $subtotal;
    }
    ?>

    <tr>
        <td colspan="3">
            ARRENDAMIENTO MENSUAL
        </td>
        <td>
            <?php echo '$'.number_format($valorArrendamiento,0,'.','.') ?>
        </td>
        </tr>
        <tr>
        <td colspan="3">
            TRANSPORTE IDA Y REGRESO
        </td>
        <td>
            <?php
            $trasporte = 20000;
            echo '$'.number_format($trasporte,0,'.','.') ?>
        </td>

    </tr>
    <tr>
        <td colspan="3">
            IVA 16%
        </td>
        <td>
            <?php
            $iva = ($valorArrendamiento + $trasporte)* 0.16;
            echo '$'.number_format($iva,0,'.','.') ?>
        </td>

    </tr>
    <tr>
        <td colspan="3">
            TOTAL A PAGAR PRIMER MES
        </td>
        <td>
            <?php
            $valorPrimerMes = ($valorArrendamiento + $trasporte)+ $iva;
            echo '$'.number_format($valorPrimerMes,0,'.','.') ?>
        </td>

    </tr>

    <tr>
        <td colspan="3"><b>ARRENDAMIENTO MESES SIGUIENTES</b></td>
    </tr>

    <tr>
        <td colspan="3">
            ARRENDAMIENTO MENSUAL
        </td>
        <td>
            <?php echo '$'.number_format($valorArrendamiento,0,'.','.') ?>
        </td>
    </tr>



</table>

<h2>CLÁUSULAS INFORMATIVAS</h2>
<p>
    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum. Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.
</p>