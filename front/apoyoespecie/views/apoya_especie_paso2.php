<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/apply.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/just_numbers_input.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancys.js"></script>
</head>

<body class="device_320">

<div class="clear"></div>
<div class="txt_reglamento device_300 pad_bottom_0_mobile pad_5_mobile">
  <h2 class="h2_b" style="font-size:44px"><?php echo $proyecto->titulo; ?></h2>
  <h2 class="h2_des">Por: <?php echo $gestor->nombre; ?></h2>
</div>

    
    
<div class="box_shadow_big clearfix device_320">

  <div class="col_e min_h_900 pad_int_20 device_300">
      
  	   <div class="spacer"></div>
       
        <h2 class="h2_b left" style="color:#7f3f98">¡Gracias por contribuir!</h2>
  		
        <div class="icon_apoya icon_apoya_2"></div>
        <div class="clear"></div>
        
        <div class="div_line_2 margin_bottom_20_mobile"></div>
        <div class="spacer"></div>
        <h2 class="h2_b font_17">Lista(s)<br />seleccionada(s):</h2>
  		
  		
       
        <div class="div_line_2"></div>
        
        <div class="spacer"></div>
        
    	<h2 class="h2_c left">Ayudas:</h2>
        
        
       



       <div class="clear"></div>
       
       <?php echo $html; ?>



 
       <center>
           <h3 class="h3_purpure" style="font-size:25px">Valor total:</h3>
           <h2 class="h2_verde" id="dinerotot" style="font-size:33px">$<?php echo number_format($valortotal, 0, '', '.'); ?><span>(cop)</span></h2>
       </center>
       
       
       <div class="clear div_line_2 margin_bottom_20_mobile" style="margin-bottom:20px;"></div>
        <div class="col_g">
        	
            <div class="info_data">
            	<div class="label_data">Nombres</div>
                <div class="data_user"><?php echo $usuario->nombres;?></div>
            </div>
        	
            <div class="info_data">
            	<div class="label_data">Apellidos</div>
                <div class="data_user"><?php echo $usuario->apellidos;?></div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Tipo de identificación</div>
                <div class="data_user"><?php echo $usuario->tipo_identificacion;?></div>
            </div>
            
            
            
            <div class="info_data">
            	<div class="label_data">No. Identificación</div>
                <div class="data_user">CC <?php echo $usuario->cedula;?></div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">País Nacionalidad</div>
                <div class="data_user"><?php echo $usuario->pais;?></div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">País Residencia</div>
                <div class="data_user"><?php echo $usuario->pais;?></div>
            </div>

 			<div class="info_data">
            	<div class="label_data">Departamento (Región)</div>
                <div class="data_user"><?php echo $usuario->departamento;?></div>
            </div>
                    
            <div class="info_data">
            	<div class="label_data">Municipio</div>
                <div class="data_user">Zipaquira</div>
            </div>


			<div class="info_data">
            	<div class="label_data">Dirección</div>
                <div class="data_user"><?php echo $usuario->direccion;?></div>
            </div>
            
			<div class="info_data">
            	<div class="label_data">Teléfono 1</div>
                <div class="data_user"><?php echo $usuario->telefono;?></div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Teléfono 2 (opcional)</div>
                <div class="data_user"><?php echo $usuario->telefono2;?></div>
            </div>
            
            
            <div class="info_data">
            	<div class="label_data">Fecha de nacimiento</div>
                <div class="data_user"><?php echo $usuario->fecha_nacimiento ?></div>
            </div>

            <div class="info_data">
            	<div class="label_data">Sexo</div>
                <div class="data_user"><?php echo $usuario->sexo;?></div>
            </div>
            
            
            
            <div class="info_data">
            	<div class="label_data">Estado Civil</div>
                <div class="data_user"><?php echo $usuario->estado_civil;?></div>
            </div>

            
            <div class="info_data">
            	<div class="label_data">Facebook (opcional)</div>
                <div class="data_user"><?php echo $usuario->facebook;?></div>
            </div>
            
            
            <div class="info_data">
            	<div class="label_data">Twitter (opcional)</div>
                <div class="data_user"><?php echo $usuario->twitter;?></div>
            </div>
            
            <div class="info_data" style="margin-bottom:0pxh2_c">
            	<div class="label_data">Email</div>
                <div class="data_user"><?php echo $usuario->email;?></div>
            </div>
			<div class="clear" style="margin-top:-20px"></div>

        </div>
        
        
       
        <div class="acept_box clear">
            <a class="verde_bt2">Acepto </a> 
            <a href="#terminos_cond"  class="fancybox terms">términos y condiciones</a>
        </div>
       <a href="<?php echo base_url().'apoyoespecie/index/'.$proyecto->id; ?>" class="boton_a" style="display:block; float:left; margin-right:10px; margin-left:150px">Atrás</a>
        <a href="#" class="boton_a" style="display:block; float:left;">Apoyar</a>

        
        <div class="div_line_2 visible_mobile"></div>
  </div>
  
  
  
  
  
    <div class="col_f device_320">
    	
        <div class="logo_fundacion">
  <?php 
                    function image_exists($url) {
                        if(@getimagesize($url)){
                        return true;
                      }else{
                       return false;
                     }
                    }
                    $imagen = base_url().'uploads/proyectos/imagenes/'.$proyecto->imagen; 
                    $fundacionimagen = base_url().'uploads/proyectos/gestores/'.$gestor->imagen; 
                    $liderimagen = base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; 
                   ?>       
            <a href="#inline2" onclick="javascript:$('.fancybox').fancybox()"  class="fancybox">
                <img src="<?php if(image_exists($fundacionimagen)){ echo base_url().'uploads/proyectos/gestores/'.$gestor->imagen; }else{ echo 'img/060.jpg'; }?>" width="200" />
            </a>
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Por: <a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Liderado por: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>
        
        <div class="state box_min_sh" style="padding:13px 0px">
        	<div class="estado left"><div class="active_point left"><span>Status:</span> Activo</div></div>
            <div class="clear" style="margin-bottom:5px;"></div>
            <?php 
             function dameFecha($fecha,$dia)
                {   list($day,$mon,$year) = explode('/',$fecha);
                    return date('d/m/Y',mktime(0,0,0,$mon,$day+$dia,$year));        
                }
            ?>
            <div class="estado"><span>Hasta:</span> <?php echo dameFecha(date('d/m/Y'),$proyecto->dias_para_cerrar); ?></div>
        </div>
        
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Total recibido</center></div>
            <div class="barra_porcentaje2">
                <div class="bar2" style="width:<?php echo $proyecto->porcentaje_total;?>%"><?php echo $proyecto->porcentaje_total;?>%</div>
            </div>

		</div>


        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>
        
         <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Importante</h3>
            <div class="txt_box_min">
            	Wepopuli es un buscador de organizaciones y proyectos sociales, una plataforma de crowdsource social donde puedes encontrar las organizaciones y proyectos a nivel mundial que mejor se adapten a tus talentos y necesidades, teniendo en cuenta criterios como sector, localización y objetivos del proyecto, fechas, aptitudes solicitadas, etc., a las que podrás ayudar como voluntario de campo o desde tu casa u oficina, dependiendo de sus requerimientos.
			</div>
            <div class="line_color"></div>
		</div>
		
        <div class="spacer"></div>	
            
      <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Preguntas Frecuentes</h3>
            <div class="txt_box_min">
            	¿Cómo contribuyo a un proyecto? Cada proyecto tiene sus necesidades puntuales. Para verlas, ingresa como usuario registrado a la web y revisa las necesidades desplegadas por la organización que lidera el proyecto.</div>
            <div class="line_color color2"></div>
		</div>
        
    	<div class="spacer visible_mobile"></div>

    </div>
    
    <div class="div_line clear dis_none_movil"></div>
    
    <div class="pad_int_20">
    
    
    </div>

</div>
    


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/aplication.js"></script>


<!-- //popup -->
<div id="inline2" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px; color:#1c75bc">Fundación</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->nombre;?></h2>
    	<div class="spacer"></div>
    	<div class="txt_user">
            <p><?php echo $gestor->descripcion;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Por: <a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Liderado por: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
	</div>
    
</div>

<div id="inline3" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px"><?php echo $gestor->nombreslider.' '.$gestor->apellidoslider;?></h2>
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px"><?php echo $gestor->nombre;?></h3>
    
    	<div class="txt_user">
            <p><?php echo $gestor->acercadelider;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($liderimagen)){ echo base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; }else{ echo 'img/080.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Información de contacto:</span></div>
            <?php  if($gestor->emaillider != ''){ echo '<div class="dato"><b>Email: <a href="mailto:'.$gestor->emaillider.'" style="color:#602483">'.$gestor->emaillider.'</a></b></div>'; } ?>
            <?php  if($gestor->facebooklider != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebooklider.'</b></div>'; } ?>
            <?php  if($gestor->twitterlider != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitterlider.'</b></div>'; } ?>
        </div>
	</div>
    
</div>
<!-- //popup -->
<div id="terminos_cond" style="display:none;" class="device_250">
	<div class="device_250">
        <h2 class="h2_b" style="margin-bottom:20px;">Terminos y Condiciones</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet libero viverra mi tristique ultrices. Fusce nec leo dapibus, pulvinar justo et, lacinia tortor. Duis a consequat magna. Cras sodales, lectus ut pellentesque congue, diam metus mattis enim, ac tincidunt nisi turpis ut metus. Ut in sapien at lacus semper facilisis vitae sed odio. Morbi facilisis faucibus eros vitae pharetra. Fusce eu ipsum congue, viverra velit a, mollis nunc. Nam mattis ligula eget metus aliquet, sed commodo justo aliquet.
        Donec consequat at sapien nec ullamcorper. <br />
        <br />
        Aliquam erat volutpat. Morbi dolor lacus, bibendum eu velit sit amet, cursus facilisis arcu. Fusce eu facilisis lorem. Pellentesque eu augue at mauris fringilla volutpat. Suspendisse tincidunt sit amet nisi id adipiscing. Integer porttitor nisl quis dignissim cursus. In suscipit sapien eros, ac vulputate quam iaculis vitae. Suspendisse justo odio, porttitor quis eros ut, adipiscing gravida nunc.
        <br /><br />
        Praesent accumsan eget nulla eget faucibus. Fusce eu nulla in odio pharetra gravida. Curabitur sollicitudin enim tellus, vitae commodo nunc dapibus vel. Ut rutrum quam id quam tempor, in pulvinar justo ultrices. Morbi ultricies, dolor eu tempus pretium, neque leo scelerisque neque, sit amet blandit ligula diam ac tortor. Cras eleifend scelerisque nibh, tempor vehicula ligula auctor at. Nullam arcu justo, egestas et libero non, placerat aliquam augue. Fusce elementum et sem nec malesuada. Suspendisse interdum vitae diam quis hendrerit. Etiam convallis malesuada porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
        <br /><br />
        Proin laoreet gravida eleifend. Ut varius semper metus vel consectetur. Integer ut massa sed ligula aliquam tincidunt. Aenean purus nunc, pulvinar eu scelerisque vel, commodo et erat. Sed consequat, diam sed lacinia congue, mi lectus porttitor nisi, ac fermentum justo erat a nisi. Vivamus ac rutrum felis, et commodo ante. Aliquam dignissim, neque vel volutpat rutrum, nisl ante sollicitudin lectus, at interdum nulla elit consectetur tellus. In commodo nisi quis sem euismod feugiat. Aenean accumsan porta tempor. Nullam porta faucibus risus, nec rhoncus orci feugiat in. Nunc hendrerit consequat laoreet. Aliquam congue vestibulum porta.</p>
	</div>
</div>
    
</body>
    <script>
        function format(num){
            if(!isNaN(num)){
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
            num = num.split('').reverse().join('').replace(/^[\.]/,'');
            return num;
        }
 
else{ alert('Solo se permiten numeros');
input.value = input.value.replace(/[^\d\.]*/g,'');
}
}
        function sumar(){
        var importe_total = 0;
	$(".sumaselect").each(
		function(index, value) {
                if($(this).is(':visible')){
                    importe_total = importe_total + eval($(this).val());
                }
			
		}
	);
        $('#dinerotot').html('$'+format(importe_total));
    }
    </script>
</html>
