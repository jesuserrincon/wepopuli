<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class historia_ci extends Front_Controller {

    public function __construct() {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."imagenes/imagenes",
               CMSPREFIX."imagenescomplementarias/imagenescomplementarias",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0,$accion = 0) {

        parent::index($proyectoid);
        
        $vista = $this->build2('../../home/views/detalleProyecto');
        $this->_data["vista"] = $vista;

        $this->set_title('Bienvenidos a ' . SITENAME, true);

        $imagenes = new Imagenes();
        $imagenes = $imagenes->getImagenes($proyectoid);
        $this->_data["imagenes"] = $imagenes;
        $this->_data["accion"] = $accion;

        $i=0;
        foreach($imagenes as $data){
           $i++;
        }

        $imagenesComplementarias = new Imagenescomplementarias();
        $imgComplementarias = $imagenesComplementarias->getImagenescomplementariasByProyecto($proyectoid);
        $this->_data['imgComplementarias'] = $imgComplementarias;

        $this->_data["id"] = $proyectoid;
        $this->_data["count"] = $i;


        $this->_data["proyectoid"] = $proyectoid;
        return $this->build('historia');

    }

    public function add(){

        $config['upload_path'] = UPLOADSFOLDER.'proyectos/imagenescomplementarias';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|rgb|psd';
        $config['remove_spaces']=TRUE;
        $config['max_size']	= '10000';
        $config['encrypt_name']	= true;


        $this->load->library('upload', $config);
        $contadorimg = 0;
        for($i=0; $i<5; $i++){

            $img = new Imagenescomplementarias();

            if(isset($_FILES['archivo'.$i]['name']) && !empty($_FILES['archivo'.$i]['name'])){
                $contadorimg++;
                if ( ! $this->upload->do_upload('archivo'.$i))
                {
                    die($this->upload->display_errors());
                }
                else
                {
                    $data = array(
                        'data' => $this->upload->data()
                    );

                    $datos = array(
                        'imagen' => $data["data"]["file_name"],
                        'proyectos_id' => $this->input->post('id')
                    );

                    $update = $img->saveImagenescomplementarias($datos);
                }
            }
        }

        $b = new Proyectos();

        if(isset($_FILES['imagen']['name']) && !empty($_FILES['imagen']['name'])){

            $contadorimg++;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('imagen'))
            {
                die($this->upload->display_errors());
            }
            else
            {
                $data = array(
                    'data' => $this->upload->data()
                );

                $datos = array(
                    'imagen' => $data["data"]["file_name"],
                    'id' => $this->input->post('id')
                );
                $update = $b->updateProyectos($datos);
            }
        }


        $datos = array(
            'descripcion' => $this->input->post('descripcion'),
            'objetivosymetas' => $this->input->post('objetivo'),
            'comoselleva' => $this->input->post('llevar'),
            'riesgos'=>$this->input->post('riesgos'),
            'preguntas' => $this->input->post('preguntas'),
            'video' => $this->input->post('video'),
            'id' => $this->input->post('id')
        );

        $update = $b->updateProyectos($datos);

        if($this->input->post('redirec') == 1){
            return redirect("historia_ci/index/".$this->input->post('id')."/1");
        }

        if($this->input->post('redirec') == 2){
                 return redirect("gestores_front_ci/index/".$this->input->post('id')."/1");
            }


    }

    public function eliminarImagen($idp, $id){

        $img = new Imagenescomplementarias();
        $img->eliminar($id);
        return redirect("historia_ci/index/".$idp."/1#anclaelimina");

    }


    
}
