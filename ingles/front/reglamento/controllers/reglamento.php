<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class reglamento extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."textos/textos",
               CMSPREFIX . "proyectos/proyectos",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($idpro = 0) {

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        
        $textosOBJ = new Textos();
        $texto = $textosOBJ->getTextosById(2);
        $this->_data["texto"] = $texto->texto;
        
        //enviamos el registro del proyecto a la vista
        $proyectoOBJ = new Proyectos();
        $this->_data["proyecto"] = $proyectoOBJ->getProyectosById($idpro); 
        //Fin envio registro del proyecto
           
        $this->_data["idpro"] = $idpro;
        return $this->build('reglamento');
    }
    
    
    public function checkproyecto() {
        $data = array(
            'id' => $this->input->post('idp'),
            'checkterminos' => '1'
        );
        $proyectoOBJ = new Proyectos();
        $proyectoOBJ->updateProyectos($data);
        
    }
    
 
    

    // ----------------------------------------------------------------------


}
