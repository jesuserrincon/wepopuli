<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author rigobcastro
 * @author Brayan Acebo
 * @author Jose Fonseca
 */
class rentamueble extends Front_Controller {

    public function __construct() {
        $this->load->library('session');

        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index() {

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        $this->load->model(array(
            CMSPREFIX."rentamuebles/rentamuebles",
            CMSPREFIX."slider/slider",
            CMSPREFIX."clientes/clientes",
            CMSPREFIX."footer/footer",
            CMSPREFIX."redes/redes"
        ));

        $r = new Redes();
        $redes = $r->getRedesById(1);
        $this->_data['redes'] = $redes;

        $f = new Footer();
        $footer = $f->getFooterById(1);
        $this->_data['footer'] = $footer;

        $c = new Clientes();
        $clientes = $c->getClientes();
        $this->_data["clientes"] = $clientes;

        $s = new Slider();
        $slider = $s->getSlider();
        $this->_data["slider"] = $slider;

        $b = new Rentamuebles();
        $info = $b->getRentamueblesById(1);
        $this->_data["info"] = $info;
        return $this->build('rentamuebles');
    }

    // ----------------------------------------------------------------------


}
