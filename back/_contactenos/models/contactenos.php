<?php

class Contactenos extends DataMapper {

    public $model = 'contactenos';
    public $table = 'contactenos';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getContactenos(){
        return $this->get();
    }

    public function getContactenos2(){
        return $this->get();
}
    
    public function saveContactenos($object = "") {
        $this->informacion = $object['informacion'];
        $this->mapa = $object['mapa'];
        $this->texto = $object['texto'];
        return $this->save();
    }
    
    public function getContactenosById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateContactenos($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

