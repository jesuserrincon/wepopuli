<link rel="image_src" href="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $proyecto->imagen_miniatura; ?>"/>
<script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<?php
function convertirUrls($txt) {
//filtro los enlaces normales
    $cadena_resultante= preg_replace("/((http|https|www)[^\s]+)/", '<a href="$1">$0</a>', $txt);
//miro si hay enlaces con solamente www, si es así le añado el http://
    $cadena_resultante= preg_replace("/href=\"www/", 'href="http://www', $cadena_resultante);
    return $cadena_resultante;
}
?>
<script type="text/javascript">
		$(document).ready(function() {			
			$('.fancybox').fancybox();
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});
                    
                        $('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			
			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});
			
			

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});
    $(".rslides").show(100);


		});
	</script>     
    <style type="text/css" >
	.rslides {
		display:none;
	}
	</style>  
        
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript">stLight.options({publisher: "09422a6d-4673-4a62-ac1c-160a92973882", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>



    
<div style="margin-top:20px; margin-bottom:20px;">


	<div class="wrapper relative">
        <ul class="rslides" id="slider2">
          <?php foreach ($bannerspro as $item){?>
            <li><a href="<?php echo $item->link; ?>" target="_blanck"><img src="<?php echo base_url()."uploads/banner_proyecto/".$item->imagen?>" width="955" height="256" /></a></li>
           <?php }?>
        </ul>
      </div>
  
</div>    

<div class="clear"></div>
<div class="txt_reglamento device_300 pad_bottom_0_mobile pad_5_mobile">
  <h2 class="h2_b" style="font-size:38px; padding-left:7px; margin-bottom:0px"><?php echo $proyecto->titulo; ?></h2>
  <h2 class="h2_des" style="padding-left:7px">Project by: <?php echo $gestor->nombre;?></h2>
</div>

    
<div class="box_shadow_big clearfix device_320">

  <div class="col_e pad_int_20 device_300" style="min-height:1100px;">
      
      <div class="img_detalle_big relative">
            <?php if($proyecto->exitoso == 1){ echo '<div class="banda bnd1">Successful</div>';} ?>
            <?php if($proyecto->urgente == 1){ echo '<div class="banda bnd2">Urgent</div>';} ?>
            <?php if($proyecto->estados_id == 3){ echo '<div class="banda bnd3">Closed</div>';} ?>
                        <img src="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $proyecto->imagen_miniatura; ?>" class="rounded_img device_300" width="100%" />
       </div>
        	<div class="share clearfix">
            	<div class="device_300">
                    <!--<div class="left" style="width:90px">
                        <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35&amp;appId=236778076359723" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>                
                    </div -->
                     <div class="left">
                        <span class='st_facebook'></span>
                        <span class='st_twitter'></span>
                        <span class='st_googleplus'></span>
                        <span class='st_email'></span>
                        <span class='st_sharethis'></span>
                    </div>
						
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->

					<div class="addthis_toolbox addthis_pill_combo left addthis_32x32_style" style="width:100%; margin-bottom:10px">
                        <a class="addthis_button_facebook_like"></a>
                        <div class="right" style="width:200px">
                            <a class="addthis_button_compact right" style="margin-right:5px"></a>
                            <a class="addthis_button_preferred_3 right" style="margin-right:5px"></a>
                            <a class="addthis_button_preferred_2 right" style="margin-right:5px"></a>
                            <a class="addthis_button_preferred_1 right" style="margin-right:5px"></a>
                            </div>
                    </div>
                   
                    </div>
                
            	<div class="device_300">
                    <?php if($proyecto->ocutlar_barra_porcentaje != 1){?>
                    <div class="days_left">
                        <?php
                        if($proyecto->fechafin){
                            $datetime1 = new DateTime($proyecto->fechafin);
                            $datetime2 = new DateTime('now');
                            $interval = $datetime2->diff($datetime1);
                            $dias_para_cerrar = ($interval->format('%R%a'))+1;

                        }else{
                            $dias_para_cerrar = 0;
                        }
                        if($dias_para_cerrar <= 0){?>
                            <h2 style="color:#a01f80; font-size:17px"> Deadline reached</h2>
                        <?php }else{ ?>
                            <h2 style="color:#a01f80; font-size:17px"><?php echo $dias_para_cerrar;?> days to go</h2>
                        <?php }?>

                    </div>
                    <?php } ?>
                </div>
            </div>
        
        	<div class="description_project"><?php if($proyecto->descripcion != ''){ echo ''; } ?>
        	  <h3 class="h3_purpure"><?php if($proyecto->descripcion != ''){ echo 'PROJECT DESCRIPTION'; } ?></h3>
        	  <p><?php echo convertirUrls(nl2br($proyecto->descripcion)); ?></p>



             <?php if($proyecto->objetivosymetas != ''){ echo '<div class="spacer"></div>
              <h3 class="h3_purpure">OBJECTIVES AND GOALS</h3>'; } ?>
              <p><?php echo convertirUrls(nl2br($proyecto->objetivosymetas)); ?></p>


                <?php if($proyecto->comoselleva != ''){ echo '<div class="spacer"></div>
              <h3 class="h3_purpure">HOW IT IS GOING TO BE ACHIEVED</h3>'; } ?>

        	  <p><?php echo convertirUrls(nl2br($proyecto->comoselleva)); ?></p>




                  <?php if($proyecto->riesgos != ''){ echo '<div class="spacer"></div>
        	  <h3 class="h3_purpure">RISKS AND CHALLENGES</h3>'; } ?>

        	  <p><?php echo convertirUrls(nl2br($proyecto->riesgos)); ?><p/>



                <?php if($proyecto->preguntas != ''){ echo '<div class="spacer"></div>
        	  <h3 class="h3_purpure">FAQS</h3>'; } ?>

        	  <p><?php echo convertirUrls(nl2br($proyecto->preguntas)); ?><p/>
	  </div>

      <?php if($proyecto->video != ''){ echo '<div class="spacer"></div>
          <h3 class="h3_purpure">VIDEO</h3>'; } ?>

           
          <?php if(strpos($proyecto->video,'youtu' )) {
              $url = $proyecto->video;
              parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );

              ?>
        <!--  <iframe class="device_300" width="590" height="315" src="<?php echo $proyecto->video; ?>" frameborder="0" allowfullscreen></iframe>
        -->
        <iframe width="100%" height="315" src="//www.youtube.com/embed/<?php echo $my_array_of_vars['v']; ?>" frameborder="0" allowfullscreen></iframe>
         <?php   }   ?>
         <div class="spacer"></div>
            <?php if(strpos($proyecto->video,'vimeo')) { 
            ?>
                        <!--player.vimeo.com/video/55751501-->
                 <iframe src="//player.vimeo.com/video/<?php echo (int) substr(parse_url($proyecto->video, PHP_URL_PATH), 1);?>" width="100%" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
             <?php   } ?>


      <?php if($imgComplementarias->imagen){?>
      <div class="spacer"></div>
      <h3 class="h3_purpure">+ PHOTOS</h3>
      <?php } ?>
      <?php foreach ($imgComplementarias as $items) {?>
          <div class="spacer"></div>
          <img src="<?php echo base_url() ?>uploads/proyectos/imagenescomplementarias/<?php echo $items->imagen ?>" class="rounded_img" width="100%" />
      <?php } ?>
          <!--<h3 class="h3_purpure">Otras Imagenes</h3>
            <div class="spacer"></div>
            <img src="img/058.jpg" class="rounded_img device_300" width="590" />
            <div class="spacer"></div>
            <img src="img/058.jpg" class="rounded_img device_300" width="590" />
            <div class="spacer"></div>
        	<img src="img/058.jpg" class="rounded_img device_300" width="590" />
-->
			<div class="tags_project">

                             <?php
                            $tags = explode(',', $proyecto->tags_busqueda);
                             $tags = array_filter($tags);
                             ?>

                <?php if(count($tags) > 0){?>
                <span style="background:#fff; color:#333;">Tags:</span>
                <?php }?>

                           <?php for($i=0;$i<count($tags);$i++){
                                echo '<span><a href="javascript:filtrartags('."'".$tags[$i]."'".')">'.$tags[$i].'</a></span>'; 
                                if($i < count($tags)-1){ echo ''; }
                                ?>
                           
                          <?php }
                            ?>
                            <script>
                                function filtrartags(valor){
                                    $('#tags').val(valor);
                                    $('#formtags').submit();
                                    
                                }
                            </script>
                            <form id="formtags" action="<?php echo base_url().'resultados_busqueda/index/0/6/'; ?>" method="post">
                                <input type="hidden" name="tags" id="tags">
                            </form>
            </div>


  </div>
    <div class="col_f device_320">
    	
        <div class="logo_fundacion">
  <?php 
                    function image_exists($url) {
                        if(@getimagesize($url)){
                        return true;
                      }else{
                       return false;
                     }
                    }
                    $imagen = base_url().'uploads/proyectos/imagenescomplementarias/'.$proyecto->imagen;
                    $fundacionimagen = base_url().'uploads/proyectos/gestores/'.$gestor->imagen; 
                    $liderimagen = base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; 
                   ?>       
            <a href="#inline2" onclick="javascript:$('.fancybox').fancybox()"  class="fancybox">
                <img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" width="200" />
            </a>
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Project by: <br /><a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre .'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Leader: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider. '</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Web: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>

            <div class="dato"><?php echo $proyecto->departamento ?><?php if($proyecto->departamento != '' && $proyecto->pais != ''){ echo ','; } ?> <?php echo $proyecto->pais; ?></div>
        </div>
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>
        <div class="state box_min_sh" style="padding:13px 0px">
        	<div class="estado left"><div class="<?php echo $clases[$proyecto->estados_id]; ?> left"><span>Status:</span> <?php echo $estados[$proyecto->estados_id]; ?></div></div>
            <div class="clear" style="margin-bottom:5px;"></div>
            <?php 
             function dameFecha($fecha,$dia)
                {   list($day,$mon,$year) = explode('/',$fecha);
                    return date('d/m/Y',mktime(0,0,0,$mon,$day+$dia,$year));        
                }
            ?>
            <?php if($proyecto->ocutlar_barra_porcentaje != 1){?>
            <div class="estado"><span>Due Date:</span>
                <?php if($dias_para_cerrar <= 0){?>
                Deadline reached
                <?php }else{ ?>
                    <?php echo $dias_para_cerrar;?> days to go
                <?php }?>
            </div>
            <?php }?>
        </div>
        
        <div class="spacer"></div>

      		<div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px">
                <?php if($proyecto->ocutlar_barra_porcentaje == 1){?>
                    <center>This project requires of constant support.</center>
                    <div style="height: 14px"></div>
                <?php }else{ ?>
                    <center>Supported</center>
                    <div class="barra_porcentaje2">
                        <div class="bar2" style="width:<?php echo $proyecto->porcentaje_total;?>%"><?php echo $proyecto->porcentaje_total;?>%</div>
                    </div>
                <?php } ?>

                </div>

 
        	
            </div>
	
    			<div class="spacer"></div>
                <div class="div_line"></div>
				<div class="spacer"></div>

      		  <div class="state box_min_sh" style="padding:13px 0px">
                <div style="margin-bottom:10px"><center>Subscribe to receive updates and<br /> more information about this project!</center></div>
                <input type="text" class="email_input" placeholder="E-mail" />
                <input type="submit" value="Subscribe" class="sub_bt" />
		</div>


        <?php if($proyecto->valormonetario > 20000){  ?>
            <!-- ******** <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <a  href="<?php if($proyecto->estados_id != 1){ echo "javascript:alert('This project is not available to receive donations.')"; }else {  if($login == 'si'){ if($proyecto->valormonetario < 20000){   echo "javascript:alert('The project does not require this type of donation.')"; } else{ echo base_url().'apoyodinero/index/'.$proyecto->id;} }else{ echo '#inlinelogin'; } }?>"  class="link_apoya fancybox">
        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon <?php if($proyecto->valormonetario < 20000){ echo 'ico_big_desactivado'; }?>"><img src="img/063.png" /></div>
            <div class="tit">Apoyo Económico</div>
            <div class="barra_porcentaje2" style="margin:5px auto 10px auto">
                <div class="bar2" style="width:<?php echo $proyecto->porcentaje_monetario;?>%"><?php echo $proyecto->porcentaje_monetario;?>%</div>
            </div>
		    </div>
		    </a>-->
          <?php }?>

             <?php if($proyecto->valormonetario > 20000 && $proyecto->link_economico != ''){  ?>
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <a target="_blank" href="<?php echo $proyecto->link_economico; ?>"  class="link_apoya fancybox">
        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon <?php if($proyecto->valormonetario < 20000){ echo 'ico_big_desactivado'; }?>"><img src="img/063.png" /></div>
            <div class="tit">Donate</div>
            <!--<div class="barra_porcentaje2" style="margin:5px auto 10px auto">
                <div class="bar2" style="width:<?php echo $proyecto->porcentaje_monetario;?>%"><?php echo $proyecto->porcentaje_monetario;?>%</div>
            </div>-->
		    </div>
		    </a>
          <?php }?>
                

        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        
		
         <?php if($cantidadvoluntariados > 0){  ?>
       <!-- <a onclick="javascript:$('.fancybox').fancybox()"  href="<?php if($proyecto->estados_id != 1){ echo "javascript:alert('El proyecto no está disponible para recibir ayudas.')"; }else {  if($login == 'si'){  if($cantidadvoluntariados == 0){  echo "javascript:alert('El proyecto no requiere este tipo de ayuda.')"; } else{ echo base_url().'voluntariado_front/index/'.$proyecto->id; } }else{ echo '#inlinelogin'; } }?>" class="link_apoya fancybox">
        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon <?php if($cantidadvoluntariados == 0){ echo 'ico_big_desactivado'; }?> "><img src="img/065.png" /></div>
            <div class="tit">Volunteer</div>
            <div class="barra_porcentaje2" style="margin:5px auto 10px auto">
                <div class="bar2" style="width:<?php echo $proyecto->porcentaje_voluntariado;?>%"><?php echo $proyecto->porcentaje_voluntariado;?>%</div>
            </div>
		</div>
        </a>-->
        <?php } ?>
        <?php if($cantidadvoluntariados > 0 && $proyecto->link_voluntariado != ''){  ?>
            <a target="_blank"  href="<?php echo $proyecto->link_voluntariado; ?>" class="link_apoya fancybox">
                <div class="state box_min_sh" style="padding:13px 0px">
                    <div class="img_icon <?php if($cantidadvoluntariados == 0){ echo 'ico_big_desactivado'; }?> "><img src="img/065.png" /></div>
                    <div class="tit">Volunteer</div>
                    <!--<div class="barra_porcentaje2" style="margin:5px auto 10px auto">
                        <div class="bar2" style="width:<?php echo $proyecto->porcentaje_voluntariado;?>%"><?php echo $proyecto->porcentaje_voluntariado;?>%</div>
                    </div>-->
                </div>
            </a>
        <?php } ?>
        
        <!--
        
         <?php if($cantidadespecie > 0){  ?>
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <a href="<?php if($proyecto->estados_id != 1){ echo "javascript:alert('El proyecto no está disponible para recibir ayudas.')"; }else { if($login == 'si'){ if($cantidadespecie == 0){  echo "javascript:alert('El proyecto no requiere este tipo de ayuda.')"; } else{  echo base_url().'apoyoespecie/index/'.$proyecto->id; } }else{ echo '#inlinelogin'; }  } ?>"  class="fancybox link_apoya">
        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon  <?php if($cantidadespecie == 0){ echo 'ico_big_desactivado'; }?>"><img src="img/064.png" /></div>
            <div class="tit">Apoyo en Especie</div>
            <div class="barra_porcentaje2" style="margin:5px auto 10px auto">
                <div class="bar2" style="width:<?php echo $proyecto->porcentaje_especie ;?>%"><?php echo $proyecto->porcentaje_especie;?>%</div>
            </div>
		</div>
		</a>
    <?php } ?> -->

    </div>
    
    
    <div class="div_line clear"></div>
    
    <div class="pad_int_20">
    
    
    
    
    <div id="disqus_thread"></div>
<script type="text/javascript">
var disqus_shortname = 'wepopuliblog';
(function() {
    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>

		
    </div>

</div>





<div id="inline2" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px; color:#1c75bc">Project by:</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->nombre;?></h2>
    	<div class="spacer"></div>
    	<div class="txt_user">
            <p><?php echo nl2br($gestor->descripcion);?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Project by: <br /> <a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Leader: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Web: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
	</div>
    
</div>

<div id="inline3" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px"><?php echo $gestor->nombreslider.' '.$gestor->apellidoslider;?></h2>
    	<!--<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->acercadelider;?></h2>-->
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px"><?php echo $gestor->nombre;?></h3>
    
    	<div class="txt_user">
            <p><?php echo nl2br($gestor->acercadelider);?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($liderimagen)){ echo base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; }else{ echo 'img/080.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Contact info:</span></div>
            <?php  if($gestor->emaillider != ''){ echo '<div class="dato"><b>Email: <a href="mailto:'.$gestor->emaillider.'" style="color:#602483">'.$gestor->emaillider.'</a></b></div>'; } ?>
            <?php  if($gestor->facebooklider != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebooklider.'</b></div>'; } ?>
            <?php  if($gestor->twitterlider != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitterlider.'</b></div>'; } ?>
        </div>
	</div>
    
</div>
<?php if($validacion == 1){?>
<script>alert('Gracias por postularse como voluntario. Pronto será contactado por la organización gestora del proyecto.');</script>
<?php } ?>

<div id="inlinelogin" style="width:400px;display:none;">
    <h2 class="h2_b" style="margin-bottom:10px; color:#7f3f98">¿Ya eres un usuario registrado?</h2>
    <p>Para apoyar un proyecto debes ser un usuario registrado en nuestra plataforma. Ingresa con tus datos o   <a href="<?php echo base_url() .'registro'?>" style="color:#0091e9">Regístrate Aquí</a>
  <div class="form_pop_box" style="width:226px;">
    	<div class="label">E-mail <span>*</span></div>
        <input type="text" class="input_a" id="emaillog" />
    	<div class="label">Contraseña <span>*</span></div>
        <input type="password"  onkeypress="checkKey(event);" class="input_a" id="password" />
    </div>
    
    <div class=" clear"></div>
    <input type="submit" class="bt_green" value="Ingresar" onclick="conectarse()" />
    <div class="clear" style="height:10px"></div>

</div>

  <script>
         function checkKey(key){
        var unicode;
        if (key.charCode)
        {unicode=key.charCode;}
        else
        {unicode=key.keyCode;}
        if (unicode == 13){
           conectarse();
        }
    }
    
        function conectarse(){
         var emaillog = $('#emaillog').val();
         var password = $('#password').val();
         var validacion = true;
         
         
         if(emaillog == ''){
             $('#emaillog').addClass("errorform");
             $('#emaillog').attr('placeholder', 'Campo obligatorio.');
             $('#emaillog').focus();
             validacion = false;
        }else{
             $('#emaillog').removeClass("errorform");
            
        }
        
        if(password == ''){
             $('#password').addClass("errorform");
             $('#password').attr('placeholder', 'Campo obligatorio.');
             $('#password').focus();
             validacion = false;
        }else{
             $('#password').removeClass("errorform");
            
        }
        
        $.post( "<?php echo base_url(); ?>registro/login", { emaillog : emaillog , password : password})
          .done(function( data ) {
                if(data == 'Error en los datos suministrados'){
                     alert('Debe ingresar un usuario y contraseña válidos.');
                }else{
                    window.location = '<?php echo base_url(); ?>detalle_proyecto/index/<?php echo $proyecto->id; ?>';
                }
          });
        
        
     }

</script>