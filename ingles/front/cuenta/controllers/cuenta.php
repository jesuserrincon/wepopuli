<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class cuenta extends Front_Controller {

    public function __construct() {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
            CMSPREFIX."cuentas/cuentas",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0, $tipo = '') {
        $this->set_title('Bienvenidos a ' . SITENAME, true);

        parent::index($proyectoid);

        $vista = $this->build2('../../home/views/detalleProyectoCompleto');
        $this->_data["vista"] = $vista;

        if($tipo == 1){
            $this->_data["tipo"] = $tipo;
        }

        $this->_data["id"] = $proyectoid;
        //enviamos el registro del proyecto
        $cuentaOBJ = new Cuentas();
        $this->_data["cuenta"] = $cuentaOBJ->getCuentaProyectosById($proyectoid);
        //Fin envio registro del proyecto

        $this->_data["proyectoid"] = $proyectoid;
        return $this->build('cuenta');
    }


    public function add(){

        $b = new Cuentas();


        if(isset($_FILES['archivo1']['name']) && !empty($_FILES['archivo1']['name'])){
            $config['upload_path'] = UPLOADSFOLDER.'proyectos/cuentas';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|rgb|psd|pdf|doc|docx';
            $config['remove_spaces']=TRUE;
            $config['max_size']	= '10000';

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('archivo1'))
            {
                die($this->upload->display_errors());
            }
            else
            {
                $datos = array(
                    'certificado' => str_replace(' ','_', $_FILES['archivo1']['name']),
                    'proyectos_id' => $this->input->post('id')
                );
                $update = $b->updateCuentas($datos);

            }
        }

        if(isset($_FILES['archivo']['name']) && !empty($_FILES['archivo']['name'])){
            $config['upload_path'] = UPLOADSFOLDER.'proyectos/cuentas';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|rgb|psd|pdf|doc|docx';
            $config['remove_spaces']=TRUE;
            $config['max_size']	= '10000';

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('archivo'))
            {
                die($this->upload->display_errors());
            }
            else
            {
                $datos = array(
                    'nit' => str_replace(' ','_', $_FILES['archivo']['name']),
                    'proyectos_id' => $this->input->post('id')
                );
                $update = $b->updateCuentas($datos);

            }
        }
        //die(var_dump($_POST));

        $datos = array(
            'titular' => $this->input->post('titulo'),
            'banco' => $this->input->post('banco'),
            'tipo_de_cuenta' => $this->input->post('tipo'),
            'numero_de_cuenta'=>$this->input->post('numero'),
            'nombres' => $this->input->post('nombre'),
            'cargo' => $this->input->post('cargo'),
            'email' => $this->input->post('email'),
            'telefono2' => $this->input->post('telefono2'),
            'pais' => $this->input->post('pais'),
            'municipio' => $this->input->post('municipio'),
            'apellidos' => $this->input->post('apellidos'),
            'cedula' => $this->input->post('cedula'),
            'telefono' => $this->input->post('telefono1'),
            'departamento' => $this->input->post('departamento'),
            'direccion' => $this->input->post('direccion'),
            'proyectos_id' => $this->input->post('id')
        );

        $update = $b->updateCuentas($datos);

        if($this->input->post('redirec') == 1){
            return redirect("cuenta/index/".$this->input->post('id')."/1");
        }


        if($this->input->post('redirec') == 2){
            echo $this->input->post('redirec');
        }

        if($this->input->post('redirec') == 3){
            
            return redirect("revision/index/".$this->input->post('id')."");
        }

    }
    
}
