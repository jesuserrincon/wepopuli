<link href="<?php echo base_url(); ?>assets/css/colombia_incluyente_movil.css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/apply.js"></script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "09422a6d-4673-4a62-ac1c-160a92973882", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<!-- Add mousewheel plugin (this is optional) -->	
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancy.js"></script>
<div class="clear"></div>
<div class="txt_reglamento device_300 pad_bottom_0_mobile pad_5_mobile">
  <h2 class="h2_b" style="font-size:44px"><?php echo $proyecto->titulo; ?></h2>
  <h2 class="h2_des">Proyecto por: <?php echo $gestor->nombre; ?></h2>
</div>

    
    
<div class="box_shadow_big clearfix device_320">

  <div class="col_e min_h_900 pad_int_20 device_300">
       <img src="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $proyecto->imagen_miniatura; ?>" class="rounded_img device_300" width="590" />
  	   <div class="spacer"></div>
       
       <h2 class="h2_c left">Adelante! <br /><span class="h2_c_span">Apoya como voluntario.</span></h2>
  		
        <div class="icon_apoya icon_apoya_3"></div>
        <div class="clear"></div>

        
    <h2 class="h2_azul device_320">¡Es hora de apoyar el cambio!</h2>
  		<div class="txt_14">Sus ganas de apoyar se sumarán a las de muchas más personas que también creen y quieren un mundo mejor, más humano e inclusivo.</div>
        <div class="div_line_2"></div>
        
        <div class="spacer"></div>
        
    	<h2 class="h2_c left">Selecciona cómo apoyar.</h2>
       <div class="clear"></div>
       <input type="hidden" name="idvoluntariado" id="idvoluntariado" value="0">
       <?php 
       $contador = 0;
       $contadorcompletos = 0;
       foreach ($voluntariados as $item){ $contador++; ?>
       	<div class="box_shadow3 relative">
        	<div class="pad_box_10">
                    <?php if($item->cantidad == $item->donados){ $contadorcompletos++;?>
                    Completo!!
                    <?php }else{ ?>
                    <a href="" onclick="javascript:checkvolintariado(<?php echo $item->id; ?>)" id="a<?php echo $item->id; ?>" class="no_apply2 left grupovol">Aplicar</a>
                    <?php } ?>
                <div class="clear"></div>
            	<h2 class="h3_b"><?php echo $item->titulo;?></h2>
                <div class="txt_voluntario">
                <p><?php echo $item->descripcion;?></p>
                </div>
        	</div>
            <div  class="active_box_green grupovoldiv"></div>
        </div>
       <?php } ?>
        
       
        <div class="spacer pad_top_10_mobile clear" style="padding-top:20px;"></div>
       <?php if($contadorcompletos == $contador){?>
        <a href="<?php echo base_url();?>detalle_proyecto/index/<?php echo $proyecto->id; ?>" class="boton_a" style="display:block; float:left; margin-right:10px; margin-left:115px">Atrás</a>
       <?php }else{ ?>
        <a href="<?php echo base_url();?>detalle_proyecto/index/<?php echo $proyecto->id; ?>" class="boton_a" style="display:block; float:left; margin-right:10px; margin-left:115px">Atrás</a>
        <a href="javascript:validar_paso()" class="boton_a" style="display:block; float:left">Continuar</a>
       <?php } ?>

  </div>
  
  
  
  
  
    <div class="col_f device_320">
    	
        <div class="logo_fundacion">
  <?php 
                    function image_exists($url) {
                        if(@getimagesize($url)){
                        return true;
                      }else{
                       return false;
                     }
                    }
                    $imagen = base_url().'uploads/proyectos/imagenes/'.$proyecto->imagen; 
                    $liderimagen = base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; 
                    $fundacionimagen = base_url().'uploads/proyectos/gestores/'.$gestor->imagen; 
                   ?>       
            <a href="#inline2" onclick="javascript:$('.fancybox').fancybox()"  class="fancybox">
                    
       <img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" width="200" /></a>
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Proyecto por: <br /><a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Liderado por: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Total recibido</center></div>
            <div class="barra_porcentaje2">
                <div class="bar2" style="width:<?php echo $proyecto->porcentaje_voluntariado;?>%"><?php echo $proyecto->porcentaje_voluntariado;?>%</div>
            </div>

		</div>
        <div class="spacer"></div>

        <div class="div_line"></div>
        <div class="spacer"></div>
        
        <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Importante</h3>
            <div class="txt_box_min">
            	Wepopuli es un buscador de organizaciones y proyectos sociales, una plataforma de crowdsource social donde puedes encontrar las organizaciones y proyectos a nivel mundial que mejor se adapten a tus talentos y necesidades, teniendo en cuenta criterios como sector, localización y objetivos del proyecto, fechas, aptitudes solicitadas, etc., a las que podrás ayudar como voluntario de campo o desde tu casa u oficina, dependiendo de sus requerimientos.
			</div>
            <div class="line_color"></div>
		</div>
		
        <div class="spacer"></div>	
            
      <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Preguntas Frecuentes</h3>
            <div class="txt_box_min">
            	¿Cómo contribuyo a un proyecto? Cada proyecto tiene sus necesidades puntuales. Para verlas, ingresa como usuario registrado a la web y revisa las necesidades desplegadas por la organización que lidera el proyecto.</div>
            <div class="line_color color2"></div>
		</div>


        <div class="spacer"></div>
        <div class="spacer"></div>

    </div>
    
    
    <div class="div_line clear dis_none_movil"></div>
    
    <div class="pad_int_20">
    
    
    </div>

</div>
    

<script>
    function checkvolintariado(id){
        $( "a" ).removeClass( "apply2" );
        $('.grupovol').html('Aplicar');
        $('.grupovoldiv').css("display", "none");
        $('#idvoluntariado').val(id);
    }
    function validar_paso(){
        var idvol = $('#idvoluntariado').val();
        if(idvol == 0){
            alert('Debe seleccionar una ayuda para seguir en el proceso.');
            return false;
        }else{
            location.href = '<?php echo base_url().'voluntariado_paso2/index/'.$proyecto->id.'/';?>'+idvol;
        }
    }
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/aplication.js"></script>

<div id="inline2" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px; color:#1c75bc">Fundación</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->nombre;?></h2>
    	<div class="spacer"></div>
    	<div class="txt_user">
            <p><?php echo $gestor->descripcion;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Proyecto por: <a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Liderado por: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
	</div>
    
</div>

<div id="inline3" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px"><?php echo $gestor->nombreslider.' '.$gestor->apellidoslider;?></h2>
    	
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px"><?php echo $gestor->nombre;?></h3>
    
    	<div class="txt_user">
            <p><?php echo $gestor->acercadelider;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($liderimagen)){ echo base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; }else{ echo 'img/080.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Información de contacto:</span></div>
            <?php  if($gestor->emaillider != ''){ echo '<div class="dato"><b>Email: <a href="mailto:'.$gestor->emaillider.'" style="color:#602483">'.$gestor->emaillider.'</a></b></div>'; } ?>
            <?php  if($gestor->facebooklider != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebooklider.'</b></div>'; } ?>
            <?php  if($gestor->twitterlider != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitterlider.'</b></div>'; } ?>
        </div>
	</div>
    
</div>

    