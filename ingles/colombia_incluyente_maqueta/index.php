<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<?php include"metadata.php" ?>  
</head>

<body>
<?php include"header.php" ?>  
<div class="box_a">
    <div class="info_txt">
        <h2>¿De donde vienen los proyectos?</h2>
        Vienen de las necesidades de Colombia, de ti, de mi, de todos.<br />
        Si eres una fundación, proyecto, persona y necesitas apoyo para un proyecto social, unete ya. 
    </div>
</div>

<div class="div_line"></div>
  <div class="wrapper relative">
    <ul class="rslides" id="slider2">
      <li><a href="#"><img src="assets/img/009.jpg" alt=""></a></li>
      <li><a href="#"><img src="assets/img/025.png" alt="" style="height:223px"></a></li>
      <li><a href="#"><img src="assets/img/009.jpg" alt=""></a></li>
    </ul>
  </div>


<div class="box_a">
	<div class="div_line"></div>
    <div class="info_txt">
        <h2>¿De donde viene el apoyo?</h2>
        De todos los que queremos hacer una sociedad incluyente y optimizar las posibilidades de las personas<br />
        que necesitan de nuestra ayuda.
    </div>
    <div class="div_line"></div>
	<a href="busca_apoyo.php" class="bt_a">Busca Apoyo</a>
</div>

<div class="box_b search_area clearfix">
	<h2 class="h2_b search_h2">Descrubre y apoya proyectos</h2>
    <div class="search_box left">
        <input type="submit" class="go" value="" title="Buscar" />
        <input type="text" class="search" placeholder="Buscar" />
    </div>
    
    <div class="right">
    <select class="select">
        <option value="">Seleccione el valor</option>
        <option>$100.000 - $200.000</option>
        <option>$100.000 - $200.000</option>
        <option>$100.000 - $200.000</option>
        <option>$100.000 - $200.000</option>
        <option>$100.000 - $200.000</option>
        <option>$100.000 - $200.000</option>
    </select>
	</div>    
</div>

    <h3 class="h3_line">Destacados</h3>
    <div class="box_a relative">
        <div class="slides">
            <div class="slides_container">
                <div class="content clearfix">
                	<div class="project_box">
						<div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="detalle_proyecto.php">
                                <img class="thumbnail_a" src="assets/img/058.jpg" />
                            </a>
                        </div>
						<div class="title">Por un país mejor <br />Colombia</div>
                        <div class="des">
                         Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                        </div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                            	<li class="by">Fundación pies descalsos</li>
                            	<li class="by">Rio Magdalena, Colombia </li>
                            	<li class="by">6 dias faltantes</li>
							</ul>
                        </div>
                        
                    	<div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                        	<div class="money_icon_a icons"></div>
                            <div class="percent_circle">100%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="members_icon_a icons"></div>
                            <div class="percent_circle">50%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="shop_icon_a icons"></div>
                            <div class="percent_circle">20%</div>
                        </div>
                        <div class="clear"></div>
                        
                        
                        <div class="barra_porcentaje">
                        	<div class="bar" style="width:45%">45%</div>
                        </div>
                        
                    </div><!-- // project box -->
                	
                	<div class="project_box">
						<div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="#"  target="_blank">
                                <img class="thumbnail_a" src="assets/img/024.jpg" />
                            </a>
                        </div>
						<div class="title">Por un país mejor</div>
                        <div class="des">
                         Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                        </div>
                        <div class="location">Rio Magdalena, Colombia</div>
                    	<div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                        	<div class="money_icon_a icons"></div>
                            <div class="percent_circle">100%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="members_icon_a icons"></div>
                            <div class="percent_circle">50%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="shop_icon_a icons"></div>
                            <div class="percent_circle">20%</div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="title_percent">Porcentaje total</div>
                        
                        <div class="barra_porcentaje">
                        	<div class="bar" style="width:25%">25%</div>
                        </div>
                        
                        
                    </div><!-- // project box -->

                	<div class="project_box">
						<div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="#"  target="_blank">
                                <img class="thumbnail_a" src="assets/img/024.jpg" />
                            </a>
                        </div>
						<div class="title">Por un país mejor</div>
                        <div class="des">
                         Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                        </div>
                        <div class="location">Rio Magdalena, Colombia</div>
                    	<div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                        	<div class="money_icon_a icons"></div>
                            <div class="percent_circle">100%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="members_icon_a icons"></div>
                            <div class="percent_circle">50%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="shop_icon_a icons"></div>
                            <div class="percent_circle">20%</div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="title_percent">Porcentaje total</div>
                        
                        <div class="barra_porcentaje">
                        	<div class="bar" style="width:95%">95%</div>
                        </div>
                                                
                    </div><!-- // project box -->

                	<div class="project_box">
						<div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="#"  target="_blank">
                                <img class="thumbnail_a" src="assets/img/024.jpg" />
                            </a>
                        </div>
						<div class="title">Por un país mejor</div>
                        <div class="des">
                         Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                        </div>
                        <div class="location">Rio Magdalena, Colombia</div>
                    	<div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                        	<div class="money_icon_a icons"></div>
                            <div class="percent_circle">100%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="members_icon_a icons"></div>
                            <div class="percent_circle">50%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="shop_icon_a icons"></div>
                            <div class="percent_circle">20%</div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="title_percent">Porcentaje total</div>
                        
                        <div class="barra_porcentaje">
                        	<div class="bar" style="width:65%">65%</div>
                        </div>                        
                        
                    </div><!-- // project box -->
                </div><!-- // content  -->

                <div class="content clearfix">
                	<div class="project_box">
						<div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="#"  target="_blank">
                                <img class="thumbnail_a" src="assets/img/024.jpg" />
                            </a>
                        </div>
						<div class="title">Por un país mejor</div>
                        <div class="des">
                         Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                        </div>
                        <div class="location">Rio Magdalena, Colombia</div>
                    	<div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                        	<div class="money_icon_a icons"></div>
                            <div class="percent_circle">100%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="members_icon_a icons"></div>
                            <div class="percent_circle">50%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="shop_icon_a icons"></div>
                            <div class="percent_circle">20%</div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="title_percent">Porcentaje total</div>
                        
                        <div class="barra_porcentaje">
                        	<div class="bar" style="width:100%">100%</div>
                        </div>
                        
                    </div><!-- // project box -->
                	
                	<div class="project_box">
						<div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="#"  target="_blank">
                                <img class="thumbnail_a" src="assets/img/024.jpg" />
                            </a>
                        </div>
						<div class="title">Por un país mejor</div>
                        <div class="des">
                         Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                        </div>
                        <div class="location">Rio Magdalena, Colombia</div>
                    	<div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                        	<div class="money_icon_a icons"></div>
                            <div class="percent_circle">100%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="members_icon_a icons"></div>
                            <div class="percent_circle">50%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="shop_icon_a icons"></div>
                            <div class="percent_circle">20%</div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="title_percent">Porcentaje total</div>
                        
                        <div class="barra_porcentaje">
                        	<div class="bar" style="width:95%">95%</div>
                        </div>
                        
                                
                        
                    </div><!-- // project box -->

                	<div class="project_box">
						<div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="#"  target="_blank">
                                <img class="thumbnail_a" src="assets/img/024.jpg" />
                            </a>
                        </div>
						<div class="title">Por un país mejor</div>
                        <div class="des">
                         Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                        </div>
                        <div class="location">Rio Magdalena, Colombia</div>
                    	<div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                        	<div class="money_icon_a icons"></div>
                            <div class="percent_circle">100%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="members_icon_a icons"></div>
                            <div class="percent_circle">50%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="shop_icon_a icons"></div>
                            <div class="percent_circle">20%</div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="title_percent">Porcentaje total</div>
                        
                        <div class="barra_porcentaje">
                        	<div class="bar" style="width:95%">95%</div>
                        </div>
                        
                                
                        
                    </div><!-- // project box -->

                	<div class="project_box">
						<div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="#"  target="_blank">
                                <img class="thumbnail_a" src="assets/img/024.jpg" />
                            </a>
                        </div>
						<div class="title">Por un país mejor</div>
                        <div class="des">
                         Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                        </div>
                        <div class="location">Rio Magdalena, Colombia</div>
                    	<div class="line_div_project"></div>
                        
                        <div class="percent_icon_box first_precent">
                        	<div class="money_icon_a icons"></div>
                            <div class="percent_circle">100%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="members_icon_a icons"></div>
                            <div class="percent_circle">50%</div>
                        </div>
                        <div class="percent_icon_box">
                        	<div class="shop_icon_a icons"></div>
                            <div class="percent_circle">20%</div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="title_percent">Porcentaje total</div>
                        
                        <div class="barra_porcentaje">
                        	<div class="bar" style="width:95%">95%</div>
                        </div>
                        
                                
                        
                    </div><!-- // project box -->
                </div><!-- // content  -->

            </div>
        </div>
	</div>
    
    
    <h3 class="h3_line">Todos</h3>
 	<div class="box_a pad_box clearfix" style="width:960px">
        
        <div id="projects_show">
            <div class="project_box">
                <div class="imgLiquidFill imgLiquid liquid_a">
                    <a href="#"  target="_blank">
                        <img class="thumbnail_a" src="assets/img/024.jpg" />
                    </a>
                </div>
                <div class="title">Por un país mejor</div>
                <div class="des">
                 Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                </div>
                <div class="location">Rio Magdalena, Colombia</div>
                <div class="line_div_project"></div>
                
                <div class="percent_icon_box first_precent">
                    <div class="money_icon_a icons"></div>
                    <div class="percent_circle">100%</div>
                </div>
                <div class="percent_icon_box">
                    <div class="members_icon_a icons"></div>
                    <div class="percent_circle">50%</div>
                </div>
                <div class="percent_icon_box">
                    <div class="shop_icon_a icons"></div>
                    <div class="percent_circle">20%</div>
                </div>
                <div class="clear"></div>
                
                <div class="title_percent">Porcentaje total</div>
                
                <div class="barra_porcentaje">
                    <div class="bar" style="width:5%">5%</div>
                </div>
                
                
                
            </div><!-- // project box -->
            
            <div class="project_box">
                <div class="imgLiquidFill imgLiquid liquid_a">
                    <a href="#"  target="_blank">
                        <img class="thumbnail_a" src="assets/img/024.jpg" />
                    </a>
                </div>
                <div class="title">Por un país mejor</div>
                <div class="des">
                 Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                </div>
                <div class="location">Rio Magdalena, Colombia</div>
                <div class="line_div_project"></div>
                
                <div class="percent_icon_box first_precent">
                    <div class="money_icon_a icons"></div>
                    <div class="percent_circle">100%</div>
                </div>
                <div class="percent_icon_box">
                    <div class="members_icon_a icons"></div>
                    <div class="percent_circle">50%</div>
                </div>
                <div class="percent_icon_box">
                    <div class="shop_icon_a icons"></div>
                    <div class="percent_circle">20%</div>
                </div>
                <div class="clear"></div>
                
                <div class="title_percent">Porcentaje total</div>
                
                <div class="barra_porcentaje">
                    <div class="bar" style="width:95%">95%</div>
                </div>
                
                
                
            </div><!-- // project box -->

            <div class="project_box">
                <div class="imgLiquidFill imgLiquid liquid_a">
                    <a href="#"  target="_blank">
                        <img class="thumbnail_a" src="assets/img/024.jpg" />
                    </a>
                </div>
                <div class="title">Por un país mejor</div>
                <div class="des">
                 Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                </div>
                <div class="location">Rio Magdalena, Colombia</div>
                <div class="line_div_project"></div>
                
                <div class="percent_icon_box first_precent">
                    <div class="money_icon_a icons"></div>
                    <div class="percent_circle">100%</div>
                </div>
                <div class="percent_icon_box">
                    <div class="members_icon_a icons"></div>
                    <div class="percent_circle">50%</div>
                </div>
                <div class="percent_icon_box">
                    <div class="shop_icon_a icons"></div>
                    <div class="percent_circle">20%</div>
                </div>
                <div class="clear"></div>
                
                <div class="title_percent">Porcentaje total</div>
                
                <div class="barra_porcentaje">
                    <div class="bar" style="width:95%">95%</div>
                </div>
                
                
                
            </div><!-- // project box -->

            <div class="project_box">
                <div class="imgLiquidFill imgLiquid liquid_a">
                    <a href="#"  target="_blank">
                        <img class="thumbnail_a" src="assets/img/024.jpg" />
                    </a>
                </div>
                <div class="title">Por un país mejor</div>
                <div class="des">
                 Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                </div>
                <div class="location">Rio Magdalena, Colombia</div>
                <div class="line_div_project"></div>
                
                <div class="percent_icon_box first_precent">
                    <div class="money_icon_a icons"></div>
                    <div class="percent_circle">100%</div>
                </div>
                <div class="percent_icon_box">
                    <div class="members_icon_a icons"></div>
                    <div class="percent_circle">50%</div>
                </div>
                <div class="percent_icon_box">
                    <div class="shop_icon_a icons"></div>
                    <div class="percent_circle">20%</div>
                </div>
                <div class="clear"></div>
                
                <div class="title_percent">Porcentaje total</div>
                
                <div class="barra_porcentaje">
                    <div class="bar" style="width:95%">95%</div>
                </div>
                
                
                
            </div><!-- // project box -->
        </div>
        
        
        
        <div class="clear"></div>
        
        <h3 onclick="javascript:cargar_contenido()" class="h3_line more_projects">Ver más proyectos</h3>
		<div class="hidden_projects">
        
        </div>
		
    </div>
    
    
    <div class="wrapper relative">
        <ul class="rslides" id="slider3">
          <li><a href="#"><img src="assets/img/025.png" alt=""></a></li>
      	  <li><a href="#"><img src="assets/img/009.jpg" style="height:215px"></a></li>
          <li><a href="#"><img src="assets/img/025.png" alt=""></a></li>
        </ul>
    </div>
	<?php include"footer.php" ?>  
</body>
</html>