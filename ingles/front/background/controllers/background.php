<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author rigobcastro
 * @author Brayan Acebo
 * @author Jose Fonseca
 */
class Background extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $idioma = $this->session->userdata('lang');
        if($idioma == false){           
            return redirect('home/index');
        }
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($paginaActual = 0) {
        $this->load->model(array(
                    CMSPREFIX."antecedentes/antecedentes",
                    CMSPREFIX."antecedentes/campanas",
                    CMSPREFIX."servicios/servicios",
                    CMSPREFIX."directorios/directorios",
                    CMSPREFIX."redes/redes",
                    )
                );
        $lang = $this->session->userdata('lang');
        
        $cServi = new Servicios();
        $dServi = '';
            $cServi->limit(9,0);
            $servi = $cServi->getServiciosByIdioma($lang);
            foreach ($servi as $data){
                $dServi .='<li><a href="'.  base_url() .'services/detail/'.$data->id.'">'.$data->titulo.'</a></li>';
            }
        
        if($lang == 1){
            $titulo ='Antecedentes';
        }else{
            $titulo ='Antecedents';
        }
       $cantidadPoPagina = 6;
       $registroActual = $cantidadPoPagina * $paginaActual;
       $cAnte = new Antecedentes();
       $cantidad = $cAnte->where('idioma_id',$lang)->count();
       $cAnte->limit($cantidadPoPagina,$registroActual);
       $ante = $cAnte->getAntecedentesByIdioma($lang);
       
       $cCampana = new Campanas();
       $dAnte = '';
       foreach($ante as $data){   
           $cCampana->limit(1,0);
           $campana = $cCampana->getCampanasByIdAntecedentes($data->id);
           $cant = $cCampana->where("antecedente_id", $data->id)->count();           
           $dAnte .= '<li>
                            <figure>';
           if($cant > 0){
                      $dAnte .='<a href="'.  base_url() .'background/detail/'.$campana->id.'">';
           }else{
             $dAnte .='<a href="#">';
           }                    
             $dAnte .='             <img src="'.  base_url() .'uploads/antecedentes/new/'.$data->imagen.'" class="opacity portafolio-img" />
                                    <div class="lupa"></div>
                                </a>
                            </figure>
                        </li>'; 
       }
        $paginador = '<div class="paginador">';
         $cantidadPaginas = ceil($cantidad / $cantidadPoPagina);
         for ($i = 0; $i < $cantidadPaginas; $i++) {
            if($paginaActual == $i){ 
                $paginador .='<a class="pagina_activa">'.($i + 1).'</a>';
            }else{ 
                $paginador .='<a href="'.  base_url() .'background/index/'.$i.'">'. ($i + 1).'</a>';
          }
        }
            $paginador .= '</div>';
        
        $cDirectorios = new Directorios();
        $directorios = $cDirectorios->getDirectorios();
        $cant = $cDirectorios->count();
        $total = ($cant / 2);
       
       $dDirectorios = '';
        for($j = 0 ; $j < ceil($total) ; $j++){
           $h = $j*2;
           $cDirectorios->limit(2,$h)->order_by("id", "ASC");           
           $directorios = $cDirectorios->getDirectorios();        
        $i = 1;
            $dDirectorios .='<div class="clearfix">';
            foreach ($directorios as $data){
                if($i == 1){
                    $dDirectorios .='<ul class="left">';
                }else{
                    $dDirectorios .='<ul class="right">';
                }
                $dDirectorios .='
                                    <li><h4>'.$data->ciudad.'</h4></li>
                                    <li class="footer-icon-1">'.nl2br($data->direccion).'</li>
                                    <li class="footer-icon-2">'.$data->telefono.'</li>
                                    <li class="footer-icon-3"><a href="mailto:'.$data->email.'">'.$data->email.'</a></li>
                                </ul>';
            $i = $i + 1;
            }
            $dDirectorios .='</div>';
        }
        $cRedes = new Redes();
        $redes = $cRedes->getRedesById(1);
        
        $this->_data["seccion"] = 'background';
        $this->_data["titulo"] = $titulo;
        $this->_data["antecedentes"] = $dAnte;        
        $this->_data["paginador"] = $paginador;
        $this->_data["directorios"] = $dDirectorios;
        $this->_data["redes"] = $redes;
        $this->_data["servi"] = $dServi;
        $this->build('antecedentes');
    }

    // ----------------------------------------------------------------------
    
     public function detail($id = '') {
        $this->load->model(array(
                    CMSPREFIX."antecedentes/antecedentes",
                    CMSPREFIX."antecedentes/campanas",
                    CMSPREFIX."antecedentes/imgcampanas",
                    CMSPREFIX."servicios/servicios",
                    CMSPREFIX."directorios/directorios",
                    CMSPREFIX."redes/redes",
                    )
                );  
       $lang = $this->session->userdata('lang');
       $cServi = new Servicios();
       $dServi = '';
        $cServi->limit(9, 0);
        $servi = $cServi->getServiciosByIdioma($lang);
        foreach ($servi as $data) {
            $dServi .='<li><a href="' . base_url() . 'services/detail/' . $data->id . '">' . $data->titulo . '</a></li>';
        }
       
       
       $cDetalle = new Campanas();
       $detalle = $cDetalle->getCampanasById($id);
       $cMenu = new Campanas();
       $menu = $cMenu->getCampanasByIdAntecedentes($detalle->antecedente_id); 
       $dMenu ='';
       foreach ($menu as $data){
           if($data->id == $id){
              $dMenu .='<li class="camp_activa"><a href="'.  base_url() .'background/detail/'.$data->id.'">'.$data->titulo.'</a></li>';
           }else{
              $dMenu .='<li><a href="'.  base_url() .'background/detail/'.$data->id.'">'.$data->titulo.'</a></li>';   
           }
       }
       
       
       $cImagenes = new Imgcampanas();
       $imagenes = $cImagenes->getImgcampanasByIdCampanas($id);
       $cant = $cImagenes->where('campana_id',$id)->count();
       $dImagenes = '';
       foreach ($imagenes as $data){
           $dImagenes .='<li>
                            <img src="'.  base_url() .'uploads/antecedentes/new/'.$data->imagen.'" />
                         </li>';
       }
        
      $cDirectorios = new Directorios();
        $directorios = $cDirectorios->getDirectorios();
        $cant = $cDirectorios->count();
        $total = ($cant / 2);
       
       $dDirectorios = '';
        for($j = 0 ; $j < ceil($total) ; $j++){
           $h = $j*2;
           $cDirectorios->limit(2,$h)->order_by("id", "ASC");           
           $directorios = $cDirectorios->getDirectorios();        
        $i = 1;
            $dDirectorios .='<div class="clearfix">';
            foreach ($directorios as $data){
                if($i == 1){
                    $dDirectorios .='<ul class="left">';
                }else{
                    $dDirectorios .='<ul class="right">';
                }
                $dDirectorios .='
                                    <li><h4>'.$data->ciudad.'</h4></li>
                                    <li class="footer-icon-1">'.nl2br($data->direccion).'</li>
                                    <li class="footer-icon-2">'.$data->telefono.'</li>
                                    <li class="footer-icon-3"><a href="mailto:'.$data->email.'">'.$data->email.'</a></li>
                                </ul>';
            $i = $i + 1;
            }
            $dDirectorios .='</div>';
        }
        
        $cRedes = new Redes();
        $redes = $cRedes->getRedesById(1);
        
        $this->_data["seccion"] = 'background';
        $this->_data["detalle"] = $detalle;
        $this->_data["menu"] = $dMenu;        
        $this->_data["imagenes"] = $dImagenes;        
        $this->_data["cant"] = $cant;        
        $this->_data["directorios"] = $dDirectorios;
        $this->_data["redes"] = $redes;
        $this->_data["servi"] = $dServi;       
        $this->build('detalle');
     }
}
