<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class usuario extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."textos/textos",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($idpro = 0) {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        
        $usuario = $this->nativesession->get('usuario');
        $usuarioOBJ = new Usuarios();
        $this->_data["usuario"] =  $usuarioOBJ->getUsuariosById($usuario['id']);
        $this->_data['clave'] = $usuario['id'];
        $this->_data["idpro"] = $idpro;
        return $this->build('usuario');
    }

    public function eliminarcorreo(){
        $usuario = $this->nativesession->get('usuario');
         $datos = array(
            'cambio_correo' => '',
            'id' => $usuario['id']
        );
            $usuariosOBJ = new Usuarios();
            $actualizar = $usuariosOBJ->updateUsuarios($datos);
            redirect('usuario/index/1', 'refresh');
    }
    
    public function validarclave(){
        $usuario = $this->nativesession->get('usuario');
        if(md5($this->input->post('clave')) == $usuario['clave']){
            $datos = array(
            'clave' => md5($this->input->post('nuevaclave')),
            'id' => $usuario['id']
        );
            $usuariosOBJ = new Usuarios();
            $actualizar = $usuariosOBJ->updateUsuarios($datos);
                //actualizamos la variable de SESSION
                $datosusu = $usuariosOBJ->loginUsuario($usuario['email'],md5($this->input->post('nuevaclave')));
                if ($datosusu == 0) {
                    echo 2;
                } else {
                    $this->nativesession->set('usuario', $datosusu);
                    
                }
                //Fin actualizar la variable de SESSION
            echo 1;
        }else{
            echo 0;
        }
    }
    
    
    
    public function cambiarcorreo(){
        $correo = $this->input->post('correocambio');
        $usuario = new Usuarios();
        $datosUsuario = $usuario->usuarioByCorreo($correo);

        if(empty($datosUsuario->id)){
            $usuarioOBJ1 = new Usuarios();
            $usuario = $this->nativesession->get('usuario');
            $datosUsuariocon = $usuarioOBJ1->usuarioByCc($usuario['cedula']);
            $datos = array(
            'cambio_correo' => $correo,
            'id' => $datosUsuariocon->id,
        );
            $usuarioOBJ = new Usuarios();
            $usuarioOBJ->updateUsuarios($datos);
            $this->load->library('email');
            $emali = $correo;
            $asunto = 'Cambio de correo en Colombia Incluyente';
            $titulo = 'Cambio de correo';
            $mensaje = 'Haga click en el siguiente link para activar el cambio de correo electrónico en su cuenta de Colombia Incluyente
            <a href="'.  base_url().'home/cambiodecorreo/' . $usuario['cedula'] . '">Cambiar correo</a>';
            $this->enviarCorreo($emali,$asunto,$titulo,$mensaje);

            echo 1;
        }else{
            echo 0;
        }
    }
 
    
    public function UsuarioAct() {
         
        $usuario = $this->nativesession->get('usuario');

        $this->load->model(array(
            CMSPREFIX . "usuarios/usuarios",
        ));

        $datos = array(
            'nombres' => $this->input->post('nombres'),
            'apellidos' => $this->input->post('apellidos'),
            'telefono' => $this->input->post('telefono'),
            'telefono2' => $this->input->post('telefono2'),
            'pais' => $this->input->post('pais'),
            'departamento' => $this->input->post('departamento'),
            'direccion' => $this->input->post('direccion'),
            'facebook' => $this->input->post('facebook'),
            'twitter' => $this->input->post('twitter'),
            'ciudad' => $this->input->post('ciudad'),
            'sexo' => $this->input->post('sexo'),
            'estado_civil' => $this->input->post('estado_civil'),
            'newsletter' => $this->input->post('newsletter'),
            'recordarme' => $this->input->post('recordarme'),
            'fecha_nacimiento' => $this->input->post('fecha_nacimiento'),
            'pais_nacionalidad' => $this->input->post('pais_nacionalidad'),
            'newsletter' => $this->input->post('newsletter'),
            'id' => $usuario['id'],
        );
            $usuariosOBJ = new Usuarios();
            $usuariosOBJ->updateUsuarios($datos);
            
        //Volvemos a crear la variable de SESSION para actualizar los datos
        $usuarioOBJ = new Usuarios();
        $usuario = $this->nativesession->get('usuario');
        $datosusu = $usuarioOBJ->loginUsuario($usuario['email'], $usuario['clave']);
        if ($datosusu == 0) {
            echo 'Error en los datos suministrados';
        } else {
            $this->nativesession->set('usuario', $datosusu);
            redirect('usuario/index/1', 'refresh');
        }
        
    }
    

    // ----------------------------------------------------------------------


}
