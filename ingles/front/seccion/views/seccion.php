<style type="text/css">
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>
<div class="box_a pad_box clearfix">

<div class="wrapper relative">
    <ul class="rslides" id="slider2">
        <?php foreach($bannerSuperior as $data){?>
      <li><a href="#"><img src="<?php echo base_url()."uploads/banner/".$data->imagen ?>" alt="" style="height:223px"></a></li>
        <?php } ?>

    </ul>
  </div>
  
  <div class="big_box">
        <h2 class="h2_b"><span style="font-family:Arial; font-weight:bold; color:#75796f"><?php echo $texto->titulo?></span></h2>
    		<div class="txt">
            <div class="clear" style="margin-bottom:30px;"></div>
            
            <?php 
                    function str_replace_limit($search, $replace, $string, $limit = 1) {
                            if (is_bool($pos = (strpos($string, $search))))
                                return $string;

                            $search_len = strlen($search);

                            for ($i = 0; $i < $limit; $i++) {
                                $string = substr_replace($string, $replace, $pos, $search_len);

                                if (is_bool($pos = (strpos($string, $search))))
                                    break;
                                }
                            return $string;

                    }
            $texto2 = str_replace('/colombiaincluyente/back/assets/js/kcfinder-2.51/upload/files/', base_url().'back/assets/js/kcfinder-2.51/upload/files/',$texto->texto);
            //echo str_replace_limit('colombiaincluyente', '',  $texto2,0);
            echo $texto2;
            ?>
            <center></center>
            <?php 
            function youtubeId($url) {
                if($url != '') {
                    $match = preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);
                    if((int) $match > 0) {
                            return $matches[0];
                    }
                }
                return false;
            }
            if(youtubeId($texto->video) != false){
                echo '<center><iframe width="560" height="315" src="//www.youtube.com/embed/'.youtubeId($texto->video).'" frameborder="0" allowfullscreen></iframe></center>';
            }else{
                 if($texto->video != '') {
                     echo '<center><iframe src="'.$texto->video.'" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="'.$texto->video.'">Shepard Fairey: Obey This Film</a> from <a href="http://vimeo.com/brettnovak">Brett Novak</a> on <a href="https://vimeo.com">Vimeo</a>.</p><center>';
                 }
            }
            ?>
            <div class="clear" style="margin-top:30px;"></div>
            
                <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'wepopuli'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    
        </div>
        
  </div>
  
  <div class="wrapper relative">
    <ul class="rslides" id="slider3">
        <?php foreach($bannerInferior as $data){?>
      <li><a href="#"><img src="<?php echo base_url()."uploads/banner/".$data->imagen ?>" alt="" style="height:223px"></a></li>
      <?php } ?>
    </ul>
  </div>

</div>

<div id="inline1" style="width:550px;display:none;">
    <h4>Antes, debes saber que:<br />
      <br />
    </h4>
    <p>Lo que se publique debe ser un proyecto concreto y debe
      poder medirse el impacto y verse el resultado de manera tangible.</p>
    <p>El proyecto debe estár localizado en Colombia.</p>
    <p>El proyecto lo debe gestionar una organización debidamente registrada en la camara de comercio Colombiana, con fecha de constitución no menor a 3 años y un minimo de 3 proyectos realizados que cumplan con criterios reservados de Colombia Incluyente.</p>


    <h4>¿Dónde se encuentra el proyecto?<br />
      <br />
    </h4>
  <div class="form_pop_box">
   	  <div class="label">Country </div>
        <input type="text" disabled="disabled" class="input_a" value="Colombia" />
    	<div class="label">State/Province <span>*</span></div>
        <input type="text" class="input_a" />
    	<div class="label">Municipio <span>*</span></div>
        <input type="text" class="input_a" />
    </div>
    
    <div class="div_line clear" style="margin-bottom:20px; margin-top:20px;"></div>
  <input type="submit" class="bt_green" value="continuar" />
</div>



<div id="inline2" style="width:400px;display:none;">
    <h4>¿Eres Usuario de Colombia Incluyente?<br /><br /></h4>
    <p>Para postular un proyecto debe ser un usuario registrado en nuestra plataforma. Ingrese con sus datos o de lo contario puede <a href="" style="color:#0091e9">registrarse gratis.</a>
  <div class="form_pop_box"><br />
    	<div class="label">Usuario <span>*</span></div>
        <input type="text" class="input_a" />
    	<div class="label">Password<span>*</span></div>
        <input type="password" class="input_a" />
    </div>
    
    <div class="div_line clear" style="margin-bottom:20px; margin-top:20px;"></div>
  	<a href="Rules.php"><input type="submit" class="bt_green" value="ingresa" /></a>
</div>

