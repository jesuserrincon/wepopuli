<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class buscaapoyo_ci extends Front_Controller {

    public function __construct() {
        $this->load->library('session');

        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($accion = 0) {

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."textos/textos",
        ));
        
        $textosOBJ = new Textos();
        $texto = $textosOBJ->getTextosById(1);
        $this->_data["texto"] = $texto->texto;
        
        $this->_data["accion"] = $accion;
        $this->_data["usuario"] = $this->nativesession->get('usuario');
        return $this->build('buscaapoyo_ci');
    }
    
 
    

    // ----------------------------------------------------------------------


}
