<?php

class _Usuarios extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "USUARIOS";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Usuarios();
        $info = $b->getUsuarios();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('usuarios');
    }

    public function add() {
        $this->_data['nombremodulo'] = "CATEGORIAS / New";
        $this->build('usuarios_new');
    }

    public function new_Usuarios() {
        $datos = array(
            'banner_superior' => $this->input->post('imagen'),
            'banner_inferior' => $this->input->post('imagen2'),
            'nombre' => $this->input->post('nombre'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'usuarios';
        $b = new Usuarios();
        if ($datos['banner_superior'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'usuarios_new';
            if ($b->saveUsuarios($datos)){
                $build = 'usuarios';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getUsuarios();
        $this->_data["info"] = $info;
        return redirect('cms/usuarios/index/'.$save);
        
    }

    
    
    
    
    
    
    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'usuarios/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."usuarios/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('usuarios/'.$data["data"]["file_name"], array('longside'=>'usuarios/new','width' => 1000, 'height' => 223,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = 0,$tipo = 0,$idpro = 0){
        $b = new Usuarios();
        $usuarioOBJ = new Usuarios();
        $datosusu = $usuarioOBJ->loginUsuarioAdmin($id);
        if ($datosusu == 0) {
            echo 'Error en los datos suministrados';
        } else {
            $this->nativesession->set('usuario', $datosusu);
            $this->nativesession->set('usuario_admin', 1);
            //print_r($datosusu);
            if($tipo == 1){
                redirect('basicos/index/'.$idpro,'refresh');
            }else{
                redirect('usuario','refresh');
            }
        }
    }
    
    public function update_Usuarios(){
        $b = new Usuarios();
        $datos = array(
            'id' => $this->input->post('id'),
            'nombre' => $this->input->post('nombre'),

        );

        $update = $b->updateUsuarios($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'banner_superior' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateUsuarios($datos);
        }
        if($this->input->post('imagen2') != ''){
            $datos = array(
            'banner_inferior' => $this->input->post('imagen2'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateUsuarios($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/usuarios/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Usuarios();
        $return = FALSE;
        //if ($b->eliminar($id)){
            $usuario = $b->getUsuariosById($id);
            $asunto = 'Cierre de cuenta';
            $titulo = 'Cuenta cerrada';
            $mensaje = 'Su cuenta de usuario ha sido cerrada.';
            $this->enviarCorreo($usuario->email,$asunto,$titulo,$mensaje);
            $return = TRUE;
        //};
        return redirect("cms/usuarios/index/".$return);
    }
    
    
    public function export_reporte($id = 0){
        
        $this->load->library('excel');
        $objPHPExcel = new Excel();
        $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

        
        //Llamamos la consulta y obtenemos los usuarios registrados
        $b = new Usuarios();
        $info = $b->getReporteByUsuarios($id);
        //Fin obtener datos
        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nombres');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Apellidos');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Proyecto');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Tipo');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Ayuda voluntariado');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Monto de ayuda');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Fecha');
        
       
        
        $i = 3;
        foreach($info as $item){
            
               switch ($item->tipo){
                    case 0:
                        $tipo = 'Voluntariado';
                    break;
                    case 1:
                        $tipo = 'Especie';
                    break;
                    case 2:
                        $tipo = 'Monetario';
                    break;
                }
            
                
             $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $item->nombres);
             $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $item->apellidos);
             $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $item->nombreproyecto);
             $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $tipo);
             $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $item->titulo);
             $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 0);
             $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $item->fecha);
             
             $i++;
        }
        
        // Rename sheet
        //echo date('H:i: s') . " Rename sheet\n";
        $objPHPExcel->getActiveSheet()->setTitle('Reporte ');


        // Save Excel 2007 file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte_usuario.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        // Save it as an excel 2003 file
        //$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter->save("nameoffile.xls");
    }


    public function exportar_usuarios() {
         
        $this->load->library('excel');
        $objPHPExcel = new Excel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        
        //Llamamos la consulta y obtenemos los usuarios registrados
        $b = new Usuarios();
        $info = $b->getUsuarios();
        //Fin obtener datos
        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nombres');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Apellidos');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Tipo identificación');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Teléfono');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Pais nacionalidad');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Fecha de nacimiento');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Teléfono');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'No de identificación');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Email');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Teléfono 2');
        $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Pais');
        $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Ciudad');
        $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Departamento');
        $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Sexo');
        $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Dirección');
        $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Estado civil');
        $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Facebook');
        $objPHPExcel->getActiveSheet()->setCellValue('R1', 'Twitter');
        $objPHPExcel->getActiveSheet()->setCellValue('S1', 'Newsletter');
        
        $i = 3;
        foreach($info as $item){
             $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $item->nombres);
             $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $item->apellidos);
             $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $item->tipo_identificacion);
             $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $item->telefono);
             $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $item->pais_nacionalidad);
             $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $item->fecha_nacimiento);
             $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $item->telefono);
             $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $item->cedula);
             $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $item->email);
             $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $item->telefono2);
             $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $item->pais);
             $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $item->ciudad);
             $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $item->departamento);
             $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $item->sexo);
             $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $item->direccion);
             $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $item->estado_civil);
             $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $item->facebook);
             $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $item->twitter);
             $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $item->newsletter);
             $i++;
        }
        
        // Rename sheet
        //echo date('H:i: s') . " Rename sheet\n";
        $objPHPExcel->getActiveSheet()->setTitle('Base de datos usuarios');


        // Save Excel 2007 file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte_general.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        // Save it as an excel 2003 file
        //$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter->save("nameoffile.xls");
    }

}

