<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo 'Proyectos' ?></span>
        <br />          
    </div><!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div class="imu_info" id="info"></div>
                    <?php //print_r($info)?>
                    <?php echo form_open_multipart('cms/proyectos/UpdateDefaultDias/', 'id="form"') ?>

                    <div class="section">
                        <label>Dias para cerrar proyecto</label>
                        <div><input type="text" name="dias" id="dias" value="<?php echo $dias;?>"></div>
                        
                    </div>
                      
                    
                    <div style="width: 100px;height: 30px;position: relative;top: 30px">
                        <input onclick="valida();" type="button" value="Guardar" class="uibutton confirm" />
                        
                    </div>
                    <?php echo form_close() ?>
                    <p>&nbsp;</p>
                 </fieldset>
                <br>
            </div>
        </div>
    </div>	
</div><!-- End content -->
<script type="text/javascript">
    function valida(){
        var titulo = $("#titulo").val();
        var texto = $("#texto").val();
        if (titulo == "" || texto == ""){
            $('#info').focus();
            showError('Complete todos los campos.',3000);
        }else{
            $('#form').submit();
        }
    }
    $(document).ready(function() {
        var texto = '';
        var nextinput = parseInt($("#inputTotal").val());
        $('#fileUpload1').uploadify({
                    'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                    'script'            : '<?php echo site_url(); ?>' + 'cms/proyectos/upload/',
                    'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                    'auto'              : true,
                    'folder'            : '',
                    'queueSizeLimit'    : 3,
                    'multi'             : false,
                    'fileExt'           : '*.jpg;*.jpeg;*.png;*gif',
                    'auto'              : true,
                    'buttonText'        : 'Cargar imagen.',
                    'onComplete'  : function(event, queueId, fileObj, response, data) {
                var responseJson = $.parseJSON(response);
                
                if (responseJson.ok === true) {
                        $("#img1").attr('src','<?php echo base_url() ?>uploads/proyectos/'+responseJson.data.file_name);
                        $("#img1").hide(800).delay(2000).show(800);
                        $("#divimg1").html('<h2>Cargando imagen...</h2>')
                        $("#divimg1").show(850).delay(2000).hide(750);
                        $("#imagen").val(responseJson.data.file_name)
                } else {
                        showError('Problemas al carga imagen.',3000);                            
                }
            }
                });   
                
                
                
                
                    
             //Subimos la segunda imagen
             
               $('#fileUpload2').uploadify({
                    'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                    'script'            : '<?php echo site_url(); ?>' + 'cms/proyectos/upload/',
                    'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                    'auto'              : true,
                    'folder'            : '',
                    'queueSizeLimit'    : 3,
                    'multi'             : false,
                    'fileExt'           : '*.jpg;*.jpeg;*.png;*.gif',
                    'auto'              : true,
                    'buttonText'        : 'Cargar imagen.',
                    'onComplete'  : function(event, queueId, fileObj, response, data) {
                var responseJson = $.parseJSON(response);
                //alert(responseJson.ok);
                if (responseJson.ok === true) {
                        $("#img2").attr('src','<?php echo base_url()?>uploads/proyectos/'+responseJson.data.file_name);
                        $("#img2").hide(800).delay(2000).show(800);
                        $("#divimg2").html('<h2>Cargando imagen...</h2>')
                        $("#divimg2").show(850).delay(2000).hide(750);
                        $("#imagen2").val(responseJson.data.file_name)
                } else {
                        showError('Problemas al carga imagen.',3000);                            
                }
            }
                });
                
                
                
    
    });
</script>
 <script>
    <?php if (isset($update)){
            if ($update ==  1){?>
            showSuccess('Actualización correcta.',3000);
            <?php } ?>
    <?php } ?>
</script>