﻿
  <style type="text/css">#nav-bt3 {color: #464646;} #nav-bt3 span {background-position:-92px -60px;} .gal-img-b img {-webkit-box-shadow: 0px 10px 20px -5px rgba(0, 0, 0, 0.4); box-shadow: 0px 10px 20px -5px rgba(0, 0, 0, 0.4);} .gal-img-b img {height:auto; width:100%;}</style>


  <section>
    <div class="con-section">
      <div class="mg-section cfx">
        <h1 class="int-tt cfx"><?php echo $info->titulo; ?></h1>
        <div class="con-info-pro cfx">
        	<!--Galería-->
        	<div class="con-gal-info fl">
            <div class="gal-img-b cfx">
            	<img class="big-gal ver-foto-1 fl" src="<?php echo base_url() ?>uploads/servicios/new/<?php echo $info->imagen ?>" alt=""><!--Foto de 486px * 486px-->
            </div>
          </div>
          <!--Fin galería-->
          <div class="con-gal-tx fr">
            <p align="justify">
                <?php echo nl2br($info->texto); ?>
            </p>

          </div>
        </div>
        <div class="con-back-bt"><a class="back-bt tr" href="javascript:history.back()"><span class="tr">«</span>Volver</a></div>
      </div>
    </div>
  </section>
