<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  
<style type="text/css">
#s5 {
	color:#fff;
	background-position:0px -100px;
}
#s4 {
	color:#fff;
	background-position:0px -50px;
}
#s3 {
	color:#fff;
	background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -50px;
}
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>

<link href="assets/css/jquery-filestyle.css" rel="stylesheet" />
  	
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
<script src="assets/js/jquery.sticky.js"></script>
<script>
  $(document).ready(function(){
    $(".sticker").sticky({topSpacing:30});
	
	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
		   $('.sticker').addClass('fixed_button_cuenta');
		   }else{
			   $('.sticker').removeClass('fixed_button_cuenta');
		   }
	});
  });
</script>
</head>

<body>
<?php include"header.php" ?>  
<?php include"paso_paso.php" ?>

<div class="txt_reglamento">
  <h2 class="h2_b">Cuenta Bancaria</h2>
	La siguiente información es necesaria para llevar a cabo la recolección de los fondos para apoyar el proyecto. <br />
Los datos suministrados serán tratados como confidenciales por Ci. </div>

<div class="box_a pad_box clearfix">
    
    <div class="col_c">
      <div class="box_shadow box_c">
       	  <h6>Esta cuenta obligatoriamente debe estar registrada en Colombia.</h6>
          
	        
            <div class="col_int_box_c left">
                <div class="label">Titular de la cuenta</div>
                <input type="text" class="input_a" />
            	<div class="label">Banco</div>
                <input type="text" class="input_a" />
            </div>
            
            <div class="col_int_box_c right">
           		<div class="label">Tipo de Cuenta</div>
            	<input type="text" class="input_a" />
                <div class="label">Número de cuenta</div>
                <input type="text" class="input_a" />

            </div>
            <div class="clear"></div>
                <div class="label">Nit / Rut</div>
                <input type="file" class="jfilestyle" data-theme="green">

                <div class="label">Certificado Bancario</div>
                <input type="file" class="jfilestyle" data-theme="green2">
	  </div>

	  <div class="div_green clear"></div>
            
      <div class="box_shadow box_c" >
            <h6>Datos de contacto financiero</h6>
            <p>A continuación se deberán ingresar los datos de la persona designada por la organización para el manejo de los procesos bancarios.</p>
		
        
            <div class="col_int_box_c left">
                    <div class="label">Nombres</div>
                    <input type="text" class="input_a" />
                    <div class="label">Cargo</div>
                    <input type="text" class="input_a" />
                    <div class="label">Email</div>
                    <input type="text" class="input_a" />
                    <div class="label">Teléfono 2 (opcional)</div>
                    <input type="text" class="input_a" />
                    <div class="label">País</div>
                    <input type="text" class="input_a" />
                    <div class="label">Municipio</div>
                    <input type="text" class="input_a" />
            </div>
            <div class="col_int_box_c right">
           		<div class="label">Apellidos</div>
            	<input type="text" class="input_a" />
            	<div class="label">No. Cédula</div>
            	<input type="text" class="input_a" />
            	<div class="label">Teléfono 1</div>
            	<input type="text" class="input_a" />
            	<div class="label">Departamento (Región)</div>
            	<input type="text" class="input_a" />
            	<div class="label">Dirección</div>
            	<input type="text" class="input_a" />
            </div>
		
      </div>

      <div class="clear spacer"></div>
          
          
            <input type="submit" value="Borrar Proyecto" class="boton_c left" style="margin-right:10px;">
            <input type="submit" value="Guardar Cambios" class="boton_a left" style="margin-right:10px;">
            <a href="reglamentos.php" class="boton_b left" style="margin-right:10px;">Atrás</a>
            <a href="reglamentos.php" class="boton_b left">Siguiente</a>

        
    </div><!-- // columna C -->
    
    <div class="col_d mar_right">
        <div class="project_box sticker">
            <div class="imgLiquidFill imgLiquid liquid_a">
                <a href="#"  target="_blank">
                    <img class="thumbnail_a" src="assets/img/024.jpg" />
                </a>
            </div>
            <div class="title">Por un país mejor <br />Colombia</div>
            <div class="des">
             Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
            </div>
            
            <div class="resumen">
                <ul class="data_project_min"> 
                    <li class="by">Fundación pies descalsos</li>
                    <li class="by">Rio Magdalena, Colombia </li>
                    <li class="by">6 dias faltantes</li>
                </ul>
            </div>
            
            <div class="line_div_project"></div>
            
            <div class="percent_icon_box first_precent">
                <div class="money_icon_a icons"></div>
                <div class="percent_circle">100%</div>
            </div>
            <div class="percent_icon_box">
                <div class="members_icon_a icons"></div>
                <div class="percent_circle">50%</div>
            </div>
            <div class="percent_icon_box">
                <div class="shop_icon_a icons"></div>
                <div class="percent_circle">20%</div>
            </div>
            <div class="clear"></div>
            
            
            <div class="barra_porcentaje">
                <div class="bar" style="width:45%">45%</div>
            </div>
            
        </div><!-- // project box -->
    </div>
</div>
<script type="text/javascript" src="assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="assets/js/aplication.js"></script>

<?php include"footer.php" ?>  
</body>
</html>