<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo $nombremodulo ?></span>
    </div>
    <!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div style="position: absolute;float: left;height: 60px;">
                        <?php
                        //if ($count < 9) {
                            echo anchor(base_url()."cms/usuarios/exportar_usuarios", "Exportar usuarios", 'class="uibutton icon add" style="top: 100%; border-radius: 10px;"');
                        //}
                        ?>
                    </div>
                    <p>&nbsp;</p>

                    <div class="tableName toolbar">
                        <table class="display data_table2">
                            <thead>
                            <tr>
                                <th><div class='th_wrapp'>Nombres</div> </th>
                                <th><div class='th_wrapp'>Apellidos</div> </th>
                                <th><div class='th_wrapp'>Email</div></th>
                                <th><div class='th_wrapp'>No identificación</div></th>
                                <th><div class='th_wrapp'>Teléfono</div></th>
                                <th>
                                    <div class='th_wrapp'> Acciones</div>
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($info as $d) { ?>
                                <tr class="odd gradeX">

                                    <td width="25%" class="center"><?php echo $d->nombres; ?></td>
                                    <td width="25%" class="center"><?php echo $d->apellidos; ?></td>
                                    <td width="25%" class="center"><?php echo $d->email; ?></td>
                                    <td width="25%" class="center"><?php echo $d->cedula; ?></td>
                                    <td width="25%" class="center"><?php echo $d->telefono; ?></td>
                                   
                                    <td width="25%" class="center">
                                        <?php 
                                              echo anchor("cms/usuarios/export_reporte/" . $d->id, "Reporte", "class='uibutton'"); 
                                              echo anchor("cms/usuarios/edit/" . $d->id, "Editar", "class='uibutton' target='_blanck'");
                                              
                                        ?>
                                        <a onclick="return confirm('Seguro desea eliminar el item ?');" href="<?php echo base_url('cms/usuarios/delete') .'/'.$d->id ?>" class="uibutton  special" >Eliminar</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>
<script>
    function valida() {
        var titulo = $("#titulo").val();
        var texto = $("#texto").val();
        if (titulo == "" || texto == "") {
            $('#info').focus();
            showError('Complete todos los campos.', 3000);
        } else {
            $('#form').submit();
        }
    }
    <?php if (isset($save)){
            if ($save==1){?>
    showSuccess('Acción correcta.', 3000);
    <?php } ?>
    <?php } ?>
</script>