<link href="<?php echo base_url(); ?>assets/css/colombia_incluyente_movil.css" rel="stylesheet" />
<script type="text/javascript">
$(document).ready(function () {
	
	$(".no_apply").click(function(){
	  $(this).toggleClass("apply");
	  return false;
	});
	
	
	$(".apply").click(function(){
	  $(this).toggleClass("no_apply");
	  return false;
	});
	
	
	
	$('.no_apply').click(function(){
       if($(this).text() == 'No aplicar'){
           $(this).text('Aplicar');
       } else {
           $(this).text('No aplicar');
       }
	});
	
	$('.no_apply').click(function() {
		
		$(this).parent().parent().find('.active_box_green').fadeToggle(400);		
		
		
		return false;
	});
  

	
});
</script>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "09422a6d-4673-4a62-ac1c-160a92973882", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fancy.js"></script>
<div class="clear"></div>
<div class="txt_reglamento device_300 pad_bottom_0_mobile pad_5_mobile">
  <h2 class="h2_b" style="font-size:44px"><?php echo $proyecto->titulo; ?></h2>
  <h2 class="h2_des">Proyecto por: <?php echo $gestor->nombre; ?></h2>
</div>

    
    
<div class="box_shadow_big clearfix device_320">

  <div class="col_e min_h_900 pad_int_20 device_300">
       
        <h2 class="h2_b left" style="color:#7f3f98">¡Gracias por contribuir!</h2>
  		
        <div class="icon_apoya icon_apoya_3"></div>
        <div class="clear"></div>

        
	<div class="div_line_2"></div>
        
        <div class="spacer"></div>
    	<h2 class="h2_c left">Título del apoyo seleccionado.</h2>
       <div class="clear"></div>
       
       	<div class="box_shadow3 relative">
        	<div class="pad_box_10">
        	  <h2 class="h3_b"><?php echo $voluntariado->titulo;?></h2>
                <div class="txt_voluntario">
                <p><?php echo $voluntariado->descripcion;?></p>
                </div>
        	</div>
            <div class="active_box_green"></div>
        </div>
        
        
        <div class="div_line_2 spacer" style="background:none"></div>
        
        <div class="col_g">
        	
            <div class="info_data">
            	<div class="label_data">Nombres</div>
                <div class="data_user"><?php echo $usuario->nombres;?></div>
            </div>
        	
            <div class="info_data">
            	<div class="label_data">Apellidos</div>
                <div class="data_user"><?php echo $usuario->apellidos;?></div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Tipo de identificación</div>
                <div class="data_user"><?php echo $usuario->tipo_identificacion;?></div>
            </div>
            
            
            
            <div class="info_data">
            	<div class="label_data">No. Identificación</div>
                <div class="data_user">CC <?php echo $usuario->cedula;?></div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">País Nacionalidad</div>
                <div class="data_user"><?php echo $usuario->pais;?></div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">País Residencia</div>
                <div class="data_user"><?php echo $usuario->pais;?></div>
            </div>

 			<div class="info_data">
            	<div class="label_data">Departamento (Región)</div>
                <div class="data_user"><?php echo $usuario->departamento;?></div>
            </div>
                    
            <div class="info_data">
            	<div class="label_data">Municipio</div>
                <div class="data_user"><?php echo 'Zipaquira';?></div>
            </div>


			<div class="info_data">
            	<div class="label_data">Dirección</div>
                <div class="data_user"><?php echo $usuario->direccion;?></div>
            </div>
            
			<div class="info_data">
            	<div class="label_data">Teléfono 1</div>
                <div class="data_user"><?php echo $usuario->telefono;?></div>
            </div>
            
            <div class="info_data">
            	<div class="label_data">Teléfono 2 (opcional)</div>
                <div class="data_user"><?php echo $usuario->telefono2;?></div>
            </div>
            
            
            <div class="info_data">
            	<div class="label_data">Fecha de nacimiento</div>
                <div class="data_user"><?php echo $usuario->fecha_nacimiento ?></div>
            </div>

            <div class="info_data">
            	<div class="label_data">Sexo</div>
                <div class="data_user"><?php echo $usuario->sexo;?></div>
            </div>
            
            
            
            <div class="info_data">
            	<div class="label_data">Estado Civil</div>
                <div class="data_user"><?php echo $usuario->estado_civil;?></div>
            </div>

            
            <div class="info_data">
            	<div class="label_data">Facebook (opcional)</div>
                <div class="data_user"><?php echo $usuario->facebook;?></div>
            </div>
            
            
            <div class="info_data">
            	<div class="label_data">Twitter (opcional)</div>
                <div class="data_user"><?php echo $usuario->twitter;?></div>
            </div>
            
            <div class="info_data" style="margin-bottom:0px">
            	<div class="label_data">Email</div>
                <div class="data_user"><?php echo $usuario->email;?></div>
            </div>

			<div class="clear"></div>
        </div>
        
        <div class="clear" style="height:30px"></div>
        
        <form id="formulario" name="formulario" method="post" enctype="multipart/form-data" action="<?php echo base_url().'voluntariado_paso2/registrarayuda';?>">
            <div class="info_data">
            	<div class="label_data" style="font-size:15px; margin-bottom:4px">¿Tienes algún tipo de discapacidad?</div>
                <select class="select" id="tienediscapacidad" name="tienediscapacidad">
                    <option value="no">NO</option>
                    <option value="si">SI</option>
                </select>
            </div>
            
            
            <div class="info_data">
            	<div class="label_data">¿Cúal?</div>
                <input type="text" class="input_b" name="cual" id="cual" />
            </div>
            <div class="clear"></div>
        <div class="label_data">¿Por qué quiero apoyar como voluntario?</div>
        
        <textarea class="text_area_small" name="porque" id="porque"></textarea><br />
        Máximo de caracteres <span id="porque-text">0/600</span>
        
       	<div class="spacer pad_top_10_mobile clear" style="padding-top:20px;"></div>


        <div class="clear"></div>
        
        <center>
            Sube una fotografía tuya que sea reciente.<br />
            <input type="file" name="imagen" id="imagen" />
        </center>
        <input type="hidden" name="tipo_voluntariado" id="tipo_voluntariado" value="<?php echo $idvol; ?>">
        <input type="hidden" name="idpro" id="idpro" value="<?php echo $idpro; ?>">
        <input type="hidden" name="nombreayuda" id="nombreayuda" value="<?php echo $voluntariado->titulo;?>">
        </form>
        <div class="clear"></div>




        <div class="acept_box clear">
            <a class="verde_bt2">Acepto </a> 
            <a href="#terminos_cond"  class="fancybox terms">términos y condiciones</a>
        </div>
        <a href="<?php echo base_url().'voluntariado_front/index/'.$proyecto->id ;?>" class="boton_a"  style="display:block; float:left; margin-right:10px; margin-left:140px">Atrás</a>
        <a href="javascript:ayudarvoluntariado()" class="boton_a" style="display:block; float:left">Enviar formulario</a>
        
        </div>
  
  
  
  
  
    <div class="col_f device_320">
    	
        <div class="logo_fundacion">
  <?php 
                    function image_exists($url) {
                        if(@getimagesize($url)){
                        return true;
                      }else{
                       return false;
                     }
                    }
                    $imagen = base_url().'uploads/proyectos/imagenes/'.$proyecto->imagen; 
                    $liderimagen = base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; 
                     $fundacionimagen = base_url().'uploads/proyectos/gestores/'.$gestor->imagen; 
                   ?>       
            <a href="#inline2" onclick="javascript:$('.fancybox').fancybox()"  class="fancybox">
                    
       <img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" width="200" /></a>
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Proyecto por: <br /><a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Liderado por: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
        
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Total recibido</center></div>
            <div class="barra_porcentaje2">
                <div class="bar2" style="width:<?php echo $proyecto->porcentaje_voluntariado;?>%"><?php echo $proyecto->porcentaje_voluntariado;?>%</div>
            </div>

		</div>
        <div class="spacer"></div>

        <div class="div_line"></div>
        <div class="spacer"></div>
        
        <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Importante</h3>
            <div class="txt_box_min">
            	Wepopuli es un buscador de organizaciones y proyectos sociales, una plataforma de crowdsource social donde puedes encontrar las organizaciones y proyectos a nivel mundial que mejor se adapten a tus talentos y necesidades, teniendo en cuenta criterios como sector, localización y objetivos del proyecto, fechas, aptitudes solicitadas, etc., a las que podrás ayudar como voluntario de campo o desde tu casa u oficina, dependiendo de sus requerimientos.
			</div>
            <div class="line_color"></div>
		</div>
		
        <div class="spacer"></div>	
            
      <div class="state box_min_sh_2">
        	<h3 class="h3_box_min">Preguntas Frecuentes</h3>
            <div class="txt_box_min">
            	¿Cómo contribuyo a un proyecto? Cada proyecto tiene sus necesidades puntuales. Para verlas, ingresa como usuario registrado a la web y revisa las necesidades desplegadas por la organización que lidera el proyecto.</div>
            <div class="line_color color2"></div>
		</div>


        <div class="spacer"></div>
        <div class="spacer"></div>

    </div>
    
    
    <div class="div_line clear dis_none_movil"></div>
    
    <div class="pad_int_20">
    
    
    </div>

</div>
    
<script>
    
    
    
function init_contadorTa(idtextarea, idcontador,max)
{
    $("#"+idtextarea).keyup(function()
            {
                updateContadorTa(idtextarea, idcontador,max);
            });
    
    $("#"+idtextarea).change(function()
    {
            updateContadorTa(idtextarea, idcontador,max);
    });
    
}

function updateContadorTa(idtextarea, idcontador,max)
{
    var contador = $("#"+idcontador);
    var ta =     $("#"+idtextarea);
    contador.html("0/"+max);
    
    contador.html(ta.val().length+"/"+max);
    if(parseInt(ta.val().length)>max)
    {
        ta.val(ta.val().substring(0,max-1));
        contador.html(max+"/"+max);
    }

}


init_contadorTa("porque","porque-text", 600);

    function ayudarvoluntariado(){
        var tiene = $('#tienediscapacidad').val();
        var cual = $('#cual').val();
        var porque = $('#porque').val();
        var imagen = $('#imagen').val();
        
        if(tiene == 'si'){
            if(cual == ''){
                alert('Debe describir cual dispacidad tiene.');
                return false;
            }
            
        }
        if(porque == ''){
            alert('Debe dar una breve descripción de porqué quiere apoyar como voluntario.');
            return false;
        }
        if(imagen == ''){
            alert('Debe seleccionar una imagen.');
            return false;
        }
        if($('.verde_bt2').hasClass('active_recordarme')){
            
        }else{
            alert('Debe aceptar los términos y condiciones para postularse como voluntario.');
            return false;
        }
        
        $('#formulario').submit();
        
    }
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/aplication.js"></script>



<div id="inline2" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px; color:#1c75bc">Fundación</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->nombre;?></h2>
    	<div class="spacer"></div>
    	<div class="txt_user">
            <p><?php echo $gestor->descripcion;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Proyecto por:<br /> <a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Liderado por: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
	</div>
    
</div>

<div id="inline3" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px"><?php echo $gestor->nombreslider.' '.$gestor->apellidoslider;?></h2>
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px"><?php echo $gestor->nombre;?></h3>
    
    	<div class="txt_user">
            <p><?php echo $gestor->acercadelider;?></p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php if(image_exists($liderimagen)){ echo base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; }else{ echo 'img/080.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Información de contacto:</span></div>
            <?php  if($gestor->emaillider != ''){ echo '<div class="dato"><b>Email: <a href="mailto:'.$gestor->emaillider.'" style="color:#602483">'.$gestor->emaillider.'</a></b></div>'; } ?>
            <?php  if($gestor->facebooklider != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebooklider.'</b></div>'; } ?>
            <?php  if($gestor->twitterlider != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitterlider.'</b></div>'; } ?>
        </div>
	</div>
    
</div>

    
    
<!-- //popup -->
<div id="terminos_cond" style="display:none;" class="device_250">
	<div class="device_250">
        <h2 class="h2_b" style="margin-bottom:20px;">Terminos y Condiciones</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet libero viverra mi tristique ultrices. Fusce nec leo dapibus, pulvinar justo et, lacinia tortor. Duis a consequat magna. Cras sodales, lectus ut pellentesque congue, diam metus mattis enim, ac tincidunt nisi turpis ut metus. Ut in sapien at lacus semper facilisis vitae sed odio. Morbi facilisis faucibus eros vitae pharetra. Fusce eu ipsum congue, viverra velit a, mollis nunc. Nam mattis ligula eget metus aliquet, sed commodo justo aliquet.
        Donec consequat at sapien nec ullamcorper. <br />
        <br />
        Aliquam erat volutpat. Morbi dolor lacus, bibendum eu velit sit amet, cursus facilisis arcu. Fusce eu facilisis lorem. Pellentesque eu augue at mauris fringilla volutpat. Suspendisse tincidunt sit amet nisi id adipiscing. Integer porttitor nisl quis dignissim cursus. In suscipit sapien eros, ac vulputate quam iaculis vitae. Suspendisse justo odio, porttitor quis eros ut, adipiscing gravida nunc.
        <br /><br />
        Praesent accumsan eget nulla eget faucibus. Fusce eu nulla in odio pharetra gravida. Curabitur sollicitudin enim tellus, vitae commodo nunc dapibus vel. Ut rutrum quam id quam tempor, in pulvinar justo ultrices. Morbi ultricies, dolor eu tempus pretium, neque leo scelerisque neque, sit amet blandit ligula diam ac tortor. Cras eleifend scelerisque nibh, tempor vehicula ligula auctor at. Nullam arcu justo, egestas et libero non, placerat aliquam augue. Fusce elementum et sem nec malesuada. Suspendisse interdum vitae diam quis hendrerit. Etiam convallis malesuada porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
        <br /><br />
        Proin laoreet gravida eleifend. Ut varius semper metus vel consectetur. Integer ut massa sed ligula aliquam tincidunt. Aenean purus nunc, pulvinar eu scelerisque vel, commodo et erat. Sed consequat, diam sed lacinia congue, mi lectus porttitor nisi, ac fermentum justo erat a nisi. Vivamus ac rutrum felis, et commodo ante. Aliquam dignissim, neque vel volutpat rutrum, nisl ante sollicitudin lectus, at interdum nulla elit consectetur tellus. In commodo nisi quis sem euismod feugiat. Aenean accumsan porta tempor. Nullam porta faucibus risus, nec rhoncus orci feugiat in. Nunc hendrerit consequat laoreet. Aliquam congue vestibulum porta.</p>
	</div>
</div>