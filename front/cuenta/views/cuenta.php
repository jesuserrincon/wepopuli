<style type="text/css">
#s5 {
	color:#fff;
	background-position:0px -100px;
}
#s4 {
	color:#fff;
	background-position:0px -50px;
}
#s3 {
	color:#fff;
	background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -50px;
}
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>

<link href="css/jquery-filestyle.css" rel="stylesheet" />
  	
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
<script src="js/jquery.sticky.js"></script>
<script>
  $(document).ready(function(){
      
      $("#archivo").bind('change', function(){
      if (this.files[0].size >= 2097152) {
      var tamanio = this.files[0].size/1048576;
      tamanio = tamanio.toFixed(2);
     alert('ATENCIÓN:\n\nSu archivo pesa '+(tamanio)+' Mb y supera el límite permitido de 2,0 MB.');
      $(this).val('');
      return false;
      }else{
          seleccionado();
      }
      });
      
      $("#archivo1").bind('change', function(){
      if (this.files[0].size >= 2097152) {
      var tamanio = this.files[0].size/1048576;
      tamanio = tamanio.toFixed(2);
     alert('ATENCIÓN:\n\nSu archivo pesa '+(tamanio)+' Mb y supera el límite permitido de 2,0 MB.');
      $(this).val('');
      return false;
      }else{
          seleccionado();
      }
      });
      
    $(".sticker").sticky({topSpacing:30});
	
	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
		   $('.sticker').addClass('fixed_button_cuenta');
		   }else{
			   $('.sticker').removeClass('fixed_button_cuenta');
		   }
	});
  });
  
function confirm_click(){
return confirm("Si ya guardó cambios, o no lo desea hacer en este momento, presione Aceptar/Ok para continuar.");
}
</script>
<div class="paso_paso clearfix">
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>reglamento/index/<?php echo $proyectoid; ?>" id="s1" class="step">Reglamentos</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>basicos/index/<?php echo $proyectoid; ?>" id="s2" class="step">Básicos</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>historia/index/<?php echo $proyectoid; ?>" id="s3" class="step">Historia</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>gestores_front/index/<?php echo $proyectoid; ?>" id="s4" class="step">Gestores</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>cuenta/index/<?php echo $proyectoid; ?>" id="s5" class="step">Cuenta</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>revision/index/<?php echo $proyectoid; ?>" id="s6" class="step">Revisión</a>
    <a onclick="return confirm_click();"  href="<?php echo base_url(); ?>previsualizar/index/<?php echo $proyectoid; ?>" id="s7" class="step">Previsualizar</a>
</div>

<div class="txt_reglamento">
  <h2 class="h2_b">Cuenta Bancaria</h2>
	La siguiente información es necesaria para llevar a cabo la recolección de los fondos para apoyar el proyecto. <br />
Los datos suministrados serán tratados como confidenciales. </div>

<div class="box_a2 pad_box clearfix">
    <form id="form" action="<?php echo base_url() ?>cuenta/add" method="post" enctype="multipart/form-data">
    <div class="col_c">

      <div class="box_shadow box_c">
       	  <h6>Información de cuenta bancaria</h6>
          <p>La cuenta bancaria debe estar debidamente registrada a nombre de la organización gestora<br /> del proyecto.</p>
	   <br />
     <form action="<?php echo base_url() ?>cuenta/add" method="post" enctype="multipart/form-data">
       
        <table width="490" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>
            
            <div class="col_int_box_c left">
                <div class="label">Titular de la cuenta</div>
                <input tabindex="1" name="titulo" id="titulo" value="<?php echo $cuenta->titular;  ?>" type="text" class="input_a guardar" />
            	<div class="label">Banco</div>
                <input tabindex="3" type="text" value="<?php echo $cuenta->banco;  ?>" name="banco" id="banco" class="input_a guardar" />
            </div>
            
            <div class="col_int_box_c right">
           		<div class="label">Tipo de Cuenta</div>
                <input tabindex="2" type="text" name="tipo" id="tipocu" value="<?php echo $cuenta->tipo_de_cuenta;  ?>" class="input_a guardar" />
                <div class="label">Número de cuenta</div>
                <input tabindex="4" type="text" onkeypress="return isNumberKey(event)" name="numero" id="numero" value="<?php echo $cuenta->numero_de_cuenta;  ?>" class="input_a guardar" />

            </div>
            
            <div class="clear"></div>
                <div class="label">Nit / Rut</div>
                <input tabindex="5" type="file" id="archivo" name="archivo" >
                <div></div>
				<?php if(!empty($cuenta->nit)){ ?>
                 <span class="span_ver">Documento subido correctamente - <a class="link_ver" target="_blank" href="<?php echo base_url() ?>uploads/proyectos/cuentas/<?php echo $cuenta->nit ?>">Ver Archivo Actual </a></span><br>
                <?php } ?>
              
				<div class="spacer"></div>
              <div class="label">Certificado Bancario</div>
                <input tabindex="6" type="file" id="archivo1" name="archivo1" >
			  <div></div>
              <?php if(!empty($cuenta->certificado)){ ?>
            <span class="span_ver">Documento subido correctamente - <a class="link_ver" target="_blank" href="<?php echo base_url() ?>uploads/proyectos/cuentas/<?php echo $cuenta->certificado ?>">Ver Archivo Actual </a></span><br>
              <?php } ?>

            </td>
            </tr>
            </table>
                          	
	  </div>

	  <div class="div_green clear"></div>
            
      <div class="box_shadow box_c" >
            <h6>Datos de contacto financiero</h6>
            <p>A continuación se deberán ingresar los datos de la persona designada por la organización para el manejo de los procesos bancarios.</p>
		
        <table width="490" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td>
            
            <div class="col_int_box_c left">
                    <div class="label">Nombres</div>
                    <input tabindex="7" type="text" value="<?php echo $cuenta->nombres;  ?>" name="nombre" id="nombres" class="input_a guardar" />
                    <div class="label">Cargo</div>
                    <input tabindex="9" type="text" value="<?php echo $cuenta->cargo;  ?>" name="cargo" id="cargo" class="input_a guardar" />
                    <div class="label">E-mail</div>
                    <input tabindex="11" type="text" value="<?php echo $cuenta->email;  ?>" name="email" id="email" class="input_a guardar" />
                    <div class="label">Teléfono 2 (opcional)</div>
                    <input tabindex="13" type="text" onkeypress="return isNumberKey(event)" value="<?php echo $cuenta->telefono2;  ?>" name="telefono2" class="input_a guardar" />
                    <div class="label">País</div>
                    <input tabindex="15" type="text" name="pais" value="<?php echo $cuenta->pais;  ?>" class="input_a guardar" />
                    <div class="label">Municipio</div>
                    <input tabindex="17" type="text" name="municipio" value="<?php echo $cuenta->municipio;  ?>" class="input_a guardar" />
            </div>
            <div class="col_int_box_c right">
           		<div class="label">Apellidos</div>
				<input tabindex="8" type="text" name="apellidos" value=" <?php echo $cuenta->apellidos;  ?>" id="apellidos" class="input_a guardar" />
            	<div class="label">No. Identificación</div>
                <input tabindex="10" type="text" onkeypress="return isNumberKey(event)" name="cedula" id="cedula" value="<?php echo $cuenta->cedula;  ?>" class="input_a guardar" />
            	<div class="label">Teléfono 1</div>
                <input tabindex="12" type="text" onkeypress="return isNumberKey(event)" name="telefono1" id="telefono" value="<?php echo $cuenta->telefono;  ?>" class="input_a guardar" />
            	<div class="label">Departamento (Región)</div>
            	<input  tabindex="14" type="text" name="departamento" value="<?php echo $cuenta->departamento;  ?>" class="input_a guardar" />
            	<div class="label">Dirección</div>
                <input  tabindex="16" type="text" name="direccion" id="direccion" value="<?php echo $cuenta->direccion;  ?>" class="input_a guardar" />
            </div>
            </td>
            </tr>
            </table>
		
      </div>

      <div class="clear spacer"></div>

            <input type="hidden" id="redirec" name="redirec" class="guardar">
            <input type="hidden" name="id" class="guardar" value="<?php echo $proyectoid ?>">
            <input type="button" onclick="javascript:borrarproyecto()" value="Borrar Proyecto" class="boton_c left" style="margin-right:10px;">
            <input type="button" onclick="javascript:guardarproyecto(1)" value="Guardar Cambios" class="boton_a left" style="margin-right:10px;">
            <a href="<?php echo base_url(); ?>gestores_front/index/<?php echo $proyectoid; ?>" class="boton_b left" style="margin-right:10px;">Atrás</a>
            <a href="javascript:guardarproyecto(3)"  class="boton_b left">Siguiente</a>


    </div><!-- // columna C -->
</form>
    <div class="col_d mar_right">

        <?php echo $vista; ?>

    </div>

</div>
<script type="text/javascript" src="js/jquery-filestyle.js"></script>
<script type="text/javascript" src="js/aplication.js"></script>

<?php if(isset($tipo)){ ?>
<script>
    alert('Se han guardado los cambios realizados.');
</script>
<?php } ?>
<script>
function isNumberKey(e){
	var key = window.Event ? e.which : e.keyCode;
	return (key >= 48 && key <= 57);
}

function validaEmail(email) {
    var re  = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
    if (!re.test(email)) {
        alert ("Debe ingresar un E-mail válido.");
        return false;
    }
    return true;
}

    function guardarproyecto(tipo){

        var email2 = $('#email').val();
        if(email2 != ''){
            if(validaEmail(email2) == false){
                $('#email').focus();
                return false;
            }
        }

        $('#redirec').val(tipo);

        if(tipo == 3){
            $('#redirec').val(3);
            $('#form').submit();
            return false;
        }
        
         if(tipo == 2){
            var titulo = $('#titulo').val();
            var banco = $('#banco').val();
            var numero = $('#numero').val();
            var tipocu = $('#tipocu').val();
            var nombres = $('#nombres').val();
            var cargo = $('#cargo').val();
            var email = $('#email').val();
            var apellidos = $('#apellidos').val();
            var cedula = $('#cedula').val();
            var telefono = $('#telefono').val();
            var direccion = $('#direccion').val();

        }

        var archivo = $('#archivo').val();
        var archivo1 = $('#archivo1').val();

        if(archivo1 != ''){
            $('#redirec').val(1);
            $('#form').submit();
        }

        if(archivo != ''){
            $('#redirec').val(1);
            $('#form').submit();
        }


        $('#redirec').val(2);

        var data = $('.guardar').serialize();
        $.ajax({
            url: "<?php echo base_url()?>cuenta/add",
            type: "post",
            dataType: "text",
            data: data
        }).done(function (data) {

                if(data == 5){
                    location.href = '<?php echo base_url(); ?>revision/index/<?php echo $proyectoid; ?>';
                }else{
                    alert('Se han guardado los cambios realizados.');
                }
            });

    }

function borrarproyecto(){
        if(confirm('¿Desea eliminar el proyecto ?')){
            location.href = '<?php echo base_url(); ?>basicos/eliminarproyecto/<?php echo $proyectoid; ?>';
        }else{

        }
    }
</script>

<!-- backup validacion
 var validacion = true;
            
            if($('#imagen').val() == ''){
                alert('Imagen Organizacion vacia');
                validacion =  false;
            }
            if($('#liderimagen').val() == ''){
                alert('Imagen lider vacia');
                validacion =  false;
            }
             if(titulo == ''){
                    $('#titulo').addClass("errorform");
                    $('#titulo').attr('placeholder', 'Campo obligatorio.');
                    $('#titulo').focus();
                    validacion = false;
            }else{
                    $('#titulo').removeClass("errorform");
            }
            
             if(banco == ''){
                    $('#banco').addClass("errorform");
                    $('#banco').attr('placeholder', 'Campo obligatorio.');
                    $('#banco').focus();
                    validacion = false;
            }else{
                    $('#banco').removeClass("errorform");
            }
            
             if(numero == ''){
                    $('#numero').addClass("errorform");
                    $('#numero').attr('placeholder', 'Campo obligatorio.');
                    $('#numero').focus();
                    validacion = false;
            }else{
                    $('#numero').removeClass("errorform");
            }
             if(tipocu == ''){
                    $('#tipocu').addClass("errorform");
                    $('#tipocu').attr('placeholder', 'Campo obligatorio.');
                    $('#tipocu').focus();
                    validacion = false;
            }else{
                    $('#tipocu').removeClass("errorform");
            }
            
             if(nombres == ''){
                    $('#nombres').addClass("errorform");
                    $('#nombres').attr('placeholder', 'Campo obligatorio.');
                    $('#nombres').focus();
                    validacion = false;
            }else{
                    $('#nombres').removeClass("errorform");
            }
            
            
              if(cargo == ''){
                    $('#cargo').addClass("errorform");
                    $('#cargo').attr('placeholder', 'Campo obligatorio.');
                    $('#cargo').focus();
                    validacion = false;
            }else{
                    $('#cargo').removeClass("errorform");
            }
              if(email == ''){
                    $('#email').addClass("errorform");
                    $('#email').attr('placeholder', 'Campo obligatorio.');
                    $('#email').focus();
                    validacion = false;
            }else{
                    $('#email').removeClass("errorform");
            }
              if(apellidos == ''){
                    $('#apellidos').addClass("errorform");
                    $('#apellidos').attr('placeholder', 'Campo obligatorio.');
                    $('#apellidos').focus();
                    validacion = false;
            }else{
                    $('#apellidos').removeClass("errorform");
            }
              if(cedula == ''){
                    $('#cedula').addClass("errorform");
                    $('#cedula').attr('placeholder', 'Campo obligatorio.');
                    $('#cedula').focus();
                    validacion = false;
            }else{
                    $('#cedula').removeClass("errorform");
            }
              if(telefono == ''){
                    $('#telefono').addClass("errorform");
                    $('#telefono').attr('placeholder', 'Campo obligatorio.');
                    $('#telefono').focus();
                    validacion = false;
            }else{
                    $('#telefono').removeClass("errorform");
            }
              if(direccion == ''){
                    $('#direccion').addClass("errorform");
                    $('#direccion').attr('placeholder', 'Campo obligatorio.');
                    $('#direccion').focus();
                    validacion = false;
            }else{
                    $('#direccion').removeClass("errorform");
            }
            
            if(validacion == false){
                alert('Algunos de los campos obligatorios estan vacios.');
                return false;
            }
-->