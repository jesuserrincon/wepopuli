<script src="js/jquery.sticky.js"></script>
<script>
  $(document).ready(function(){
     
     
function seleccionado(){ 

var archivos = document.getElementById("archivos");
var archivo = archivos.files;

 if(window.XMLHttpRequest) { 
 $('.loading').css('display','block'); 
 var Req = new XMLHttpRequest(); 
 }else if(window.ActiveXObject) { 
 var Req = new ActiveXObject("Microsoft.XMLHTTP"); 
 }


var data = new FormData();


for(i=0; i<archivo.length; i++){
   data.append('archivo'+i,archivo[i]);
}


Req.open("POST", "<?php echo base_url(); ?>basicos/uploadimg", true);


Req.onload = function(Event) {
if (Req.status == 200) { 
  var msg = Req.responseText;
  $('#imagen_box').attr('src','<?php echo base_url(); ?>uploads/proyectominiatura/'+msg);
  $('#imagen_miniatura').val(msg);
  $(".imgLiquidFill").imgLiquid({fill:true});
  $('.loading').css('display','none'); 
} else { 
  console.log(Req.status); 
} 
};

 Req.send(data); 
}

     $("#archivos").bind('change', function(){
      if (this.files[0].size >= 2097152) {
      var tamanio = this.files[0].size/1048576;
      tamanio = tamanio.toFixed(2);
      alert('WARNING:\n\nFile size is '+(tamanio)+' MB and exceeds the maximum size limit of  2,0 MB.');
      $(this).val('');
      return false;
      }else{
          $('#imagen_box').attr('src','<?php echo base_url(); ?>assets/img/load.gif');
          $(".imgLiquidFill").imgLiquid({fill:true});
          seleccionado();
      }
      });
      
    $(".sticker").sticky({topSpacing:30});
	
	
	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
		   $('.sticker').addClass('fixed_button_basicos');
		   }else{
			   $('.sticker').removeClass('fixed_button_basicos');
		   }
	});
  });
</script> 
<style type="text/css">
#s1 {
    color:#fff;
    background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -100px;
}
#s6 {
	left:551px !important;
}
#s7 {
	left:687px !important;
}
.fixed_button_basicos {
	margin-top:1870px !important;	
}
.box_a2 .col_d .first_precent {
	margin-left:85px;
}

</style>
<link href="css/jquery-filestyle.css" rel="stylesheet" />
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.sticky.js"></script>
</script>  
<script>
$(function() {
	$(".datepicker").datepicker({ minDate: +2, maxDate: "+1M +10D" });
	});
	function imagenbox(){      
}

function confirm_click(){
return confirm("If you have saved changes, or don't want to save them right now, press Accept/Ok to continue.");
}
</script> 




<div class="paso_paso clearfix">
    <a onclick="return confirm_click();" href="<?php echo base_url(); ?>reglamento_ci/index/<?php echo $proyectoid; ?>" id="s1" class="step">Rules</a>
    <a href="<?php echo base_url(); ?>basicos_ci/index/<?php echo $proyectoid; ?>" id="s2" class="step">Basics</a>
    <a onclick="return confirm_click();" href="<?php echo base_url(); ?>historia_ci/index/<?php echo $proyectoid; ?>" id="s3" class="step"> Story </a>
    <a onclick="return confirm_click();" href="<?php echo base_url(); ?>gestores_front_ci/index/<?php echo $proyectoid; ?>" id="s4" class="step">Gestores</a>
    <a onclick="return confirm_click();" href="<?php echo base_url(); ?>revision_ci/index/<?php echo $proyectoid; ?>" id="s6" class="step">Revisión</a>
    <a onclick="return confirm_click();" href="<?php echo base_url(); ?>previsualizar_ci/index/<?php echo $proyectoid; ?>" id="s7" class="step">Previsualizar</a>
</div>
<div class="txt_reglamento">
    <h2 class="h2_b">Start building your project</h2>
    It's time to start describing your project. The more clear it is the higher the chances to get the support you are looking for.
</div>

<div class="box_a2 pad_box clearfix line_none">
    <div id="cargados">
        
    </div>
    <div class="col_c">
        <div class="box_shadow box_c">
            <h6>Imagen  del proyecto <span>*</span> </h6>
            <div class="left" style="width:470px; margin-bottom:20px">
                <!--a href="javascript:imagenbox()">imagen box</a-->Los proyectos con una buena imagen de portada tienen más altas posibilidades de éxito. Recuerde montar la imagen que mejor muestre las características fundamentales del proyecto.<br /><br />
Escoja una imagen de su computador, en formato JPEG o PNG <br />• Tamaño máximo: 2MB. • Dimensiones mínimas: 590 x 396 pixeles.
            </div>
            <div class="clear"></div>
            <div class="relative">
                <input type="hidden" name="imagen_miniatura" id="imagen_miniatura" value="<?php echo $proyecto->imagen_miniatura;?>" /> 
               <input id="archivos" type="file" name="archivos[]" multiple="multiple" />
            </div>
        </div>
        <div class="div_green clear"></div>
        <div class="box_shadow box_c">
            <h6>Project title <span>*</span> </h6>
            <p>El título del proyecto debe ser simple, específico y fácil de recordar. Evite palabras como “ayuda”, “apoyo” o “patrocine”.</p>
            <input type="text" id="titulo" name="titulo" value="<?php echo $proyecto->titulo;?>" onkeyup="cambiartexto()" placeholder="Ej. Vamos todos por agua para la comuna 17" class="input_c" style="margin-bottom:10px"  /><br />Maximum characters 
           <span id="titulo-text">0/60</span>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c">
            <h6>Category<span>*</span> </h6>
            Choose the category that better suits your project.<br /><br />
            <div class="clear"></div>
            
            
            <table width="250" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><select class="select" name="categoria" id="categoria" onchange="cambiarcategoria()">
        <option value="0">Choose a Category</option>
                <?php foreach ($categorias as $item){?>
            <option value="<?php echo $item->id;?>" <?php if($proyecto->categorias_id == $item->id){ echo 'SELECTED'; }?>><?php echo $item->nombre;?></option>
        <?php } ?>
            </select></td>
  </tr>
</table>

            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        <div class="box_shadow box_c">
            <h6>Short Description <span>*</span> </h6>
            <p>Si se utilizara una frase para describir el proyecto, ¿cuál sería?</p>
            <textarea class="texarea_1" name="descripcion" id="descripcion" onkeyup="cambiartexto_descripcion()" style="height:50px !important" ><?php echo $proyecto->descripcion_corta;?></textarea>
            
            <br />Maximum characters <span id="descripcion-text">0/125</span>
            <div class="div_line_2"></div>
        </div> 
        <div class="div_green clear"></div>
        <div class="box_shadow box_c">
            <h6>Tags<span></span> </h6>
            <!--input id="tags_2" name="tags" type="text" value="<?php echo $proyecto->tags_busqueda;?>"  class="tags input_c"/-->
			<p>Seleccione una serie de palabras individuales que por si solas se relacionen directamente con el área del proyecto. Se deben separar las palabra con comas.</p>
            <input id="tags_1" type="text" class="tags input_c"  value="<?php echo $proyecto->tags_busqueda;?>"/>
           
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Project Location <span>*</span> </h6>
            <div style="margin-left:35px;">
            <p>Where it will be held?</p>
           </div>
            <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td align="center">
                <div class="col_int_box_c left">
                <div class="label"><div style="width:226px; text-align:left">País</div></div>
                <input type="text" tabindex=1 class="input_a" />
                <div class="label"><div style="width:226px; text-align:left">Municipio</div></div>
                <input type="text" tabindex=3 class="input_a" />
            </div>
            <div class="col_int_box_c right">
                <div class="label"><div style="width:226px; text-align:left">State/Province</div></div>
                <input type="text" tabindex=2 class="input_a" />
                <div class="label"><div style="width:226px; text-align:left">Dirección (Si aplica)</div></div>
                <input type="text" tabindex=4 class="input_a" />
            </div>
            </td>
            </tr>
            </table>
            <div class="clear"></div>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Tiempo de Publicación <span>*</span> </h6>
            <p>We recommend 30 days or less. Shorter durations have higher success rates and create an urgent sense to get more support for the project. </p>
            <center>
            <b>Number of days</b><br />
            
            <table class="table1" width="90" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td>
                <div style="margin-right:-20px;">
                
                <select  tabindex=5 class="select" id="diasfaltantes" name="diasfaltantes" onchange="cambiarfecha()">
					<?php for($i=30;$i<61;$i++){?>
                    <option value="<?php echo $i; ?>" <?php if($proyecto->dias_para_cerrar == $i){ echo 'SELECTED'; }?>><?php echo $i; ?>
                    <?php } ?>
                </select>
                </div>
                </td>
              </tr>
            </table>
            
            
            </center>
            <div class="clear"></div>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix" style="background:;">
            <h6 style="color:#a32382">Clases de apoyo<span>*</span> </h6>
            <p>Below you will find the types of support you can ask for your project. You can choose one, two or all the types of support, according to the project requirements.
</p>           
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Volunteer work</h6>
            <div class="justify">
                <p>Diligencie esta ayuda si el proyecto necesita de voluntarios para mejorar/construir una obra, ayuda general, ayuda virtual o presencial para la difusión del proyecto, etc.</p>
                <p>The fields below must be filled in according to the following instructions: On the first one you should write the title of the volunteer work; on the second one you must enter the number of volunteers needed; finally, you have to explain why and what for you are requesting volunteers. By clicking the button "Add" you will save the information entered above and will be able to add as many volunteer works as your project requires..</p>     
			</div>          
            <br />
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top">  
                    <p>Volunteer Work Title</p>
                    <input type="text" tabindex=6 id="titulo_voluntariado" class="input_b" placeholder="Ej. Personal para pintar casas" style="margin-right:20px; margin-bottom:5px; width:370px" />
                    <input type="hidden" id="idvoluntariado" value="0" />
                   <br />
                    Maximum characters  <span id="titulo_voluntariado-text">0/135</span><p></p>
               </td>
                <td valign="top">
                    <p>Cantidad de Voluntarios</p>
                    <input placeholder="Ej. 50" type="text" tabindex=9 class="input_d" style="width:160px; height:35px" id="cantidad_voluntariado"  onkeypress="return isNumberKey(event)"/>
                </td>
              </tr>
            </table>
            
            <br />
            <p>Describe the volunteer work your project requires. Why and What for?</p>           
            <textarea class="texarea_1" tabindex=8 id="texto_voluntariado"></textarea>
            <br />Maximum characters  <span id="texto_voluntariado-text">0/600</span>
            <div class="clear" style="margin-bottom:10px;"></div>
            <div class="left" id="apoyo_voluntariado">
                <?php foreach ($voluntariados as $item ){?>
                <div class="clear"></div><div id="vol_box<?php echo $item->id ?>">
                    <a onclick="return confirm('Desea eliminar este item ?')" href="javascript:eliminarvoluntariado(<?php echo $item->id ?>)" title="eliminar" class="less_2"></a>
                    <a href="javascript:actualizardatos(<?php echo $item->id ?>)" class="edit" title="editar"></a><span id="acttitulo<?php echo $item->id ?>"><?php echo $item->titulo;?></span></div>
                <?php } ?>
            </div>
            <div class="right">
            </div>
            <div class="clear"></div>
            <div style="float: right; margin-top:-20px"><a href="javascript:agregarvoluntariado()" class="boton_b left" >Agregar</a></div>
        </div>
               
        
        
        
      <!--   <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Apoyo económico</h6>
            <p>Si el proyecto requiere de apoyo económico, diligencie los dos campos siguientes, primero nombrando por qué y para qué se necesitaría el dinero y después especificando la cantidad solicitada.</p>           
            <textarea class="texarea_1" tabindex="10" id="comentarios_monetario" name="comentarios_monetario"><?php echo $proyecto->comentarios_monetario ; ?></textarea>
            <br />Maximum characters <span id="texto_monetario-text">0/600</span>
            <div class="clear" style="margin-bottom:10px;"></div>
            <div class="right">
                Cuánto requiere para su proyecto:
                <input  type="text" tabindex="11" class="input_e" id="valormonetario" name="valormonetario" value="<?php echo $proyecto->valormonetario ; ?>"  onkeypress="return isNumberKey(event)" />

            </div>

        </div>
        <div class="div_green clear"></div>        
         <div class="box_shadow box_c clearfix">
          <form id="formularioespecie">
          <div id="contenedorespecie">
            <h6>Apoyo en Especie / paquetes de ayuda</h6>
            <div class="justify">
                <p>
                Si se necesitan paquetes de ayudas específicas, como por ejemplo de alimentación o materiales de construcción, diligencie los siguientes campos.<br /><br />
			  Primero, escriba el nombre del paquete de ayuda; luego, escriba por filas cada uno de los elementos requeridos, (elemento, marca y volumen o medidas), y la cantidad que solicita de cada uno de estos elementos. -Recuerde que un paquete es lo que probablemente aportará una sola persona, por lo que se debe totalizar el número de elementos que necesita y dividirlos en una cantidad de paquetes que por sí solos sean de un valor razonable para un solo individuo-. Luego de describir cada uno de los elementos que necesitará para ese paquete de ayuda, debe decir cuantos paquetes de estos necesitará para completar el total requerido y el precio por paquete.</p>           
            </div>
            
            <table width="350" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>            <p>Nombre del paquete</p>
                            <input type="text" tabindex=12 class="input_f" id="nombrepaquete" placeholder="Ej.Materiales de construcción" style="margin-right:20px; margin-bottom:5px" />
                            <br />Maximum characters <span id="nombrepaquete-text">0/30</span>
                </td>
                  </tr>
                </table>
            
            
            <div class="clear" style="margin-bottom:30px"></div>
            <input type="hidden" tabindex=13 name="idespecie" id="idespecie" value="0" >
            <input type="hidden" tabindex=14 name="cantidaditems" id="cantidaditems" value="7" >
            <div>
                <div class="col_a_especie left" id="cajaelemento">
                <center>Elemento</center><div class="clear" style="margin-bottom:10px"></div>
                <input tabindex=15 type="text" class="input_g" id="elemento1" placeholder="Ej: Atún" />
                <input tabindex=19 type="text" class="input_g" id="elemento2" placeholder="Ej: Baldosas" />
                <input tabindex=23 type="text" class="input_g" id="elemento3" placeholder="Ej: Leche"/>
                <input tabindex=27 type="text" class="input_g" id="elemento4" />
                <input tabindex=31 type="text" class="input_g" id="elemento5" />
                <input tabindex=35 type="text" class="input_g" id="elemento6" />
                <input tabindex=39 type="text" class="input_g" id="elemento7" />
            </div>
                <div class="col_a_especie left" id="cajamarca">
                <center>Marca</center><div class="clear" style="margin-bottom:10px"></div>
                <input tabindex=16 type="text" class="input_g" id="marca1" placeholder="Ej: Van camps" />
                <input tabindex=20 type="text" class="input_g" id="marca2" placeholder="Ej: Corona (Ref. tauro)" />
                <input tabindex=24 type="text" class="input_g" id="marca3" placeholder="Ej: Alpina deslactosada" />
                <input tabindex=28 type="text" class="input_g" id="marca4" />
                <input tabindex=32 type="text" class="input_g" id="marca5" />
                <input tabindex=36 type="text" class="input_g" id="marca6" />
                <input tabindex=40  type="text" class="input_g" id="marca7" />
            </div>
                <div class="col_b_especie left" id="cajacantidad">
                <center>Cantidad</center><div class="clear" style="margin-bottom:10px"></div>
                <input tabindex=17 type="text" class="input_h" id="cantidad1"  onkeypress="return isNumberKey(event)" placeholder="Ej: 450" />
                <input tabindex=21 type="text" class="input_h" id="cantidad2"  onkeypress="return isNumberKey(event)" placeholder="Ej: 150" />
                <input tabindex=25 type="text" class="input_h" id="cantidad3"  onkeypress="return isNumberKey(event)" placeholder="Ej: 1000" />
                <input tabindex=29 type="text" class="input_h" id="cantidad4"  onkeypress="return isNumberKey(event)" />
                <input tabindex=33 type="text" class="input_h" id="cantidad5"  onkeypress="return isNumberKey(event)" />
                <input tabindex=37  type="text" class="input_h" id="cantidad6"  onkeypress="return isNumberKey(event)" />
                <input tabindex=41  type="text" class="input_h" id="cantidad7"  onkeypress="return isNumberKey(event)" />
            </div>
                <div class="col_b_especie col_c_especie left" id="cajacontenido" style="margin-right:0px; width:108px;">
                <center>Cont/Med x und.</center><div class="clear" style="margin-bottom:10px"></div>
                <input tabindex=18 type="text" class="input_h" id="contenido1" placeholder="Ej: 180gr" />
                <input tabindex=22 type="text" class="input_h" id="contenido2" placeholder="Ej: 1m x 1m" />
                <input tabindex=26 type="text" class="input_h" id="contenido3" placeholder="Ej: 1 Litro" />
                <input tabindex=30 type="text" class="input_h" id="contenido4" />
                <input tabindex=34 type="text" class="input_h" id="contenido5" />
                <input tabindex=38 type="text" class="input_h" id="contenido6" />
                <input tabindex=42 type="text" class="input_h" id="contenido7" />
            </div>
            <div class="clear" style="margin-bottom:20px;"></div>
            
            </div>
            
            
            <div>
                <a href="javascript:additemespecie()" class="add_2 right" title="Agregar" style="width:120px; text-align:right; float:right">Añadir nuevo item</a>
                <div id="apoyoespecie" style="float:left">
                    <?php foreach ($especies as $item ){?>
                
					<div class="clear"></div>
                
                    <div id="del_box<?php echo $item->id ?>" style="clear:both">
                    <a onclick="return confirm('Desea eliminar este item ?')" href="javascript:eliminarespecie(<?php echo $item->id ?>)"  class="less_2"></a>
                    <a href="javascript:actualizardatosespecie(<?php echo $item->id ?>)" class="edit"></a><span id="acttitulo<?php echo $item->id ?>"><?php echo $item->nombre;?></span>
                    </div>
                    
                <?php } ?>
                </div>
            </div>
            
            
            <div style="clear:both; margin-bottom:30px;"></div>
            <table border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td align="center">
                		<div  style="width:360px;">
                         	Valor paquete de especie:<br />
                            <input  tabindex=44 type="text" class="input_e" id="valorpaquete" onkeypress="return isNumberKey(event)" style="margin-bottom:4px;" />
                            <br /><span style="font-size:12px;">El valor mínimo de un paquete debe ser de 20.000 pesos.</span>
                            <div class="clear spacer"></div>
                            
                            Cantidad de paquetes necesarios:<br />
							<input  tabindex=43 type="text" id="cantidadpaquete" class="input_d" onkeypress="return isNumberKey(event)"  style="width: 205px; height:36px" />
                            <div class="clear"></div>
                        </div>
                  </td>
              </tr>
            </table>
			<center><a href="javascript:agregarespecie()" class="boton_a" style="display:block; width:210px; background-size:210px 84px">Agregar paquete de ayuda</a></center>

            
            
            </div>
            
            
            
            
          </form>
        </div>-->
        
            <div class="clear" style="height:30px;"></div>
            <input  tabindex=45 type="button" onclick="javascript:borrarproyecto()" value="Delete Project" class="boton_c left" style="margin-right:10px;">
            <input  tabindex=46 type="button" onclick="javascript:guardarproyecto()" value="Save Changes"  class="boton_a left" style="margin-right:10px;">
            <a href="<?php echo base_url(); ?>reglamento_ci/index/<?php echo $proyectoid; ?>" class="boton_b left" style="margin-right:10px;">Back</a>
            <a href="javascript:siguiente()" class="boton_b left">Continue</a>

        
    </div><!-- // columna C -->
    
    <div class="col_d mar_right">
      <?php echo $vista; ?>
    </div>
</div>
<script>
     function isNumberKey(e){
	var key = window.Event ? e.which : e.keyCode;
	return (key >= 48 && key <= 57);
}
    function cambiartexto(){
        var titulo = $('#titulo').val();
        $('#titulo_box').html(titulo);
    }

    function cambiartexto_descripcion(){
    var titulo = $('#descripcion').val();
    $('#descripcion_box').html(titulo);
    }

    function cambiarfecha(){
        var titulo = $('#diasfaltantes').val();
        $('#diasfaltantes_box').html(titulo);
    }

init_contadorTa("titulo","titulo-text", 60);
init_contadorTa("nombrepaquete","nombrepaquete-text", 30);
init_contadorTa("descripcion","descripcion-text", 125);
init_contadorTa("titulo_voluntariado","titulo_voluntariado-text", 135);
init_contadorTa("texto_voluntariado","texto_voluntariado-text", 600);
init_contadorTa("comentarios_monetario","texto_monetario-text", 600);



function init_contadorTa(idtextarea, idcontador,max)
{
    $("#"+idtextarea).keyup(function()
            {
                updateContadorTa(idtextarea, idcontador,max);
            });
    
    $("#"+idtextarea).change(function()
    {
            updateContadorTa(idtextarea, idcontador,max);
    });
    
}

function updateContadorTa(idtextarea, idcontador,max)
{
    var contador = $("#"+idcontador);
    var ta =     $("#"+idtextarea);
    contador.html("0/"+max);
    
    contador.html(ta.val().length+"/"+max);
    if(parseInt(ta.val().length)>max)
    {
        ta.val(ta.val().substring(0,max-1));
        contador.html(max+"/"+max);
    }

}

function guardarproyecto(){
     var titulo = $('#titulo').val();
     var categoria = $('#categoria').val();
     var tags = $('#tags_1').val();
     var descripcion = $('#descripcion').val();
     var diasfaltantes = $('#diasfaltantes').val();
     var comentarios_monetario = $('#comentarios_monetario').val();
     var valormonetario = $('#valormonetario').val();
     var imagen_miniatura = $('#imagen_miniatura').val();
     if(valormonetario < 20000 && valormonetario > 0){  
                    alert('El valor ingresado debe ser mayor a $20.000 pesos.');
                    return false;
     }
     $.post( "<?php echo base_url()?>basicos/ActualizarProyectoBasicas/", { id:<?php echo $proyectoid; ?>,titulo:titulo,categoria:categoria,tags:tags,descripcion:descripcion,diasfaltantes:diasfaltantes, comentarios_monetario : comentarios_monetario, valormonetario:valormonetario, imagen_miniatura:imagen_miniatura })
            .done(function( data ) {
                alert('Changes have been saved.');
            });
}


function agregarvoluntariado(){
    var titulo_voluntariado = $('#titulo_voluntariado').val();
    var texto_voluntariado = $('#texto_voluntariado').val();
    var cantidad_voluntariado = $('#cantidad_voluntariado').val();
    var idvoluntariado = $('#idvoluntariado').val();
    var validacion = true;
    
        
        
    if(titulo_voluntariado == ''){
             $('#titulo_voluntariado').addClass("errorform");
             $('#titulo_voluntariado').attr('placeholder', 'Obligatorio.');
             $('#titulo_voluntariado').focus();
             validacion = false;
        }else{
             $('#titulo_voluntariado').removeClass("errorform");
            
        }
        
         if(texto_voluntariado == ''){
             $('#texto_voluntariado').addClass("errorform");
             $('#texto_voluntariado').attr('placeholder', 'Obligatorio.');
             $('#texto_voluntariado').focus();
             validacion = false;
        }else{
             $('#texto_voluntariado').removeClass("errorform");
            
        }
        
         if(cantidad_voluntariado == ''){
             $('#cantidad_voluntariado').addClass("errorform");
             $('#cantidad_voluntariado').attr('placeholder', 'Obligatorio.');
             $('#cantidad_voluntariado').focus();
             validacion = false;
        }else{
             $('#cantidad_voluntariado').removeClass("errorform");
            
        }
        if(validacion == false){
            return false;
        }else{
            
            if(idvoluntariado == 0){
                $.post( "<?php echo base_url()?>basicos/AddVoluntariado/", { titulo: titulo_voluntariado, descripcion: texto_voluntariado, cantidad: cantidad_voluntariado, proyectos_id: <?php echo $proyectoid; ?> })
                .done(function( data ) {
                    $('#apoyo_voluntariado').append('<div class="clear"></div><div id="vol_box'+data+'"><a onclick="return confirm('+"'"+'Desea eliminar este item ?'+"'"+')" href="javascript:eliminarvoluntariado('+data+')" class="less_2"></a> <a href="javascript:actualizardatos('+data+')" class="edit"></a><span id="acttitulo'+data+'">'+titulo_voluntariado+'</span></div>');
                });
            }else{
                 $.post( "<?php echo base_url()?>basicos/UpdateVoluntariado/", { titulo: titulo_voluntariado, descripcion: texto_voluntariado, cantidad: cantidad_voluntariado, idvoluntariado: idvoluntariado })
                .done(function( data ) {
                    $('#acttitulo'+idvoluntariado).html(titulo_voluntariado);
                });
            }
            $('#titulo_voluntariado').val('');
            $('#texto_voluntariado').val('');
            $('#cantidad_voluntariado').val('');
            $('#idvoluntariado').val(0);
       
        }
        
}





function eliminarvoluntariado(id){
    $('#vol_box'+id).remove();
    $.post( "<?php echo base_url()?>basicos/DelVoluntariado/", { id:id })
            .done(function( data ) {
                if($('#idvoluntariado').val() == id){
                    $('#titulo_voluntariado').val('');
                    $('#texto_voluntariado').val('');
                    $('#cantidad_voluntariado').val('');
                }
            });
}


function eliminarespecie(id){
    $('#del_box'+id).remove();
    $.post( "<?php echo base_url()?>basicos/Delespecie/", { id:id })
            .done(function( data ) {
                if($('#idespecie').val() == id){
                    $.post( "<?php echo base_url()?>basicos/formulario/", {  })
                            .done(function( data ) {
                                $('#contenedorespecie').html(data);
                                init_contadorTa("nombrepaquete","nombrepaquete-text", 30);
                      });
                }
            });
}


function actualizardatos(id){
     $.post( "<?php echo base_url()?>basicos/ConsultarVoluntariado/", { id:id })
            .done(function( data ) {
                var datos = data.split("19585");
                
                $('#titulo_voluntariado').val(datos[0]);
                $('#texto_voluntariado').val(datos[1]);
                $('#cantidad_voluntariado').val(datos[2]);
                $('#idvoluntariado').val(datos[3]);
              
            });
}

function borrarproyecto(){
    if(confirm('Are you sure you want to delete this project?')){
        location.href = '<?php echo base_url(); ?>basicos/eliminarproyecto/<?php echo $proyectoid; ?>';
    }else{
        
    }
}

function actualizardatosespecie(id){
      $.post( "<?php echo base_url()?>basicos/ConsultarEspecie/", { id:id })
            .done(function( data ) {
                $('#contenedorespecie').html(data);
                init_contadorTa("nombrepaquete","nombrepaquete-text", 30);
            });
      $('body').addClass('edicion');
}


function additemespecie(){
    
    var cantidaditems = parseInt($('#cantidaditems').val())+1;
    $('#cajaelemento').append('<input type="text" class="input_g" id="elemento'+cantidaditems+'" />');
    $('#cajamarca').append('<input type="text" class="input_g" id="marca'+cantidaditems+'" />');
    $('#cajacantidad').append('<input type="text" class="input_h"  onkeypress="return isNumberKey(event)" id="cantidad'+cantidaditems+'" />');
    $('#cajacontenido').append('<input type="text" class="input_h" id="contenido'+cantidaditems+'" />');
    $('#cantidaditems').val(cantidaditems);
}







function agregarespecie(){
    
    $('body').removeClass('edicion');
    var cantidaditems = parseInt($('#cantidaditems').val())+1;
    var registros = 0;
    var camposvacios = 0;
    for(var i=1;i<cantidaditems;i++){
         if($('#elemento'+i).val() != '' || $('#marca'+i).val() != ''  || $('#cantidad'+i).val() != ''  || $('#contenido'+i).val() != ''){
                
                registros = parseInt(registros)+1;
                if($('#elemento'+i).val() == ''){
                    $('#elemento'+i).addClass("errorform");
                     camposvacios =  true;
                }else{
                     $('#elemento'+i).removeClass("errorform");
                }
                if($('#marca'+i).val() == ''){
                    $('#marca'+i).addClass("errorform");
                     camposvacios =  true;
                }else{
                     $('#marca'+i).removeClass("errorform");
                }
                if($('#cantidad'+i).val() == ''){
                    $('#cantidad'+i).addClass("errorform");
                     camposvacios =  true;
                }else{
                     $('#cantidad'+i).removeClass("errorform");
                }
                if($('#contenido'+i).val() == ''){
                    $('#contenido'+i).addClass("errorform");
                     camposvacios =  true;
                }else{
                     $('#contenido'+i).removeClass("errorform");
                }
         }else{
                     $('#contenido'+i).removeClass("errorform");
                     $('#cantidad'+i).removeClass("errorform");
                     $('#marca'+i).removeClass("errorform");
                     $('#elemento'+i).removeClass("errorform");
             
         }
    }
    
     if(registros == 0){
            alert('Su lista debe tener al menos un registro.');
            return false;
     }else{
            if(camposvacios == true){
                alert('Alguno(s) de los campos está(n) vacío(s).');
                return false;
            }else{
                var nombrepaquete = $('#nombrepaquete').val();
                var valorpaquete = $('#valorpaquete').val();
                var cantidadpaquete = $('#cantidadpaquete').val();
                var validacioncampos = true;
                if(nombrepaquete == ''){
                    $('#nombrepaquete').addClass("errorform");
                    $('#nombrepaquete').attr('placeholder', 'Obligatorio.');
                    $('#nombrepaquete').focus();
                    validacioncampos = false;
                }else{
                    $('#nombrepaquete').removeClass("errorform");

                }
                if(valorpaquete == ''){
                    $('#valorpaquete').addClass("errorform");
                    $('#valorpaquete').attr('placeholder', 'Obligatorio.');
                    $('#valorpaquete').focus();
                    validacioncampos = false;
                }else{
                    $('#valorpaquete').removeClass("errorform");

                }
                if(cantidadpaquete == ''){
                    $('#cantidadpaquete').addClass("errorform");
                    $('#cantidadpaquete').attr('placeholder', 'Obligatorio.');
                    $('#cantidadpaquete').focus();
                    validacioncampos = false;
                }else{
                    $('#cantidadpaquete').removeClass("errorform");

                }
                if(validacioncampos == false){
                    return false;
                }
                if(valorpaquete < 20000){  
                    alert('El valor del paquete ingresado debe ser mayor a $20.000 pesos.');
                    return false;
                }
                
                 $.post( "<?php echo base_url()?>basicos/AddEspecie/", { nombre:nombrepaquete,proyectos_id:<?php echo $proyectoid; ?>,cantidad_paquetes:cantidadpaquete,valor:valorpaquete,idespecie: $('#idespecie').val() })
                 .done(function( data ) {
                    var idespecie = data;
                    for(var i=1;i<cantidaditems;i++){
                        if($('#elemento'+i).val() != '' || $('#marca'+i).val() != ''  || $('#cantidad'+i).val() != ''  || $('#contenido'+i).val() != ''){
                            $.post( "<?php echo base_url()?>basicos/AddEspecieDetalle/", { elemento:$('#elemento'+i).val(),marca:$('#marca'+i).val(),cantidad:$('#cantidad'+i).val(),contenido:$('#contenido'+i).val(),especie_id:idespecie })
                               .done(function( data ) {
                                            
                                    });
                         }
                    }
                   
                    if($('#idespecie').val() > 0){
                        $('#del_box'+$('#idespecie').val()).remove();
                    }
                        
                      $.post( "<?php echo base_url()?>basicos/formulario/", { proyectos_id:<?php echo $proyectoid; ?>  })
                            .done(function( data ) {
                                $('#contenedorespecie').html(data);
                                init_contadorTa("nombrepaquete","nombrepaquete-text", 30);
                      });
                        
                });
                 
                
            }
     }   
}


function editar_volver(){
    $('body').removeClass('edicion');
    $.post( "<?php echo base_url()?>basicos/formulario/", { proyectos_id:<?php echo $proyectoid; ?>  })
                            .done(function( data ) {
                                $('#contenedorespecie').html(data);
                                init_contadorTa("nombrepaquete","nombrepaquete-text", 30);
                      });
}
function siguiente(){
     var titulo = $('#titulo').val();
     var categoria = $('#categoria').val();
     var tags = $('.tags').val();
     var descripcion = $('#descripcion').val();
     var diasfaltantes = $('#diasfaltantes').val();
     var comentarios_monetario = $('#comentarios_monetario').val();
     var valormonetario = $('#valormonetario').val();
     var imagen_miniatura = $('#imagen_miniatura').val();
     if(valormonetario < 20000 && valormonetario > 0){  
                    alert('El valor ingresado debe ser mayor a $20.000 pesos.');
                    return false;
     }
     
     
    
     $.post( "<?php echo base_url()?>basicos/ActualizarProyectoBasicas/", { id:<?php echo $proyectoid; ?>,titulo:titulo,categoria:categoria,tags:tags,descripcion:descripcion,diasfaltantes:diasfaltantes, comentarios_monetario : comentarios_monetario, valormonetario:valormonetario,imagen_miniatura:imagen_miniatura })
            .done(function( data ) {
                location.href = '<?php echo base_url(); ?>historia_ci/index/<?php echo $proyectoid; ?>';
            });

}

function cambiarcategoria(){
    var idc = $('#categoria').val();
    $.post( "<?php echo base_url()?>basicos/nombrecategoria/", { id:idc })
            .done(function( data ) {
                $('#categoria_box').html(data);
            });
}






</script>
<script type="text/javascript" src="js/jquery-filestyle.js"></script>
<script type="text/javascript" src="js/aplication.js"></script>
<!--
BACKUP VALIDACIIONES
 var validacioncampos = true;
            if(titulo == ''){
                $('#titulo').addClass("errorform");
                $('#titulo').attr('placeholder', '* Indicates required');
                $('#titulo').focus();
                validacioncampos = false;
            }else{
                $('#titulo').removeClass("errorform");

            }
            if(categoria == 0){
                $('#categoria').addClass("errorform");
                $('#categoria').focus();
                validacioncampos = false;
            }else{
                $('#categoria').removeClass("errorform");
            }
            
            if(descripcion == ''){
                $('#descripcion').addClass("errorform");
                $('#descripcion').attr('placeholder', '* Indicates required');
                $('#descripcion').focus();
                validacioncampos = false;
            }else{
                $('#descripcion').removeClass("errorform");
            }
            
     if(validacioncampos == false){
         alert('Por favor revisa el formulario. Los campos en rojo son obligatorios');
         return false;
     }
-->