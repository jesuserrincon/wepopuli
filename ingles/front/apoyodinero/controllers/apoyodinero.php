<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class apoyodinero extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
         $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."textos/textos",
               CMSPREFIX."gestores/gestores",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0) {

        parent::index($proyectoid);
        $this->set_title('Bienvenidos a ' . SITENAME, true);


        $especies = new Especie();

        $especie = $especies->getEspecieByProyecto($proyectoid);

        $hatml = '';

        foreach($especie as $item){

        $hatml .='<div class="box_shadow3 relative">
        	<div class="pad_box_10">

                <a href="#" id="'.$item->id.'" class="no_apply left">Aplicar</a>

                <div class="clear"></div>
            	<h2 class="h3_b">'.$item->nombre.'</h2>';

            $detalleEspecie = new Especie_detalle();
            $detalle = $detalleEspecie->getEspecie_detalleByEspecie($item->id);



            $hatml .=' <div class="table_ayuda_div">
                  <table class="table_ayuda" width="100%" border="0" cellspacing="0" cellpadding="0">';
            $i=1;
            foreach($detalle as $datos){

                        $hatml .='<tr>
                            <td width="6%" align="center" valign="middle">#'.$i.'</td>
                            <td width="4%" align="center" valign="middle">'.$datos->cantidad.'</td>
                            <td width="9%" align="center" valign="middle">'.$datos->contenido.'</td>
                            <td width="65%" valign="middle">'.$datos->elemento.'</td>
                            <td width="16%" valign="middle">'.$datos->marca.'</td>

                      </tr>';
                $i++;
            }

				$hatml .='</table>
              </div>';

              $hatml .='<div class="paquete_valor">
              	<h3 class="h3_purpure">Valor del paquete:</h3>
                <h2 class="h2_verde">$'.$item->valor.'<span>(cop)</span></h2>
              </div>
              <div class="clear"></div>

        	</div>

            <div class="active_box_green"></div>
        </div>';

        }


        $this->_data['data'] = $hatml;
        $this->_data['id'] = $proyectoid;
        //enviamos una variable para validar si el usuario esta conectado 
        if($this->nativesession->get('usuario') == ''){
            $this->_data["login"] = 'no';
        }else{
            $this->_data["login"] = 'si';
        }
        //Fin envio voluntariado
        return $this->build('apoyodinero');
    }

    public function paso2($cantidad = 0,$idpro = 0){

        if(isset($_POST['ids'])){
            $this->nativesession->set('apoyo',$_POST);
        }

       // $datos = $this->nativesession->get('apoyo');

        parent::index($idpro);

        $html = '';

        
        //enviamos una variable para validar si el usuario esta conectado 
        if($this->nativesession->get('usuario') == ''){
            $this->_data["login"] = 'no';
        }else{
            $this->_data["login"] = 'si';
        }
         $usuario = $this->nativesession->get('usuario');
        $usuarioOBJ = new Usuarios();
        $this->_data["usuario"] =  $usuarioOBJ->getUsuariosById($usuario['id']);
        $this->_data['cantidad']=$cantidad;
        $this->_data['html']=$html;
        return $this->build('apoyodinero_paso2');
    }


    // ----------------------------------------------------------------------


} ?>