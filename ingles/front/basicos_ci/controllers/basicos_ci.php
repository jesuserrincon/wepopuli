<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class basicos_ci extends Front_Controller {

    public function __construct() {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0) {

        parent::index($proyectoid);

        $vista = $this->build2('../../home/views/detalleProyecto');
        $this->_data["vista"] = $vista;

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        //enviamos las ayudas de voluntariado creadas por el usuario
        $voluntariadoOBJ = new Voluntariado();
        $this->_data['voluntariados'] = $voluntariadoOBJ->getVoluntariadoByProyecto($proyectoid);
        //Fin envio voluntariado
        //enviamos las ayudas de especie creadas por el usuario
        $especieOBJ = new Especie();
        $this->_data['especies'] = $especieOBJ->getEspecieByProyecto($proyectoid);
        //Fin envio voluntariado
        
           $categoriasOBJ = new Categorias();
           $results = $categoriasOBJ->getCategorias();
           $this->_data['categorias'] = $results;


           
        $this->_data["proyectoid"] = $proyectoid;
        return $this->build('basicos');
    }
    
    public function ActualizarProyectoBasicas(){
        $datos = array(
            'id' => $this->input->post('id'),
            'titulo' => $this->input->post('titulo'),
            'categorias_id' => $this->input->post('categoria'),
            'tags_busqueda' => $this->input->post('tags'),
            'descripcion_corta' => $this->input->post('descripcion'),
            'dias_para_cerrar' => $this->input->post('diasfaltantes'),
            'comentarios_monetario' => $this->input->post('comentarios_monetario'),
            'valormonetario' => $this->input->post('valormonetario'),
            'imagen_miniatura' => $this->input->post('imagen_miniatura'),
        );
        $proyectoOBJ = new Proyectos();
        $proyectoOBJ->updateProyectos($datos);
        
    }
    
    public function eliminarproyecto($id){
        $proyectoOBJ = new Proyectos();
        $proyectoOBJ->eliminar($id);
        redirect('home/index/10');
    }
    
    public function AddVoluntariado(){
        $datos = array(
            'titulo' => $this->input->post('titulo'),
            'descripcion' => $this->input->post('descripcion'),
            'cantidad' => $this->input->post('cantidad'),
            'proyectos_id' => $this->input->post('proyectos_id'),
        );
        $voluntariadoOBJ = new Voluntariado();
        $voluntariadoOBJ->saveVoluntariado($datos);
        echo $voluntariadoOBJ->getLastId();
    }

    
    public function UpdateVoluntariado(){
        $datos = array(
            'titulo' => $this->input->post('titulo'),
            'descripcion' => $this->input->post('descripcion'),
            'cantidad' => $this->input->post('cantidad'),
            'id' => $this->input->post('idvoluntariado'),
        );
        $voluntariadoOBJ = new Voluntariado();
        $voluntariadoOBJ->updateVoluntariado($datos);
    }
    
    

    public function DelVoluntariado(){
        $voluntariadoOBJ = new Voluntariado();
        $voluntariadoOBJ->eliminar($this->input->post('id'));
        
    }
    
    public function Delespecie(){
        
        $especieOBJ = new Especie();
        $especieOBJ->eliminar($this->input->post('id'));
        
    }
    
    public function formulario(){
        
        $idpro = $this->input->post('proyectos_id');
        $especieOBJ = new Especie();
        $especies = $especieOBJ->getEspecieByProyecto($idpro);
        $htmlespecie  = '';
         foreach ($especies as $item ){
             $htmlespecie .= ' <div id="del_box<?php echo $item->id ?>" style="clear:both">
                    <a onclick="return confirm('."'".'Desea eliminar este item ?'."'".')" href="javascript:eliminarespecie('.$item->id.')"  class="less_2"></a>
                    <a href="javascript:actualizardatosespecie('.$item->id.')" class="edit"></a><span id="acttitulo'.$item->id.'">'.$item->nombre.'</span>
                    </div>';
         }
        echo '<h6>Apoyo en Especie</h6>
            <p>Si se necesitan paquetes de ayudas específicas, como por ejemplo de alimentación o materiales de construcción, diligencia los siguientes campos.</p>
<p>Primero, escribe el nombre del paquete de ayuda; luego, escribe por filas cada uno de los elementos requeridos, (elemento, marca y volumen o medidas), y la cantidad que solicita de cada uno de estos elementos. -Recuerde que un paquete es lo que probablemente aportará una sola persona, por lo que se debe totalizar el número de elementos que necesita y dividirlos en una cantidad de paquetes que por sí solos sean de un valor razonable para un solo individuo-. Luego de describir cada uno de los elementos que necesitará para ese paquete de ayuda, debe decir cuantos paquetes de estos necesitará para completar el total requerido y el precio por paquete.</p>           
           
		   <table border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
				       <p>Nombre del paquete</p>
						<input type="text" class="input_f" id="nombrepaquete" placeholder="Nombre del paquete. Ej.Materiales de construcción" style="margin-right:20px" /><br />
						Maximum characters <span id="nombrepaquete-text">0/30</span>
					</td>
			  </tr>
			</table>
 
            
			
			<div class="clear" style="margin-bottom:30px"></div>
            <input type="hidden" name="idespecie" id="idespecie" value="0" >
            <input type="hidden" name="cantidaditems" id="cantidaditems" value="7" >
            <div>
                <div class="col_a_especie left" id="cajaelemento">
                <center>Elemento</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_g" id="elemento1" placeholder="Ej: Atún" />
                <input type="text" class="input_g" id="elemento2" placeholder="Ej: Baldosas" />
                <input type="text" class="input_g" id="elemento3" placeholder="Ej: Leche"/>
                <input type="text" class="input_g" id="elemento4" />
                <input type="text" class="input_g" id="elemento5" />
                <input type="text" class="input_g" id="elemento6" />
                <input type="text" class="input_g" id="elemento7" />
            </div>
                <div class="col_a_especie left" id="cajamarca">
                <center>Marca</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_g" id="marca1" placeholder="Ej: Van camps" />
                <input type="text" class="input_g" id="marca2" placeholder="Ej: Corona (Ref. tauro)" />
                <input type="text" class="input_g" id="marca3" placeholder="Ej: Alpina deslactosada" />
                <input type="text" class="input_g" id="marca4" />
                <input type="text" class="input_g" id="marca5" />
                <input type="text" class="input_g" id="marca6" />
                <input type="text" class="input_g" id="marca7" />
            </div>
                <div class="col_b_especie left" id="cajacantidad">
                <center>Cantidad</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_h" id="cantidad1"  onkeypress="return isNumberKey(event)" placeholder="Ej: 450" />
                <input type="text" class="input_h" id="cantidad2"  onkeypress="return isNumberKey(event)" placeholder="Ej: 150" />
                <input type="text" class="input_h" id="cantidad3"  onkeypress="return isNumberKey(event)" placeholder="Ej: 1000" />
                <input type="text" class="input_h" id="cantidad4"  onkeypress="return isNumberKey(event)" />
                <input type="text" class="input_h" id="cantidad5"  onkeypress="return isNumberKey(event)" />
                <input type="text" class="input_h" id="cantidad6"  onkeypress="return isNumberKey(event)" />
                <input type="text" class="input_h" id="cantidad7"  onkeypress="return isNumberKey(event)" />
            </div>
                <div class="col_b_especie left" id="cajacontenido" style="margin-right:0px; width:108px;">
                <center>Cont/Med x und.</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_h" id="contenido1" placeholder="Ej: 180gr" />
                <input type="text" class="input_h" id="contenido2" placeholder="Ej: 45cm x 45cm" />
                <input type="text" class="input_h" id="contenido3" placeholder="Ej: 1 Litro" />
                <input type="text" class="input_h" id="contenido4" />
                <input type="text" class="input_h" id="contenido5" />
                <input type="text" class="input_h" id="contenido6" />
                <input type="text" class="input_h" id="contenido7" />
            </div>
            <div class="clear" style="margin-bottom:20px;"></div>
            
            </div><!-- // CONTENEDOR inputs -->

            <div>
                <a href="javascript:additemespecie()" class="add_2 right" title="Agregar" style="width:120px; text-align:right; float:right">Añadir nuevo item</a>
                <div id="apoyoespecie" style="float:left">
                '.$htmlespecie.'
                </div>
            </div>

            
            <div style="clear:both; margin-bottom:30px;"></div>			
			<table border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
				<td align="center">
					 Valor paquete de especie:<br />
					<input type="text" class="input_e" id="valorpaquete" onkeypress="return isNumberKey(event)" style="margin-bottom:4px;" />
					<br /><span style="font-size:12px;">El valor mínimo de un paquete debe ser de 20.000 pesos.</span>
					<div class="clear spacer"></div>
					
					Cantidad de paquetes necesarios:<br />
					<input type="text" id="cantidadpaquete" class="input_d" onkeypress="return isNumberKey(event)" style="width: 205px; height:36px" />
					<div class="clear"></div>
				</td>
              </tr>
            </table>
			<center><a href="javascript:agregarespecie()" class="boton_a" style="display:block; width:210px; background-size:210px 84px">Agregar paquete de ayuda</a></center>
            </div>';
    }
    public function ConsultarEspecie(){
        $especieOBJ = new Especie();
        $especie = $especieOBJ->getEspecieById($this->input->post('id'));
        $detalleespecieOBJ = new Especie_detalle();
        $detalleespecie = $detalleespecieOBJ->getEspecie_detalleByEspecie($this->input->post('id'));
        //declaramos las variables para crear los inputs
        $inputelementos = '';
        $inputmarcas = '';
        $inputcantidad = '';
        $inputcontenidos = '';
        //fin
        $j = 1;
        foreach ($detalleespecie as $item){
            $inputelementos .= '<input type="text" class="input_g" id="elemento'.$j.'" value="'.$item->elemento.'" placeholder="Ej: Atún" />';
            $inputmarcas .= '<input type="text" class="input_g" id="marca'.$j.'"  value="'.$item->marca.'" placeholder="Ej: Van camps" />';
            $inputcantidad .= '<input type="text" class="input_h" id="cantidad'.$j.'"  value="'.$item->cantidad.'" onkeypress="return isNumberKey(event)" placeholder="Ej: 450" />';
            $inputcontenidos .= '<input type="text" class="input_h" id="contenido'.$j.'"  value="'.$item->contenido.'" placeholder="Ej: 180gr" />';
            $j++;
        }
        
        $html = '<h6>Apoyo en Especie</h6>
            <p>Si se necesitan paquetes de ayudas específicas, como por ejemplo de alimentación o materiales de construcción, diligencia los siguientes campos.</p><p>Primero, escribe el nombre del paquete de ayuda; luego, escribe por filas cada uno de los elementos requeridos, (elemento, marca y volumen o medidas), y la cantidad que solicita de cada uno de estos elementos. -Recuerde que un paquete es lo que probablemente aportará una sola persona, por lo que se debe totalizar el número de elementos que necesita y dividirlos en una cantidad de paquetes que por sí solos sean de un valor razonable para un solo individuo-. Luego de describir cada uno de los elementos que necesitará para ese paquete de ayuda, debe decir cuantos paquetes de estos necesitará para completar el total requerido y el precio por paquete.</p>           
           
		   
		   <table border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td>
							<input type="text" value="'.$especie->nombre.'" class="input_f" id="nombrepaquete" placeholder="Nombre del paquete. Ej.Materiales de construcción" style="margin-right:20px" />
						<br />Maximum characters <span id="nombrepaquete-text">0/30</span>
			
				</td>
			  </tr>
			</table>
						
			
			
            <div class="clear" style="margin-bottom:30px"></div>
            <input type="hidden" name="idespecie" id="idespecie" value="'.$this->input->post('id').'" >
            <input type="hidden" name="cantidaditems" id="cantidaditems" value="'.($j-1).'" >
            <div>
                <div class="col_a_especie left" id="cajaelemento">
                <center>Elemento</center><div class="clear" style="margin-bottom:10px"></div>
                '.$inputelementos.'
            </div>
                <div class="col_a_especie left" id="cajamarca">
                <center>Marca</center><div class="clear" style="margin-bottom:10px"></div>
                '.$inputmarcas.'
            </div>
                <div class="col_b_especie left" id="cajacantidad">
                <center>Cantidad</center><div class="clear" style="margin-bottom:10px"></div>
                '.$inputcantidad.'
            </div>
                <div class="col_b_especie left" id="cajacontenido" style="margin-right:0px; width:108px;">
                <center>Cont/Med x und.</center><div class="clear" style="margin-bottom:10px"></div>
                '.$inputcontenidos.'
            </div>
            <div class="clear" style="margin-bottom:20px;"></div>
            
            </div><!-- // CONTENEDOR inputs -->
            
		<div>
                <a href="javascript:additemespecie()" class="add_2 right" title="Agregar" style="width:120px; text-align:right; float:right">Añadir nuevo item</a>
                <div id="apoyoespecie" style="float:left">
               
                </div>
            </div>



            <div style="clear:both; margin-bottom:30px;"></div>
            <table border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td align="center">
				
              	 Valor paquete de especie:<br />
                <input type="text" class="input_e" id="valorpaquete" value="'.$especie->valor.'" onkeypress="return isNumberKey(event)" style="margin-bottom:4px;" />
                <div style="font-size:12px;">El valor mínimo de un paquete debe ser de 20.000 pesos.</div>
                <div class="clear spacer"></div>
				 
				 Cantidad de paquetes necesarios:<br />
                 <input type="text" id="cantidadpaquete" class="input_d" value="'.$especie->cantidad_paquetes.'" onkeypress="return isNumberKey(event)"  style="width: 205px; height:36px" />
                <div class="clear"></div>
				
				</td>
              </tr>
            </table>
			<center><a href="javascript:editar_volver()" class="boton_a" style="display:block; float:left; margin-right:10px; margin-left:135px">Back</a><a href="javascript:agregarespecie()" class="boton_a" style="display:block; float:left">Guardar</a></center>';
        echo $html;
    } 
            
            
            
    public function ConsultarVoluntariado(){
        $voluntariadoOBJ = new Voluntariado();
        $voluntariado = $voluntariadoOBJ->getVoluntariadoById($this->input->post('id'));
        echo $voluntariado->titulo .'19585'.$voluntariado->descripcion.'19585'.$voluntariado->cantidad.'19585'.$voluntariado->id ;
    }
    
      public function AddEspecie(){
        $datos = array(
            'nombre' => $this->input->post('nombre'),
            'cantidad_paquetes' => $this->input->post('cantidad_paquetes'),
            'valor' => $this->input->post('valor'),
            'proyectos_id' => $this->input->post('proyectos_id'),
        );
        $idespecie = $this->input->post('idespecie');
        
        $especieOBJ = new Especie();
        if($idespecie > 0){
            $especieOBJ->eliminar($idespecie);
        }
        $especieOBJ->saveEspecie($datos);
        echo $especieOBJ->getLastId();
    }
    
     public function AddEspecieDetalle(){
        $datos = array(
            'elemento' => $this->input->post('elemento'),
            'marca' => $this->input->post('marca'),
            'cantidad' => $this->input->post('cantidad'),
            'contenido' => $this->input->post('contenido'),
            'especie_id' => $this->input->post('especie_id'),
        );
        $detalleespecieOBJ = new Especie_detalle();
        $detalleespecieOBJ->saveEspecie_detalle($datos);
        echo $detalleespecieOBJ->getLastId();
    }
    
    
    
    public function uploadimg(){
        
        
        
           function sanear_string($string) {

              $string = trim($string);

              $string = str_replace(
                  array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
                  array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
                  $string
              );

              $string = str_replace(
                  array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
                  array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
                  $string
              );

              $string = str_replace(
                  array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
                  array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
                  $string
              );

              $string = str_replace(
                  array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
                  array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
                  $string
              );

              $string = str_replace(
                  array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
                  array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
                  $string
              );

              $string = str_replace(
                  array('ñ', 'Ñ', 'ç', 'Ç'),
                  array('n', 'N', 'c', 'C',),
                  $string
              );

              //Esta parte se encarga de eliminar cualquier caracter extraño
              $string = str_replace(
                  array("\\", "¨", "º", "-", "~",
                       "#", "@", "|", "!", "\"",
                       "·", "$", "%", "&", "/",
                       "(", ")", "?", "'", "¡",
                       "¿", "[", "^", "`", "]",
                       "+", "}", "{", "¨", "´",
                       ">", "< ", ";", ",", ":",
                       ".", " "),
                  '',
                  $string
              );


              return $string;
          }
          $ruta= "uploads/proyectominiatura/";
          foreach ($_FILES as $key) {
            if($key['error'] == UPLOAD_ERR_OK ){//Verificamos si se subio correctamente
              $nombreTemporal = sanear_string($key['name']);
              $nombre = str_replace(' ', '', $nombreTemporal);//Obtenemos el nombre del archivo
              $temporal = $key['tmp_name']; //Obtenemos el nombre del archivo temporal
              $tamano= ($key['size'] / 1000)."Kb"; //Obtenemos el tamaño en KB
              move_uploaded_file($temporal, $ruta . $nombre); //Movemos el archivo temporal a la ruta especificada
              //El echo es para que lo reciba jquery y lo ponga en el div "cargados"
              echo $nombre;
            }else{
              echo $key['error']; //Si no se cargo mostramos el error
            }
          }
    }
    
    
    public function nombrecategoria(){
        $id = $this->input->post('id');
        $categoriasOBJ = new Categorias();
        $categoria = $categoriasOBJ->getCategoriasById($id);
        echo $categoria->nombre;
    }
    


    // ----------------------------------------------------------------------


}
