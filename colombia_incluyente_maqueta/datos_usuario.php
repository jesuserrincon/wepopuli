<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  
<!-- Add mousewheel plugin (this is optional) -->	
<script type="text/javascript" src="assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="assets/js/fancy.js"></script>
</head>

<body>
<?php include"header.php" ?>  


<div class="txt_reglamento">
  <h2 class="h2_b" style="font-size:44px">Datos de <span>usuario</span></h2>
</div>

  <div class="box_shadow_big pad_box clearfix">
        
        <div class="clearfix col_middle">
            <div class="col_int_b_1 clearfix">
                <div class="label">Nombres <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">No. Cédula <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">Teléfono 2 (Opcional)</div>
                <input type="text" class="input_b" />
                <div class="label">Pais <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">Departamento (Región) <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">Dirección <span class="requiered">*</span></div>
                <input type="text" class="input_b" />
                <div class="label">Facebook (opcional)</div>
                <input type="text" class="input_b" />
                <div class="label">Twitter (opcional)</div>
                <input type="text" class="input_b" />
                <div class="clear" style="margin-bottom:40px"></div>

            </div>
            
            <div class="col_int_b_1 clearfix right">
                <div class="label">Apellidos <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

                <div class="label">Télefono 1 <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

                <div class="label">E-mail <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

                <div class="label">Confirmar E-mail <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

                <div class="label">Municipio <span class="requiered">*</span></div>
                <input type="text" class="input_b" />

              <div class="label">Sexo <span class="requiered">*</span></div>
               
                <label><input class="sex_f" type="radio" name="n" title="femenino"/></label>
                <label class="rr"><input class="sex_m" type="radio" name="n" title="masculino"/></label>
				<div class="clear" style="margin-bottom:20px"></div>
                
                <div class="label">Estado Civil <span class="requiered">*</span></div>
                <select class="select">
                    <option value="">Soltero</option>
                    <option>Casado</option>
                    <option>Divorciado</option>
                </select>
				<div class="clear m_20_bottom"></div>

                <div class="change_pass">
                	<a href="#cambiar_pass"  class="fancybox">Cambiar contraseña</a>
                </div>

            </div>
            
        </div>
                    <div class="div_line clear" style="margin-bottom:0px; widows:100%"></div>
            <div class="pad_box_10 clearfix">
            <a href="reglamentos.php" class="bt_green">Guardar Cambios</a>	
			</div>

        
        
</div>
<?php include"footer.php" ?>  


<!-- //popup -->
<div id="cambiar_pass" style="width:225px;display:none;">
	<div class="label">Contraseña actual<span>*</span></div>
    <input type="text" class="input_a" />
	<div class="label">Contraseña actual<span>*</span></div>
    <input type="password" class="input_a" />
	<div class="label">Confirmar contraseña actual<span>*</span></div>
    <input type="password" class="input_a" />
    <br />
    <input type="submit" class="bt_green" value="Guardar Cambio" style="font-size:13px" />
</div>
 

</body>
</html>