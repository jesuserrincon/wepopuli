<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="no-js" lang="es" xmlns:og="http://ogp.me/ns#/fb#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- Styles -->
<link href="<?php echo base_url('assets');?>/css/colombia_incluyente.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/responsiveslides.css">
<link rel="stylesheet" media="screen, projection" href="<?php echo base_url('assets');?>/css/fancySelect.css">

<!-- Metadata -->
<title>Wepopuli</title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets');?>/img/favicon.png" />
<meta http-equiv="content-language" content="es" />
<meta http-equiv="pragma" content="No-Cache" />
<meta name="designer" content="romerorodrigo.com">

<!-- JS -->


<script type="text/javascript" src="<?php echo base_url('assets');?>/js/jquery-1.11.0.min.js"></script>

<script type="text/javascript" src="<?php echo base_url('assets');?>/js/responsiveslides.js"></script>
<script type="text/javascript">
    $(function () {
        $("#slider2").responsiveSlides({
            auto: true,
            pager: true,
            speed: 300,
            maxwidth:1000
        });
        $("#slider3").responsiveSlides({
            auto: true,
            pager: true,
            speed: 300,
            maxwidth:1000
        });
    });
</script>

<script src="<?php echo base_url('assets');?>/js/fancySelect.js"></script>
<script>
    $(document).ready(function() {
        setTimeout(function(){ $('.select').fancySelect() },1000);
    });
</script>

<script>

    $(document).ready(function() {


        // Boilerplate
        var repoName = 'fancyselect'

        $.get('https://api.github.com/repos/octopuscreative/' + repoName, function(repo) {
            var el = $('#top').find('.repo');

            el.find('.stars').text(repo.watchers_count);
            el.find('.forks').text(repo.forks_count);
        });

        var menu = $('#top').find('menu');

        function positionMenuArrow() {
            var current = menu.find('.current');

            menu.find('.arrow').css('left', current.offset().left + (current.outerWidth() / 2));
        }

        $(window).on('resize', positionMenuArrow);

        menu.on('click', 'a', function(e) {
            var el = $(this),
                href = el.attr('href'),
                currentSection = $('#main').find('.current');

            e.preventDefault();

            menu.find('.current').removeClass('current');

            el.addClass('current');

            positionMenuArrow();

            if (currentSection.length) {
                currentSection.fadeOut(300).promise().done(function() {
                    $(href).addClass('current').fadeIn(300);
                });
            } else {
                $(href).addClass('current').fadeIn(300);
            }
        });

        menu.find('a:first').trigger('click')
    });
</script>




<script src="<?php echo base_url('assets');?>/js/slides.min.jquery.js"></script>
<script>
    $(function(){
        $('.slides').slides({
            preload: true,
            generateNextPrev: true,
            generatePagination: false
        });

    });
</script>
<script type="text/javascript">
    /*
     * customRadioCheck: jQuery plguin for checkbox and radio replacement
     * Usage: $('input[type=checkbox], input[type=radio]').customRadioCheck();
     * Author: Cedric Ruiz
     * License: MIT
     */
    ;(function(){
        $.fn.customRadioCheck = function() {

            return this.each(function() {

                var $this = $(this);
                var $span = $('<span/>');

                $span.addClass('custom-'+ ($this.is(':checkbox') ? 'check' : 'radio'));
                $this.is(':checked') && $span.addClass('checked'); // init
                $span.insertAfter($this);

                $this.parent('label').addClass('custom-label')
                    .attr('onclick', ''); // Fix clicking label in iOS
                // hide by shifting left
                $this.css({ position: 'absolute', left: '-9999px' });

                // Events
                $this.on({
                    change: function() {
                        if ($this.is(':radio')) {
                            $this.parent().siblings('label')
                                .find('.custom-radio').removeClass('checked');
                        }
                        $span.toggleClass('checked', $this.is(':checked'));
                    },
                    focus: function() { $span.addClass('focus'); },
                    blur: function() { $span.removeClass('focus'); }
                });
            });
        };
    }());



    ;(function(){
        $.fn.sexCheck = function() {

            return this.each(function() {

                var $this = $(this);
                var $span = $('<span/>');

                $span.addClass('custom2-'+ ($this.is(':checkbox') ? 'check' : 'radio'));
                $this.is(':checked') && $span.addClass('checked'); // init
                $span.insertAfter($this);

                $this.parent('label').addClass('custom2-label')
                    .attr('onclick', ''); // Fix clicking label in iOS
                // hide by shifting left
                $this.css({ position: 'absolute', left: '-9999px' });

                // Events
                $this.on({
                    change: function() {
                        if ($this.is(':radio')) {
                            $this.parent().siblings('label')
                                .find('.custom2-radio').removeClass('checked');
                        }
                        $span.toggleClass('checked', $this.is(':checked'));
                    },
                    focus: function() { $span.addClass('focus'); },
                    blur: function() { $span.removeClass('focus'); }
                });
            });
        };
    }());


    ;(function(){
        $.fn.sexMCheck = function() {

            return this.each(function() {

                var $this = $(this);
                var $span = $('<span/>');

                $span.addClass('custom3-'+ ($this.is(':checkbox') ? 'check' : 'radio'));
                $this.is(':checked') && $span.addClass('checked'); // init
                $span.insertAfter($this);

                $this.parent('label').addClass('custom3-label')
                    .attr('onclick', ''); // Fix clicking label in iOS
                // hide by shifting left
                $this.css({ position: 'absolute', left: '-9999px' });

                // Events
                $this.on({
                    change: function() {
                        if ($this.is(':radio')) {
                            $this.parent().siblings('label')
                                .find('.custom3-radio').removeClass('checked');
                        }
                        $span.toggleClass('checked', $this.is(':checked'));
                    },
                    focus: function() { $span.addClass('focus'); },
                    blur: function() { $span.removeClass('focus'); }
                });
            });
        };
    }());
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".recordarme").click(function(){
            $(".recordarme").toggleClass("active_recordarme");
        });

        $(".verde_bt").click(function(){
            $(".verde_bt").toggleClass("active_recordarme");
        });
        $(".verde_bt2").click(function(){
            $(".verde_bt2").toggleClass("active_recordarme");
        });
        $(".verde_bt3").click(function(){
            $(".verde_bt3").toggleClass("active_recordarme");
        });
    });
</script>
<script src="<?php echo back_asset('components/uploadify/swfobject.js') ?>"></script>
<script src="<?php echo back_asset('components/uploadify/uploadify.js') ?>"></script>

<?php

//echo $template['partials']['header'];
echo $template['body'];
//echo $template['partials']['footer'];

?>

<script src ="<?php echo base_url('assets');?>/js/imgLiquid.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".imgLiquidFill").imgLiquid({fill:true});
        $(".imgLiquidNoFill").imgLiquid({fill:false});
    });
</script>

<script>

    function keydown(e,s){
        if (!e) var e = window.event;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        if (code==13){
            buscar();
        }
    }

    function buscar(){
        var palabrabuscar = $('#palabrabuscar').val();
        if(palabrabuscar == ''){
            return false;
        }else{
            $('#formulariobusqueda').submit();
        }

    }

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.over_project_ref').hide(1);

       
        $('.volver').click(function() {
            $(this).parent().parent().slideToggle(400);
            return false;
        });

        $('.estadisticas_bt').click(function() {
            $(this).parent().parent().find('.over_project_ref').slideToggle(400);
            return false;
        });


    });
</script>


</body>
</html>