<?php if ($cart = $this->cart->contents()){
    $cart = $this->cart->contents();
}else{
    $cart = array();
};
?>
<a class="check-bt" href="<?php echo base_url()?>lista/carrito">
    <?php if($total_items=$this->cart->total_items()) {?>
        <h1 class="items"> <?php echo $total_items?> Item(s)</h1>
    <?php }else{?>
        <h1> 0 Item(s)</h1>
    <?php } ?>
    <p>Check out</p>
</a>
  <section>
    <div class="con-section">
      <div class="mg-section cfx">
        <h1 class="int-tt cfx">Carrito de cotización</h1>
        <table class="resumen-table cfx">
          <tr>
            <th>Nombre del producto</th>
            <th>Cantidad</th>
            <th>Eliminar</th>
          </tr>

            <?php foreach($cart as $item): ?>

          <tr>
            <td class="pro-name"><?=$item['name']; ?></td>
            <td>

                <input value="<?=$item['qty']; ?>" onkeypress="return numbs(event)" onchange="actualizar('<?php echo $item['rowid'] ?>', this)" onpaste="return numbs(event)"></td>
            <td><div class="con-clear-bt fl"><a class="clear-bt" href="<?php echo base_url() ?>lista/remove/<?=$item['rowid']; ?>"></a></div></td>
          </tr>

            <?php endforeach; ?>

        </table>
        <div class="con-bts-list">
          <a class="bt-list-back tr" href="javascript:history.back()">Volver<span class="bt-list-t4 fr"></span></a>
          <a id="test4" class="bt-list-cot modal-box tr" href="#modal-cotiza" style="border-left:2px solid #fff; border-right:none; left:inherit; right:0;">Siguiente<span class="bt-list-t1 fr"></span></a>
        </div>
      </div>
    </div>
  </section>
  
  <!--Modal-box-->
  <div class="con-modal" id="modal-cotiza">
    <div class="modal-info">
      <h2>Cotización</h2>
      <p align="center">* El tiempo mínimo de alquiler es de un mes.</p>
      <form action="<?php echo base_url()?>lista/pdf" class="grl-form cfx" method="post">
        <input class="tr fl" data-validation="alphanumeric" name="nombre" type="text" placeholder="Nombre..." value="">
        <input class="tr fr" data-validation="email" name="correo" type="text" placeholder="E-mail..." value="">
        <input class="tr fl" type="text" placeholder="Dirección..." value="">
        <input class="tr fr" type="text" placeholder="Teléfono..." value="">
        <select class="tr" name="zona" data-validation="alphanumeric">
            <?php
            $i=0;
            foreach($zonas as $item){
                if($i == 0){
                ?>
                  <option value="<?php echo $item->precio ?>">Zona de residencia...</option>
                    <?php } ?>
          <option value="<?php echo $item->precio ?>">&nbsp; &bull; <?php echo $item->nombre ?></option>
            <?php
            $i++;
            } ?>
        </select>
        <fieldset>
          <input  data-validation="required" id="ok" type="checkbox">
          <label for="ok">Acepto los términos y condiciones. <a class="tr" href="<?php base_url() ?>lista/terminos">Ver acá</a></label>
        </fieldset>
        <input class="bt-submit tr fr" type="submit" value="Enviar">
      </form>
    </div>
    <button class="close-modal-sc close">×</button>
  </div>
  <!--Fin modal-box-->


<script type="text/javascript">

    <?php
if(isset($modal)){

?>

         document.getElementById('#test4').click()


    <?php
    }
    ?>

</script>

<script>


    function actualizar(rowid ,elemento){

        $.ajax({
            url: "<?php echo base_url()?>lista/update",
            type: "post",
            dataType: "text",
            data: 'rowid='+rowid+'&valor='+$(elemento).val()
        }).done(function (data) {

          $('.items').html(data)

            })

    }
</script>
