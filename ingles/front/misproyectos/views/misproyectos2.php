

<link href="<?php echo base_url(); ?>assets/css/jquery-filestyle.css" rel="stylesheet" />



<script src="<?php echo base_url(); ?>assets/js/jquery.tools.min.js"></script>
<script>
$(function() {
	$(".tool").tooltip();
});
</script>



<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
		$(document).ready(function() {
			

			$('.fancybox').fancybox();

			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			
			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

		
			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});
		});
</script>

<div class="txt_reglamento" style="margin-top:40px; padding-left:40px;">
  <h2 class="h2_b" style="font-size:44px">My  <span>Projects
</div>

    
    
<div class="box_shadow_big clearfix">
  <div class="clear" style="height:30px"></div>
  <?php 
  //funciones en la pagina WEB
  function dameFecha($fecha,$dia)
                {   list($day,$mon,$year) = explode('/',$fecha);
                    return date('d/m/Y',mktime(0,0,0,$mon,$day+$dia,$year));        
                }
  ?>
  <?php foreach ($proyectos as $item){?>
  <div class="box_shadow2 box_margin clearfix project_ref">
  
  			<div class="over_project_ref">
            	<div class="col_ref line_right">
                	<div class="volver">Go back</div>
                    <div class="img_icon"><img src="<?php echo base_url(); ?>assets/img/063.png"></div>
                    <div class="spacer"></div>
                    <h2 class="h2_b">Total recaudado</h2>
                    <div class="monto">$2.000.000 <span>(USD)</span></div>
                </div>
            	
                
                <div class="col_ref line_right">
                	<div class="img_icon"><img src="<?php echo base_url(); ?>assets/img/065.png"></div>
                	<div class="spacer"></div>
                    <h2 class="h2_b">Has apoyado:</h2>
                    <div class="apoyos">
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td>Paquete(s) de Alimento:</td>
                            <td>2</td>
                          </tr>
                          <tr>
                            <td>Paquete(s) de Herramientas:</td>
                            <td>1</td>
                          </tr>
                          <tr class="total">
                            <td>Total:</td>
                            <td>3</td>
                          </tr>
                        </table>
                    </div>
                </div>
                
                
            	<div class="col_ref">
                	<div class="img_icon"><img src="<?php echo base_url(); ?>assets/img/064.png"></div>
                	<div class="spacer"></div>
                    <h2 class="h2_b">Has apoyado:</h2>
                    <div class="apoyos">
                    	-Voluntariado  para alfabetización.<br />
						-Voluntariado  para construir casas.
                    </div>
                </div>
            	
            	
            	
            </div>
            
            
            
  
  
        	<div class="left line_right" style="height:300px">
            	<div class="pic_project" style="margin-right:8px !important">
                <?php if($item->imagen_miniatura != ''){ ?>	
                    <img src="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $item->imagen_miniatura; ?>" width="226" height="145" />
                <?php } ?>    
                </div>
                	
                <div class="div_line" style="margin-bottom:10px"></div>
				
                <div style="margin-bottom:5px"><center>Supported</center></div>
                <div class="barra_porcentaje2" style="margin:5px 0px 25px 10px; width:88%">
                    <div class="bar2" style="width:0%">0%</div>
                </div>
                <div class="estadisticas_bt">Statistics</div>

            </div>
            
            <div class="right" style="padding:5px; width:250px">
            	<div class="logo_fundacion">
                    <img src="<?php echo base_url(); if($item->imagenges != ''){ echo 'uploads/proyectos/gestores/'.$item->imagenges; }else{ echo 'assets/img/066.png'; }; ?>" width="184" height="122" />
                </div> 
                <div class="data_fundacion">
            <?php  if($item->nombreges != ''){ echo '<div class="dato"><b>Por: '.$item->nombreges.'</b></div>'; } ?>
            <?php  if($item->nombreslider != ''){ echo '<div class="dato"><b>Leader: </b> <a href="#inline2"  class="fancybox"  style="color:#27aae1">'.$item->nombreslider.'</a> </div>'; } ?>
            <?php  if($item->paginages != ''){ echo '<div class="dato"><b>Pagina: '.$item->paginages.'</b></div>'; } ?>
            <?php  if($item->facebookges != ''){ echo '<div class="dato"><b>Facebook: '.$item->facebookges.'</b></div>'; } ?>
            <?php  if($item->twitterges != ''){ echo '<div class="dato"><b>Twitter: '.$item->twitterges.'</b></div>'; } ?>
           
                </div>
            </div>
            
            
            <div class="right line_right" style="width:350px; height:300px; padding-right:20px">
            	<div class="des_project_int_mis_proyectos" style="height:290px; position:relative;">
            		<div class="title_my_projects left">
                	    <h3 class="h3_purpure"><?php echo $item->titulo;?></h3>
                	</div>
                    <?php if($item->nombreestado == 'Sin terminar'){?>
                    <a href="<?php echo base_url().'basicos/index/'.$item->id ?>" class="right edit2">editar</a>
                    <?php }?>
                    <div class="clear"></div>
                    <div class="txt_des">
                    <?php echo $item->descripcion_corta; ?>
                    </div>
                    <div class="city">Bogotá, Colombia.</div>
                    
                    <div style="margin:10px 10px 0px 65px; position:absolute; top:160px">
                    	 <div class="percent_icon_box first_precent">
                         <div class="money_icon_a icons"></div>
                            <div class="percent_circle">0%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a icons"></div>
                            <div class="percent_circle">0%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a icons"></div>
                            <div class="percent_circle">0%</div>
                        </div>
                        <div class="clear"></div>
                    </div>
					
                    <div style="position:absolute; bottom:15px;">
                    <div class="estado left" style="margin-right:10px; padding-left:10px"><div class="active_point left"><span>Status:</span> <?php echo $item->nombreestado?></div><span class="tool" title="Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.">?</span> </div>
                    <div class="estado left"><span>Due Date:</span> <?php echo dameFecha(date('d/m/Y'),$item->dias_para_cerrar); ?></div>
                     </div>
                     
                    <div class="clear" style="margin-bottom:10px"></div>
                </div>
            </div>
            
            
            
            
        </div> <!-- // ************* fin proyecto ************** -->
<?php } ?>


        <div class="pad_int_20">
    <div class="div_line_2"></div>
    <a href="<?php echo base_url(); ?>buscaapoyo" class="bt_green2" style="margin-top:20px">Start a New Project</a>
        <div class="div_line_2"></div>
       <!-- <div class="pg">
        	<div class="left"><a href="#">Primero </a></div>
        	<div class="left"><a href="#" class="prev2"></a></div>
        	<div class="left"><a href="#" class="prev"></a></div>
        	<div class="left num">
            	<a href="#">1</a>
            	<a href="#">2</a>
            	<a href="#">3</a>
            	<a href="#">4</a>
            </div>
        	<div class="left"><a href="#" class="next2"></a></div>
        	<div class="left"><a href="#" class="next"></a></div>
            <div class="left"><a href="#">Último </a></div>
            <div class="clear"></div>
        </div>-->
        <div class="spacer"></div>
	</div>	
    
</div>
    
    
<script type="text/javascript" src="<?php echo base_url(); ?><?php echo base_url(); ?>assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?><?php echo base_url(); ?>assets/js/aplication.js"></script>



<!-- //popup -->
<div id="inline2" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px">Lider</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;">Rodrigo Romero Castro</h2>
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px">Organization pies descalzos</h3>
    
    	<div class="txt_user">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent purus turpis, tincidunt in malesuada ut, mattis eget purus. Vivamus semper fringilla nunc, iaculis ultricies nibh scelerisque eget. Nam tellus orci, elementum et erat at, sollicitudin ultrices mauris. Maecenas id purus at nisi eleifend dignissim. Duis non mi id nisi venenatis ornare. Donec suscipit enim eget mauris pharetra, venenatis commodo elit congue. Maecenas non eleifend libero, non interdum nisi. Nunc a lorem ornare, volutpat nisi sed, molestie eros. Nam in interdum libero. Donec hendrerit vitae dui ut imperdiet.</p>
            <p>Vestibulum tempus id ante a eleifend. Morbi at elementum erat. Nam in nulla ac ante fermentum ultrices quis nec sem. Donec ultricies in est et dignissim. Donec sollicitudin tortor eget mauris pharetra, a congue metus feugiat. Integer velit risus, hendrerit et pulvinar eget, mollis a odio. Donec sed sodales nisi. Aenean accumsan tellus in est pretium euismod. Praesent eu risus eros. Donec pulvinar, eros vel tincidunt sagittis, eros velit tempus diam, et feugiat elit enim at augue.</p>
            <p>Sed adipiscing nisi ac justo suscipit lobortis. Suspendisse potenti. Integer a ullamcorper orci. Curabitur eget suscipit metus, et aliquam est. Praesent eget leo sed purus tempus eleifend sit amet a mauris. Suspendisse lacus dolor, rhoncus sed hendrerit at, laoreet eu turpis. Aliquam massa libero, pellentesque eget risus vel, vehicula tristique ipsum.</p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="<?php echo base_url(); ?><?php echo base_url(); ?>assets/img/080.png" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Contact Information</span></div>
            <div class="dato"><b>Email:</b> <a href="mailto:info@miemail.es" style="color:#602483"> info@romerorodrigo.com</a></div>
            <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
            <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
            <div class="dato"><b>Bogotá, Colombia</div>
        </div>
	</div>
    
</div>
