<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author rigobcastro
 * @author Brayan Acebo
 * @author Jose Fonseca
 */
class contacto extends Front_Controller {

    public function __construct() {
        $this->load->library('session');

        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index() {

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        $this->load->model(array(
            CMSPREFIX."contactenos/contactenos",
            CMSPREFIX."footer/footer",
            CMSPREFIX."redes/redes"
        ));

        $r = new Redes();
        $redes = $r->getRedesById(1);
        $this->_data['redes'] = $redes;

        $f = new Footer();
        $footer = $f->getFooterById(1);
        $this->_data['footer'] = $footer;


        $b = new Contactenos();
        $info = $b->getContactenosById(1);
        $this->_data["info"] = $info;
        return $this->build('contacto');
    }

    // ----------------------------------------------------------------------


}
