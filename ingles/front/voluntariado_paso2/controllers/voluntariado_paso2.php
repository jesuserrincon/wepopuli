<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class voluntariado_paso2 extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
         $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."textos/textos",
               CMSPREFIX."gestores/gestores",
               CMSPREFIX."ayuda_voluntariado/ayuda_voluntariado",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0,$idvol = 0) {
        $this->set_title('Bienvenidos a ' . SITENAME, true);
         $proyectoOBJ = new Proyectos();
        $gestorOBJ = new Gestores();
        //seleccionamos el proyecto a mostrar
        $this->_data["proyecto"] = $proyectoOBJ->getProyectosById($proyectoid);
        //Fin mostrar el proyecto 
        //traemos la informacion de la fundacion y lider
        $this->_data["gestor"] = $gestorOBJ->getGestoresByIdProyecto($proyectoid);
        //Fin informacion fundacion
        //enviamos la ayuda seleccionada por el usuario
        $voluntariadoOBJ = new Voluntariado();
        $this->_data['voluntariado'] = $voluntariadoOBJ->getVoluntariadoById($idvol);
        //Fin envio voluntariado
        //enviamos los datos del usuario para mostrar la info
        $usuario = $this->nativesession->get('usuario');
        $usuarioOBJ = new Usuarios();
        $this->_data["usuario"] =  $usuarioOBJ->getUsuariosById($usuario['id']);
        //fin envio datos de usuario
        $this->_data['idvol'] = $idvol;
        $this->_data['idpro'] = $proyectoid;
        return $this->build('voluntariado_paso2');
    }
    


    // ----------------------------------------------------------------------

    public function registrarayuda(){
        $b = new Ayuda_voluntariado();    
        $idpro = $this->input->post('idpro');
        if(isset($_FILES['imagen']['name']) && !empty($_FILES['imagen']['name'])){
            $config['upload_path'] = UPLOADSFOLDER.'ayudas/imagenes/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|rgb|psd';
            $config['remove_spaces']=TRUE;
            $config['max_size']	= '10000';

            $this->load->library('upload', $config);
            $this->upload->do_upload('imagen');
        }


       
        //Obtenemos el usuario 
        $usuario = $this->nativesession->get('usuario');
        $usuarioOBJ = new Usuarios();
        $usuarioobj =  $usuarioOBJ->getUsuariosById($usuario['id']);
        //fin usuario
        $datos = array(
                    'usuarios_id' => $usuario['id'],
                    'proyectos_id' => $idpro,
                    'nombres' => $usuarioobj->nombres,
                    'apellidos' => $usuarioobj->apellidos,
                    'tipo_identificacion' => $usuarioobj->tipo_identificacion,
                    'identificacion' => $usuarioobj->cedula,
                    'telefono' => $usuarioobj->telefono,
                    'telefono2' => $usuarioobj->telefono2,
                    'email' => $usuarioobj->email,
                    'pais' => $usuarioobj->pais,
                    'departamento' => $usuarioobj->departamento,
                    'municipio' => $usuarioobj->ciudad,
                    'direccion' => $usuarioobj->direccion,
                    'estado_civil' => $usuarioobj->estado_civil,
                    'sexo' => $usuarioobj->sexo,
                    'facebook' => $usuarioobj->facebook,
                    'twitter' => $usuarioobj->twitter,
                    'discapacidad' => $this->input->post('tienediscapacidad'),
                    'cual' => $this->input->post('cual'),
                    'porque' => $this->input->post('porque'),
                    'imagen' => str_replace(' ','_', $_FILES['imagen']['name']),
                    'tipo_voluntariado' => $this->input->post('tipo_voluntariado'),
        );
        $insertar = $b->saveAyuda_voluntariado($datos);
        
        
         $proyectosOBJ = new Proyectos();
        $cantidadvol = $proyectosOBJ->getCantidadVolByProyecto($idpro);
        //actualizamos el porcentaje de voluntariado al proyecto 
        $datos = array(
                    'porcentaje_voluntariado' => $cantidadvol,
                    'id' => $idpro,
        );


        $proyectosOBJ->updateProyectos($datos);

        //fin actualizar porcentaje 
        //actualizamos el porcentaje total
        $porcentaje = 100;
         $proyectosOBJ2 = new Proyectos();
         $proyectovalidacion = $proyectosOBJ2->getProyectosById($idpro);
         if($proyectovalidacion->valormonetario > 20000){
            $porcentaje = $porcentaje + 100; 
         }
         $especie = new Especie();
         $cantidadespecie = $especie->getEspecieByProyectoCOUNT($idpro);
         if($cantidadespecie > 0){
            $porcentaje = $porcentaje + 100;    
         }
        $proyectosOBJ->ActualizarTotalayudas($idpro,$porcentaje);
        //Fin actualizar porcentaje total

        $b = new Proyectos();
        $datosProyecto = $b->getProyectosById($idpro);
        $proyecto = $b->getDatosAprobados($idpro);

        // verificamos porsentaje total = 100% Enviamos un correo.
        if($datosProyecto->porcentaje_total == 100){

            $email = $proyecto->email;
            $asunto = 'Apoyo total 100%';
            $titulo ='Apoyo total 100%';
            $mensaje = 'Su proyecto ha alcanzado el 100% del apoyo total solicitado.';

            $this->enviarCorreo($email,$asunto,$titulo,$mensaje);
        }
        //Fin envio correo

        // verificamos porsentaje voluntariado = 100% Enviamos un correo.
        if($cantidadvol == 100){

            $email = $proyecto->email;
            $asunto = 'Apoyo voluntario 100%';
            $titulo ='Apoyo voluntario 100%';
            $mensaje = 'Su proyecto ha recibido el 100% de los voluntarios solicitados. ';

            $this->enviarCorreo($email,$asunto,$titulo,$mensaje);

        }
        //Fin envio correo

        //Enviamos un correo al creador para avisar de la ayuda
        $email = $proyecto->email;
        $asunto = 'Ayuda voluntariado exitosa.';
        $titulo = 'Proyecto: '.$proyecto->titulo;
        $mensaje =  'Le informamos que su proyecto ha recibido una ayuda como voluntariado</br><br/>
                Nombre: '.$usuarioobj->nombres.'<br/>
                Apellidos: '.$usuarioobj->apellidos.'<br/>
                Tipo de identificación: '.$usuarioobj->tipo_identificacion.'<br/>
                Teléfono: '.$usuarioobj->telefono.'<br/>
                Teléfono 2: '.$usuarioobj->telefono2.'<br/>
                Email: '.$usuarioobj->email.'<br/>
                País: '.$usuarioobj->pais.'<br/>
                Departamento: '.$usuarioobj->departamento.'<br/>
                Ciudad: '.$usuarioobj->ciudad.'<br/>
                Dirección: '.$usuarioobj->direccion.'<br/>
                Estado civil: '.$usuarioobj->estado_civil.'<br/>
                Sexo: '.$usuarioobj->sexo.'<br/>
                Facebook: '.$usuarioobj->facebook.'<br/>
                Twitter: '.$usuarioobj->twitter.'<br/>
                Discapacidad: '.$this->input->post('tienediscapacidad').'<br/>
                Cual: '.$this->input->post('tienediscapacidad').'<br/>
                Porque: '.$this->input->post('porque').'<br/>
                Imagen:<a href="'.base_url().'uploads/ayudas/imagenes/'.str_replace(' ','_', $_FILES['imagen']['name']).'">Ver</a>';

        $this->enviarCorreo($email,$asunto,$titulo,$mensaje);

        //Fin envio correo creador

        //Enviamos un correo al colaborador.

        $email = $_SESSION['usuario']['email'];
        $asunto = 'Apoyo voluntario';
        $titulo ='Apoyo voluntario';
        $mensaje = 'Gracias por ofrecerse como voluntario. Pronto será contactado por la organización gestora del proyecto con los detalles para realizar su participación. ';

        $this->enviarCorreo($email,$asunto,$titulo,$mensaje);

        //Fin envio correo colaborador

        redirect('detalle_proyecto/index/'.$idpro.'/1', 'refresh');
        
    }
} ?>