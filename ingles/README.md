# Imaginamos CMS

Versión estable actual : 1.6.2

## Team

* [Rigo B castro]
* [Jose Fonseca](http://josefonseca.me/)

## Description

CMS Para uso general en Imginamos

## IMPORTANTE

- Crear un folder llamado cache en la ruta assets\ y darle permisos 777, el sistema guarda archivos cache en ese folder, si no se crea se generará un error.

### Contribuidores

Para estar en la lista por favor hacer pull request.

### Change Log

1.6.2 Agregando Folders CSS y JS en assets del front.