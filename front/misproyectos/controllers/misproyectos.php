<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class misproyectos extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $this->load->model(array(
            CMSPREFIX . "usuarios/usuarios",
            CMSPREFIX . "proyectos/proyectos",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------
    public function eliminarpro($id){
        $proyectoOBJ = new Proyectos();
        $proyectoOBJ->eliminar($id);
        redirect('misproyectos/index/0/1');
    
    }
    public function index($desde = 0,$eliminar = 0) {
        
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
        $usuario = $this->nativesession->get('usuario');
        $proyectosOBJ = new Proyectos();
        $proyectos = $proyectosOBJ->getProyectosByUsuario($usuario['id'],$desde);
        function dameFecha($fecha,$dia)
                {   list($day,$mon,$year) = explode('/',$fecha);
                    return date('d/m/Y',mktime(0,0,0,$mon,$day+$dia,$year));        
                }
        function image_exists($url) {
                    if(@getimagesize($url)){
                        return true;
                    }else{
                        return false;
                    }
                }
        $countpro = 0;  
        $html = '';
        
        
         foreach ($proyectos as $item){ $countpro++;
         $proyectosOBJ2 = new Proyectos();
         $ayudavoluntariado = $proyectosOBJ2->getVoluntariadoAyudaTotal($item->id);
         $htmlvol = '';
         foreach ($ayudavoluntariado as $item2){
             $htmlvol .= '-'.$item2->tituloayuda.' '.$item2->cantidad.' <br />';
         }
         
                
              if($item->titulo == ''){ $titulo = 'Título sin completar'; }else{ $titulo = $item->titulo; }
              if($item->descripcion_corta == ''){ $descripcion = 'Descripción sin completar'; }else{ $descripcion = $item->descripcion_corta; }
              if($item->paginages != ''){ $paginav =  '<div class="dato"><b>Web: '.$item->paginages.'</b></div>'; }else{ $paginav = ''; } 
              if($item->facebookges != ''){ $facebookv = '<div class="dato"><b>'.$item->facebookges.'</b></div>'; }else{ $facebookv = '';  }
              if($item->twitterges != ''){ $twitterv = '<div class="dato"><b>Twitter: '.$item->twitterges.'</b></div>'; }else{ $twitterv = ''; }
              $imagen = base_url().'uploads/proyectominiatura/'.$item->imagen_miniatura;
              if(image_exists($imagen)){ $imagenprev = $imagen; }else{ $imagenprev = base_url().'assets/img/066.png'; }
               $imagenfundacion = base_url().'uploads/proyectos/gestores/'.$item->imagenges;
              if(image_exists($imagenfundacion)){ $imagenfun = $imagenfundacion; }else{ $imagenfun = base_url().'assets/img/066.png'; }
              if($item->nombreestado == 'Sin terminar' || $item->nombreestado == 'Revisado' ){ $editar = '<a href="'.  base_url().'misproyectos/eliminarpro/'.$item->id.'" onclick="return confirm('."'".'¿Está seguro que desea eliminar este proyecto?'."'".')" class="right edit2" style="color:#fff; margin-right:0px !important; margin-left:2px; background-color:#9c1977; float:right">eliminar</a> <a href="'.  base_url().'reglamento/index/'.$item->id.'" class="right edit2" style="background:#9c1977; color:#fff">editar</a>'; }else{ $editar = ''; }
              //validamos que ayudas tiene para apagar los botones que no tienenn ayuda
              if($item->valormonetario < 20000){ $clasemon = 'ico_desactivado'; }else{ $clasemon = ''; }
              if($item->cantidadvoluntariado == 0){ $clasevol = 'ico_desactivado'; }else{ $clasevol = ''; }
              if($item->cantidadespecie == 0){ $claseesp = 'ico_desactivado'; }else{ $claseesp = ''; }
              //fin validacion botones
              
              
              $html .= ' <div class="box_shadow2 box_margin clearfix project_ref">
  
  			<div class="over_project_ref">
            	<div class="col_ref line_right">
                	<div class="volver">Volver</div>
                        <div class="img_icon"><img src="'.base_url().'assets/img/063.png"></div>
                    <div class="spacer"></div>
                    <h2 class="h2_b">Total recaudado:</h2>
                    <div class="monto">$0 <span>(COP)</span></div>
                </div>
            	
                
                <div class="col_ref line_right">
                	<div class="img_icon"><img src="'.base_url().'assets/img/064.png"></div>
                	<div class="spacer"></div>
                    <h2 class="h2_b">Total Paquetes Recibidos:</h2>
                    <div class="apoyos">
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td>Ningún paquete</td>
                            <td>0</td>
                          </tr>
                          
                          <tr class="total">
                            <td>Total:</td>
                            <td>0</td>
                          </tr>
                        </table>
                    </div>
                </div>
                
                
            	<div class="col_ref">
                	<div class="img_icon"><img src="'.base_url().'assets/img/065.png"></div>
                	<div class="spacer"></div>
                    <h2 class="h2_b">Total Voluntarios postulados:</h2>
                    <div class="apoyos">
                    '.$htmlvol.'
                    </div>
                </div>
            	
            	
            	
            </div>
            
            
            
  
  
        	<div class="left line_right" style="height:300px; padding-right:5px">
            	<div class="pic_project" style="width:220px; height:146px; margin:5px 10px 5px 10px !important; ">
                <a href="detalle_proyecto/index/'.$item->id.'">
						<img class="thumbnail_a" src="'.$imagenprev.'" style="display:block; width:227px; height:150px; margin-left:-3px" />
				</a>
                </div>
                	
                <div class="div_line" style="margin-bottom:10px"></div>
				
                <div style="margin-bottom:5px"><center>Total recibido</center></div>
                <div class="barra_porcentaje2" style="margin:5px 0px 15px 10px; width:88%">
                    <div class="bar2" style="width:'.$item->porcentaje_total.'%">'.$item->porcentaje_total.'%</div>
                </div>
                <div class="estadisticas_bt">Estadísticas</div>

            </div>
            
            <div class="right" style="padding:5px; width:250px">
            	<div class="logo_fundacion">
                    <img src="'.$imagenfun.'" width="184" height="122" />
                </div> 
                <div class="data_fundacion">
            <div class="dato"><b>Proyecto por: <br /><a href="#inline2'.$item->id.'" class="fancybox">'.$item->nombreges.'</a></b></div>
            <div class="dato"><b>Liderado por: <br /></b><a href="#inline3'.$item->id.'" class="fancybox"> '.$item->nombreslider.' '.$item->apellidoslider.'</a> </div>
          '.$paginav.'
           '.$facebookv.'
           '.$twitterv.'
                </div>
            </div>
            
            
            <div class="right line_right" style="width:350px; height:300px; padding-right:20px">
            	<div class="des_project_int_mis_proyectos" style="height:290px; position:relative;">
            		<div class="title_my_projects left">
                	 <a href="'.base_url().'detalle_proyecto/index/'.$item->id.'">   
                             <h3 class="h3_purpure">'.$titulo.'</h3></a>
                	</div>
                        '.$editar.'
                    <div class="clear"></div>
                    <div class="txt_des">
                   '.$descripcion.'
                    </div>
                    <div class="city">Departamento, País.</div>
                    
                    <div style="margin:10px 10px 0px 65px; position:absolute; top:160px">
                    	 <div class="percent_icon_box first_precent">
                         <div class="money_icon_a icons '.$clasemon.'"></div>
                            <div class="percent_circle '.$clasemon.'">'.$item->porcentaje_monetario.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a icons '.$clasevol.'"></div>
                            <div class="percent_circle '.$clasevol.'">'.$item->porcentaje_voluntariado.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a icons '.$claseesp.'"></div>
                            <div class="percent_circle '.$claseesp.'">'.$item->porcentaje_especie.'%</div>
                        </div>
                        <div class="clear"></div>
                    </div>
					
                    <div style="position:absolute; bottom:15px;">
                    <div class="estado left" style="margin-right:10px; padding-left:10px"><div class="'.$item->clase.' left"><span>Status:</span> '.$item->nombreestado.'</div><span class="tool" title="'.$item->textotooltip.'">?</span> </div>
                    <div class="estado left"><span>Hasta:</span> '.dameFecha(date('d/m/Y'),$item->dias_para_cerrar).'</div>
                     </div>
                     
                    <div class="clear" style="margin-bottom:10px"></div>
                </div>
            </div>
            
            
            
            
        </div>';
         }
         if($countpro == 0){
            $html .= '<div class="alert_no">
    Aún no has creado proyectos
</div>'; 
         }
         
         
          $inline = '';
          foreach ($proyectos as $item){ 
         
              $imagenfundacion = base_url().'uploads/proyectos/gestores/'.$item->imagenges;
              if(image_exists($imagenfundacion)){ }else{ $imagenfundacion = base_url().'/assets/img/066.png';}
              $imagenlider = base_url().'uploads/proyectos/gestores/'.$item->imagenlider;
              if(image_exists($imagenlider)){ }else{ $imagenlider = base_url().'/assets/img/066.png';}
              if($item->pagina != ''){ $pagina =  '<div class="dato"><b>Web: '.$item->pagina.'</b></div>'; }else{ $pagina = ''; } 
              if($item->facebook!= ''){ $facebook = '<div class="dato"><b>Facebook: '.$item->facebook.'</b></div>'; }else{ $facebook = '';  }
              if($item->twitter != ''){ $twitter = '<div class="dato"><b>Twitter: '.$item->twitter.'</b></div>'; }else{ $twitter = ''; }
              if($item->facebooklider != ''){ $facebookv =  '<div class="dato"><b>'.$item->facebooklider.'</b></div>'; }else{ $facebookv = ''; } 
              if($item->emaillider != ''){ $emailv = '<div class="dato"><b>Email: '.$item->emaillider.'</b></div>'; }else{ $emailv = '';  }
              if($item->twitterlider != ''){ $twitterv = '<div class="dato"><b>Twitter: '.$item->twitterlider.'</b></div>'; }else{ $twitterv = ''; }
            
         $inline .= '<div id="inline2'.$item->id.'" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px; color:#1c75bc">Fundación</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;">'.$item->nombreges.'</h2>
    	<div class="spacer"></div>
    	<div class="txt_user">
            <p>'.$item->descripcionges.'</p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="'.$imagenfundacion.'" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato"><b>Proyecto por: <br /><a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$item->nombreges.'</a></b></div>
            <div class="dato"><b>Liderado por: <br /></b> <a  href="#inline3'.$item->id.'"  class="fancybox"  style="color:#27aae1">'.$item->nombreslider.' '.$item->apellidoslider.'</a> </div>
             '.$pagina.'
           '.$facebook.'
           '.$twitter.'
           
        </div>
	</div>
    
</div>

<div id="inline3'.$item->id.'" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px"> '.$item->nombreslider.' '.$item->apellidoslider.'</h2>
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px">'.$item->nombreges.'</h3>
    
    	<div class="txt_user">
            <p>'. $item->acercadelider.'</p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="'.$imagenlider.'" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Información de contacto:</span></div>
             '.$emailv.'
           '.$facebookv.'
           '.$twitterv.'
        </div>
	</div>
    
</div>';
          }
        //Fin foreach  
        $this->_data['inline'] = $inline;  
        $this->_data['html'] = $html;
        $proyectosOBJ3 = new Proyectos();
        $this->_data['cantidad'] = $proyectosOBJ3->getProyectosByUsuarioCOUNT($usuario['id']);
        $this->_data['desde'] = $desde;
        $this->_data['eliminar'] = $eliminar;
        return $this->build('misproyectos');
    }
    
  
    
}
