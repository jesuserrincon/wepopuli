<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  
<style type="text/css">
#s7 {
	color:#fff;
	background-position:0px -100px;
}
#s6 {
	color:#fff;
	background-position:0px -50px;
}
#s5 {
	color:#fff;
	background-position:0px -50px;
}
#s4 {
	color:#fff;
	background-position:0px -50px;
}
#s3 {
	color:#fff;
	background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -50px;
}
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>

<link href="assets/css/jquery-filestyle.css" rel="stylesheet" />
  	
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
</head>

<body>
<?php include"header.php" ?>  
<?php include"paso_paso.php" ?>

<div class="txt_reglamento">
  <h2 class="h2_b" style="font-size:44px">Proyecto X</h2>
  <h2 class="h2_des">Por: Fundación Píes descalzos</h2>
</div>

    
    
<div class="box_shadow_big clearfix">

  <div class="col_e pad_int_20">
        	<img src="assets/img/058.jpg" class="rounded_img" width="590" />
        
        	<div class="description_project">
        	  <h3 class="h3_black" style="font-size:23px; margin-bottom:10px;">DESCRIPCIÓN DEL PROYECTO</h3>
        	  <p>Gray House interweaves documentary and narrative footage into a film structure that blurs the lines between fiction and reality. Through an investigation of five politically- and socially-charged environments—as well as the histories and current events that shape them—Gray House forms a narrative of conjured meanings and associations.</p>
             <div class="spacer"></div>
              <h3 class="h3_purpure">OBJETIVOS Y METAS</h3>

        	  <p>The film begins on East Matagorda Bay, where a bait fisherman trawls for diurnal brown shrimp off the Texas Coast. The fisherman goes about his daily routine, casting and re-casting his net and sorting what he pulls out of the water. This repetitive task becomes a visual meditation on his primary relationship with the land.ng recovery method, commonly referred to as 'fracking'.<br />
      	    </p>
        	  <div class="spacer"></div>
              <h3 class="h3_purpure">COMO PIENSO LLEVARLO A CABO</h3>
        	  <p>The film begins on East Matagorda Bay, where a bait fisherman trawls for diurnal brown shrimp off the Texas Coast. The fisherman goes about his daily routine, casting and re-casting his net and sorting what he pulls out of the water. This repetitive task becomes a visual meditation on his primary relationship with the land.ng recovery method, commonly referred to as 'fracking'.<br />

				<div class="spacer"></div>
        	  <h3 class="h3_purpure">RIESGOS Y DESAFIOS</h3>
        	  <p>The film begins on East Matagorda Bay, where a bait fisherman trawls for diurnal brown shrimp off the Texas Coast. The fisherman goes about his daily routine, casting and re-casting his net and sorting what he pulls out of the water. This repetitive task becomes a visual meditation on his primary relationship with the land.ng recovery method, commonly referred to as 'fracking'.<br />
			<div class="spacer"></div>
        	  <h3 class="h3_purpure">PREGUNTAS Y RESPUESTAS</h3>
        	  <p>The film begins on East Matagorda Bay, where a bait fisherman trawls for diurnal brown shrimp off the Texas Coast. The fisherman goes about his daily routine, casting and re-casting his net and sorting what he pulls out of the water. This repetitive task becomes a visual meditation on his primary relationship with the land.ng recovery method, commonly referred to as 'fracking'.<br />
	  </div>
            <div class="spacer"></div>
          <h3 class="h3_purpure">VIDEO</h3>
            <iframe width="590" height="315" src="//www.youtube.com/embed/va87iu9hm4Y?rel=0" frameborder="0" allowfullscreen></iframe>            
 			
            <div class="spacer"></div>
            
            <iframe src="//player.vimeo.com/video/55751501" width="590" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <div class="spacer"></div>
          <h3 class="h3_purpure">Otras Imagenes</h3>
            <div class="spacer"></div>
            <img src="assets/img/058.jpg" class="rounded_img" width="590" />
            <div class="spacer"></div>
            <img src="assets/img/058.jpg" class="rounded_img" width="590" />
            <div class="spacer"></div>
        	<img src="assets/img/058.jpg" class="rounded_img" width="590" />




  </div>
    <div class="col_f">
    	
        <div class="logo_fundacion">
        	<img src="assets/img/060.jpg" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato"><b>Por:</b> Fundación Píes descalzos</div>
            <div class="dato"><b>Liderado por:</b> <a href="#" style="color:#27aae1">Jhon Mccain</a> </div>
            <div class="dato"><b>www.website1.com</div>
            <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
            <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
            <div class="dato"><b>Bogotá, Colombia</div>
        </div>
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>
        
        <div class="state box_min_sh" style="padding:13px 0px">
        	<div class="estado left"><div class="active_point left"><span>Status:</span> Activo</div></div>
            <div class="clear" style="margin-bottom:5px;"></div>
        	<div class="estado"><span>Hasta:</span> 20/08/2013</div>
        </div>
        
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Porcentaje total</center></div>
            <div class="barra_porcentaje">
                <div class="bar" style="width:45%">45%</div>
            </div>

		</div>


        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon"><img src="assets/img/063.png" /></div>
            <div class="tit">Apoya</div>
            <div class="barra_porcentaje" style="margin:5px auto 10px auto">
                <div class="bar" style="width:45%">45%</div>
            </div>
		</div>


        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon"><img src="assets/img/064.png" /></div>
            <div class="tit">Apoya</div>
            <div class="barra_porcentaje" style="margin:5px auto 10px auto">
                <div class="bar" style="width:45%">45%</div>
            </div>
		</div>


        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon"><img src="assets/img/065.png" /></div>
            <div class="tit">Apoya</div>
            <div class="barra_porcentaje" style="margin:5px auto 10px auto">
                <div class="bar" style="width:45%">45%</div>
            </div>
		</div>

    </div>
        

</div>
    
    
<script type="text/javascript" src="assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="assets/js/aplication.js"></script>

<?php include"footer.php" ?>  
</body>
</html>