<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo $nombremodulo ?></span>
        <br /><?php echo anchor('cms/banner_home/index/2/'.$idseccion, 'Volver', 'class="uibutton icon special answer" style="float:right;position: relative;top: -5px"') ?>                 
    </div><!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div class="imu_info" id="info"></div>
                    <?php echo form_open_multipart('cms/banner_home/update_Banner_home/', 'id="form"') ?>
                        <input style="width:250px" type="hidden"  id="id" name="id" class="small" value="<?php echo $info->id; ?>" />

                    <!--<div class="section">
                        <label>link</label>
                        <div><input value="<?php echo $info->link; ?>"  class="large text" type="text"  id="link" name="link" /></div>
                    </div>-->
                    
                    
                    <div class="section">
                            <label>Titulo</label>
                            <div><input  class="large text" type="titulo"  id="titulo" name="titulo" value="<?php echo $info->titulo; ?>" /></div>
                            <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="titulo"></span></span>
                            <script type="text/javascript">
                                $("#titulo").limit("50",".titulo");
                            </script>
                        </div>
                        <div class="section">
                            <label>Texto</label>
                            <div><textarea  class="large text"   id="texto" name="texto" /><?php echo $info->texto; ?></textarea></div>
                            <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="texto"></span></span>
                            <script type="text/javascript">
                                $("#texto").limit("160",".texto");
                            </script>
                        </div>
                         <div class="section">
                            <label>Pie de pagina</label>
                            <div><input  class="large text" type="text"  id="pie_de_pagina" name="pie_de_pagina" value="<?php echo $info->pie_de_pagina; ?>" /></div>
                            <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="pie_de_pagina"></span></span>
                            <script type="text/javascript">
                                $("#pie_de_pagina").limit("50",".pie_de_pagina");
                            </script>
                        </div>
                         <div class="section">
                            <label>Link</label>
                            <div><input  class="large text" type="text"  id="link" name="link" value="<?php echo $info->link; ?>" /></div>
                            
                        </div>
                        <div class="section">
                            <label>Imagen
                        </label>
                        <label>Subir nueva imagen (1000 px X 223 px)</label><br/><br/>
                            <div>
                                 <img class="cuadro_edicion_fotos" id="img1"  src="<?php echo base_url()."uploads/banner_home/new/".$info->imagen; ?>" width="450">
                           <div class="cuadro_edicion_fotos" id="divimg1"></div>
                                <br />
                                <input type="file" name="nombre" id="fileUpload1" class="fileUpload" />    
                                <input type="hidden" name="imagen" id="imagen" />
                            </div>
                        </div>
                         <div class="section">
                            <label>Tipo</label>
                            <div>
                                <select name="tipo" id="tipo">
                                    <option value="1" <?php if($info->tipo == 1){ echo 'SELECTED'; } ?>>Superior
                                    <option value="3" <?php if($info->tipo == 3){ echo 'SELECTED'; } ?>>Medio
                                    <option value="2" <?php if($info->tipo == 2){ echo 'SELECTED'; } ?>>Inferior
                                </select>
                            </div>
                            
                            
                
                    
                 
                            
                  
                    
                    <div style="width: 100px;height: 30px;position: relative;top: 30px">
                         <input type="hidden" name="seccion" id="seccion" value="<?php echo $idseccion; ?>" />
                        <input onclick="valida();" type="button" value="Guardar" class="uibutton confirm" />
                        
                    </div>
                    <?php echo form_close() ?>
                    <p>&nbsp;</p>
                 </fieldset>
                <br>
            </div>
        </div>
    </div>	
</div><!-- End content -->
<script type="text/javascript">
    function valida(){
        var titulo = $("#titulo").val();
        var texto = $("#texto").val();
        if (titulo == "" || texto == ""){
            $('#info').focus();
            showError('Complete todos los campos.',3000);
        }else{
            $('#form').submit();
        }
    }
    $(document).ready(function() {
        var texto = '';
        var nextinput = parseInt($("#inputTotal").val());
        $('#fileUpload1').uploadify({
                    'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                    'script'            : '<?php echo site_url(); ?>' + 'cms/banner_home/upload/',
                    'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                    'auto'              : true,
                    'folder'            : '',
                    'queueSizeLimit'    : 3,
                    'multi'             : false,
                    'fileExt'           : '*.jpg;*.jpeg;*.png;*gif',
                    'auto'              : true,
                    'buttonText'        : 'Cargar imagen.',
                    'onComplete'  : function(event, queueId, fileObj, response, data) {
                var responseJson = $.parseJSON(response);
                
                if (responseJson.ok === true) {
                        $("#img1").attr('src','<?php echo base_url() ?>uploads/banner_home/'+responseJson.data.file_name);
                        $("#img1").hide(800).delay(2000).show(800);
                        $("#divimg1").html('<h2>Cargando imagen...</h2>')
                        $("#divimg1").show(850).delay(2000).hide(750);
                        $("#imagen").val(responseJson.data.file_name)
                } else {
                        showError('Problemas al carga imagen.',3000);                            
                }
            }
                });   
    
    });
</script>
 <script>
    <?php if (isset($update)){
            if ($update ==  1){?>
            showSuccess('Actualización correcta.',3000);
            <?php } ?>
    <?php } ?>
</script>