<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  
<style type="text/css">
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>
<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<!-- Add mousewheel plugin (this is optional) -->	
<script type="text/javascript" src="assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>
</head>

<body>
<?php include"header.php" ?>  


<div class="box_a pad_box clearfix">
  <div class="big_box">
    	<h2 class="h2_b">Busca Apoyo</h2>
    		<div class="txt">
            <div class="clear" style="margin-bottom:30px;"></div>
			<p>Hola, está a punto de comenzar el proceso para buscar el apoyo de las personas que compartan su mismo deseo de 
            mejorar a Colombia, para convertir al país en uno más incluyente y solidario con las problematicas puntuales que lo aquejan. Como debe saber, el compromiso es muy grande, por lo que debe tenerse claro cuales son los objetivos y como se va a llevar a cabo el proyecto. </p>
            <p>Recuerde, Colombia Incluyente sirve como herramienta o canal por el cual ustedes, los que tienen proyectos que sacar adelante pueden darlos a conocer para los que pueden ayudar sepan de ellos. Todo depende de ustedes, esmerense por mostrarlo de la mejor forma posible. No olviden pasar por la sección “Escuela de proyectos”.<br />
              <br />
            </p>
            <center><img class="rounded_img" src="assets/img/054.jpg" /></center>
            <div class="clear" style="margin-top:30px;"></div>
        </div>
        
  </div>
  <a href="#inline2"  class="fancybox bt_a">Continuar</a>
</div>

<div id="inline1" style="width:550px;display:none;">
    <h4>Antes, debes saber que:<br />
      <br />
    </h4>
    <p>Lo que se publique debe ser un proyecto concreto y debe
      poder medirse el impacto y verse el resultado de manera tangible.</p>
    <p>El proyecto debe estár localizado en Colombia.</p>
    <p>El proyecto lo debe gestionar una organización debidamente registrada en la camara de comercio Colombiana, con fecha de constitución no menor a 3 años y un minimo de 3 proyectos realizados que cumplan con criterios reservados de Colombia Incluyente.</p>


    <h4>¿Dónde se encuentra el proyecto?<br />
      <br />
    </h4>
  <div class="form_pop_box">
   	  <div class="label">País</div>
        <input type="text" disabled="disabled" class="input_a" value="Colombia" />
    	<div class="label">Departamento/Región <span>*</span></div>
        <input type="text" class="input_a" />
    	<div class="label">Municipio <span>*</span></div>
        <input type="text" class="input_a" />
    </div>
    
    <div class="div_line clear" style="margin-bottom:20px; margin-top:20px;"></div>
  <input type="submit" class="bt_green" value="continuar" />
</div>



<div id="inline2" style="width:400px;display:none;">
    <h4>¿Eres Usuario de Colombia Incluyente?<br /><br /></h4>
    <p>Para postular un proyecto debes ser ususario registrado.
	Ingresa o de lo contario puedes <a href="" style="color:#0091e9">registrarte gratis.</a>
  <div class="form_pop_box"><br />
    	<div class="label">Usuario <span>*</span></div>
        <input type="text" class="input_a" />
    	<div class="label">Contraseña <span>*</span></div>
        <input type="password" class="input_a" />
    </div>
    
    <div class="div_line clear" style="margin-bottom:20px; margin-top:20px;"></div>
  	<a href="reglamentos.php"><input type="submit" class="bt_green" value="ingresa" /></a>
</div>

<?php include"footer.php" ?>  
</body>
</html>