﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author rigobcastro
 * @author Brayan Acebo
 * @author Jose Fonseca
 */
class lista extends Front_Controller
{

    public function __construct()
    {
        $this->load->model(array(
            CMSPREFIX . "productos/productos",
            CMSPREFIX . "subcategorias/subcategorias",
            CMSPREFIX . "opciones/opciones",
            CMSPREFIX . "modal/modal",
            CMSPREFIX . "footer/footer",
            CMSPREFIX . "redes/redes",
            CMSPREFIX . "textos/textos",
        ));

        //cargamos la libreria html2pdf
        $this->load->library('html2pdf');
        $this->load->library('pdf');


        $r = new Redes();
        $redes = $r->getRedesById(1);
        $this->_data['redes'] = $redes;

        $f = new Footer();
        $footer = $f->getFooterById(1);
        $this->_data['footer'] = $footer;


        $this->load->library('session');
        $this->load->library('pagination');
        parent::__construct();
    }

    // ----------------------------------------------------------------------


    public function terminos(){

        $t = new Textos();
        $texto = $t->getTextosById(1);
        $this->_data['terminos']=$texto;

        return $this->build('terminos');

    }

    public function index($idp = '')
    {

        $o = new Opciones();
        $opciones = $o->getOpciones();
        $this->_data['opciones'] = $opciones;

        $m = new Modal();
        $modal = $m->getModalById(1);
        $this->_data['modal'] = $modal;

        $b = new Productos();
        $info = $b->getProductos($idp);
        $this->_data["info"] = $info;

       $items = $b->getProductos($idp);
        $i=1;
        foreach($items as $item){
            $i++;
        }


                $pagination = 6;
                $config['uri_segment'] = 4;
                $config['base_url'] = base_url() . 'lista/index/1';
                $config['total_rows'] = $i;
                $config['per_page'] = $pagination;
                $config['num_links'] = 3;
                $config['first_link'] = '&laquo;';//primer link
                $config['last_link'] = '&raquo;';//último link
                $config['next_link'] = '&rsaquo;';
                $config['prev_link'] = '&lsaquo;';

                $this->pagination->initialize($config);

                $this->_data['results'] = $b->getProductos3($idp, $pagination, $this->uri->segment(4));

        $p = new Subcategorias();
        $subcategoria = $p->getSubcategoriasById($idp);
        $this->_data["subcategoria"] = $subcategoria;

        return $this->build('lista');
    }


    public function productos($idp = '')
    {

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        $this->load->model(array(
            CMSPREFIX . "productos/productos",
            CMSPREFIX . "categorias/categorias",
            CMSPREFIX . "opciones/opciones",
            CMSPREFIX . "modal/modal",
        ));

        $o = new Opciones();
        $opciones = $o->getOpciones();
        $this->_data['opciones'] = $opciones;
        $this->_data['tipo'] = 1;

        $m = new Modal();
        $modal = $m->getModalById(1);
        $this->_data['modal'] = $modal;

        $b = new Productos();
        $info = $b->getProductos2($idp);
        $this->_data["info"] = $info;


        $items = $b->getProductos2($idp);
        $i=1;
        foreach($items as $item){
            $i++;
        }


        $pagination = 6;
        $config['uri_segment'] = 4;
        $config['base_url'] = base_url() . 'lista/index/1';
        $config['total_rows'] = $i;
        $config['per_page'] = $pagination;
        $config['num_links'] = 3;
        $config['first_link'] = '&laquo;';//primer link
        $config['last_link'] = '&raquo;';//último link
        $config['next_link'] = '&rsaquo;';
        $config['prev_link'] = '&lsaquo;';

        $this->pagination->initialize($config);

        $this->_data['results'] = $b->getProductos4($idp, $pagination, $this->uri->segment(4));


        $p = new Categorias();
        $categoria = $p->getCategoriasById($idp);
        $this->_data["subcategoria"] = $categoria;

        return $this->build('lista');
    }

    public function datos()
    {

        $this->load->model(array(
            CMSPREFIX . "productos/productos"
        ));

        $p = new Productos();
        $datos = $p->getProductosById($_POST['id']);

        $data = array(
            'id' => $datos->id,
            'nombre' => $datos->nombre,
            'precio' => $datos->precio,
            'descripcion' => $datos->descripcion_pdf,
            'subcategoria' => $datos->cms_subcategorias_id,
            'categoria' => $datos->cms_categorias_id
        );

        echo json_encode($data);
    }

    public function carrito($modal = '')
    {

        $this->load->model(array(
            CMSPREFIX . "zonas/zonas"
        ));

        if($modal == 2){

            $this->_data['modal'] = $modal;
        }

        $z = new Zonas();
        $zonas = $z->getZonas();
        $this->_data['zonas'] = $zonas;

        return $this->build('carrito');
    }

    // ----------------------------------------------------------------------


    function add()
    {
        /*
         * Lo primero que hace este método es verificar si algún producto ya fué
         * seleccionado, si es así tomará el rowid y el qty del carrito,
         * para actualizarlo, de lo contrario se insertará.
         *
         * */
        $segment = $this->input->post('segment');
        $url = base_url() . $segment;
        //die($url);
        $id = $this->input->post('id');
        $p = new Productos();
        $product = $p->getProductosById($id);
        $option = $this->input->post('opciones');

        if ($this->input->post('opciones')) { // si el producto tiene opciones las colocamos en un arreglo

            foreach ($this->input->post('opciones') as $key => $values) {

                $value[] = $values;

            }


            //$id_option = $id.count($value); // se crea una variable como identificador único
            //$selected = count($value); // la opción seleccionada está en la posición $option
        }

        $row = '';

        if ($cart = $this->cart->contents()) { // verificamos si el carrito existe

            foreach ($cart as $item) { //foreach contenedor

                if ($item['id'] === $id && !$this->input->post('opciones')) {

                    $row = $item['rowid'] . "-" . ($item['qty'] + 1);
                    break; // si se cumple la condición el foreach dejará de ejecutarse
                }


                if ($this->cart->has_options($item['rowid'])) {


                    //$result = array_diff_assoc($this->cart->product_options($item['rowid']),$this->input->post('opciones'));

                    if ($this->cart->product_options($item['rowid']) === $this->input->post('opciones')) {
                        $result = true;
                    }
                    //foreach($this->cart->product_options($item['rowid']) as $key => $options){ // foreach interno

                    // $cart_option = $item['id'].$options;
                    //var_dump($this->cart->product_options($item['rowid']));
                    //echo "-----";
                    //var_dump($this->input->post('opciones'));
                    //echo $item['id'];
                    //echo "-".$id;
                    //die(var_dump($result));

                    if (isset($result) && $item['id'] == $id) {

                        $row = $item['rowid'] . "-" . ($item['qty'] + 1);
                        break;

                    }
                    //} // fin del foreach interno

                } // fin del if que evalua si los productos insertados en el carrtito tienen opciones

            }
            // fin del foreach contenedor

        } // fin del if que evalua si el carrito existe

        /* la variable $row contiene el rowid y el qty de cada producto concatenados; si esta
         * variable no está vacia significa que se debe actualizar el producto */

        if ($row !== '') {

            $this->update2($row, $url);

        } else {
            $insert = array(
                'id' => $id,
                'qty' => 1,
                'price' => $product->precio,
                'name' => convert_accented_characters($product->nombre) // para quitar los acentos
            );

            if ($this->input->post('opciones')) {

                $insert['options'] = $this->input->post('opciones');


            }

            //die(var_dump($insert));
            $this->cart->insert($insert);
            redirect($url, 'refresh'); // si en Windows da algún problema remplazarlo por: redirect($url, 'refresh');
        }


    } /// fin del método add

    function update2($row, $url) {

        $row=explode('-',$row);
        $this->cart->update(array(
            'rowid' => $row[0],
            'qty' => $row[1]
        ));

        redirect($url,'refresh');

    }

    function update()
    {


        $this->cart->update(array(
            'rowid' => $_POST['rowid'],
            'qty' => $_POST['valor'],
        ));

        echo $_POST['valor'];

    }

    function remove($rowid)
    {

        $this->cart->update(array(
            'rowid' => $rowid,
            'qty' => 0
        ));

        redirect('lista/carrito');

    }

    public function pdf2()
    {

        // die(base_url(). 'assets/pdf/');

        //establecemos la carpeta en la que queremos guardar los pdfs,
        //si no existen las creamos y damos permisos
        //$this->createFolder();

        //importante el slash del final o no funcionará correctamente
        if(is_dir('pdfs')){
            $this->html2pdf->folder('pdfs/');
        }else{
            die('no');
        }

        //establecemos el nombre del archivo
        $nombre = $this->input->post('nombre');
        $this->html2pdf->filename($nombre.' - cotizacion.pdf');

        //establecemos el tipo de papel
        $this->html2pdf->paper('a4', 'portrait');

        //datos que queremos enviar a la vista, lo mismo de siempre
        $this->_data['subcategorias'] = $this->datosPdfSubcategorias();
        $this->_data['categorias'] = $this->datosPdfCategorias();
        $this->_data['nombre'] = $this->input->post('nombre');
        $this->_data['correo'] = $this->input->post('correo');
        $this->_data['zona'] = $this->input->post('zona');

        //hacemos que coja la vista como datos a imprimir
        //importante utf8_decode para mostrar bien las tildes, ñ y demás
        $this->html2pdf->html(utf8_decode($this->build2('pdf')));

        //si el pdf se guarda correctamente lo mostramos en pantalla
        if($path = $this->html2pdf->create('save')) {

            $this->load->library('email');

            $this->email->from('viviana.gomez@imaginamos.com','nombre');
            //$this->email->from($this->input->post('correo'),'nombre');
            $this->email->to('waltercabezasr@gmail.com');

            $this->email->subject('Cotizacion');
            $this->email->message('Cotizacion');

            $this->email->attach($path);

            $this->email->send();

            header('location: http://repositorio.imaginamos.com.co/DG/rentamuebles/catalogo');

        }
    }

    public function downloadPdf()
    {
        //si existe el directorio
        if(is_dir("pdfs"))
        {
            //ruta completa al archivo
            $route = base_url("pdfs/test2.pdf");
            //nombre del archivo
            $filename = "test2.pdf";
            //si existe el archivo empezamos la descarga del pdf
            if(file_exists("pdfs/".$filename))
            {
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header('Content-disposition: attachment; filename='.basename($route));
                header("Content-Type: application/pdf");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length: '. filesize($route));
                readfile($route);
            }
        }
    }

    public function mail_pdf()
    {

        //establecemos la carpeta en la que queremos guardar los pdfs,
        //si no existen las creamos y damos permisos
        $this->createFolder();

        //importante el slash del final o no funcionará correctamente
        $this->html2pdf->folder('./files/pdfs/');

        //establecemos el nombre del archivo
        $this->html2pdf->filename('email_test.pdf');

        //establecemos el tipo de papel
        $this->html2pdf->paper('a4', 'portrait');

        //datos que queremos enviar a la vista, lo mismo de siempre
        $data = array(
            'title' => 'PDF Created',
            'message' => 'Hello World!'
        );
        //Load html view
        $this->html2pdf->html($this->load->view('pdf', $data, true));

        //Check that the PDF was created before we send it
        if($path = $this->html2pdf->create('save')) {

            $this->load->library('email');

            $this->email->from('wcabezas@its-solutions.net', 'Your Name');
            $this->email->from($this->input->post('correo'), 'Your Name');
            $this->email->to('waltercabezasr@gmail.com');

            $this->email->subject('Email PDF Test');
            $this->email->message('Testing the email a freshly created PDF');

            $this->email->attach($path);

            $this->email->send();

            echo "El email ha sido enviado correctamente";

        }

    }


    //esta función muestra el pdf en el navegador siempre que existan
    //tanto la carpeta como el archivo pdf
    public function show()
    {
        if(is_dir("./files/pdfs"))
        {
            $filename = "test2.pdf";
            $route = base_url("pdfs/test2.pdf");
            if(file_exists("pdfs/".$filename))
            {
                header('Content-type: application/pdf');
                readfile($route);
            }
        }
    }

    public function datosPdfSubcategorias(){

        if ($cart = $this->cart->contents()){
            $cart = $this->cart->contents();
        }else{
            $cart = array();
        };

        $data = array();
        foreach($cart as $key => $val){
            foreach($val as $item){
                if(is_array($item)){
                    if($item['subcategoria'] != 'null'){
                        $data[$item['subcategoria']][] = $val;
                    }
                }
            }
        }

        return $data;

    }

    public function datosPdfCategorias(){

        if ($cart = $this->cart->contents()){
            $cart = $this->cart->contents();
        }else{
            $cart = array();
        };

        $data = array();
        foreach($cart as $key => $val){
            foreach($val as $item){
                if(is_array($item)){
                    if($item['subcategoria'] == 'null'){
                        $data[$item['categoria']][] = $val;
                    }
                }
            }
        }

        return $data;

    }

    public function pdf() {
        ob_end_clean();
        $pdf = new Pdf();

       // set document information
       // $pdf->SetCreator(PDF_CREATOR);
        //$pdf->SetAuthor('Nicola Asuni');
        //$pdf->SetTitle('TCPDF Example 001');
        //$pdf->SetSubject('TCPDF Tutorial');
       // $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        //$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            //$pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------

// set default font subsetting mode
        $pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P', 'A3');


// set text shadow effect
        $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

        //datos que queremos enviar a la vista, lo mismo de siempre
        $this->_data['subcategorias'] = $this->datosPdfSubcategorias();
        $this->_data['categorias'] = $this->datosPdfCategorias();
        $this->_data['nombre'] = $this->input->post('nombre');
        $this->_data['correo'] = $this->input->post('correo');
        $this->_data['zona'] = $this->input->post('zona');
// Set some content to print
        $html = $this->build2('pdf');


// Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
        //$pdf->Output('Cotizacion de muebles.pdf', 'I');// CON F SE GUARDA

        $pdf->Output('pdfs/'.$this->input->post('nombre').', Cotizacion de muebles.pdf', 'F');


            $path = 'pdfs/'.$this->input->post('nombre').', Cotizacion de muebles.pdf';

            $this->load->library('email');

            $this->email->from('info@rentamueblescolombia.com','SC RENTAMUEBLES SAS');
        $this->email->to('waltercabezasr@gmail.com , '.$this->input->post('correo'));
        //$this->email->to('info@rentamueblescolombia.com, viviana.gomez@imaginamos.com , '.$this->input->post('correo'));


            $this->email->subject('Cotizacion');
            $this->email->message('Atendiendo su amable solicitud sometemos a su consideración la siguiente propuesta para la adquisición en arrendamiento por parte de ustedes, muebles y electrodomésticos los cuales relacionamos en el archivo adjunto.');

            $this->email->attach($path);

            $this->email->send();

        $this->cart->destroy();

            //header('location: http://repositorio.imaginamos.com.co/DG/rentamuebles/catalogo');
        redirect('home', 'refresh');
        }


}