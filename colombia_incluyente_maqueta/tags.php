<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<link href="assets/css/jquery.tagsinput.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/jquery.tagsinput.js"></script>
<script type="text/javascript">
	function onAddTag(tag) {
		alert("Added a tag: " + tag);
	}
	function onRemoveTag(tag) {
		alert("Removed a tag: " + tag);
	}
	
	function onChangeTag(input,tag) {
		alert("Changed a tag: " + tag);
	}
	
	$(function() {

		$('#tags_1').tagsInput({width:'auto'});
		
	});

</script>
</head>

<body> 
  <input id="tags_1" type="text" class="tags input_c"/>
</body>
</html>