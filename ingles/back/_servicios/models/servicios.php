<?php

class Servicios extends DataMapper {

    public $model = 'servicios';
    public $table = 'servicios';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getServicios(){
        return $this->get();
    }
    
    public function saveServicios($object = "") {
        $this->titulo = $object['titulo'];
        $this->texto = $object['texto'];
        $this->imagen = $object['imagen'];
        return $this->save();
    }
    
    public function getServiciosById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateServicios($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

