<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class proyectosapoyados extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $this->load->model(array(
            CMSPREFIX . "usuarios/usuarios",
            CMSPREFIX . "proyectos/proyectos",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($desde = 0) {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
        $usuario = $this->nativesession->get('usuario');
        $proyectosOBJ = new Proyectos();
        $proyectos = $proyectosOBJ->getProyectosApoyadosByUsuario($usuario['id'],$desde);
        function dameFecha($fecha,$dia)
                {   list($day,$mon,$year) = explode('/',$fecha);
                    return date('d/m/Y',mktime(0,0,0,$mon,$day+$dia,$year));        
                }
        $countpro = 0;  
        $html = '';
        
        
         foreach ($proyectos as $item){ $countpro++;
         $proyectosOBJ2 = new Proyectos();
         $ayudavoluntariado = $proyectosOBJ2->getVoluntariadoAyudaUsuario($usuario['id'],$item->id);
         $htmlvol = '';
         foreach ($ayudavoluntariado as $item2){
             $htmlvol .= '-'.$item2->tituloayuda.' '.$item2->cantidad.'<br />';
         }
              if($item->paginages != ''){ $paginav =  '<div class="dato"><b>Web: '.$item->paginages.'</b></div>'; }else{ $paginav = ''; } 
              if($item->facebookges != ''){ $facebookv = '<div class="dato"><b> '.$item->facebookges.'</b></div>'; }else{ $facebookv = '';  }
              if($item->twitterges != ''){ $twitterv = '<div class="dato"><b>Twitter: '.$item->twitterges.'</b></div>'; }else{ $twitterv = ''; }
               //validamos que ayudas tiene para apagar los botones que no tienenn ayuda
              if($item->valormonetario < 20000){ $clasemon = 'ico_desactivado'; }else{ $clasemon = ''; }
              if($item->cantidadvoluntariado == 0){ $clasevol = 'ico_desactivado'; }else{ $clasevol = ''; }
              if($item->cantidadespecie == 0){ $claseesp = 'ico_desactivado'; }else{ $claseesp = ''; }
              //fin validacion botones
        $html .= ' <div class="box_shadow2 box_margin clearfix project_ref">
  
  			<div class="over_project_ref">
            	<div class="col_ref line_right">
                	<div class="volver">Volver</div>
                        <div class="img_icon"><img src="'.base_url().'assets/img/063.png"></div>
                    <div class="spacer"></div>
                    <h2 class="h2_b">Total donado:</h2>
                    <div class="monto">$0 <span>(COP)</span></div>
                </div>
            	
                
                <div class="col_ref line_right">
                	<div class="img_icon"><img src="'.base_url().'assets/img/064.png"></div>
                	<div class="spacer"></div>
                    <h2 class="h2_b">Paquetes que he donado:</h2>
                    <div class="apoyos">
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td>Ningún paquete</td>
                            <td>0</td>
                          </tr>
                          
                          <tr class="total">
                            <td>Total:</td>
                            <td>0</td>
                          </tr>
                        </table>
                    </div>
                </div>
                
                
            	<div class="col_ref">
                	<div class="img_icon"><img src="'.base_url().'assets/img/065.png"></div>
                	<div class="spacer"></div>
                    <h2 class="h2_b">Intenciones de Voluntariado:</h2>
                    <div class="apoyos">
                    '.$htmlvol.'
                    </div>
                </div>
            	
            	
            	
            </div>
            
            
            
  
  
        	<div class="left line_right" style="height:300px">
            	<div class="pic_project" style="margin-right:8px !important">
                <a href="detalle_proyecto/index/'.$item->id.'"><img src="'.base_url().'uploads/proyectominiatura/'.$item->imagen_miniatura.'" width="226" height="145" /></a>
                </div>
                	
                <div class="div_line" style="margin-bottom:10px"></div>
				
                <div style="margin-bottom:5px"><center>Total recibido</center></div>
                <div class="barra_porcentaje2" style="margin:5px 0px 25px 10px; width:88%">
                    <div class="bar2" style="width:'.$item->porcentaje_total.'%">'.$item->porcentaje_total.'%</div>
                </div>
                <div class="estadisticas_bt">Historial</div>

            </div>
            
            <div class="right" style="padding:5px; width:250px">
            	<div class="logo_fundacion">
                    <img src="'.base_url().'uploads/proyectos/gestores/'.$item->imagenges.'" width="184" height="122" />
                </div> 
                <div class="data_fundacion">
           <div class="dato"><b>Proyecto por: <br /><a href="#inline2'.$item->id.'" class="fancybox">'.$item->nombreges.'</a></b></div>
            <div class="dato"><b>Liderado por: </b><br /><a href="#inline3'.$item->id.'" class="fancybox"> '.$item->nombreslider.' '.$item->apellidoslider.'</a> </div>
           '.$paginav.'
           '.$facebookv.'
           '.$twitterv.'
                </div>
            </div>
            
            
            <div class="right line_right" style="width:350px; height:300px; padding-right:20px">
            	<div class="des_project_int_mis_proyectos" style="height:290px; position:relative;">
            		<div class="title_my_projects left">
                	 <a href="'.base_url().'detalle_proyecto/index/'.$item->id.'">   
                             <h3 class="h3_purpure">'.$item->titulo.'</h3></a>
                	</div>
                    <div class="clear"></div>
                    <div class="txt_des">
                   '.$item->descripcion_corta.'
                    </div>
                    <div class="city">Bogotá, Colombia.</div>
                    
                    <div style="margin:10px 10px 0px 65px; position:absolute; top:160px">
                    	 <div class="percent_icon_box first_precent">
                         <div class="money_icon_a icons '.$clasemon.'"></div>
                            <div class="percent_circle '.$clasemon.'">'.$item->porcentaje_monetario.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a icons '.$clasevol.'"></div>
                            <div class="percent_circle  '.$clasevol.'">'.$item->porcentaje_voluntariado.'%</div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a icons  '.$claseesp.'"></div>
                            <div class="percent_circle  '.$claseesp.'">'.$item->porcentaje_especie.'%</div>
                        </div>
                        <div class="clear"></div>
                    </div>
					
                    <div style="position:absolute; bottom:15px;">
                    <div class="estado left" style="margin-right:10px; padding-left:10px"><div class="'.$item->clase.' left"><span>Status:</span> '.$item->nombreestado.'</div><span class="tool" title="'.$item->textotooltip.'">?</span> </div>
                    <div class="estado left"><span>Hasta:</span> '.dameFecha(date('d/m/Y'),$item->dias_para_cerrar).'</div>
                     </div>
                     
                    <div class="clear" style="margin-bottom:10px"></div>
                </div>
            </div>
            
            
            
            
        </div>';
         }
         
         
         
         
         
         
         $inline = '';
          foreach ($proyectos as $item){ 
         
              
              if($item->pagina != ''){ $pagina =  '<div class="dato"><b>Pagina: '.$item->pagina.'</b></div>'; }else{ $pagina = ''; } 
              if($item->facebook!= ''){ $facebook = '<div class="dato"><b>Facebook: '.$item->facebook.'</b></div>'; }else{ $facebook = '';  }
              if($item->twitter != ''){ $twitter = '<div class="dato"><b>Twitter: '.$item->twitter.'</b></div>'; }else{ $twitter = ''; }
              if($item->facebooklider != ''){ $facebookv =  '<div class="dato"><b>Facebook: '.$item->facebooklider.'</b></div>'; }else{ $facebookv = ''; } 
              if($item->emaillider != ''){ $emailv = '<div class="dato"><b>Email: '.$item->emaillider.'</b></div>'; }else{ $emailv = '';  }
              if($item->twitterlider != ''){ $twitterv = '<div class="dato"><b>Twitter: '.$item->twitterlider.'</b></div>'; }else{ $twitterv = ''; }
            
         $inline .= '<div id="inline2'.$item->id.'" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px; color:#1c75bc">Fundación</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;">'.$item->nombreges.'</h2>
    	<div class="spacer"></div>
    	<div class="txt_user">
            <p>'.$item->descripcionges.'</p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="'.base_url().'uploads/proyectos/gestores/'.$item->imagenges.'" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato"><b>Por: <a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$item->nombreges.'</a></b></div>
            <div class="dato"><b>Liderado por: </b> <a  href="#inline3'.$item->id.'"  class="fancybox"  style="color:#27aae1">'.$item->nombreslider.' '.$item->apellidoslider.'</a> </div>
             '.$pagina.'
           '.$facebook.'
           '.$twitter.'
           
        </div>
	</div>
    
</div>

<div id="inline3'.$item->id.'" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px"> '.$item->nombreslider.' '.$item->apellidoslider.'</h2>
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px">'.$item->nombreges.'</h3>
    
    	<div class="txt_user">
            <p>'. $item->acercadelider.'</p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="'.base_url().'uploads/proyectos/gestores/'.$item->imagenlider.'" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Información de contacto:</span></div>
             '.$emailv.'
           '.$facebookv.'
           '.$twitterv.'
        </div>
	</div>
    
</div>';
          }
         
         if($countpro == 0){
            $html .= '<div class="alert_no">
    You have not back any project.
</div>'; 
         }
        //Fin foreach  
        $proyectosOBJ3 = new Proyectos();
        $ayudascount = $proyectosOBJ3->getProyectosApoyadosByUsuarioCOUNT($usuario['id']);
        $cantidad = 0;
        foreach ($ayudascount as $item2){
             $cantidad++;
         }
        $this->_data['cantidad'] = $cantidad;
        $this->_data['desde'] = $desde;
        $this->_data['inline'] = $inline; 
        $this->_data['html'] = $html;
        return $this->build('proyectosapoyados');
    }
    
  
    
}
