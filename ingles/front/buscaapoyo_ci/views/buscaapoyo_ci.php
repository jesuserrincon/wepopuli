<style type="text/css">
#s1 {
	color:#fff;
	background-position:0px -50px;
}
.fancy-select {
	float:left;
}
#s6 {
	left:551px !important;
}
#s7 {
	left:687px !important;
}
.fixed_button_basicos {
	margin-top:1870px !important;	
}
.box_a2 .col_d .first_precent {
	margin-left:85px;
}

</style>
<script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="js/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="js/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
		$(document).ready(function() {
			
			$(".hide_time").hide(10);
                    
                        
			/*
			 *  Simple image gallery. Uses default settings
			 */

			//setTimeout(,1000);

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>
        <script type="text/javascript">
		$(document).ready(function() {
                   
                $('.hide').hide(10);          
        });
	</script>
<div class="box_a pad_box clearfix">
  <div class="big_box">
    	<h2 class="h2_b">Get Support</h2>
    		<div class="txt">
            <div class="clear" style="margin-bottom:30px;"></div>
		<?php
		$texto2 = str_replace('/colombiaincluyente/back/assets/js/kcfinder-2.51/upload/files/', base_url().'back/assets/js/kcfinder-2.51/upload/files/',$texto);
		 echo $texto2;?> <br />
            </p>
            <center><img class="rounded_img" src="img/054.jpg" /></center>
            <div class="clear" style="margin-top:30px;"></div>
        </div>
        
  </div>
  <a onclick="javascript:$('.fancybox').fancybox()" href="<?php if($usuario != ''){ echo '#inline1'; }else{ echo '#inline2'; }?>"  class="fancybox bt_a">Continuar</a>
</div>

        <div id="inline1" style="width:500px; display:none" class="hide_time">
    <div><h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">Before you continue, you must know:</h2></div>
 
    <p>Lo que se publique debe ser un proyecto concreto, debe poder medirse el impacto y verse el resultado de manera tangible.</p>
    <p>El proyecto lo debe gestionar una organización legalmente registrada, con fecha de constitución no menor a 3 años y un mínimo de 3 proyectos realizados que cumplan con los criterios de nuestros términos y condiciones.</p>

	<center>
    <h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">¿Dónde se encuentra el proyecto?</h2>
    </center>
  <div class="form_pop_box">
   	  <div class="label">Country </div>
        <select class="select" name="pais" id="pais">
			<option value="1">Colombia</option>
		</select>
          <div class="label">State/Province <span>*</span></div>
       <select class="select" name="departamento" id="departamento">
			<option value="1">Bogota D.C</option>
		</select>
    	<div class="label">Municipio/Ciudad <span>*</span></div>
        <select class="select" name="ciudad" id="ciudad">
			<option value="1">Bogota D.C</option>
		</select>
    </div>
        <input type="submit" class="bt_green" value="Continuar" style="width:255px; background-size:100% 100%" onclick="nuevoproyecto()" />
</div>



<div id="inline2" style="width:400px;display:none;">
    <h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">¿Ya es un usuario registrado?</h2>
    <p>Para postular un proyecto debe ser un usuario registrado en nuestra plataforma. Ingrese con sus datos o de lo contario puede <a href="<?php echo base_url() .'registro'?>" style="color:#0091e9">registrarse gratis.</a>
  <div class="form_pop_box"  style="margin-bottom:20px"><br />
    	<div class="label">Usuario <span>*</span></div>
        <input type="text" class="input_a" id="emaillog" style="width:236px; background-size:100% 100%" />
    	<div class="label">Password<span>*</span></div>
        <input type="password" class="input_a" id="password" onkeypress="checkKey(event);" style="margin-bottom:2px; width:236px; background-size:100% 100%" />
		<div class="clear"></div>	
        <span style="font-size:12px"><span class="requiered">*</span> * Indicates required

</span>
    </div>
        <input type="submit" class="bt_green" value="Log in" onclick="conectarse()" style="margin-left:75px; width:256px; background-size:100% 100%" />
</div>
        
        <script>
         function checkKey(key){
        var unicode;
        if (key.charCode)
        {unicode=key.charCode;}
        else
        {unicode=key.keyCode;}
        if (unicode == 13){
           conectarse();
        }
    }
    
        function conectarse(){
         var emaillog = $('#emaillog').val();
         var password = $('#password').val();
         var validacion = true;
         
         
         if(emaillog == ''){
             $('#emaillog').addClass("errorform");
             $('#emaillog').focus();
             validacion = false;
        }else{
             $('#emaillog').removeClass("errorform");
            
        }
        
        if(password == ''){
             $('#password').addClass("errorform");
             $('#password').focus();
             validacion = false;
        }else{
             $('#password').removeClass("errorform");
            
        }
        
        $.post( "<?php echo base_url(); ?>registro/login", { emaillog : emaillog , password : password})
          .done(function( data ) {
                if(data == 'Error en los datos suministrados'){
                     alert('You must enter a valid username and password.');
                }else{
                    window.location = '<?php echo base_url(); ?>buscaapoyo_ci';
                }
          });
        
        
     }
     
     function nuevoproyecto(){
         var pais = $('#pais').val();
         var departamento = $('#departamento').val();
         var ciudad = $('#ciudad').val();
         
         
          $.post( "<?php echo base_url(); ?>registro/crearproyecto", { pais : pais , departamento : departamento, ciudad : ciudad})
          .done(function( data ) {
                window.location = '<?php echo base_url(); ?>reglamento_ci/index/'+data;
          });
         
     }
       </script>
       