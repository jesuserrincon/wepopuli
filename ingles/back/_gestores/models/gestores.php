<?php

class Gestores extends DataMapper {

    public $model = 'gestor';
    public $table = 'gestores';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getGestores(){
        return $this->query('SELECT id,titulo FROM cms_gestores');
    }
    
    public function saveGestores($object = "") {
        $this->proyectos_id = $object['proyectos_id'];

        return $this->save();
    }
    
    public function getLastId(){
        $usuario = $this->query('SELECT MAX(id) AS id FROM cms_gestores');
        return $usuario->id;
    }
    
    public function getGestoresById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    
    public function getGestoresByIdProyecto($idp = 0){
         return $this->where('proyectos_id',$idp)->get();
    }
    
   
    
    public function updateGestores($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

