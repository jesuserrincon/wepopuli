<?php

class _Zonas extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "REDES SOCIALES";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Zonas();
        $info = $b->getZonas();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('zonas');
    }

    public function add() {
        $this->_data['nombremodulo'] = "ZONAS / New";
        $this->build('zonas_new');
    }

    public function new_zonas() {
        $datos = array(
            'nombre' => $this->input->post('nombre'),
            'precio' => $this->input->post('precio'),
        );

        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'zonas';
        $b = new Zonas();
        if ($datos['nombre'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'zonas_new';
            if ($b->saveZonas($datos)){
                $build = 'zonas';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getZonas();
        $this->_data["info"] = $info;
        return redirect('cms/zonas/index/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'zonas/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."zonas/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('zonas/'.$data["data"]["file_name"], array('longside'=>'zonas/new','width' => 272, 'height' => 304,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Zonas();
        $dat = $b->getZonasById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("zonas_edit");
    }
    
    public function update_zonas(){
        $b = new Zonas();
            $datos = array(
                'nombre' => $this->input->post('nombre'),
                'precio' => $this->input->post('precio'),
                'id' => $this->input->post('id')
        );

                    $update = $b->updateZonas($datos);

        if ($update){
            $a = TRUE;
        }
        return redirect("cms/zonas/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Zonas();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/zonas/index/".$return);
    }

}

