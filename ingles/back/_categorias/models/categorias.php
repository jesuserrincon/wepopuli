<?php

class Categorias extends DataMapper {

    public $model = 'categorias';
    public $table = 'categorias';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getCategorias(){
        return $this->get();
    }
    
    public function saveCategorias($object = "") {
        $this->nombre = $object['nombre'];
        $this->banner_superior = $object['banner_superior'];
        $this->banner_inferior = $object['banner_inferior'];
        $this->link_superior = $object['link_superior'];
        $this->link_inferior = $object['link_inferior'];
        return $this->save();
    }
    
    public function getCategoriasById($id = ""){
        
        return $this->get_by_id($id);
    }
    public function getNombrecategoria($id = 0){
        
        return $this->query('SELECT nombre FROM cms_categorias WHERE id = '.$id);
    }
    
    public function updateCategorias($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

