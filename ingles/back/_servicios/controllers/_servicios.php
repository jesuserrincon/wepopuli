<?php

class _Servicios extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "Servicios";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Servicios();
        $info = $b->getServicios();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('servicios');
    }

    public function add() {
        $this->_data['nombremodulo'] = "Servicios / New";
        $this->build('servicios_new');
    }

    public function new_Servicios() {
        $datos = array(
            'imagen' => $this->input->post('imagen'),
            'texto' => $this->input->post('texto'),
            'titulo' => $this->input->post('titulo'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'servicios';
        $b = new Servicios();
        if ($datos['imagen'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'servicios_new';
            if ($b->saveServicios($datos)){
                $build = 'servicios';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getServicios();
        $this->_data["info"] = $info;
        return redirect('cms/servicios/index/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'servicios/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."servicios/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('servicios/'.$data["data"]["file_name"], array('longside'=>'servicios/new','width' => 394, 'height' => 395,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Servicios();
        $dat = $b->getServiciosById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("servicios_edit");
    }
    
    public function update_Servicios(){
        $b = new Servicios();
        $datos = array(
            'id' => $this->input->post('id'),
            'titulo' => $this->input->post('titulo'),
            'texto' => $this->input->post('texto'),

        );

        $update = $b->updateServicios($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateServicios($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/servicios/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Servicios();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/servicios/index/".$return);
    }

}

