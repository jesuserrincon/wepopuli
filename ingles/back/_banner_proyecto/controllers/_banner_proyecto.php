<?php

class _Banner_proyecto extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->load->helper('url');
        $this->_data['nombremodulo'] = "BANNER";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '',$idseccion = 0) {
        $this->_data['save'] = $value;
        $b = new Banner_proyecto();
        $info = $b->getBanner_proyecto_by_seccion($idseccion);
        $this->_data["idseccion"] = $idseccion;
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('banner_proyecto');
    }

    public function add($idseccion = 0) {
        $this->_data["idseccion"] = $idseccion;
        $this->_data['nombremodulo'] = "BANNER / New";
        $this->build('banner_proyecto_new');
    }

    public function new_Banner_proyecto() {
        $datos = array(
            'imagen' => $this->input->post('imagen'),
            'titulo' => $this->input->post('titulo'),
            'texto' => $this->input->post('texto'),
            'link' => $this->input->post('link'),
            'tipo' => $this->input->post('tipo'),
            'pie_de_pagina' => $this->input->post('pie_de_pagina'),
            'seccion' => $this->input->post('seccion')
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'banner_proyecto';
        $b = new Banner_proyecto();
        if ($datos['imagen'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'banner_proyecto_new';
            if ($b->saveBanner_proyecto($datos)){
                $build = 'banner_proyecto';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getBanner_proyecto();
        $this->_data["info"] = $info;
        return redirect('cms/banner_proyecto/index/'.$save.'/'.$this->input->post('seccion'));
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'banner_proyecto/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."banner_proyecto/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

                $data = array(
                    'ok' => true,
                    'resize' => true,
                    'data' => $this->upload->data()
                );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('banner_proyecto/'.$data["data"]["file_name"], array('longside'=>'banner_proyecto/new','width' => 1000, 'height' => 223,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = "",$idseccion = 0){
        $b = new Banner_proyecto();
        $dat = $b->getBanner_proyectoById($id);
        $this->_data["idseccion"] = $idseccion;
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("banner_proyecto_edit");
    }
    
    public function update_Banner_proyecto(){
        $b = new Banner_proyecto();
        $datos = array(
            'id' => $this->input->post('id'),
            'link' => $this->input->post('link')

        );
        $update = $b->updateBanner_proyecto($datos); 
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateBanner_proyecto($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/banner_proyecto/edit/".$this->input->post("id")."/".$a.'/'.$this->input->post('idseccion'));
    }
    
    public function delete($id = "",$idseccion = 0){
        $b = new Banner_proyecto();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/banner_proyecto/index/".$return.'/'.$idseccion);
    }

}

