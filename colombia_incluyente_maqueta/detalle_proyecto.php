<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<link href="assets/css/colombia_incluyente_movil.css" rel="stylesheet" />
<?php include"metadata.php" ?>  

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->

<script type="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "09422a6d-4673-4a62-ac1c-160a92973882", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>


<body>
<?php include"header.php" ?>
<div class="clear"></div>
<div class="txt_reglamento device_300 pad_bottom_0_mobile pad_5_mobile">
  <h2 class="h2_b" style="font-size:44px">Proyecto X</h2>
  <h2 class="h2_des">Por: Fundación Píes descalzos</h2>
</div>

    
    
<div class="box_shadow_big clearfix device_320">

  <div class="col_e pad_int_20 device_300">
        	<img src="assets/img/058.jpg" class="rounded_img device_300" width="590" />
        
        	<div class="share clearfix">
            	<div class="device_300">
                    <div class="left" style="width:90px">
                        <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35&amp;appId=236778076359723" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>                
                    </div>
                    <div class="left">
                        <span class='st_facebook'></span>
                        <span class='st_twitter'></span>
                        <span class='st_googleplus'></span>
                        <span class='st_email'></span>
                        <span class='st_sharethis'></span>
                    </div>
                </div>
                
            	<div class="device_300">
                    <div class="days_left">
                        <h2 style="color:#a01f80; font-size:17px">26 Dias restantes</h2>
                    </div>
                </div>
            </div>
        
        	<div class="description_project">
        	  <h3 class="h3_black" style="font-size:23px; margin-bottom:10px;">DESCRIPCIÓN DEL PROYECTO</h3>
        	  <p>Gray House interweaves documentary and narrative footage into a film structure that blurs the lines between fiction and reality. Through an investigation of five politically- and socially-charged environments—as well as the histories and current events that shape them—Gray House forms a narrative of conjured meanings and associations.</p>
             <div class="spacer"></div>
              <h3 class="h3_purpure">OBJETIVOS Y METAS</h3>

        	  <p>The film begins on East Matagorda Bay, where a bait fisherman trawls for diurnal brown shrimp off the Texas Coast. The fisherman goes about his daily routine, casting and re-casting his net and sorting what he pulls out of the water. This repetitive task becomes a visual meditation on his primary relationship with the land.ng recovery method, commonly referred to as 'fracking'.<br />
      	    </p>
        	  <div class="spacer"></div>
              <h3 class="h3_purpure">COMO PIENSO LLEVARLO A CABO</h3>
        	  <p>The film begins on East Matagorda Bay, where a bait fisherman trawls for diurnal brown shrimp off the Texas Coast. The fisherman goes about his daily routine, casting and re-casting his net and sorting what he pulls out of the water. This repetitive task becomes a visual meditation on his primary relationship with the land.ng recovery method, commonly referred to as 'fracking'.<br />

				<div class="spacer"></div>
        	  <h3 class="h3_purpure">RIESGOS Y DESAFIOS</h3>
        	  <p>The film begins on East Matagorda Bay, where a bait fisherman trawls for diurnal brown shrimp off the Texas Coast. The fisherman goes about his daily routine, casting and re-casting his net and sorting what he pulls out of the water. This repetitive task becomes a visual meditation on his primary relationship with the land.ng recovery method, commonly referred to as 'fracking'.<br />
			<div class="spacer"></div>
        	  <h3 class="h3_purpure">PREGUNTAS Y RESPUESTAS</h3>
        	  <p>The film begins on East Matagorda Bay, where a bait fisherman trawls for diurnal brown shrimp off the Texas Coast. The fisherman goes about his daily routine, casting and re-casting his net and sorting what he pulls out of the water. This repetitive task becomes a visual meditation on his primary relationship with the land.ng recovery method, commonly referred to as 'fracking'.<br />
	  </div>
            <div class="spacer"></div>
          <h3 class="h3_purpure">VIDEO</h3>
            <iframe class="device_300" width="590" height="315" src="//www.youtube.com/embed/va87iu9hm4Y?rel=0" frameborder="0" allowfullscreen></iframe>            
 			
            <div class="spacer"></div>
            
            <iframe class="device_300" src="//player.vimeo.com/video/55751501" width="590" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <div class="spacer"></div>
          <h3 class="h3_purpure">Otras Imagenes</h3>
            <div class="spacer"></div>
            <img src="assets/img/058.jpg" class="rounded_img device_300" width="590" />
            <div class="spacer"></div>
            <img src="assets/img/058.jpg" class="rounded_img device_300" width="590" />
            <div class="spacer"></div>
        	<img src="assets/img/058.jpg" class="rounded_img device_300" width="590" />

			<div class="tags_project">
            	Tags: Educación, colegios, niños
            </div>


  </div>
    <div class="col_f device_320">
    	
        <div class="logo_fundacion">
        	<img src="assets/img/060.jpg" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato"><b>Por:</b> Fundación Píes descalzos</div>
            <div class="dato"><b>Liderado por:</b> <a href="#inline2"  class="fancybox"  style="color:#27aae1">Rodrigo Romero  Castro</a></div>
            <div class="dato"><b>www.website1.com</div>
            <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
            <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
            <div class="dato"><b>Bogotá, Colombia</div>
        </div>
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>
        
        <div class="state box_min_sh" style="padding:13px 0px">
        	<div class="estado left"><div class="active_point left"><span>Status:</span> Activo</div></div>
            <div class="clear" style="margin-bottom:5px;"></div>
        	<div class="estado"><span>Hasta:</span> 20/08/2013</div>
        </div>
        
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Porcentaje total</center></div>
            <div class="barra_porcentaje2">
                <div class="bar2" style="width:25%">25%</div>
            </div>

		</div>


        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <a href="apoya_dinero.php" class="link_apoya">
        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon"><img src="assets/img/063.png" /></div>
            <div class="tit">Apoya Monetariamente</div>
            <div class="barra_porcentaje2" style="margin:5px auto 10px auto">
                <div class="bar2" style="width:45%">45%</div>
            </div>
		</div>
		</a>

        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

        <a href="apoya_especie.php"  class="fancybox link_apoya">
        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon"><img src="assets/img/064.png" /></div>
            <div class="tit">Apoya en Especie</div>
            <div class="barra_porcentaje2" style="margin:5px auto 10px auto">
                <div class="bar2" style="width:45%">45%</div>
            </div>
		</div>
		</a>

        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>


        <a href="apoya_voluntariados.php" class="link_apoya">
        <div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon"><img src="assets/img/065.png" /></div>
            <div class="tit">Apoya Voluntariado</div>
            <div class="barra_porcentaje2" style="margin:5px auto 10px auto">
                <div class="bar2" style="width:45%">45%</div>
            </div>
		</div>
        </a>
    	<div class="spacer visible_mobile"></div>

    </div>
    
    
    <div class="div_line clear dis_none_movil"></div>
    
    <div class="pad_int_20 device_300">
    
		    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'wepopuli'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    
    </div>

</div>

<script type="text/javascript" src="assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="assets/js/aplication.js"></script>

<?php include"footer.php" ?> 

<!-- //popup -->
<div id="inline2" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px">Lider</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;">Rodrigo Romero Castro</h2>
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px">Fundación pies descalzos</h3>
    
    	<div class="txt_user">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent purus turpis, tincidunt in malesuada ut, mattis eget purus. Vivamus semper fringilla nunc, iaculis ultricies nibh scelerisque eget. Nam tellus orci, elementum et erat at, sollicitudin ultrices mauris. Maecenas id purus at nisi eleifend dignissim. Duis non mi id nisi venenatis ornare. Donec suscipit enim eget mauris pharetra, venenatis commodo elit congue. Maecenas non eleifend libero, non interdum nisi. Nunc a lorem ornare, volutpat nisi sed, molestie eros. Nam in interdum libero. Donec hendrerit vitae dui ut imperdiet.</p>
            <p>Vestibulum tempus id ante a eleifend. Morbi at elementum erat. Nam in nulla ac ante fermentum ultrices quis nec sem. Donec ultricies in est et dignissim. Donec sollicitudin tortor eget mauris pharetra, a congue metus feugiat. Integer velit risus, hendrerit et pulvinar eget, mollis a odio. Donec sed sodales nisi. Aenean accumsan tellus in est pretium euismod. Praesent eu risus eros. Donec pulvinar, eros vel tincidunt sagittis, eros velit tempus diam, et feugiat elit enim at augue.</p>
            <p>Sed adipiscing nisi ac justo suscipit lobortis. Suspendisse potenti. Integer a ullamcorper orci. Curabitur eget suscipit metus, et aliquam est. Praesent eget leo sed purus tempus eleifend sit amet a mauris. Suspendisse lacus dolor, rhoncus sed hendrerit at, laoreet eu turpis. Aliquam massa libero, pellentesque eget risus vel, vehicula tristique ipsum.</p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="assets/img/080.png" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Contactame en:</span></div>
            <div class="dato"><b>Email:</b> <a href="mailto:info@miemail.es" style="color:#602483"> info@romerorodrigo.com</a></div>
            <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
            <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
            <div class="dato"><b>Bogotá, Colombia</div>
        </div>
	</div>
    
</div>
 

 
</body>
</html>