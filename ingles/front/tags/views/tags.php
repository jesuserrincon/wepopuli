<style type="text/css">
#s1 {
    color:#fff;
    background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -100px;
}
</style>
<link href="css/jquery-filestyle.css" rel="stylesheet" />
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.sticky.js"></script>
</script>  
<script>
$(function() {
	$(".datepicker").datepicker({ minDate: +2, maxDate: "+1M +10D" });
	});
	function imagenbox(){      
}
</script>  



<div class="paso_paso clearfix">
    <a href="<?php echo base_url(); ?>reglamento/index/<?php echo $proyectoid; ?>" id="s1" class="step">Rules</a>
    <a href="<?php echo base_url(); ?>basicos/index/<?php echo $proyectoid; ?>" id="s2" class="step">Basics</a>
    <a href="<?php echo base_url(); ?>historia/index/<?php echo $proyectoid; ?>" id="s3" class="step"> Story </a>
    <a href="<?php echo base_url(); ?>gestores_front/index/<?php echo $proyectoid; ?>" id="s4" class="step">Gestores</a>
    <a href="<?php echo base_url(); ?>cuenta/index/<?php echo $proyectoid; ?>" id="s5" class="step">Cuenta</a>
    <a href="<?php echo base_url(); ?>revision/index/<?php echo $proyectoid; ?>" id="s6" class="step">Revisión</a>
    <a href="<?php echo base_url(); ?>previsualizar/index/<?php echo $proyectoid; ?>" id="s7" class="step">Previsualizar</a>
</div>
<div class="txt_reglamento">
    <h2 class="h2_b">Start building your project</h2>
    It's time to start describing your project. The more clear it is the higher the chances to get the support you are looking for.
</div>

<div class="box_a pad_box clearfix">
    <div id="cargados">
        
    </div>
    <div class="col_c">
        <div class="box_shadow box_c">
            <h6>Project image <span>*</span> </h6>
            <div class="left" style="width:470px">
                <a href="javascript:imagenbox()">imagen box</a> Choose an image from your computer. • JPEG, PNG, GIF, or BMP • 2MB file limit. • At least 590 x 396 pixels. 
            </div>
            <div class="clear"></div>
            <div class="relative">
                <input type="hidden" name="imagen_miniatura" id="imagen_miniatura" value="<?php echo $proyecto->imagen_miniatura;?>" /> 
               <input id="archivos" type="file" name="archivos[]" multiple="multiple" />
            </div>
        </div>
        <div class="div_green clear"></div>
        <div class="box_shadow box_c">
            <h6>Project title <span>*</span> </h6>
            <p>Your project title should be simple, specific and easy to remember. Please avoid using terms like "help", "support" or "donate".</p>
            <input type="text" id="titulo" name="titulo" value="<?php echo $proyecto->titulo;?>" onkeyup="cambiartexto()" class="input_c" style="margin-bottom:10px"  /><br />Maximum characters 
           <span id="titulo-text">0/60</span>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c">
            <h6>Category<span>*</span> </h6>
            <select class="select" name="categoria" id="categoria" onchange="cambiarcategoria()">
        <option value="0">Choose a Category</option>
                <?php foreach ($categorias as $item){?>
            <option value="<?php echo $item->id;?>" <?php if($proyecto->categorias_id == $item->id){ echo 'SELECTED'; }?>><?php echo $item->nombre;?></option>
        <?php } ?>
            </select><br />
            <div class="clear"></div><br />
            Choose the category that better suits your project.
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        <div class="box_shadow box_c">
            <h6>Tags<span></span> </h6>
            <!--input id="tags_2" name="tags" type="text" value="<?php echo $proyecto->tags_busqueda;?>"  class="tags input_c"/-->
            <input id="tags_1" type="text" class="tags input_c"/>
           
            <p>Choose the category that better suits your project.</p>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>

        <div class="box_shadow box_c">
            <h6>Short Description <span>*</span> </h6>
            <p>If you had to describe your project in one sentence, how would you do it?</p>
            <textarea class="texarea_1" name="descripcion" id="descripcion" onkeyup="cambiartexto_descripcion()" ><?php echo $proyecto->descripcion_corta;?></textarea>
            
            <br />Maximum characters <span id="descripcion-text">0/135</span>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Project Location <span>*</span> </h6>
            <p>Where it will be held?</p>
            <div class="col_int_box_c left">
                <div class="label">Country </div>
                <input type="text" class="input_a" />
                <div class="label">Municipio</div>
                <input type="text" class="input_a" />
            </div>
            <div class="col_int_box_c right">
                <div class="label">State/Province</div>
                <input type="text" class="input_a" />
                <div class="label">Dirección (Si aplica)</div>
                <input type="text" class="input_a" />
            </div>
            <div class="clear"></div>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Tiempo de Publicación <span>*</span> </h6>
            <p>Recomendamos que los proyectos estén publicados 30 días o menos. Tiempos de publicación cortos tienen tasas de éxito más altas, y ayudarán a crear un sentido de urgencia que ayudará a conseguir más apoyo para el proyecto. </p>
            <b>Number of days</b><br />
            <select class="select" id="diasfaltantes" name="diasfaltantes" onchange="cambiarfecha()">
                <?php for($i=30;$i<61;$i++){?>
                <option value="<?php echo $i; ?>" <?php if($proyecto->dias_para_cerrar == $i){ echo 'SELECTED'; }?>><?php echo $i; ?>
                <?php } ?>
            </select>
            <div class="clear"></div>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Types of support<span>*</span> </h6>
            <p>Fill in this type of support if your project requires volunteers to improve/build a building work, general help, virtual and face-to-face work to spread the project, etc.
</p>  