<?php

class Rentamuebles extends DataMapper {

    public $model = 'rentamuebles';
    public $table = 'rentamuebles';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getRentamuebles(){
        return $this->get();
    }

    public function getRentamuebles2(){
        return $this->get();
}
    
    public function saveRentamuebles($object = "") {
        $this->titulomision = $object['titulomision'];
        $this->mision = $object['mision'];
        $this->titulovision = $object['titulovision'];
        $this->mision = $object['vision'];
        //$this->titulohistotia = $object['titulohistotia'];
        //$this->mision = $object['historia'];
        //$this->titulohistotia = $object['titulotexto'];
        $this->texto = $object['texto'];
        return $this->save();
    }
    
    public function getRentamueblesById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateRentamuebles($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

