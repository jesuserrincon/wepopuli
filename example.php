<?php
/* file-upload-2-mysql.php */

/* Init variables */
$messageOut = array();

/* Check for form post. */
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

/* Process file upload */
$maxFileSize = 50000; /* 50 kb. */
$filterAllowedTypes = array('txt', 'dat');
$fileExtension = end(explode(".", strtolower($_FILES['file']['name'])));
$fileSize = $_FILES['file']['size'];

if (($fileSize < $maxFileSize) and
in_array($fileExtension, $filterAllowedTypes)) {

if ($_FILES['file']['error'] > 0) {

$messageOut[] = "<p class=\"alert\">Error: {$_FILES['file']['error']}</p>";

} else {

$fileName = $_FILES['file']['name'];
$tmpName = $_FILES['file']['tmp_name'];
$content = file_get_contents($tmpName);

if ( ! $content) {

$messageOut[] = "<p class=\"pass\">ERROR: Could not read file content.</p>";

} else {
/* Connect to database and store file */
include_once 'db-config.php';
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PWD, DB_NAME);

if ($mysqli->connect_errno) {

$messageOut[] = "<p class=\"alert\">Error: Could nor connect to database.";

} else {

$query = sprintf("INSERT INTO file_uploads (file_name,
file_ext, file_size, file_content)
VALUES('%s','%s','%d','%s')",
$mysqli->real_escape_string($fileName),
$mysqli->real_escape_string($fileExtension),
$mysqli->real_escape_string($fileSize),
$mysqli->real_escape_string($content));

$result = $mysqli->query($query);

if(! $result) {

$messageOut[] = "<p class=\"alert\">Error: Database query failed.";

} else {

$fileSize = number_format($fileSize / 1024, 2); /* convert to kb */

/* Show user what was uploaded */
$content = str_replace(PHP_EOL, '<br />', htmlentities($content));
$messageOut[] = "<p class=\"pass\">Upload Successful: <br />
<br />File Uploaded: {$fileName}
({$fileSize} kb)<br /> <br />
{$content} 
</p>";

}

}
}
}

} else {

$messageOut[] = "<p class=\"alert\">Error: Invalid file";

}
}

$messageOut[] = "<p>Upload a file.</p>";
?>

<html>
<head>
<title>File Upload to MySQL</title>
<style type="text/css">
.alert {border: 2px solid red; background-color: yellow;}
.pass {border: 2px solid black; background-color: lightgreen;}
</style>
</head>
<body>

<?php
foreach ($messageOut as $value) echo("{$value}");
?>

<form method="POST" action="" enctype="multipart/form-data">
<input type="file" name="file" size="20">
<input type="submit" name="cmdSave" value="Upload">
</form> 
</body>
</html>