<?php

class Cuentas extends DataMapper {

    public $model = 'cuentas';
    public $table = 'cuenta_bancaria';

    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getCuentas(){
        return $this->get();
    }

    public function getCuentaProyectosById($idp){

       return $this->where('proyectos_id',$idp)->get();

    }

    public function saveCuentas($object = "") {
        $this->proyectos_id = $object['proyectos_id'];

        return $this->save();
    }
    
    public function getLastId(){
        $usuario = $this->query('SELECT MAX(id) AS id FROM cms_cuentas');
        return $usuario->id;
    }
    
    public function getCuentasById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateCuentas($datos = ""){
       return $this->where('proyectos_id',$datos["proyectos_id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

