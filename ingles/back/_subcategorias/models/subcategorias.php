<?php

class Subcategorias extends DataMapper {

    public $model = 'subcategorias';
    public $table = 'subcategorias';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getSubcategorias($idc){
        $this->where('cms_categorias_id',$idc);
        return $this->get();
    }
    
    public function saveSubcategorias($object = "") {
        $this->nombre = $object['nombre'];
        $this->texto = $object['texto'];
        $this->imagen = $object['imagen'];
        $this->cms_categorias_id = $object['cms_categorias_id'];
        return $this->save();
    }
    
    public function getSubcategoriasById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateSubcategorias($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

