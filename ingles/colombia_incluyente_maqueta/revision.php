<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  
<style type="text/css">
#s6 {
	color:#fff;
	background-position:0px -100px;
}
#s5 {
	color:#fff;
	background-position:0px -50px;
}
#s4 {
	color:#fff;
	background-position:0px -50px;
}
#s3 {
	color:#fff;
	background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -50px;
}
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>

<link href="assets/css/jquery-filestyle.css" rel="stylesheet" />
  	
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
</head>

<body>
<?php include"header.php" ?>  
<?php include"paso_paso.php" ?>

<div class="txt_reglamento">
  <h2 class="h2_b">Revisión</h2>
</div>

<div class="box_a  clearfix">
    
    
    <div class="col_c">
    	<div class="box_shadow box_c">
    	  <p>Antes de enviar el proyecto asegúrese de:</p>
    	  	<ul class="list_2">
            	<li>Haber explicado con claridad porqué el proyecto necesita apoyo.</li>
    	    	<li>Haber agregado un video del proyecto. Es la mejor forma de conectarse con las personas que posiblemente lo apoyarán.</li>
    	    	<li>Haber revisado el proyecto y tener la retroalimentación por parte de su equipo.</li>
    	    	<li>Haber visto otros proyectos en el portal y haber apoyado alguno, para vislumbrar de primera mano la experiencia.</li>
    	    	<li>Haberte asegurado de la viabilidad del proyecto en todos sus aspectos.</li>
    	  	</ul>
          <p>Luego de enviar el proyecto:</p>
          
    	  	<ul class="list_2">
    	  		<li>Una vez se haya completado el proceso anterior y se haya enviado a revisión el proyecto, este será revisado para asegurar que cumpla con las Guías de Proyectos de Ci. </li>
    	    	<li>Luego de unos días, enviaremos un mensaje sobre el status del mismo. Si ha sido aprobado, se puede lanzar en cualquier momento.</li>
    	    	<li>Colombia incluyente se reserva el total derecho de admisión u omisión de los proyectos.</li>
    		</ul>
        </div>
    </div>
    <!-- // columna C -->
    
    <div class="col_d mar_right">
        <div class="project_box sticker">
            <div class="imgLiquidFill imgLiquid liquid_a">
                <a href="#"  target="_blank">
                    <img class="thumbnail_a" src="assets/img/024.jpg" />
                </a>
            </div>
            <div class="title">Por un país mejor <br />Colombia</div>
            <div class="des">
             Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
            </div>
            
            <div class="resumen">
                <ul class="data_project_min"> 
                    <li class="by">Fundación pies descalsos</li>
                    <li class="by">Rio Magdalena, Colombia </li>
                    <li class="by">6 dias faltantes</li>
                </ul>
            </div>
            
            <div class="line_div_project"></div>
            
            <div class="percent_icon_box first_precent">
                <div class="money_icon_a icons"></div>
                <div class="percent_circle">100%</div>
            </div>
            <div class="percent_icon_box">
                <div class="members_icon_a icons"></div>
                <div class="percent_circle">50%</div>
            </div>
            <div class="percent_icon_box">
                <div class="shop_icon_a icons"></div>
                <div class="percent_circle">20%</div>
            </div>
            <div class="clear"></div>
            
            
            <div class="barra_porcentaje">
                <div class="bar" style="width:45%">45%</div>
            </div>
            
        </div><!-- // project box -->
    </div>
</div>
<script type="text/javascript" src="assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="assets/js/aplication.js"></script>

<?php include"footer.php" ?>  
</body>
</html>