<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5466aa1714710eae" async="async"></script>

<div class="back_top">
    <div class="header">
    	<a href="<?php echo base_url(); ?>home" id="logo"></a>
        <div class="social_header_links" style="padding-right:0px; width:400px">
        	<!--TWITTER
            <a href="https://twitter.com/weopopuli" class="twitter-follow-button" data-show-count="false" data-lang="eng">Seguir a @colombiaincluyente</a>
		 	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        	
            <!--FACEBOOK
            <div class="right" style="width:90px">
				<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35&amp;appId=236778076359723" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>                
			</div>
             -->
             
            <!-- 5 de marzo 2015 -->
            <a href="https://www.youtube.com/user/wepopuli" target="_blank" class="right"><img src="img/r1.png" width="43" /></a>
            <a href="https://twitter.com/wepopuli" target="_blank" class="right"><img src="img/r2.png" width="43"  /></a>
            <a href="https://facebook.com/wepopuli" target="_blank" class="right"><img src="img/r3.png" width="43"  /></a>
            <div class="right" style="margin-top:10px; margin-right:10px">Follow</div>
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <div class="addthis_horizontal_follow_toolbox"></div>
        </div>
    </div>
</div>
<div class="div_line"></div>
<div class="buttons clearfix device_320">

	<div class="device_320 left mr_mobile">
        <a href="<?php echo base_url(); ?>seccion/index/3" class="button_a">About Us</a>
        <div class="div_buttons"></div>
        <a href="<?php echo base_url(); ?>seccion/index/4" class="button_a">How it works</a>
        <div class="div_buttons"></div>
        <a href="<?php echo base_url(); ?>seccion/index/5" class="button_a">FAQ</a>
    </div>
    <div class="div_buttons dis_none_movil"></div>
    
    <div class="under_line h_mobile clearfix device_320">
            <div class="search_box device_300 device_500">
                <form id="formulariobusqueda" action="<?php echo base_url() ?>resultados_busqueda/index/0/2" method="post">
                <input type="submit" class="go" onclick="buscar()" value="" title="Search" />
                <input type="text" id="palabrabuscar" name="palabrabuscar" class="search" placeholder="Search for projects keywords" onkeypress="keydown(event,'funcion')"  />
                </form>
            </div>
            <div class="clear"></div>
    </div>
    

</div>

 <?php if(isset($_SESSION['usuario'])){
           ?>
            <div class="relative b1000">
	
        <div class="profile">
            <a href="#" class="name_id"><?php echo $_SESSION['usuario']['nombres']; ?></a>
            <div class="menu_profile">
                <div class="arrow_blue"></div>
                     <?php if(isset($_SESSION['usuario_admin'])){
               ?>
                <a href="<?php echo base_url(); ?>cms">Back to CMS</a>
                     <?php }?>
                <!--a href="<?php echo base_url(); ?>misproyectos">My projects</a-->
                <a href="<?php echo base_url(); ?>proyectosapoyados">Backed Projects</a>
                <a href="<?php echo base_url(); ?>usuario">My Profile</a>
                <a href="<?php echo base_url(); ?>registro/logout">Log out</a>
            </div>
        </div>
   
</div> 

           <?php }else {?>
<div class="relative b1000">
    <div class="profile">
            <a href="<?php echo base_url(); ?>registro">Log in/Sign up</a><a href="<?php echo base_url(); ?>registro"></a>
    </div>        
</div>
                          

           <?php } ?>
           
           
<script>
function validaEmailNews(email) { 
    var re  = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/; 
    if (!re.test(email)) { 
        return false; 
    } 
    return true; 
    }

function suscribirse(){
        var email = $('#emailsuscrito').val();
        if(validaEmailNews(email)){
            
            $.post( "<?php echo base_url(); ?>registro/suscribirsenewsletter", { email : email })
            .done(function( data ) {
                if(data == 0){
                     alert('This email address is already subscribed to our newsletter.');
                     return false;
                }
                if(data == 1){
                     alert('Thank you for subscribing to our newsletter.');
                     $('.fancybox-close').click();
                     return false;
                }
            });
            
        }else{
             alert ("This E-mail address is not valid."); 
            return false;
        }
        
    }
    
     function keydown(e,s){
    if (!e) var e = window.event;
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    if (code==13){
        buscar();	
    }
    }
    
    function buscar(){
        var palabrabuscar = $('#palabrabuscar').val();
        if(palabrabuscar == ''){
            return false;
        }else{
            $('#formulariobusqueda').submit();
        }
        
    }
    
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('.over_project_ref').hide(1);


  $('.volver').click(function() {
    $(this).parent().parent().slideToggle(400);
    return false;
  });
  
  $('.estadisticas_bt').click(function() {
    $(this).parent().parent().find('.over_project_ref').slideToggle(400);
    return false;
  });
  

});
</script>

