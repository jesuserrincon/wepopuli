<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class jobs extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $this->load->model(array(
            CMSPREFIX . "usuarios/usuarios",
            CMSPREFIX . "proyectos/proyectos",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index() {
        
        
        //Cargamos todos los proyectos con estado 3 CERRADOS y empezamos a descontar los dias que se vera cerrado
        $proyectosOBJ2 = new Proyectos();
        $proyectoscerrados = $proyectosOBJ2->getProyectosByEstadoCron(3);
        //recorremos el objeto obtenido
        foreach ($proyectoscerrados as $item) {
            //validamos si esta en 0 dejamos el proyecto quieto de lo contrario descontamos un dia
           if ($item->dias_cerrado == 0) {
                 //Cambiamos el estado a despublicado 
                $datos = array(
                    'id' => $item->id,
                    'estados_id' => 8,
                );
            }else{
                //descontamos un dia 
                $datos = array(
                    'id' => $item->id,
                    'dias_cerrado' => $item->dias_cerrado-1,
                );
            }
            $proyectosOBJ2->updateProyectos($datos);
        }
        
        $proyectosOBJ = new Proyectos();
        //Cargamos todos los proyectos con estado 1 APROBADOS
        $proyectos = $proyectosOBJ->getProyectosByEstadoCron(1);
        //recorremos el objeto obtenido
        foreach ($proyectos as $item) {
            $u = new Usuarios();

            //Si el total de dias por cerrar es igual a cero cambiamos el estado a cerrado 3
            if ($item->dias_para_cerrar == 0) {
                $usuario = $u->getUsuariosById($item->usuarios_id);

                $email = $usuario->email;
                $asunto = 'Cierre proyecto';
                $titulo = 'proyecto: '.$item->titulo;
                $mensaje = 'proyecto '.$item->titulo.' cerrará hoy.  ';

                $this->enviarCorreo($email,$asunto,$titulo,$mensaje);

            }

            if ($item->dias_para_cerrar == 2) {

                $usuario = $u->getUsuariosById($item->usuarios_id);

                $email = $usuario->email;
                $asunto = 'Cierre proyecto';
                $titulo = 'proyecto: '.$item->titulo;
                $mensaje = 'proyecto '.$item->titulo.' cerrará en 3 días.  ';

                $this->enviarCorreo($email,$asunto,$titulo,$mensaje);

            }

            if ($item->dias_para_cerrar == -1) {
                $datos = array(
                    'id' => $item->id,
                    'estados_id' => 3,
                );
            } else {
                //Si los dias es mayor a 0 descontamos un dia
                $datos = array(
                    'id' => $item->id,
                    'dias_para_cerrar' => $item->dias_para_cerrar - 1,
                );
            }
            //acemos UPDATE con los datos obtenidos por el if anterior
            $proyectosOBJ->updateProyectos($datos);
        }
    }

}
