<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>  
<style type="text/css">
#s1 {
	color:#fff;
	background-position:0px -50px;
}
</style>
</head>

<body>
<?php include"header.php" ?>  

<?php include"paso_paso.php" ?>  
<div class="txt_reglamento">
    Lea cuidadosamente los requisitos que debe cumplir un proyecto para que sea publicado, así como los términos y condiciones<br /> de uso de Ci.
</div>

<div class="box_a pad_box clearfix">
  <div class="box_shadow big_box">
    	<h2 class="h2_b">Requisitos para los Proyectos</h2>
    		<div class="txt">
            <h4>Todo en Colombia Incluyente debe ser un proyecto.</h4>
            <p>Un proyecto es algo que tiene un propósito social claro, como construir una cancha de fútbol, proporcionar dispositivos tecnológicos en pro de la educación, o construir/reparar los pisos de los hogares de una comunidad de escasos recursos. Un proyecto será culminado eventualmente, y tendrá un resultado final. Todos los proyectos deben estar localizados en Colombia y deben tener objetivos claros que permitan medir su impacto.</p>
            
            <p>Cada proyecto en Colombia Incluyente debe ajustarse a alguna de nuestras categorías
            Nuestras categorías son…</p>
            
            <h4>¿Son estas las únicas condiciones?</h4>
            <p>Esas son las dos principales, pero se debe continuar leyendo para conocer los Usos Específicos de Colombia Incluyente que no están permitidos. Colombia Incluyente hace una revisión exhaustiva de los proyectos para asegurarse de que cumplan todos los términos y condiciones.</p>
            
            <h4>¿Quiénes pueden crear un proyecto en Ci?</h4>
            <p>La creación de proyectos está abierta por el momento en Colombia. Los gestores del proyecto deben ser ciudadanos colombianos o residentes extranjeros en Colombia, mayores de 18 años, vinculados a sociedades legalmente constituidas, fundaciones u organizaciones sin ánimo de lucro.</p>
            
            <h4>¿Qué no está permitido?</h4>
            <ul class="list">
            	<li>Ci no puede ser utilizado para recaudar fondos para causas, sean de organizaciones benéficas o personas. Sólo es permitido recaudar fondos para proyectos sociales específicos, que cumplan con los requerimientos explicados anteriormente.</li>
           		<li>Ci no puede ser usado para vender acciones o solicitar préstamos.</li>
            	<li>Los gestores del proyecto no pueden donar a otras causas parte del apoyo obtenido. El apoyo obtenido debe emplearse completamente en el proyecto.</li>
            	<li>Ci no puede ser usado para fundar negocios, tiendas virtuales, redes sociales o aplicaciones.</li>
        		<li>Ci no puede ser usado para comprar propiedades. </li>
     		</ul>
            <div class="clear" style="margin-bottom:30px;"></div>
            <h2 class="h2_b">Términos y Condiciones</h2>
            <h4>Obligaciones de Ci</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. </p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. </p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. </p>
            <div class="clear" style="margin-top:40px;"></div>
            <label class="label_inputs"><input class="checkbox-style" type="checkbox"  /> Acepto Términos y condiciones</label>
        </div>  
        <div class="div_line" style="margin:20px -20px 20px -20px;"></div>
 		<center><a href="basicos.php"><input type="submit" class="boton_a" /></a></center>
     </div>
</div>
<script type="text/javascript">
	$('.checkbox-style, .radio-style').customRadioCheck();
</script>
<?php include"footer.php" ?>  
</body>
</html>