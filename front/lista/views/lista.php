
	<style type="text/css">#nav-bt2 {color: #464646;} #nav-bt2 span {background-position:-70px -60px;}</style>

  <a class="check-bt" href="<?php echo base_url()?>lista/carrito">
      <?php if($total_items=$this->cart->total_items()) {?>
      <h1> <?php echo $total_items?> Item(s)</h1>
      <?php }else{?>
          <h1> 0 Item(s)</h1>
      <?php } ?>
    <p>Check out</p>
  </a>
  <section>
    <div class="con-section">
      <div class="mg-section cfx">
        <h1 class="int-tt cfx"><?php echo $subcategoria->nombre ?></h1>
        <!--Paginador-->
          <div class="con-pagina cfx">
              <?=$this->pagination->create_links();?>
          </div>
        <!--Fin paginador-->
        <div class="con-cats">


          <!--Item lista-->
            <?php foreach($results as $item){ ?>

        	<div class="item-list">
                <form name="form<?php echo $item->id ?>" method="post" action="<?php echo base_url() ?>lista/add" class="modal-form" >
                <?=form_hidden('segment', $this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3));?>
                <input type="hidden" name="id" value="<?php echo $item->id ?>">
                <input type="hidden" name="precio" value="<?php echo $item->precio ?>">
                 <?php if(!empty($item->descripcion_pdf)){ ?>
                        <input type="hidden" name="opciones[descripcion]" value="<?php echo $item->descripcion_pdf ?>">
                    <?php } ?>
                <input type="hidden" name="nombre" value="<?php echo $item->nombre ?>">
                <input type="hidden" name="opciones[categoria]" value="<?php echo $item->cms_categorias_id ?>">
                <input type="hidden" name="opciones[subcategoria]" value="<?php echo $item->cms_subcategorias_id ?>">

                    <h1><?php echo $item->nombre ?></h1>
            <img class="tr" src="<?php echo base_url() ?>uploads/productos/new/<?php echo $item->imagen ?>" height="128" width="240" alt="">
            <p><?php echo $item->texto ?></p>
            <div class="con-bts-list">
            	<?php if(isset($tipo)){ ?>
                <a class="bt-list-car tr" href="javascript:modal(<?php echo $item->id ?>)">Agregar al<span class="bt-list-t1 fr"></span></a>
                    <a class="bt-list-more tr" href="<?php echo base_url() ?>detalles/index/<?php echo $item->id ?>/1">Ver<span class="bt-list-t2 fr"></span></a>
            	<?php }else{ ?>
                    <a class="bt-list-car tr" onclick="document.forms.form<?php echo $item->id ?>.submit()" href="#">Agregar al<span class="bt-list-t1 fr"></span></a>
                    <a class="bt-list-more tr" href="<?php echo base_url() ?>detalles/index/<?php echo $item->id ?>">Ver<span class="bt-list-t2 fr"></span></a>
                <?php } ?>



            </div>
                <a class="bt-list-car modal-box tr" id="test" href="#modal-agrega"></a>
                </form>
          </div>


            <?php } ?>

        </div>
        <!--Paginador-->
        <div class="con-pagina cfx">
            <?=$this->pagination->create_links();?>
        	<!--<a class="pag-tx tr" href="#">&laquo;</a>
          <a class="pag-tx tr" href="#">&lsaquo;</a>
          <a>...</a>
          <a class="pag-num tr" href="#">98</a>
          <a class="pag-num pag-act tr" href="#">99</a>
          <a class="pag-num tr" href="#">100</a>
          <a>...</a>
          <a class="pag-tx tr" href="#">&rsaquo;</a>
          <a class="pag-tx tr" href="#">&raquo;</a>-->
        </div>
        <!--Fin paginador-->
        <div class="con-back-bt"><a class="back-bt tr" href="javascript:history.back()"><span class="tr">«</span>Volver</a></div>
      </div>
    </div>
  </section>
  
  <!--Modal-box-->
  <div class="con-modal" id="modal-agrega">
    <div class="modal-info">
      <h2><?php echo $modal->titulo ?></h2>
      <p><?php echo $modal->texto ?></p>
      <form name="datos" action="<?php echo base_url() ?>lista/add" class="modal-form" method="post">
          <?=form_hidden('segment', $this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3));?>
          <?php
          $i=1;
          foreach($opciones as $item){
              ?>
      	<fieldset>
          <input id="<?php echo $item->id ?>" name='opciones[]' type="checkbox" value="<?php echo $item->precio ?>-<?php echo $item->nombre ?>">
          <label for="<?php echo $item->id ?>"><?php echo $item->nombre ?></label>
        </fieldset>
        <?php
          $i++;
          }
          ?>
        <div class="test2">

        </div>
      <div class="con-bts-list">
        <a class="bt-list-no close tr">No gracias<span class="bt-list-t3 fr"></span></a>
        <a onclick="document.forms.datos.submit()" class="bt-list-car tr" href="#" style="border-left:2px solid #fff; border-right:none; left:inherit; right:0;">Agregar al<span class="bt-list-t1 fr"></span></a>
      </div>
      </form>
    </div>
    <button class="close-modal-sc close">×</button>
  </div>

  <!--Fin modal-box-->
<script type="text/javascript">
    function modal(id){
        $.ajax({
            url: "<?php echo base_url()?>lista/datos",
            type: "post",
            dataType: "text",
            data: 'id='+id
        }).done(function (data) {

                var datos = $.parseJSON(data);
                var item = '<input type="hidden" name="id" value="'+datos["id"]+'">';
                    item += '<input type="hidden" name="nombre" value="'+datos["nombre"]+'">';
                    item += '<input type="hidden" name="precio" value="'+datos["precio"]+'">';
                    item += '<input type="hidden" name="opciones[subcategoria]" value="'+datos["subcategoria"]+'">';
                    item += '<input type="hidden" name="opciones[categoria]" value="'+datos["categoria"]+'">';

                if(datos["categoria"] != ''){
                    item += '<input type="hidden" name="opciones[descripcion]" value="'+datos["descripcion"]+'">';
                }

                $('.test2').html(item);
                $('#test').click()

            })
    }
</script>