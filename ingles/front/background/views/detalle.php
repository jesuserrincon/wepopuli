<section class="contenido-bg-negro clearfix" >
    <article class="wrap">
        <a class="btn_back" href="javascript:window.history.back();"><?php if($this->session->userdata('lang') == 1){ echo 'Volver';}else{ echo 'Back';} ?></a>        
        <!--Horizontal Tab-->
        <div id="horizontalTab">
            <ul class="resp-tabs-list">
               <?php echo $menu; ?>
            </ul>
            <div class="resp-tabs-container clearfix">
                <div class="content_camp" >
                    <div class="clearfix">
                        <div class="left_serv">
                            <?php if($cant > 0){ ?>
                                <div class="content_slide_serv content_slide_camp">
                                    <div class="flexslider_3 carousel ">
                                        <ul class="slides">
                                           <?php echo $imagenes; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="right_ant">
                            <h3><?php echo $detalle->titulo; ?></h3>
                            <p><?php echo nl2br($detalle->texto); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>