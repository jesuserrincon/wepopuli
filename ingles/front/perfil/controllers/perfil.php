<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class perfil extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."textos/textos",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($idpro = 0) {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        
        $usuario = $this->nativesession->get('usuario');
        $usuarioOBJ = new Usuarios();
        $this->_data["usuario"] =  $usuarioOBJ->getUsuariosById($usuario['id']);
        $this->_data['clave'] = $usuario['id'];
        $this->_data["idpro"] = $idpro;
        return $this->build('perfil');
    }
    
    


    // ----------------------------------------------------------------------


}
