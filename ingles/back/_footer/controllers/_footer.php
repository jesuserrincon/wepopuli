<?php

class _Footer extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "INFORMACIÓN FOOTER";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Footer();
        $info = $b->getFooter();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('footer');
    }

    public function add() {
        $this->_data['nombremodulo'] = "REDES / New";
        $this->build('footer_new');
    }

    public function new_Footer() {
        $datos = array(
            'direccion' => $this->input->post('direccion'),
            'ciudad' => $this->input->post('ciudad'),
            'telefono1' => $this->input->post('telefono1'),
            'telefono2' => $this->input->post('telefono2'),
            'pbx' => $this->input->post('pbx'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'footer';
        $b = new Footer();
        if ($datos['imagen'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'footer_new';
            if ($b->saveFooter($datos)){
                $build = 'footer';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getFooter();
        $this->_data["info"] = $info;
        return redirect('cms/footer/index/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'footer/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."footer/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('footer/'.$data["data"]["file_name"], array('longside'=>'footer/new','width' => 86, 'height' => 186,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Footer();
        $dat = $b->getFooterById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("footer_edit");
    }
    
    public function update_footer(){
        $b = new Footer();
        $datos = array(
            'direccion' => $this->input->post('direccion'),
            'ciudad' => $this->input->post('ciudad'),
            'telefono1' => $this->input->post('telefono1'),
            'telefono2' => $this->input->post('telefono2'),
            'pbx' => $this->input->post('pbx'),
            'correo' => $this->input->post('correo'),
            'id' => $this->input->post('id')
        );

              $update = $b->updateFooter($datos);

        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
                'logocort' => $this->input->post('imagen'),
                'id' => $this->input->post('id')
            );
            $update = $b->updateFooter($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/footer/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Footer();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/footer/index/".$return);
    }

}

