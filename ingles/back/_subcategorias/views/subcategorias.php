<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo $nombremodulo ?></span>
    </div><!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div style="position: absolute;float: left;height: 60px;">
                    <?php 
                    //if ($count < 10){
                        echo anchor("cms/subcategorias/add/", "Nueva SubCategoria", 'class="uibutton icon add" style="top: 100%; border-radius: 10px;"') ;

                    //}
                    ?>
                    </div>
                    <p>&nbsp;</p>
                    <div class="tableName toolbar">
                        <table class="display data_table2" >
                            <thead>
                                <tr>  
                                    <th><div class='th_wrapp'>Nombre</div></th>
                                    <th><div class='th_wrapp'> Texto</div></th>
                                    <th><div class='th_wrapp'> Imagen</div></th>
                                    <th><div class='th_wrapp'> Productos </div></th>
                                    <th><div class='th_wrapp'> Acciones</div></th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($info as $d){ ?>
                                <tr class="odd gradeX" > 

                                    <td width="25%" class="center"><?php echo $d->nombre ?></td>
                                    <td width="25%" class="center"><?php echo substr($d->texto,0,50) ?></td>
                                    <td width="25%" class="center"><div ><img class="cuadro_edicion_fotos"  src="<?php echo base_url()."uploads/subcategorias/".$d->imagen?>" width="100" ></div></td>
                                    <td width="25%" class="center">
                                        <?php echo anchor("cms/productos/lista/".$d->id, "Productos", "class='uibutton'"); ?>
                                    </td>
                                    <td width="25%" class="center">
                                        <?php echo anchor("cms/subcategorias/edit/".$d->id, "Editar", "class='uibutton'"); ?> 
                                        <?php echo anchor("cms/subcategorias/delete/".$d->id, "Eliminar", "class='uibutton  special'"); ?>                                        
                                    </td> 
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>	
</div>
 <script>
     function valida(){
        var titulo = $("#titulo").val();
        var texto = $("#texto").val();
        if (titulo == "" || texto == "" ){
                $('#info').focus();
                showError('Complete todos los campos.',3000);
            }else{
                $('#form').submit();
            }
   }  
    <?php if (isset($save)){
            if ($save==1){?>
            showSuccess('Acción correcta.',3000);
            <?php } ?>
    <?php } ?>
</script>