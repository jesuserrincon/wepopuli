<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class seccion extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($seccionid = 0) {
        $this->load->model(array(
            CMSPREFIX . "secciones/secciones",
            CMSPREFIX . "banner/banner",
        ));

        $banner = new Banner();
        $bannerSuperior = $banner->getBannerSuperiorBySeccion($seccionid);

        $banner = new Banner();
        $bannerInferior = $banner->getBannerInferiorBySeccion($seccionid);

        $seccionesOBJ = new Secciones();
         $this->_data["texto"] = $seccionesOBJ->getSeccionesById($seccionid);
         $this->_data["bannerSuperior"] = $bannerSuperior;
         $this->_data["bannerInferior"] = $bannerInferior;

        return $this->build('seccion');
    }

}
