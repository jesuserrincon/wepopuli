<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Back_Controller extends CMS_Controller {
    
    

    public function __construct() {

        // Si admin area esta definido y es verdadero, 
        // correr el condicional de admin area
        if (isset($this->admin_area)) {
            if (true === $this->admin_area)
                $this->admin_area();
        }
        
       
        
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    /**
     * Build mejorado del Back
     * 
     * @param string $view
     * @param type $data
     * @return type
     */
    protected function build($view = null, $data = array()) {

        if (empty($view)) {
            $view = 'body';
        }

        // Definiendo variables del back
        $data['menu_panel'] = $this->_main_menu();

        // Is superadmin?
        $data['is_superadmin'] = $this->is_superadmin();

        $data = array_merge($data, $this->_data);

        return $this->template
                    ->set_partial('menu_administrators', ADMINPATH . 'partials/menu/administrator')
                    ->set_partial('menu_panel', ADMINPATH . 'partials/menu/panel')
                    ->set_partial('menu_icons', ADMINPATH . 'partials/menu/icons')
                    ->set_layout(ADMINPATH . 'layouts/general')
                    ->build($view, $data);
    }

    // ----------------------------------------------------------------------

    protected function add_asset_module($asset = array(), $module = false) {
        return parent::add_asset_module($asset, $module, BACKPATH);
    }

    // ----------------------------------------------------------------------

    private function _main_menu() {
        $menu = array();

        // Items por defecto
        $menu[] = array(
            'title' => 'Dashboard',
            'url' => 'cms/dashboard',
            'icon' => 'home'
        );

        
        // Si es super administrador agregar el boton de administracion general
        if (true === $this->is_superadmin() && false === $this->_superadmin_area) {
            $menu[] = array(
                'title' => 'Administración',
                'url' => 'cms/admin/dashboard',
                'icon' => 'home'
            );
        } elseif (true === $this->is_superadmin() && true === $this->_superadmin_area) {
            $menu[] = array(
                'title' => 'Administradores',
                'url' => 'cms/admin/administradores',
                'icon' => 'home'
            );
            $menu[] = array(
                'title' => 'Menús',
                'url' => 'cms/admin/menus',
                'icon' => 'home'
            );
        }

        if (false === $this->_superadmin_area) {
            // Cargando modelo del menu
            $this->load->model(ADMINPATH . 'menu');

            $datos_menu = new Menu();

            if ($datos_menu->get()->exists()) {
                foreach ($datos_menu->all as $item) {
                    $menu[] = array(
                        'title' => $item->title,
                        'url' => $item->url,
                        'icon' => $item->icon
                    );
                }
            }
        }
        
        $this->_data['menu_superadmin'] = $this->_superadmin_area;

        return $menu;
    }

    // ----------------------------------------------------------------------

    public function enviarCorreo($email,$asunto,$titulo,$mensaje){

        $this->load->library('email');
        $this->load->library('encrypt');

        $this->email->from('info@colombiaincluyente.com', 'Colombia incluyente');
        $this->email->to($email);
        $this->email->subject($asunto);

        $message = $this->vistaEmail($titulo, $mensaje);

        $this->email->message($message);
        if(!$this->email->send()){
            die('error al enviar el mensaje');
        }
    }
    public function vistaEmail($titulo,$mensaje){

        $vista = '<div class="deviceWidth_b">
        <!-- Wrapper -->
  <table class="deviceWidth_b" width="682" bgcolor="#f0f0f0" style="font-family:Arial, Helvetica, sans-serif; color:#707070; " align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
		 <thead>
         	<tr>
            	<td>
                	<img src="http://3holograms.com.co/colombiaincluyente/mail/img/005.png" width="682" height="80" alt="Colombia Incluyente Logo">
                </td>
            </tr>
         </thead>
        </table>

        <table class="deviceWidth_b" width="682" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; color:#707070" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
        	<tbody>
        	<tr>
            	<td style="padding-left:20px; padding-top:20px">
                	<div style="font-size:20px;">'.$titulo.'</div>
                </td>
            </tr>

        </tbody></table>

        <table class="deviceWidth_b" width="682" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif; color:#707070" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
        	<tbody><tr>
				<td style="padding:20px">
                    '.$mensaje.'
                </td>
			</tr>
        </tbody></table>



        <table class="deviceWidth_b" width="682" bgcolor="#3e3e3e" style="font-family:Arial, Helvetica, sans-serif; color:#707070" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
		 <tfoot>
         	<tr>
            	<td valign="middle" style="height:74px">
                <img src="http://3holograms.com.co/colombiaincluyente/mail/img/001.png" width="478" height="74" alt="Ir a la pagina" style="float:left;">

                <a href="#"><img src="http://3holograms.com.co/colombiaincluyente/mail/img/002.png" style="float:right; margin-top:10px; margin-left:15px; margin-right:20px" alt="facebook"></a>
                <a href="#"><img src="http://3holograms.com.co/colombiaincluyente/mail/img/003.png" style="float:right; margin-top:10px; margin-left:15px;" alt="twitter"></a>
                <a href="#"><img src="http://3holograms.com.co/colombiaincluyente/mail/img/004.png" style="float:right; margin-top:10px; margin-left:15px;" alt="youtube"></a>
                </td>
            </tr>
         </tfoot>
        </table>
</div>';

        return $vista;

    }
}