<?php

class Banner extends DataMapper {

    public $model = 'banner';
    public $table = 'banner';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getBanner(){
        return $this->get();
    }
    
    public function saveBanner($object = "") {
        $this->link = $object['link'];
        $this->texto = $object['texto'];
        $this->imagen = $object['imagen'];
        $this->titulo = $object['titulo'];
        $this->pie_de_pagina = $object['pie_de_pagina'];
        $this->seccion = $object['seccion'];
        $this->tipo = $object['tipo'];
        return $this->save();
    }
    
    public function getBannerById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateBanner($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

    public function getBannerBySeccion($idSeccion){

        return $this->query("select * from cms_banner where seccion =".$idSeccion);

    }

    public function getBannerSuperiorBySeccion($idSeccion){

        return $this->query("select * from cms_banner where seccion =".$idSeccion." AND tipo = 1");

    }

    public function getBannerInferiorBySeccion($idSeccion){

        return $this->query("select * from cms_banner where seccion =".$idSeccion." AND tipo = 2");

    }

}

