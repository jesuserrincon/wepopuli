<style type="text/css">
#s1 {
	color:#fff;
	background-position:0px -100px;
}
#s6 {
	left:551px !important;
}
#s7 {
	left:687px !important;
}
.box_a2 .col_d .first_precent {
	margin-left:85px;
}
</style>

<div class="paso_paso clearfix">
    <a href="#" id="s1" class="step">Reglamentos</a>
     <?php if($proyecto->checkterminos == 1){  ?>
    <a href="<?php echo base_url(); ?>reglamento_ci/index/<?php echo $idpro; ?>" id="s1" class="step">Reglamentos</a>
    <a href="<?php echo base_url(); ?>basicos_ci/index/<?php echo $idpro; ?>" id="s2" class="step">Básicos</a>
    <a href="<?php echo base_url(); ?>historia_ci/index/<?php echo $idpro; ?>" id="s3" class="step">Historia</a>
    <a href="<?php echo base_url(); ?>gestores_front_ci/index/<?php echo $idpro; ?>" id="s4" class="step">Gestores</a>
    <a href="<?php echo base_url(); ?>revision_ci/index/<?php echo $idpro; ?>" id="s6" class="step">Revisión</a>
    <a href="<?php echo base_url(); ?>previsualizar_ci/index/<?php echo $idpro; ?>" id="s7" class="step">Previsualizar</a>
      
   <?php  }else{ ?>
    <a href="#" onclick="javascript:validarpasos()" id="s2" class="step">Básicos</a>
    <a href="#" onclick="javascript:validarpasos()" id="s3" class="step">Historia</a>
    <a href="#" onclick="javascript:validarpasos()" id="s4" class="step">Gestores</a>
    <a href="#" onclick="javascript:validarpasos()" id="s6" class="step">Revisión</a>
    <a href="#" onclick="javascript:validarpasos()" id="s7" class="step">Previsualizar</a>
   <?php } ?>
</div>

<div class="txt_reglamento">
    Lea cuidadosamente los requisitos que debe cumplir un proyecto para que sea publicado, así como los términos y condiciones de uso de nuestra plataforma.
</div>

<div class="box_a pad_box clearfix">
  <div class="box_shadow big_box">
    	<h2 class="h2_b">Requisitos para los Proyectos</h2>
    	
		<p style="font-family:'Roboto', sans-serif"><?php $texto2 = str_replace('/colombiaincluyente/back/assets/js/kcfinder-2.51/upload/files/', base_url().'back/assets/js/kcfinder-2.51/upload/files/',$texto);
         echo $texto2; ?></p>
        <div class="clear" style="margin-top:40px;"></div>

        <div class="div_line" style="margin:20px -20px 20px -20px;"></div>
            <label class="label_inputs " style="color:#75796f; line-height:15px">
            <input class="checkbox-style" id="checkterminos" type="checkbox" <?php if($proyecto->checkterminos == 1){ echo 'CHECKED disabled="true" ';} ?>  />
            	Acepto 
            </label>Términos y Condiciones
        	<div class="clear"></div>
        </div>  
        <div class="" style="margin:20px -20px 20px -20px;"></div>
        <center><input value="Siguiente" type="submit" class="boton_a" onclick="validarterminos()" /></center>
     </div>
</div>
<script type="text/javascript">
        function validarterminos(){
            var marcado = $("#checkterminos").prop("checked") ? true : false;
            if(marcado == true){
                $.post( "<?php echo base_url().'reglamento/checkproyecto';?>", { idp: <?php echo $idpro; ?> })
                  .done(function( data ) {
                        window.location = '<?php echo base_url(); ?>basicos_ci/index/<?php echo $idpro; ?>';
                  });
                
            }else{
                alert('Para continuar, debe aceptar nuestros términos y condiciones.');
            }
        }
	$('.checkbox-style, .radio-style').customRadioCheck();
        
        function validarpasos(){
            alert('Debe aceptar los términos y condiciones para poder continuar');
            return false;
        }
</script>