<?php
/**
 * Created by JetBrains PhpStorm.
 * User: WASCARO
 * Date: 23/02/14
 * Time: 01:11 PM
 * To change this template use File | Settings | File Templates.
 */

if ( ! defined('BASEPATH') )
    exit( 'No direct script access allowed' );

class Nativesession
{
    public function __construct()
    {
        session_start();
    }

    public function set( $key, $value )
    {
        $_SESSION[$key] = $value;
    }

    public function get( $key )
    {
        return isset( $_SESSION[$key] ) ? $_SESSION[$key] : null;
    }

    public function regenerateId( $delOld = false )
    {
        session_regenerate_id( $delOld );
    }

    public function sessionDestroy(){
        session_destroy();
    }

    public function delete( $key )
    {
        unset( $_SESSION[$key] );
    }
}