<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class gestores_front extends Front_Controller {

    public function __construct() {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."gestores/gestores",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0, $tipo = '') {
        $this->set_title('Bienvenidos a ' . SITENAME, true);

        $this->_data["id"] = $proyectoid;

        parent::index($proyectoid);
        $this->_data["proyectoid"] = $proyectoid;

        $vista = $this->build2('../../home/views/detalleProyectoCompleto');
        $this->_data["vista"] = $vista;

        if($tipo == 1){
            $this->_data["tipo"] = $tipo;
        }

        return $this->build('gestores');
    }

    public function add(){

        $b = new Gestores();
        $cargaimagen = 0;
        if(isset($_FILES['archivo1']['name']) && !empty($_FILES['archivo1']['name'])){

            $config['upload_path'] = UPLOADSFOLDER.'proyectos/gestores';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|rgb|psd|pdf|doc|docx';
            $config['remove_spaces']=TRUE;
            $config['max_size']	= '13000';
            $config['encrypt_name']	= true;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('archivo1'))
            {
                die($this->upload->display_errors());
            }
            else
            {
                $data = array(
                    'data' => $this->upload->data()
                );

                $datos1 = array(
                    'nit' => $data["data"]["file_name"],
                    'id' => $this->input->post('id')
                );
                $b1 = new Gestores();
                $update = $b1->updateGestores($datos1);

            }
            $cargaimagen++;
        }

        if(isset($_FILES['archivo']['name']) && !empty($_FILES['archivo']['name'])){
            $config['upload_path'] = UPLOADSFOLDER.'proyectos/gestores';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|rgb|psd|pdf|doc|docx';
            $config['remove_spaces']=TRUE;
            $config['max_size']	= '13000';
            $config['encrypt_name']	= true;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('archivo'))
            {
                die($this->upload->display_errors());
            }
            else
            {
                $data = array(
                    'data' => $this->upload->data()
                );

                $datos2 = array(
                    'certificado' => $data["data"]["file_name"],
                    'id' => $this->input->post('id')
                );
                $b2 = new Gestores();
                $update = $b2->updateGestores($datos2);

            }
            $cargaimagen++;
        }

        if(isset($_FILES['archivos']['name']) && !empty($_FILES['archivos']['name'])){

            $config['upload_path'] = UPLOADSFOLDER.'proyectos/gestores';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|rgb|psd|pdf|doc|docx';
            $config['remove_spaces']=TRUE;
            $config['max_size']	= '13000';
            $config['encrypt_name']	= true;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('archivos'))
            {
                die($this->upload->display_errors());
            }
            else
            {
                $data = array(
                    'data' => $this->upload->data()
                );

                $datos3 = array(
                    'imagen' => $data["data"]["file_name"],
                    'id' => $this->input->post('id')
                );

                $b3 = new Gestores();
                $update = $b3->updateGestores($datos3);

            }
            $cargaimagen++;
        }

        if(isset($_FILES['archivos2']['name']) && !empty($_FILES['archivos2']['name'])){
            $config['upload_path'] = UPLOADSFOLDER.'proyectos/gestores';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|rgb|psd|pdf|doc|docx';
            $config['remove_spaces']=TRUE;
            $config['max_size']	= '13000';
            $config['encrypt_name']	= true;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('archivos2'))
            {
                die($this->upload->display_errors());
            }
            else
            {
                $data = array(
                    'data' => $this->upload->data()
                );

                $datos4 = array(
                    'liderimagen' => $data["data"]["file_name"],
                    'id' => $this->input->post('id')
                );
                $b4 = new Gestores();
                $update = $b4->updateGestores($datos4);

            }
            $cargaimagen++;
        }

         $datos = array(
            'nombre' => $this->input->post('nombre'),
            //'imagen' => $this->input->post('imagen'),
            'descripcion' => $this->input->post('descripcion'),
            'telefono' => $this->input->post('telefono'),
            'email' => $this->input->post('email'),
            'twitter' => $this->input->post('twitter'),
            'direccion' => $this->input->post('direccion'),
            'telefono2' => $this->input->post('telefono2'),
            'pagina' => $this->input->post('pagina'),
            'facebook' => $this->input->post('facebook'),
            'acercadelider' => $this->input->post('acercadelider'),
            'nombreslider' => $this->input->post('nombreslider'),
            'apellidoslider' => $this->input->post('apellidoslider'),
            'cedulalider' => $this->input->post('cedulalider'),
            'telefonolider' => $this->input->post('telefonolider'),
            'emaillider' => $this->input->post('emaillider'),
            'facebooklider' => $this->input->post('facebooklider'),
            'twitterlider' => $this->input->post('twitterlider'),
            'telefono2lider' => $this->input->post('telefono2lider'),
            //'liderimagen' => $this->input->post('liderimagen'),
            'id' => $this->input->post('id')
        );

        $update = $b->updateGestores($datos);
        
        if($this->input->post('redirec') == 2){
            if($cargaimagen > 0){
                return redirect("cuenta/index/".$this->input->post('proyectoid')."/1");
            }else{
                echo $this->input->post('redirec');
            }
            
        }else{
            return redirect("gestores_front/index/".$this->input->post('proyectoid')."/1");
        }
        

    }
    
    public function uploadimg(){
          $ruta= "uploads/proyectos/gestores/";
          foreach ($_FILES as $key) {
            if($key['error'] == UPLOAD_ERR_OK ){//Verificamos si se subio correctamente
              $nombre = $key['name'];//Obtenemos el nombre del archivo
              $temporal = $key['tmp_name']; //Obtenemos el nombre del archivo temporal
              $tamano= ($key['size'] / 1000)."Kb"; //Obtenemos el tamaño en KB
              move_uploaded_file($temporal, $ruta . $nombre); //Movemos el archivo temporal a la ruta especificada
              //El echo es para que lo reciba jquery y lo ponga en el div "cargados"
              echo $nombre;
            }else{
              echo $key['error']; //Si no se cargo mostramos el error
            }
          }
    }
    
    
}
