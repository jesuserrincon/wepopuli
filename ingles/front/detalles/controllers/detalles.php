<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author rigobcastro
 * @author Brayan Acebo
 * @author Jose Fonseca
 */
class detalles extends Front_Controller {

    public function __construct() {
        $this->load->library('session');

        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($idp = '') {

        $this->set_title('Bienvenidos a ' . SITENAME, true);
        $this->load->model(array(
            CMSPREFIX."productos/productos",
            CMSPREFIX."detalle/detalle",
            CMSPREFIX."coloresproducto/coloresproducto",
            CMSPREFIX."colores/colores",
            CMSPREFIX."footer/footer",
            CMSPREFIX."redes/redes"
        ));

        $r = new Redes();
        $redes = $r->getRedesById(1);
        $this->_data['redes'] = $redes;

        $f = new Footer();
        $footer = $f->getFooterById(1);
        $this->_data['footer'] = $footer;

        $co = new Colores();
        $colores = $co->getColores();
        $colores2 = array();
        foreach($colores as $co){
            $colores2[$co->id] = $co->color;
        }
        $this->_data['colores'] = $colores2;


        $c = new Coloresproducto();
        $color = $c->getColoresproducto2($idp);
        $this->_data['color']= $color;

        $p = new Detalle();

        $datosDetalle = '';
        foreach($color as $datos){

            $detalle = $p->getDetalle2($idp , $datos->cms_colores_id);

            $datosDetalle .= '<div class="con-gal-grl ver-color-'.$datos->cms_colores_id.'">
                    <div class="gal-img-b cfx">';
            $a=1;
                foreach($detalle as $item){
                    $datosDetalle .='<img class="big-gal ver-foto-'.$a++.' fl" src="'.base_url().'uploads/detalle/new/'.$item->imagen.'" alt="">';

                }
            $datosDetalle .='</div>
                    <div class="gal-minis">';
            $a=1;
            foreach($detalle as $item){
                $datosDetalle .='<img class="mini-gal mini-act" data-id="ver-foto-'.$a++.'" src="'.base_url().'uploads/detalle/new/'.$item->imagen.'" alt="">';

            }
            $datosDetalle.='</div>
                </div>';
        }

        $this->_data['datoss'] = $datosDetalle;

        //$info = $p->getDetalle($idcp);
        //die(var_dump($imagenes));

        $b = new Productos();
        $producto = $b->getProductosById($idp);
        $this->_data["producto"] = $producto;


        return $this->build('detalle');
    }

    // ----------------------------------------------------------------------


}
