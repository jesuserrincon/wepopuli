<div class="back_top">
    <div class="header">
    	<a href="<?php echo base_url(); ?>home" id="logo"></a>
        <div class="social_header_links">
        	<!--TWITTER-->
            <a href="https://twitter.com/colombiaincluyente" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @colombiaincluyente</a>
		 	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        	
            <!--FACEBOOK-->
            <fb:follow href="https://www.facebook.com/pages/Colombia-incluyente/1455517308010333" colorscheme="light" layout="button_count" show_faces="true"></fb:follow>
      	    <div id="fb-root"></div>
        </div>
    </div>
</div>
<div class="div_line"></div>
<div class="buttons clearfix device_320">

	<div class="device_320 left mr_mobile">
        <a href="<?php echo base_url(); ?>seccion/index/3" class="button_a">¿Quiénes somos? </a>
        <div class="div_buttons"></div>
        <a href="<?php echo base_url(); ?>seccion/index/4" class="button_a">¿Cómo funciona?</a>
        <div class="div_buttons"></div>
        <a href="<?php echo base_url(); ?>seccion/index/5" class="button_a">Preguntas Frecuentes</a>
    </div>
    <div class="div_buttons dis_none_movil"></div>
    
    <div class="under_line h_mobile clearfix device_320">
            <div class="search_box device_300 device_500">
                <form id="formulariobusqueda" action="<?php echo base_url() ?>resultados_busqueda/index/0/2" method="post">
                <input type="submit" class="go" onclick="buscar()" value="" title="Buscar" />
                <input type="text" id="palabrabuscar" name="palabrabuscar" class="search" placeholder="Buscar" onkeypress="keydown(event,'funcion')"  />
                </form>
            </div>
            <div class="clear"></div>
    </div>
</div>
 <?php if(isset($_SESSION['usuario'])){
           ?>
            <div class="relative b1000">
	
        <div class="profile">
            <a href="#" class="name_id"><?php echo $_SESSION['usuario']['nombres']; ?></a>
            <div class="menu_profile">
                <div class="arrow_blue"></div>
                     <?php if(isset($_SESSION['usuario_admin'])){
               ?>
                <a href="<?php echo base_url(); ?>cms">Volver al CMS</a>
                     <?php }?>
                <a href="<?php echo base_url(); ?>misproyectos">Mis proyectos</a>
                <a href="<?php echo base_url(); ?>proyectosapoyados">Proyectos apoyados</a>
                <a href="<?php echo base_url(); ?>usuario">Datos de usuario</a>
                <a href="<?php echo base_url(); ?>registro/logout">Salir</a>
            </div>
        </div>
   
</div>
            
           <?php }else {?>
<div class="relative b1000 device_320">
    <div class="profile">
            <a href="<?php echo base_url(); ?>registro">Ingreso/Registro</a><a href="<?php echo base_url(); ?>registro"></a>
    </div>        
</div>
           <?php } ?>
<script>
    
     function keydown(e,s){
    if (!e) var e = window.event;
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    if (code==13){
        buscar();	
    }
    }
    
    function buscar(){
        var palabrabuscar = $('#palabrabuscar').val();
        if(palabrabuscar == ''){
            return false;
        }else{
            $('#formulariobusqueda').submit();
        }
        
    }
    
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('.over_project_ref').hide(1);


  $('.volver').click(function() {
    $(this).parent().parent().slideToggle(400);
    return false;
  });
  
  $('.estadisticas_bt').click(function() {
    $(this).parent().parent().find('.over_project_ref').slideToggle(400);
    return false;
  });
  

});
</script>

