
	<style type="text/css">#nav-bt3 {color: #464646;} #nav-bt3 span {background-position:-92px -60px;}</style>


  <section>
    <div class="con-section">
      <div class="mg-section cfx">
        <h1 class="int-tt cfx">Servicios</h1>

        <!--Item servicio-->
          <?php foreach($info as $item){ ?>
        <div class="con-service cfx">
        	<div class="service-info fl">
          	<h2><?php echo $item->titulo ?></h2>
            <img class="fl" src="<?php echo base_url() ?>uploads/servicios/new/<?php echo $item->imagen ?>" alt="">
          	<p align="justify">
                <?php echo nl2br(substr($item->texto,0,650)) ?>
          	</p>
            <div class="con-bts-list">
              <a class="bt-list-more tr" href="<?php base_url() ?>servicio/detalle/<?php echo $item->id ?>">Ver<span class="bt-list-t2 fr"></span></a>
            </div>
          </div>
        </div>
          <?php } ?>
      </div>
    </div>
  </section>
    
