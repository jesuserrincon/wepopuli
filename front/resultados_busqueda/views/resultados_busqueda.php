<?php if($filtro == 1){?> 
<div class="wrapper relative">
    <ul class="rslides" id="slider2">
        <li><a target="_blanck" href="<?php echo $categoria->link_superior; ?>"><img src="<?php echo base_url().'uploads/categorias/'.$categoria->banner_superior;?>" alt=""></a></li>
    </ul>
  </div>
<?php } ?>




<div class="box_b search_area clearfix">
	<h2 class="h2_b search_h2">Descubre y Apoya Proyectos</h2>
    <div class="search_box left">
        <form id="formulariobusqueda3" action="<?php echo base_url() ?>resultados_busqueda/index/0/2" method="post">
            <input type="submit" class="go" onclick="buscar2()" value="" title="Buscar" />
            <input type="text" id="palabrabuscar2" name="palabrabuscar" class="search" placeholder="Buscar" onkeypress="keydown2(event,'funcion')"  />
            </form>
    </div>
    
    <div class="right">
     <select id="basic-usage-demo" class="select" onchange="filtrarcategorias()">
       <option value="0">Selecciona Categoría</option>
        <?php foreach ($categorias as $item){?>
            <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
        <?php } ?>
    </select>
	</div>    
</div>

    <h3 class="h3_line_b"><?php echo $tituloresultado;?></h3>
    
    
 	<div class="box_a pad_box clearfix" style="width:960px">
        
        <div id="projects_show">
            <?php
               foreach ($proyectos as $item){ ?>
                  <div class="project_box">
                       <!-- BANDAS -->
                         <?php if($item->exitoso == 1){ echo '<div class="banda bnd1">Exitoso</div>';} ?>
                        <?php if($item->urgente == 1){ echo '<div class="banda bnd2">Urgente</div>';} ?>
                        <?php if($item->estados_id == 3){ echo '<div class="banda bnd3">Cerrado</div>';} ?>
                       
                        <div class="imgLiquidFill imgLiquid liquid_a">
                            <a href="<?php echo base_url().'detalle_proyecto/index/'.$item->id ?>">
                                <img class="thumbnail_a" src="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $item->imagen_miniatura; ?>" />
                            </a>
                        </div>
                        <div class="cat"><?php echo $item->nombrecateglria;?></div>
                        <div class="title"><a href="<?php echo base_url().'detalle_proyecto/index/'.$item->id ?>"><?php echo $item->titulo; ?></a></div>
                        <div class="des">
                         <?php echo $item->descripcion_corta; ?>
                        </div>
                        
                        <div class="resumen">
                            <ul class="data_project_min"> 
                                <li class="by"><?php echo $item->nombre_fundacion ?></li>
                                <li class="by">Municipio, País </li>
                                <li class="by"><?php echo $item->dias_para_cerrar ?> días restantes</li>
                            </ul>
                        </div>
                        
                        <div class="pos_data">
                            <div class="line_div_project"></div>
                            
                            <div class="percent_icon_box first_precent">
                                <div class="money_icon_a <?php if($item->valormonetario < 20000){ echo 'ico_desactivado'; }?> icons"></div>
                                <div class="percent_circle <?php if($item->valormonetario < 20000){ echo 'ico_desactivado'; }?>"><?php echo $item->porcentaje_monetario;?>%</div>
                            </div>
                            <div class="percent_icon_box">
                                <div class="members_icon_a <?php if($item->cantidadvoluntariado == 0){ echo 'ico_desactivado'; }?> icons"></div>
                                <div class="percent_circle <?php if($item->cantidadvoluntariado == 0){ echo 'ico_desactivado'; }?>"><?php echo $item->porcentaje_voluntariado;?>%</div>
                            </div>
                            <div class="percent_icon_box">
                                <div class="shop_icon_a icons <?php if($item->cantidadespecie == 0){ echo 'ico_desactivado'; }?>"></div>
                                <div class="percent_circle <?php if($item->cantidadespecie == 0){ echo 'ico_desactivado'; }?>"><?php echo $item->porcentaje_especie;?>%</div>
                            </div>
                            <div class="clear"></div>
                            
                            <center><div style="font-size:12px; margin-bottom:4px">Total recibido</div></center>
                            <div class="barra_porcentaje">
                                <div class="bar" style="width:<?php echo $item->porcentaje_total;?>%"><?php echo $item->porcentaje_total;?>%</div>
                            </div>
                        </div>
                    </div>
               <?php } ?>
            
           

        </div>
        
        
        
        <div class="clear"></div>
        <?php if($cantidad->cantidad > 8){ ?>
        <h3 onclick="javascript:cargar_contenido()" id="btnvermas" class="h3_line more_projects">Ver más proyectos</h3>
        <?php }?>
        <input type="hidden" name="cantidadpro" id="cantidadpro" value="<?php echo $cantidad->cantidad; ?>">
        <input type="hidden" name="cantidadactual" id="cantidadactual" value="8">
		
		
        <div id="cargador" style="text-align:center; margin-top:20px;display:none;"><img src="<?php echo base_url(); ?>assets/img/load.gif" width="32" /></div>
    </div>
    
   <?php if($filtro == 1){?>  
    <div class="wrapper relative">
        <ul class="rslides" id="slider3">
          <li><a target="_blanck" href="<?php echo $categoria->link_inferior; ?>"><img src="<?php echo base_url().'uploads/categorias/'.$categoria->banner_inferior;?>" alt=""></a></li>
        </ul>
    </div>
   <?php } ?>
    
    
       <script>
     function keydown2(e,s){
    if (!e) var e = window.event;
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    if (code==13){
        busca2r();	
    }
    }
    
    function buscar2(){
        var palabrabuscar = $('#palabrabuscar2').val();
        $('#formulariobusqueda3').submit();
        
    }
    
    function filtrarcategorias(){
        var idcategoria = $('#basic-usage-demo').val();
        location.href = '<?php echo base_url(); ?>resultados_busqueda/index/'+idcategoria+'/1';
    }
    
     function cargar_contenido(){
        $('#cargador').css('display','block');
        $('#btnvermas').css('display','none');
        var cantidadpro = $('#cantidadpro').val();
        var cantidadactual = $('#cantidadactual').val();
        if(<?php echo $filtro;?> == 1){
            var carga = 'cargarproyectosCATEGORIA';
        }
        if(<?php echo $filtro;?> == 2){
            var carga = 'cargarproyectosPALABRA';
        }
        if(<?php echo $filtro;?> == 3){
            var carga = 'cargarproyectosAPOYADOS';
        }
        if(<?php echo $filtro;?> == 4){
            var carga = 'cargarproyectosEXITOSOS';
        }
        if(<?php echo $filtro;?> == 5){
            var carga = 'cargarproyectosURGENTES';
        }
        if(<?php echo $filtro;?> == 6){
            var carga = 'cargarproyectosTAGS';
        }
        $.post( "<?php echo base_url().'resultados_busqueda/'; ?>"+carga, { cantidadactual: cantidadactual , busquedafiltro : '<?php echo $busquedafiltro; ?>' })
        .done(function( data ) {
            $('#projects_show').append(data);
            $('#cantidadactual').val(parseInt(cantidadactual)+parseInt(8));
            $('#btnvermas').css('display','block');
            if(parseInt(cantidadpro) < parseInt($('#cantidadactual').val())){
                $('#btnvermas').css('display','none');
            }
             $(".imgLiquidFill").imgLiquid();
             $('#cargador').css('display','none');
        });
        
    }
    </script>