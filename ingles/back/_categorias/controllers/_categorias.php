<?php

class _Categorias extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "CATEGORIAS";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Categorias();
        $info = $b->getCategorias();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('categorias');
    }

    public function add() {
        $this->_data['nombremodulo'] = "CATEGORIAS / New";
        $this->build('categorias_new');
    }

    public function new_Categorias() {


        $dir_subida = 'uploads/categorias/';
        $fileName = basename($_FILES['fileUpload1']['name']);
        $fichero_subido = $dir_subida . $fileName;
        if (move_uploaded_file($_FILES['fileUpload1']['tmp_name'], $fichero_subido)) {
        } else {
            $fileName = '';
        }

        $fileName2 = basename($_FILES['fileUpload2']['name']);
        $fichero_subido2 = $dir_subida . $fileName2;
        if (move_uploaded_file($_FILES['fileUpload2']['tmp_name'], $fichero_subido2)) {
        } else {
            $fileName2 = '';
        }



        $datos = array(
            'banner_superior' => $fileName,
            'banner_inferior' => $fileName2,
            'nombre' => $this->input->post('nombre'),
            'link_superior' => $this->input->post('link_superior'),
            'link_inferior' => $this->input->post('link_inferior'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'categorias';
        $b = new Categorias();
        //if ($datos['banner_superior'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'categorias_new';
            if ($b->saveCategorias($datos)){
                $build = 'categorias';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        //}
        $info = $b->getCategorias();
        $this->_data["info"] = $info;
        return redirect('cms/categorias/index/'.$save);
        
    }

    
    
    
    
    
    
    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'categorias/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."categorias/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('categorias/'.$data["data"]["file_name"], array('longside'=>'categorias/new','width' => 1000, 'height' => 223,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Categorias();
        $dat = $b->getCategoriasById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("categorias_edit");
    }
    
    public function update_Categorias(){
        $b = new Categorias();
        $datos = array(
            'id' => $this->input->post('id'),
            'nombre' => $this->input->post('nombre'),
            'link_superior' => $this->input->post('link_superior'),
            'link_inferior' => $this->input->post('link_inferior'),

        );

        $update = $b->updateCategorias($datos); 
        $a = FALSE;








        $dir_subida = 'uploads/categorias/';
        $fileName = basename($_FILES['fileUpload1']['name']);
        $fichero_subido = $dir_subida . $fileName;
        if (move_uploaded_file($_FILES['fileUpload1']['tmp_name'], $fichero_subido)) {
        } else {
            $fileName = '';
        }

        $fileName2 = basename($_FILES['fileUpload2']['name']);
        $fichero_subido2 = $dir_subida . $fileName2;
        if (move_uploaded_file($_FILES['fileUpload2']['tmp_name'], $fichero_subido2)) {
        } else {
            $fileName2 = '';
        }

        if($fileName != ''){
            $datos = array(
            'banner_superior' => $fileName,
            'id' => $this->input->post('id')
        );
            $update = $b->updateCategorias($datos);
        }
        if($fileName2 != ''){
            $datos = array(
            'banner_inferior' => $fileName2,
            'id' => $this->input->post('id')
        );
            $update = $b->updateCategorias($datos);
        }









        if ($update){
            $a = TRUE;
        }
        return redirect("cms/categorias/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Categorias();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/categorias/index/".$return);
    }

}

