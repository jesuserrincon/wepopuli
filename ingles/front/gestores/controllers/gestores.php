<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class gestores extends Front_Controller {

    public function __construct() {
        if($this->nativesession->get('usuario') == ''){
            redirect('home/index/5', 'refresh');
        }
        $this->load->library('session');
        $this->load->model(array(
               CMSPREFIX."usuarios/usuarios",
               CMSPREFIX."categorias/categorias",
               CMSPREFIX."voluntariado/voluntariado",
               CMSPREFIX."especie/especie",
               CMSPREFIX."especie_detalle/especie_detalle",
               CMSPREFIX."proyectos/proyectos",
               CMSPREFIX."gestor/gestor",
        ));
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($proyectoid = 0) {
        $this->set_title('Bienvenidos a ' . SITENAME, true);

       

        $this->_data["id"] = $proyectoid;
        //enviamos el registro del proyecto
           $proyectoOBJ = new Proyectos();
           $proyecto = $proyectoOBJ->getProyectosById($proyectoid);
           $this->_data["proyecto"] = $proyecto; 
           //Fin envio registro del proyecto
           //enviamos el nombre de la categoria
            $categorias2OBJ = new Categorias();
            $categoria = $categorias2OBJ->getCategoriasById($proyecto->categorias_id);
            $this->_data['nomcategoria'] = $categoria->nombre;
           //Fin envio nombre categoria
        
        $gestoresOBJ = new Gestores();
        $this->_data["gestor"] = $gestoresOBJ->getGestoresByIdProyecto($proyectoid);
        

        $this->_data["proyectoid"] = $proyectoid;
        return $this->build('gestores');
    }

    public function add(){

        $b = new Gestores();

         $datos = array(
            'nombre' => $this->input->post('nombre'),
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );

        //$update = $b->updateProyectos($datos);

        return redirect("historia/index/".$this->input->post('id'));

    }
    
}
