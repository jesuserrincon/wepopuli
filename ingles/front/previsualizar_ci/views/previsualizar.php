<script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="js/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="js/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.fancybox').fancybox();
        $(".fancybox-effects-a").fancybox({
            helpers: {
                title : {
                    type : 'outside'
                },
                overlay : {
                    speedOut : 0
                }
            }
        });

        $(".fancybox-effects-b").fancybox({
            openEffect  : 'none',
            closeEffect	: 'none',

            helpers : {
                title : {
                    type : 'over'
                }
            }
        });

        $(".fancybox-effects-c").fancybox({
            wrapCSS    : 'fancybox-custom',
            closeClick : true,

            openEffect : 'none',

            helpers : {
                title : {
                    type : 'inside'
                },
                overlay : {
                    css : {
                        'background' : 'rgba(238,238,238,0.85)'
                    }
                }
            }
        });

        $(".fancybox-effects-d").fancybox({
            padding: 0,

            openEffect : 'elastic',
            openSpeed  : 150,

            closeEffect : 'elastic',
            closeSpeed  : 150,

            closeClick : true,

            helpers : {
                overlay : null
            }
        });

        $('.fancybox-buttons').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',

            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,

            helpers : {
                title : {
                    type : 'inside'
                },
                buttons	: {}
            },

            afterLoad : function() {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
            }
        });



        $('.fancybox-thumbs').fancybox({
            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,
            arrows    : false,
            nextClick : true,

            helpers : {
                thumbs : {
                    width  : 50,
                    height : 50
                }
            }
        });

        $('.fancybox-media')
            .attr('rel', 'media-gallery')
            .fancybox({
                openEffect : 'none',
                closeEffect : 'none',
                prevEffect : 'none',
                nextEffect : 'none',

                arrows : false,
                helpers : {
                    media : {},
                    buttons : {}
                }
            });


        $("#fancybox-manual-a").click(function() {
            $.fancybox.open('1_b.jpg');
        });

        $("#fancybox-manual-b").click(function() {
            $.fancybox.open({
                href : 'iframe.html',
                type : 'iframe',
                padding : 5
            });
        });

        $("#fancybox-manual-c").click(function() {
            $.fancybox.open([
                {
                    href : '1_b.jpg',
                    title : 'My title'
                }, {
                    href : '2_b.jpg',
                    title : '2nd title'
                }, {
                    href : '3_b.jpg'
                }
            ], {
                helpers : {
                    thumbs : {
                        width: 75,
                        height: 50
                    }
                }
            });
        });


    });
</script>
<style type="text/css">
#s7 {
	color:#fff;
	background-position:0px -100px;
}
#s6 {
	color:#fff;
	background-position:0px -50px;
}
#s5 {
	color:#fff;
	background-position:0px -50px;
}
#s4 {
	color:#fff;
	background-position:0px -50px;
}
#s3 {
	color:#fff;
	background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -50px;
}
#s1 {
	color:#fff;
	background-position:0px -50px;
}
.col_e {
	min-height:1100px;
}
#s6 {
	left:551px !important;
}
#s7 {
	left:687px !important;
}
.box_a2 .col_d .first_precent {
	margin-left:85px;
}

</style>

<link href="assets/css/jquery-filestyle.css" rel="stylesheet" />
  	
<SCRIPT TYPE="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</SCRIPT>
<div class="paso_paso clearfix">
    <a href="<?php echo base_url(); ?>reglamento_ci/index/<?php echo $proyectoid; ?>" id="s1" class="step">Rules</a>
    <a href="<?php echo base_url(); ?>basicos_ci/index/<?php echo $proyectoid; ?>" id="s2" class="step">Basics</a>
    <a href="<?php echo base_url(); ?>historia_ci/index/<?php echo $proyectoid; ?>" id="s3" class="step"> Story </a>
    <a href="<?php echo base_url(); ?>gestores_front_ci/index/<?php echo $proyectoid; ?>" id="s4" class="step">Gestores</a>
    <a href="<?php echo base_url(); ?>revision_ci/index/<?php echo $proyectoid; ?>" id="s6" class="step">Revisión</a>
    <a href="<?php echo base_url(); ?>previsualizar_ci/index/<?php echo $proyectoid; ?>" id="s7" class="step">Previsualizar</a>
</div>

<div class="txt_reglamento">
  <h2 class="h2_b" style="font-size:44px"><?php if($proyecto->titulo == ''){ echo 'Título'; }else{ echo $proyecto->titulo; } ?></h2>
  <h2 class="h2_des">Project by: <?php echo $gestor->nombre; ?></h2>
</div>

    
    
<div class="box_shadow_big clearfix">

  <div class="col_e pad_int_20">
       <?php 
                    function image_exists($url) {
                        if(@getimagesize($url)){
                        return true;
                      }else{
                       return false;
                     }
                    }
              $imagen = base_url().'uploads/proyectominiatura/'.$proyecto->imagen_miniatura;

                    $fundacionimagen = base_url().'uploads/proyectos/gestores/'.$gestor->imagen;
                    $liderimagen = base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen;

              ?>
        	<img src="<?php if(image_exists($imagen)){ echo base_url().'uploads/proyectominiatura/'.$proyecto->imagen_miniatura; }else{ echo 'img/024.jpg'; }?>" class="rounded_img" width="590" />
        
        	<div class="description_project">
        	  <h3 class="h3_black" style="font-size:23px; margin-bottom:10px;">DESCRIPCIÓN DEL PROYECTO</h3>
        	  <p><?php echo $proyecto->descripcion; ?></p>
             <div class="spacer"></div>
              <h3 class="h3_purpure">OBJETIVOS Y METAS</h3>

        	  <p><?php echo $proyecto->objetivosymetas; ?>
      	    </p>
        	  <div class="spacer"></div>
              <h3 class="h3_purpure">CÓMO SE LLEVARÁ A CABO</h3>
        	  <p><?php echo $proyecto->comoselleva; ?>

				<div class="spacer"></div>
        	  <h3 class="h3_purpure">RIESGOS Y DESAFÍOS</h3>
        	  <p><?php echo $proyecto->riesgos; ?>
			<div class="spacer"></div>
        	  <h3 class="h3_purpure">PREGUNTAS Y RESPUESTAS</h3>
        	  <p><?php echo $proyecto->preguntas; ?>
	  </div>
            <div class="spacer"></div>
          <h3 class="h3_purpure">VIDEO</h3>
           <?php if(strpos($proyecto->video,'youtu' )) {
                
                function getYouTubeIdFromURL($url)
                {
                  $url_string = parse_url($url, PHP_URL_QUERY);
                  parse_str($url_string, $args);
                  return isset($args['v']) ? $args['v'] : false;
                }
              ?>
        <!--  <iframe class="device_300" width="590" height="315" src="<?php echo $proyecto->video; ?>" frameborder="0" allowfullscreen></iframe>
        -->
        <iframe width="590" height="315" src="//www.youtube.com/embed/<?php echo getYouTubeIdFromURL($proyecto->video);?>" frameborder="0" allowfullscreen></iframe>
         <?php   }   ?>
         <div class="spacer"></div>
            <?php if(strpos($proyecto->video,'vimeo')) { 
            ?>
                        <!--player.vimeo.com/video/55751501-->
                 <iframe src="//player.vimeo.com/video/<?php echo (int) substr(parse_url($proyecto->video, PHP_URL_PATH), 1);?>" width="590" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
             <?php   } ?>  
          <div class="spacer"></div>
          <h3 class="h3_purpure">OTRAS IMÁGENES</h3>
      <?php foreach ($imgComplementarias as $items) {?>
           <div class="spacer"></div>
            <img src="<?php echo base_url() ?>uploads/proyectos/imagenescomplementarias/<?php echo $items->imagen ?>" class="rounded_img" width="590" />
      <?php } ?>





  </div>
    <div class="col_f">
    	
        <div class="logo_fundacion">
                <?php 
                    $imagenf = base_url().'uploads/proyectos/gestores/'.$gestor->imagen; 
                ?>
        	<img src="<?php if(image_exists($imagenf)){ echo $imagenf; }else{ echo 'img/024.jpg'; }?>" width="200" />
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Project by:<br /> <a href="#inline2"  class="fancybox" style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Leader: </b> <a href="#inline3"  class="fancybox" style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>
           
        </div>
        
        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>
        
        <div class="state box_min_sh" style="padding:13px 0px">
        	<div class="estado left"><div class="active_point left"><span>Status:</span> <?php echo 'Activo'; ?></div></div>
            <div class="clear" style="margin-bottom:5px;"></div>
                <?php 
                function dameFecha($fecha,$dia)
                {   list($day,$mon,$year) = explode('/',$fecha);
                    return date('d/m/Y',mktime(0,0,0,$mon,$day+$dia,$year));        
                }
                ?>
        	<div class="estado"><span>Due Date:</span> <?php echo dameFecha(date('d/m/Y'),$proyecto->dias_para_cerrar); ?></div>
        </div>
        
        <div class="spacer"></div>

        <div class="state box_min_sh" style="padding:13px 0px">
        	<div style="margin-bottom:10px"><center>Supported</center></div>
            <div class="barra_porcentaje">
                <div class="bar" style="width:0%">0%</div>
            </div>

		</div>


        <div class="spacer"></div>
        <div class="div_line"></div>
        <div class="spacer"></div>

<div class="state box_min_sh" style="padding:13px 0px">
       		<div class="img_icon"><img src="img/065.png" /></div>
            <div class="tit">Voluntariado</div>
            <div class="barra_porcentaje" style="margin:5px auto 10px auto">
			<div class="bar" style="width:0%">0%</div>
            </div>
		</div>
        


        

    </div>
        

</div>

<div class="b1000" style="padding-left:50px;">
    <div id="divvalidar" class="" style="margin-bottom:20px; margin-bottom:0px; font-size:17px; padding-bottom:40px; color:#936; display:none; text-align:center; margin:0px auto"></div>
    <div style="clear:both"></div>
    <a href="<?php echo base_url(); ?>revision_ci/index/<?php echo $proyectoid; ?>" class="boton_b left" style="margin-right:10px; margin-left:300px">Back</a>
    <input type="hidden" id="redirec" name="redirec" class="guardar">
    <input type="button" onclick="javascript:borrarproyecto()" value="Delete Project" class="boton_c left" style="margin-right:10px;">
    <input type="button" onclick="javascript:postularproyecto()" value="Postular Proyecto" class="boton_a left postula_bt">
    <script type="text/javascript" src="js/jquery-filestyle.js"></script>
    <script type="text/javascript" src="js/aplication.js"></script>
     <div class="clear spacer"></div>
</div>

<div id="inline2" style="width:650px;display:none;">
    <div class="left col_line_right" style="width:350px;">
        <h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px; color:#1c75bc">Organization</h2>
        <h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->nombre;?></h2>
        <div class="spacer"></div>
        <div class="txt_user">
            <p><?php echo $gestor->descripcion;?></p>
        </div>
    </div>

    <div class="right" style="width:270px">
        <div class="logo_fundacion">
            <img src="<?php if(image_exists($fundacionimagen)){ echo $fundacionimagen; }else{ echo 'img/0.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <?php  if($gestor->nombre != ''){ echo '<div class="dato"><b>Project by: <br /><a  href="#inline2"  class="fancybox"  style="color:#27aae1">'.$gestor->nombre.'</a></b></div>'; } ?>
            <?php  if($gestor->nombreslider != ''){ echo '<div class="dato"><b>Leader: </b> <a  href="#inline3"  class="fancybox"  style="color:#27aae1">'.$gestor->nombreslider.' '.$gestor->apellidoslider.'</a> </div>'; } ?>
            <?php  if($gestor->pagina != ''){ echo '<div class="dato"><b>Pagina: '.$gestor->pagina.'</b></div>'; } ?>
            <?php  if($gestor->facebook != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebook.'</b></div>'; } ?>
            <?php  if($gestor->twitter != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitter.'</b></div>'; } ?>

        </div>
    </div>

</div>

<div id="inline3" style="width:650px;display:none;">
    <div class="left col_line_right" style="width:350px;">
        <h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px"><?php echo $gestor->nombreslider.' '.$gestor->apellidoslider;?></h2>
        <!--<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;"><?php echo $gestor->acercadelider;?></h2>-->
        <h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px"><?php echo $gestor->nombre;?></h3>

        <div class="txt_user">
            <p><?php echo $gestor->acercadelider;?></p>
        </div>
    </div>

    <div class="right" style="width:270px">
        <div class="logo_fundacion">
            <img src="<?php if(image_exists($liderimagen)){ echo base_url().'uploads/proyectos/gestores/'.$gestor->liderimagen; }else{ echo 'img/080.jpg'; }?>" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Contact Information</span></div>
            <?php  if($gestor->emaillider != ''){ echo '<div class="dato"><b>Email: <a href="mailto:'.$gestor->emaillider.'" style="color:#602483">'.$gestor->emaillider.'</a></b></div>'; } ?>
            <?php  if($gestor->facebooklider != ''){ echo '<div class="dato"><b>Facebook: '.$gestor->facebooklider.'</b></div>'; } ?>
            <?php  if($gestor->twitterlider != ''){ echo '<div class="dato"><b>Twitter: '.$gestor->twitterlider.'</b></div>'; } ?>
        </div>
    </div>

</div>

<script>

function borrarproyecto(){
        if(confirm('Are you sure you want to delete this project?')){
            location.href = '<?php echo base_url(); ?>basicos/eliminarproyecto/<?php echo $proyectoid; ?>';
        }else{

        }
    }
    
    
function postularproyecto(){
      $.post( "<?php echo base_url()?>previsualizar_ci/validar/", { id:<?php echo $proyectoid; ?> })
            .done(function( data ) {
                if(data == 1){
                    alert('Thank you! por postular el proyecto.');
                    location.href = '<?php echo base_url(); ?>misproyectos';
                }else{
                    $('#divvalidar').css('display','block');
                    $('#divvalidar').html(data);
                }
            });
}    
    
</script>
