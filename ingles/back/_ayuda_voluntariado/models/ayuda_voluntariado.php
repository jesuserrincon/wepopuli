<?php

class Ayuda_voluntariado extends DataMapper {

    public $model = 'ayuda_voluntariado';
    public $table = 'ayuda_voluntariado';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getAyuda_voluntariado(){
        return $this->query('SELECT id,titulo FROM cms_ayuda_voluntariado');
    }
    
    public function getAyuda_voluntariadoByProyecto($idproyecto = 0){
        return $this->query('SELECT id,titulo,descripcion FROM cms_ayuda_voluntariado WHERE proyectos_id = '.$idproyecto);
    }
    
    public function saveAyuda_voluntariado($object = "") {
        $this->usuarios_id = $object['usuarios_id'];
        $this->proyectos_id = $object['proyectos_id'];
        $this->imagen = $object['imagen'];
        $this->porque = $object['porque'];
        $this->cual = $object['cual'];
        $this->twitter = $object['twitter'];
        $this->discapacidad = $object['discapacidad'];
        $this->facebook = $object['facebook'];
        $this->sexo = $object['sexo'];
        $this->estado_civil = $object['estado_civil'];
        $this->direccion = $object['direccion'];
        $this->municipio = $object['municipio'];
        $this->departamento = $object['departamento'];
        $this->pais = $object['pais'];
        $this->email = $object['email'];
        $this->telefono = $object['telefono'];
        $this->telefono2 = $object['telefono2'];
        $this->tipo_identificacion = $object['tipo_identificacion'];
        $this->identificacion = $object['identificacion'];
        $this->nombres = $object['nombres'];
        $this->apellidos = $object['apellidos'];
        $this->tipo_voluntariado = $object['tipo_voluntariado'];
        return $this->save();
    }
    
    public function getLastId(){
        $usuario = $this->query('SELECT MAX(id) AS id FROM cms_ayuda_voluntariado');
        return $usuario->id;
    }
    
    public function getAyuda_voluntariadoById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateAyuda_voluntariado($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

