<section class="contenido-bg-negro clearfix" >
    <article class="wrap">
        <h2 class="title_int"><?php echo $quienes->titulo; ?></h2>
        <img src="<?php echo base_url()."uploads/quienes/new/".$quienes->imagen; ?>"  alt="" class="nosotros-img">
        <div class="nosotros-texto white-font left">
            <?php echo nl2br($quienes->texto); ?>
        </div>
        <div class="clear"></div>
        <div class="content_linea_tiempo">
        <ul class="linea-tiempo">
            <?php echo $lineas; ?>
        </ul>
        </div>
        <div class="btns_timeline">
          <a href="javascript:void(0)" class="btn_prev"></a>
          <a href="javascript:void(0)" class="btn_next"></a>
        </div>

        <div class="clear"></div>
        <span class="abre-form"><img src="<?php echo base_url(); ?>assets/img/btn-cobertura-<?php echo $this->session->userdata('lang'); ?>.png" alt=""></span>

        <div class='cobertura-mapa' >
            <div id="map_canvas"></div>
        </div>


    </article>
</section>
<script>
  function initialize() {
  var myLatlng = new google.maps.LatLng(4.5980556,-74.0758333);
  var mapOptions = {
    zoom: 4,
    center: myLatlng,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    panControl: true,
    zoomControl: true
	
  };
  var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

  <?php echo $puntos; ?>
  
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
