<?php

class _Clientes extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "Clientes";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Clientes();
        $info = $b->getClientes();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('clientes');
    }

    public function add() {
        $this->_data['nombremodulo'] = "Clientes / New";
        $this->build('clientes_new');
    }

    public function new_Clientes() {
        $datos = array(
            'imagen' => $this->input->post('imagen'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'clientes';
        $b = new Clientes();
        if ($datos['imagen'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'clientes_new';
            if ($b->saveClientes($datos)){
                $build = 'clientes';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getClientes();
        $this->_data["info"] = $info;
        return redirect('cms/clientes/index/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'clientes/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."clientes/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('clientes/'.$data["data"]["file_name"], array('longside'=>'clientes/new','width' => 180, 'height' => 137,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Clientes();
        $dat = $b->getClientesById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("clientes_edit");
    }
    
    public function update_Clientes(){
        $b = new Clientes();
        $a = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $b->updateClientes($datos);
        }
        if ($update){
            $a = TRUE;
        }
        return redirect("cms/clientes/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Clientes();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/clientes/index/".$return);
    }

}

