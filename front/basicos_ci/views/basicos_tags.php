<style type="text/css">
#s1 {
    color:#fff;
    background-position:0px -50px;
}
#s2 {
	color:#fff;
	background-position:0px -100px;
}
</style>
<link href="css/jquery-filestyle.css" rel="stylesheet" />
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.sticky.js"></script>
</script>  
<script>
$(function() {
	$(".datepicker").datepicker({ minDate: +2, maxDate: "+1M +10D" });
	});
	function imagenbox(){      
}
</script>  



<div class="paso_paso clearfix">
    <a href="<?php echo base_url(); ?>reglamento/index/<?php echo $proyectoid; ?>" id="s1" class="step">Reglamentos</a>
    <a href="<?php echo base_url(); ?>basicos/index/<?php echo $proyectoid; ?>" id="s2" class="step">Básicos</a>
    <a href="<?php echo base_url(); ?>historia/index/<?php echo $proyectoid; ?>" id="s3" class="step">Historia</a>
    <a href="<?php echo base_url(); ?>gestores_front/index/<?php echo $proyectoid; ?>" id="s4" class="step">Gestores</a>
    <a href="<?php echo base_url(); ?>cuenta/index/<?php echo $proyectoid; ?>" id="s5" class="step">Cuenta</a>
    <a href="<?php echo base_url(); ?>revision/index/<?php echo $proyectoid; ?>" id="s6" class="step">Revisión</a>
    <a href="<?php echo base_url(); ?>previsualizar/index/<?php echo $proyectoid; ?>" id="s7" class="step">Previsualizar</a>
</div>
<div class="txt_reglamento">
    <h2 class="h2_b">Creación del proyecto</h2>
    Es hora de empezar a describir el proyecto, siendo lo más específico posible. Entre más clara sea la descripción del proyecto, más altas serán las probabilidades de conseguir el apoyo necesario para llevarlo a cabo.
</div>

<div class="box_a pad_box clearfix">
    <div id="cargados">
        
    </div>
    <div class="col_c">
        <div class="box_shadow box_c">
            <h6>Imagen miniatura del proyecto <span>*</span> </h6>
            <div class="left" style="width:470px">
                <a href="javascript:imagenbox()">imagen box</a> Escoja una imagen de su computador, en
                formato JPEG, PNG, GIF, o BMP • Tamaño máximo: 2MB.
                Dimensiones mínimas: 640x480 pixeles • 4:3 relación radio.
            </div>
            <div class="clear"></div>
            <div class="relative">
                <input type="hidden" name="imagen_miniatura" id="imagen_miniatura" value="<?php echo $proyecto->imagen_miniatura;?>" /> 
               <input id="archivos" type="file" name="archivos[]" multiple="multiple" />
            </div>
        </div>
        <div class="div_green clear"></div>
        <div class="box_shadow box_c">
            <h6>Título del Proyecto <span>*</span> </h6>
            <p>El título del proyecto debe ser simple, específico y fácil de recordar. 
            Evite palabras como “ayuda”, “apoyo” o “patrocine”.</p>
            <input type="text" id="titulo" name="titulo" value="<?php echo $proyecto->titulo;?>" onkeyup="cambiartexto()" class="input_c" style="margin-bottom:10px"  /><br />Máximo de caracteres 
           <span id="titulo-text">0/60</span>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c">
            <h6>Categoría<span>*</span> </h6>
            <select class="select" name="categoria" id="categoria" onchange="cambiarcategoria()">
        <option value="0">Selecciona Categoría</option>
                <?php foreach ($categorias as $item){?>
            <option value="<?php echo $item->id;?>" <?php if($proyecto->categorias_id == $item->id){ echo 'SELECTED'; }?>><?php echo $item->nombre;?></option>
        <?php } ?>
            </select><br />
            <div class="clear"></div><br />
            Selecciona la categoría que más se ajuste al área de ejecución del proyecto.
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        <div class="box_shadow box_c">
            <h6>Tags<span></span> </h6>
            <!--input id="tags_2" name="tags" type="text" value="<?php echo $proyecto->tags_busqueda;?>"  class="tags input_c"/-->
            <input id="tags_1" type="text" class="tags input_c"/>
           
            <p>Selecciona la categoría que más se ajuste al área de ejecución del proyecto.</p>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>

        <div class="box_shadow box_c">
            <h6>Descripción corta <span>*</span> </h6>
            <p>¿Si se utilizara una frase para describir el proyecto, ¿cuál sería?</p>
            <textarea class="texarea_1" name="descripcion" id="descripcion" onkeyup="cambiartexto_descripcion()" ><?php echo $proyecto->descripcion_corta;?></textarea>
            
            <br />Máximo de caracteres <span id="descripcion-text">0/135</span>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Localización del proyecto <span>*</span> </h6>
            <p>¿Dónde se desarrollará el proyecto?</p>
            <div class="col_int_box_c left">
                <div class="label">País</div>
                <input type="text" class="input_a" />
                <div class="label">Municipio</div>
                <input type="text" class="input_a" />
            </div>
            <div class="col_int_box_c right">
                <div class="label">Departamento/Región</div>
                <input type="text" class="input_a" />
                <div class="label">Dirección (Si aplica)</div>
                <input type="text" class="input_a" />
            </div>
            <div class="clear"></div>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Tiempo de Publicación <span>*</span> </h6>
            <p>Recomendamos que los proyectos estén publicados 30 días o menos. Tiempos de publicación cortos tienen tasas de éxito más altas, y ayudarán a crear un sentido de urgencia que ayudará a conseguir más apoyo para el proyecto. </p>
            <b>Días Límite</b><br />
            <select class="select" id="diasfaltantes" name="diasfaltantes" onchange="cambiarfecha()">
                <?php for($i=30;$i<61;$i++){?>
                <option value="<?php echo $i; ?>" <?php if($proyecto->dias_para_cerrar == $i){ echo 'SELECTED'; }?>><?php echo $i; ?>
                <?php } ?>
            </select>
            <div class="clear"></div>
            <div class="div_line_2"></div>
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Clases de apoyo<span>*</span> </h6>
            <p>Diligencia las clases de apoyo segun requiera el proyecto. Puedes escoger una, dos o todas las clases de apoyo si el proyecto lo necesita.</p>           
        </div>
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Apoyo de voluntariado</h6>
            <input type="text" id="titulo_voluntariado" class="input_b" placeholder="Ej. Personal para pintar casas" style="margin-right:20px;" />
            <input type="hidden" id="idvoluntariado" value="0" />
             Máximo de caracteres  <span id="titulo_voluntariado-text">0/135</span><p></p>
            <p>Describa la clase de voluntariado que necesita. ¿Por qué y para qué?</p>           
            <textarea class="texarea_1" id="texto_voluntariado"></textarea>
            <br />Máximo de caracteres  <span id="texto_voluntariado-text">0/600</span>
            <div class="clear" style="margin-bottom:10px;"></div>
            <div class="left" id="apoyo_voluntariado">
                <?php foreach ($voluntariados as $item ){?>
                <div class="clear"></div><div id="vol_box<?php echo $item->id ?>">
                    <a onclick="return confirm('Desea eliminar este item ?')" href="javascript:eliminarvoluntariado(<?php echo $item->id ?>)"  class="less_2"></a>
                    <a href="javascript:actualizardatos(<?php echo $item->id ?>)" class="edit"></a><span id="acttitulo<?php echo $item->id ?>"><?php echo $item->titulo;?></span></div>
                <?php } ?>
            </div>
            <div class="right">
                Cantidad de Voluntarios:
                <input type="text" class="input_d" id="cantidad_voluntariado"  onkeypress="return isNumberKey(event)"/>
            </div>
            <div class="clear"></div>
            <div style="float: right;"><a href="javascript:agregarvoluntariado()" class="boton_b left" >Agregar</a></div>
        </div>
               
        
        
        
        <div class="div_green clear"></div>
        
        <div class="box_shadow box_c clearfix">
            <h6>Apoyo monetario</h6>
            <p>Explique por qué y para qué necesita apoyo monetario.</p>           
            <textarea class="texarea_1" id="comentarios_monetario" name="comentarios_monetario"><?php echo $proyecto->comentarios_monetario ; ?></textarea>
            <br />Máximo de caracteres <span id="texto_monetario-text">0/600</span>
            <div class="clear" style="margin-bottom:10px;"></div>
            <div class="right">
                Cuánto requiere para su proyecto:
                <input  type="text" class="input_e" id="valormonetario" name="valormonetario" value="<?php echo $proyecto->valormonetario ; ?>"  onkeypress="return isNumberKey(event)" />

            </div>

        </div>
        <div class="div_green clear"></div>        
        <div class="box_shadow box_c clearfix">
          <form id="formularioespecie">
            <div id="contenedorespecie">
            <h6>Apoyo en Especie</h6>
            <p>Ingrese los elementos que requiera su proyecto.</p>           
            <input type="text" class="input_f" id="nombrepaquete" placeholder="Nombre del paquete. Ej.Materiales de construcción" style="margin-right:20px" />
            Máximo de caracteres <span id="nombrepaquete-text">0/30</span>
            <div class="clear" style="margin-bottom:30px"></div>
            <input type="hidden" name="idespecie" id="idespecie" value="0" >
            <input type="hidden" name="cantidaditems" id="cantidaditems" value="7" >
            <div>
                <div class="col_a_especie left" id="cajaelemento">
                <center>Elemento</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_g" id="elemento1" placeholder="Ej: Atún" />
               
                <input type="text" class="input_g" id="elemento2" placeholder="Ej: Baldosas" />
                <input type="text" class="input_g" id="elemento3" placeholder="Ej: Leche"/>
                <input type="text" class="input_g" id="elemento4" />
                <input type="text" class="input_g" id="elemento5" />
                <input type="text" class="input_g" id="elemento6" />
                <input type="text" class="input_g" id="elemento7" />
            </div>
                <div class="col_a_especie left" id="cajamarca">
                <center>Marca</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_g" id="marca1" placeholder="Ej: Van camps" />
                <input type="text" class="input_g" id="marca2" placeholder="Ej: Corona (Ref. tauro)" />
                <input type="text" class="input_g" id="marca3" placeholder="Ej: Alpina deslactosada" />
                <input type="text" class="input_g" id="marca4" />
                <input type="text" class="input_g" id="marca5" />
                <input type="text" class="input_g" id="marca6" />
                <input type="text" class="input_g" id="marca7" />
            </div>
                <div class="col_b_especie left" id="cajacantidad">
                <center>Cantidad</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_h" id="cantidad1"  onkeypress="return isNumberKey(event)" placeholder="Ej: 450" />
                <input type="text" class="input_h" id="cantidad2"  onkeypress="return isNumberKey(event)" placeholder="Ej: 150" />
                <input type="text" class="input_h" id="cantidad3"  onkeypress="return isNumberKey(event)" placeholder="Ej: 1000" />
                <input type="text" class="input_h" id="cantidad4"  onkeypress="return isNumberKey(event)" />
                <input type="text" class="input_h" id="cantidad5"  onkeypress="return isNumberKey(event)" />
                <input type="text" class="input_h" id="cantidad6"  onkeypress="return isNumberKey(event)" />
                <input type="text" class="input_h" id="cantidad7"  onkeypress="return isNumberKey(event)" />
            </div>
                <div class="col_b_especie col_c_especie left" id="cajacontenido" style="margin-right:0px; width:108px;">
                <center>Cont/Med x und.</center><div class="clear" style="margin-bottom:10px"></div>
                <input type="text" class="input_h" id="contenido1" placeholder="Ej: 180gr" />
                <input type="text" class="input_h" id="contenido2" placeholder="Ej: 45cm x 45cm" />
                <input type="text" class="input_h" id="contenido3" placeholder="Ej: 1 Litro" />
                <input type="text" class="input_h" id="contenido4" />
                <input type="text" class="input_h" id="contenido5" />
                <input type="text" class="input_h" id="contenido6" />
                <input type="text" class="input_h" id="contenido7" />
            </div>
            <div class="clear" style="margin-bottom:20px;"></div>
            
            </div><!-- // CONTENEDOR inputs -->
            <div class="right" style="width:360px;">
                <div class="right">Cantidad de paquetes necesarios:
                    <input type="text" id="cantidadpaquete" class="input_d" onkeypress="return isNumberKey(event)" /></div>
                <div class="clear"></div>

                <div class="left" style="font-size:15px; margin-right:5px; line-height:36px; margin-bottom:10px;">Valor paquete de especie:</div>
                <input type="text" class="input_e right" id="valorpaquete" onkeypress="return isNumberKey(event)" style="margin-bottom:4px;" />
                <div class="clear"></div>
                <div class="right" style="text-align:right; font-size:12px;">El valor mínimo de un paquete<br /> debe ser de 20.000 pesos.</div>
                <div class="clear spacer"></div>
            <div style="float: right;"><a href="javascript:agregarespecie()" class="boton_b left" >Agregar</a></div>
            </div>
            </div>
            
            
            <div class="left" style="width:200px">
                <a href="javascript:additemespecie()" class="add_2" title="Agregar" style="width:200px;">Añadir nuevo item</a>
                
                    
                <div id="apoyoespecie">
                    <?php foreach ($especies as $item ){?>
                
                <div class="clear"></div><div id="del_box<?php echo $item->id ?>">
                    <a onclick="return confirm('Desea eliminar este item ?')" href="javascript:eliminarespecie(<?php echo $item->id ?>)"  class="less_2"></a>
                    <a href="javascript:actualizardatosespecie(<?php echo $item->id ?>)" class="edit"></a><span id="acttitulo<?php echo $item->id ?>"><?php echo $item->nombre;?></span>
                    </div>
                    
                <?php } ?>
                </div>
            </div>
            
          </form>
        </div>
        
            <div class="clear" style="margin-bottom:30px"></div>
            <input type="button" onclick="javascript:borrarproyecto()" value="Borrar Proyecto" class="boton_c left" style="margin-right:10px;">
            <input type="button" onclick="javascript:guardarproyecto()" value="Guardar Cambios" class="boton_a left" style="margin-right:10px;">
            <a href="<?php echo base_url(); ?>reglamento/index/<?php echo $proyectoid; ?>" class="boton_b left" style="margin-right:10px;">Atrás</a>
            <a href="javascript:siguiente()" class="boton_b left">Siguiente</a>

        
    </div><!-- // columna C -->
    
    <div class="col_d mar_right">
      <?php echo $vista; ?>
    </div>
</div>
<script>
     function isNumberKey(e){
	var key = window.Event ? e.which : e.keyCode;
	return (key >= 48 && key <= 57);
}
    function cambiartexto(){
        var titulo = $('#titulo').val();
        $('#titulo_box').html(titulo);
    }

    function cambiartexto_descripcion(){
    var titulo = $('#descripcion').val();
    $('#descripcion_box').html(titulo);
    }

    function cambiarfecha(){
        var titulo = $('#diasfaltantes').val();
        $('#diasfaltantes_box').html(titulo);
    }

init_contadorTa("titulo","titulo-text", 60);
init_contadorTa("nombrepaquete","nombrepaquete-text", 30);
init_contadorTa("descripcion","descripcion-text", 135);
init_contadorTa("titulo_voluntariado","titulo_voluntariado-text", 135);
init_contadorTa("texto_voluntariado","texto_voluntariado-text", 600);
init_contadorTa("comentarios_monetario","texto_monetario-text", 600);



function init_contadorTa(idtextarea, idcontador,max)
{
    $("#"+idtextarea).keyup(function()
            {
                updateContadorTa(idtextarea, idcontador,max);
            });
    
    $("#"+idtextarea).change(function()
    {
            updateContadorTa(idtextarea, idcontador,max);
    });
    
}

function updateContadorTa(idtextarea, idcontador,max)
{
    var contador = $("#"+idcontador);
    var ta =     $("#"+idtextarea);
    contador.html("0/"+max);
    
    contador.html(ta.val().length+"/"+max);
    if(parseInt(ta.val().length)>max)
    {
        ta.val(ta.val().substring(0,max-1));
        contador.html(max+"/"+max);
    }

}

function guardarproyecto(){
     var titulo = $('#titulo').val();
     var categoria = $('#categoria').val();
     var tags = $('#tags_1').val();
     var descripcion = $('#descripcion').val();
     var diasfaltantes = $('#diasfaltantes').val();
     var comentarios_monetario = $('#comentarios_monetario').val();
     var valormonetario = $('#valormonetario').val();
     var imagen_miniatura = $('#imagen_miniatura').val();
     if(valormonetario < 20000 && valormonetario > 0){  
                    alert('El valor ingresado debe ser mayor a $20.000 pesos.');
                    return false;
     }
     $.post( "<?php echo base_url()?>basicos/ActualizarProyectoBasicas/", { id:<?php echo $proyectoid; ?>,titulo:titulo,categoria:categoria,tags:tags,descripcion:descripcion,diasfaltantes:diasfaltantes, comentarios_monetario : comentarios_monetario, valormonetario:valormonetario, imagen_miniatura:imagen_miniatura })
            .done(function( data ) {
                alert('Se han guardado los cambios realizados.');
            });
}


function agregarvoluntariado(){
    var titulo_voluntariado = $('#titulo_voluntariado').val();
    var texto_voluntariado = $('#texto_voluntariado').val();
    var cantidad_voluntariado = $('#cantidad_voluntariado').val();
    var idvoluntariado = $('#idvoluntariado').val();
    var validacion = true;
    
        
        
    if(titulo_voluntariado == ''){
             $('#titulo_voluntariado').addClass("errorform");
             $('#titulo_voluntariado').attr('placeholder', 'Obligatorio.');
             $('#titulo_voluntariado').focus();
             validacion = false;
        }else{
             $('#titulo_voluntariado').removeClass("errorform");
            
        }
        
         if(texto_voluntariado == ''){
             $('#texto_voluntariado').addClass("errorform");
             $('#texto_voluntariado').attr('placeholder', 'Obligatorio.');
             $('#texto_voluntariado').focus();
             validacion = false;
        }else{
             $('#texto_voluntariado').removeClass("errorform");
            
        }
        
         if(cantidad_voluntariado == ''){
             $('#cantidad_voluntariado').addClass("errorform");
             $('#cantidad_voluntariado').attr('placeholder', 'Obligatorio.');
             $('#cantidad_voluntariado').focus();
             validacion = false;
        }else{
             $('#cantidad_voluntariado').removeClass("errorform");
            
        }
        if(validacion == false){
            return false;
        }else{
            
            if(idvoluntariado == 0){
                $.post( "<?php echo base_url()?>basicos/AddVoluntariado/", { titulo: titulo_voluntariado, descripcion: texto_voluntariado, cantidad: cantidad_voluntariado, proyectos_id: <?php echo $proyectoid; ?> })
                .done(function( data ) {
                    $('#apoyo_voluntariado').append('<div class="clear"></div><div id="vol_box'+data+'"><a onclick="return confirm('+"'"+'Desea eliminar este item ?'+"'"+')" href="javascript:eliminarvoluntariado('+data+')" class="less_2"></a> <a href="javascript:actualizardatos('+data+')" class="edit"></a><span id="acttitulo'+data+'">'+titulo_voluntariado+'</span></div>');
                });
            }else{
                 $.post( "<?php echo base_url()?>basicos/UpdateVoluntariado/", { titulo: titulo_voluntariado, descripcion: texto_voluntariado, cantidad: cantidad_voluntariado, idvoluntariado: idvoluntariado })
                .done(function( data ) {
                    $('#acttitulo'+idvoluntariado).html(titulo_voluntariado);
                });
            }
            $('#titulo_voluntariado').val('');
            $('#texto_voluntariado').val('');
            $('#cantidad_voluntariado').val('');
            $('#idvoluntariado').val(0);
       
        }
        
}





function eliminarvoluntariado(id){
    $('#vol_box'+id).remove();
    $.post( "<?php echo base_url()?>basicos/DelVoluntariado/", { id:id })
            .done(function( data ) {
                if($('#idvoluntariado').val() == id){
                    $('#titulo_voluntariado').val('');
                    $('#texto_voluntariado').val('');
                    $('#cantidad_voluntariado').val('');
                }
            });
}


function eliminarespecie(id){
    $('#del_box'+id).remove();
    $.post( "<?php echo base_url()?>basicos/Delespecie/", { id:id })
            .done(function( data ) {
                if($('#idespecie').val() == id){
                    $.post( "<?php echo base_url()?>basicos/formulario/", {  })
                            .done(function( data ) {
                                $('#contenedorespecie').html(data);
                                init_contadorTa("nombrepaquete","nombrepaquete-text", 30);
                      });
                }
            });
}


function actualizardatos(id){
     $.post( "<?php echo base_url()?>basicos/ConsultarVoluntariado/", { id:id })
            .done(function( data ) {
                var datos = data.split("19585");
                
                $('#titulo_voluntariado').val(datos[0]);
                $('#texto_voluntariado').val(datos[1]);
                $('#cantidad_voluntariado').val(datos[2]);
                $('#idvoluntariado').val(datos[3]);
              
            });
}

function borrarproyecto(){
    if(confirm('Desea eliminar el proyecto ?')){
        location.href = '<?php echo base_url(); ?>basicos/eliminarproyecto/<?php echo $proyectoid; ?>';
    }else{
        
    }
}

function actualizardatosespecie(id){
      $.post( "<?php echo base_url()?>basicos/ConsultarEspecie/", { id:id })
            .done(function( data ) {
                $('#contenedorespecie').html(data);
                init_contadorTa("nombrepaquete","nombrepaquete-text", 30);
            });
}


function additemespecie(){
    
    var cantidaditems = parseInt($('#cantidaditems').val())+1;
    $('#cajaelemento').append('<input type="text" class="input_g" id="elemento'+cantidaditems+'" />');
    $('#cajamarca').append('<input type="text" class="input_g" id="marca'+cantidaditems+'" />');
    $('#cajacantidad').append('<input type="text" class="input_h"  onkeypress="return isNumberKey(event)" id="cantidad'+cantidaditems+'" />');
    $('#cajacontenido').append('<input type="text" class="input_h" id="contenido'+cantidaditems+'" />');
    $('#cantidaditems').val(cantidaditems);
}







function agregarespecie(){
    var cantidaditems = parseInt($('#cantidaditems').val())+1;
    var registros = 0;
    var camposvacios = 0;
    for(var i=1;i<cantidaditems;i++){
         if($('#elemento'+i).val() != '' || $('#marca'+i).val() != ''  || $('#cantidad'+i).val() != ''  || $('#contenido'+i).val() != ''){
                
                registros = parseInt(registros)+1;
                if($('#elemento'+i).val() == ''){
                    $('#elemento'+i).addClass("errorform");
                     camposvacios =  true;
                }else{
                     $('#elemento'+i).removeClass("errorform");
                }
                if($('#marca'+i).val() == ''){
                    $('#marca'+i).addClass("errorform");
                     camposvacios =  true;
                }else{
                     $('#marca'+i).removeClass("errorform");
                }
                if($('#cantidad'+i).val() == ''){
                    $('#cantidad'+i).addClass("errorform");
                     camposvacios =  true;
                }else{
                     $('#cantidad'+i).removeClass("errorform");
                }
                if($('#contenido'+i).val() == ''){
                    $('#contenido'+i).addClass("errorform");
                     camposvacios =  true;
                }else{
                     $('#contenido'+i).removeClass("errorform");
                }
         }else{
                     $('#contenido'+i).removeClass("errorform");
                     $('#cantidad'+i).removeClass("errorform");
                     $('#marca'+i).removeClass("errorform");
                     $('#elemento'+i).removeClass("errorform");
             
         }
    }
    
     if(registros == 0){
            alert('Su lista debe tener al menos un registro.');
            return false;
     }else{
            if(camposvacios == true){
                alert('Alguno de los campos esta vacio');
                return false;
            }else{
                var nombrepaquete = $('#nombrepaquete').val();
                var valorpaquete = $('#valorpaquete').val();
                var cantidadpaquete = $('#cantidadpaquete').val();
                var validacioncampos = true;
                if(nombrepaquete == ''){
                    $('#nombrepaquete').addClass("errorform");
                    $('#nombrepaquete').attr('placeholder', 'Obligatorio.');
                    $('#nombrepaquete').focus();
                    validacioncampos = false;
                }else{
                    $('#nombrepaquete').removeClass("errorform");

                }
                if(valorpaquete == ''){
                    $('#valorpaquete').addClass("errorform");
                    $('#valorpaquete').attr('placeholder', 'Obligatorio.');
                    $('#valorpaquete').focus();
                    validacioncampos = false;
                }else{
                    $('#valorpaquete').removeClass("errorform");

                }
                if(cantidadpaquete == ''){
                    $('#cantidadpaquete').addClass("errorform");
                    $('#cantidadpaquete').attr('placeholder', 'Obligatorio.');
                    $('#cantidadpaquete').focus();
                    validacioncampos = false;
                }else{
                    $('#cantidadpaquete').removeClass("errorform");

                }
                if(validacioncampos == false){
                    return false;
                }
                if(valorpaquete < 20000){  
                    alert('El valor del paquete debe ser mayor a 20.000');
                    return false;
                }
                
                 $.post( "<?php echo base_url()?>basicos/AddEspecie/", { nombre:nombrepaquete,proyectos_id:<?php echo $proyectoid; ?>,cantidad_paquetes:cantidadpaquete,valor:valorpaquete,idespecie: $('#idespecie').val() })
                 .done(function( data ) {
                    var idespecie = data;
                    for(var i=1;i<cantidaditems;i++){
                        if($('#elemento'+i).val() != '' || $('#marca'+i).val() != ''  || $('#cantidad'+i).val() != ''  || $('#contenido'+i).val() != ''){
                            $.post( "<?php echo base_url()?>basicos/AddEspecieDetalle/", { elemento:$('#elemento'+i).val(),marca:$('#marca'+i).val(),cantidad:$('#cantidad'+i).val(),contenido:$('#contenido'+i).val(),especie_id:idespecie })
                               .done(function( data ) {
                                            
                                    });
                         }
                    }
                    $('#apoyoespecie').append('<div class="clear"></div><div id="del_box'+idespecie+'"><a onclick="return confirm('+"'"+'Desea eliminar este item ?'+"'"+')" href="javascript:eliminarespecie('+idespecie+')" class="less_2"></a> <a href="javascript:actualizardatosespecie('+idespecie+')" class="edit"></a><span id="especienombre'+idespecie+'">'+nombrepaquete+'</span></div>');
                   
                    if($('#idespecie').val() > 0){
                        $('#del_box'+$('#idespecie').val()).remove();
                    }
                        
                      $.post( "<?php echo base_url()?>basicos/formulario/", {  })
                            .done(function( data ) {
                                $('#contenedorespecie').html(data);
                                init_contadorTa("nombrepaquete","nombrepaquete-text", 30);
                      });
                        
                });
                 
                
            }
     }   
}



function siguiente(){
     var titulo = $('#titulo').val();
     var categoria = $('#categoria').val();
     var tags = $('.tags').val();
     var descripcion = $('#descripcion').val();
     var diasfaltantes = $('#diasfaltantes').val();
     var comentarios_monetario = $('#comentarios_monetario').val();
     var valormonetario = $('#valormonetario').val();
     var imagen_miniatura = $('#imagen_miniatura').val();
     if(valormonetario < 20000 && valormonetario > 0){  
                    alert('El valor ingresado debe ser mayor a $20.000 pesos.');
                    return false;
     }
     
     
    
     $.post( "<?php echo base_url()?>basicos/ActualizarProyectoBasicas/", { id:<?php echo $proyectoid; ?>,titulo:titulo,categoria:categoria,tags:tags,descripcion:descripcion,diasfaltantes:diasfaltantes, comentarios_monetario : comentarios_monetario, valormonetario:valormonetario,imagen_miniatura:imagen_miniatura })
            .done(function( data ) {
                location.href = '<?php echo base_url(); ?>historia/index/<?php echo $proyectoid; ?>';
            });

}

function cambiarcategoria(){
    var idc = $('#categoria').val();
    $.post( "<?php echo base_url()?>basicos/nombrecategoria/", { id:idc })
            .done(function( data ) {
                $('#categoria_box').html(data);
            });
}






function seleccionado(){ 

var archivos = document.getElementById("archivos");
var archivo = archivos.files;

 if(window.XMLHttpRequest) { 
 $('.loading').css('display','block'); 
 var Req = new XMLHttpRequest(); 
 }else if(window.ActiveXObject) { 
 var Req = new ActiveXObject("Microsoft.XMLHTTP"); 
 }


var data = new FormData();


for(i=0; i<archivo.length; i++){
   data.append('archivo'+i,archivo[i]);
}


Req.open("POST", "<?php echo base_url(); ?>basicos/uploadimg", true);


Req.onload = function(Event) {
if (Req.status == 200) { 
  var msg = Req.responseText;
  $('#imagen_box').attr('src','<?php echo base_url(); ?>uploads/proyectominiatura/'+msg);
  $('#imagen_miniatura').val(msg);
  $(".imgLiquidFill").imgLiquid({fill:true});
  $('.loading').css('display','none'); 
} else { 
  console.log(Req.status); 
} 
};

 Req.send(data); 
}
</script>
<script type="text/javascript" src="js/jquery-filestyle.js"></script>
<script type="text/javascript" src="js/aplication.js"></script>
<!--
BACKUP VALIDACIIONES
 var validacioncampos = true;
            if(titulo == ''){
                $('#titulo').addClass("errorform");
                $('#titulo').attr('placeholder', 'Campo obligatorio.');
                $('#titulo').focus();
                validacioncampos = false;
            }else{
                $('#titulo').removeClass("errorform");

            }
            if(categoria == 0){
                $('#categoria').addClass("errorform");
                $('#categoria').focus();
                validacioncampos = false;
            }else{
                $('#categoria').removeClass("errorform");
            }
            
            if(descripcion == ''){
                $('#descripcion').addClass("errorform");
                $('#descripcion').attr('placeholder', 'Campo obligatorio.');
                $('#descripcion').focus();
                validacioncampos = false;
            }else{
                $('#descripcion').removeClass("errorform");
            }
            
     if(validacioncampos == false){
         alert('Por favor revisa el formulario. Los campos en rojo son obligatorios');
         return false;
     }
-->