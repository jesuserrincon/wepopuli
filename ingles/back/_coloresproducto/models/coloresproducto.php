<?php

class Coloresproducto extends DataMapper {

    public $model = 'coloresproducto';
    public $table = 'coloresproducto';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getColoresproducto(){
        return $this->get();
    }

    public function getColoresproducto2($idp){
        //$this->select('*');
        //$this->from_array(array('cms_cms_coloresproducto'));
        $this->where('cms_productos_id',$idp);
        //$this->where('cms_coloresproducto.cms_colores_id',$idc);
        //$this->set_join_field('cms_colores', 'cms_colores.id = cms_coloresproducto.cms_colores_id');
        //$this->set_join_field('cms_productos', 'cms_productos.id = cms_coloresproducto.cms_productos_id');
        return $this->get();
   }
    
    public function saveColoresproducto($object = "") {
        $this->cms_colores_id = $object['color'];
        $this->cms_productos_id = $object['productos'];
        return $this->save();
    }
    
    public function getColoresproductoById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateColoresproducto($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

