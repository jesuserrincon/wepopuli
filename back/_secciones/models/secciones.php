<?php

class Secciones extends DataMapper {

    public $model = 'secciones';
    public $table = 'secciones';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getSecciones(){
        return $this->get();
    }
    
    public function saveSecciones($object = "") {
        $this->titulo = $object['titulo'];
        $this->texto = $object['texto'];
        $this->tipo_video = $object['tipo_video'];
        $this->video = $object['video'];
        $this->imagen = $object['imagen'];
        $this->ubicacion = $object['ubicacion'];
        return $this->save();
    }
    
    public function getSeccionesById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateSecciones($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

    public function seccionesByUbucacion($ubicacion){

        return $this->query('select * from cms_secciones where ubicacion like "'.$ubicacion.'"');

    }

}

