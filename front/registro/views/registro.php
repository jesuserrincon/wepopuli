<link rel="stylesheet" href="../captcha/css/bootstrap-responsive.min.css">
<link rel="stylesheet" href="../captcha/css/main.css">
<script data-main="js/app" src="js/vendor/require.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
$(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    yearRange: '1930:2000',
    dateFormat: 'yy-mm-dd'
});
});
</script>
<script type="text/javascript" src="js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="js/fancys.js"></script>
  
<div class="box_a pad_box clearfix form_ax1">
    <form method="POST" name="formularioregistro" id="formularioregistro" action="registro/UsuarioAdd"> 
	<div class="col_a">
        <h2 class="h2_b" style="margin-left:80px">Ingresar</h2>
        <div class="ingresar">

        	<div class="int_pad font_12">
                <div class="label">E-mail  <span class="requiered">*</span></div>
                <input type="text" class="input_a" id="emaillog" name="emaillog"  />
                
                <div class="label">Contraseña  <span class="requiered">*</span></div>
                <input type="password" id="password"  name="password" onkeypress="checkKey(event);" class="input_a" />
                <div style="font-size:11px; margin-top:-10px; margin-bottom:10px;"><span class="requiered">*</span> Los campos con asterisco son obligatorios.</div>
                
                <a href="#olvido" class="right link_a fancybox">Olvidé mi contraseña</a>
                <div class="clear"></div>
                <!--a class="recordarme right">Recordarme</a-->
            </div>
            <div class="div_line" style="margin:10px 0px;"></div>
            <a href="javascript:conectarse()" class="bt_green">Ingresar</a>	

        </div>
    
    </div>
    
    <div class="col_b">
    	<h2 class="h2_b">¿Aún no eres un usuario registrado? ¡Regístrate!</h2>
        
        <div class="box_shadow clearfix">
            <div class="col_int_b_1 clearfix">
                <div class="label">Nombres <span class="requiered">*</span></div>
                <input type="text" class="input_b" name="nombres" tabindex="1" id="nombres" />
                <div class="label">Tipo de identificación <span class="requiered">*</span></div>
                <select tabindex="3" class="select" id="tipo_identificacion" name="tipo_identificacion" onchange="verificarCedula($('#cedula'))" >
                    <option value="Tarjeta de identidad">Tarjeta de identidad
                    <option value="Cédula">Cédula
                    <option value="Cédula de extrajería">Cédula de extrajería
                    <option value="Pasaporte">Pasaporte 
                </select>
                <div class="label">Pais Nacionalidad <span class="requiered">*</span></div>
                 <select  tabindex=5 class="select" name="pais_nacionalidad" id="pais_nacionalidad">
                    <option value="1">Colombia</option>
                </select>

				<div class="label">Departamento (Región) <span class="requiered">*</span></div>
               <select class="select" tabindex="7" name="departamento" id="departamento">
                    <option value="1">Bogota D.C</option>
                </select>

                <div class="label">Dirección <span class="requiered">*</span></div>
                <input type="text" tabindex="9" class="input_b"    name="direccion" id="direccion"/>


              <div class="label">Teléfono 2 (Opcional)</div>
                <input type="text" tabindex="11" class="input_b"    name="telefono2" id="telefono2" onkeypress="ValidaSoloNumeros()" />
              
                <div class="label">Sexo <span class="requiered">*</span></div>
                <select tabindex="13" class="select" name="sexo" id="sexo">
                    <option value="femenino">Femenino</option>
                    <option value="masculino">Masculino</option>
                </select>
                
                <div class="label">Facebook (opcional)</div>
                <input tabindex="15" type="text" class="input_b" name="facebook" id="facebook"/>


                <div class="label">E-mail <span class="requiered">*</span></div>
                <input type="text" tabindex="17" class="input_b" onchange="verificarCorreo2(this)"  name="email" id="email"/>


                <div class="label">Contraseña <span class="requiered">*</span></div>
                <input tabindex="19" type="password" class="input_b"  name="clave" id="clave" />

                
                <div class="label">Ingresar Captcha <span class="requiered">*</span></div>
                <div id="captcha-wrap">
	
                    <input type="text" tabindex="21" name="captcha" id="captchatext" class="input_b" style="float:left; width:78px;">
                <img onclick="javascript:cambiarcap()" src="img/084.png" alt="refresh captcha" id="refresh-captcha" style="float: left; cursor:pointer" />
	
    			<img src="../captcha/php/newCaptcha.php" alt="" id="captcha" style="float:left;" height="42" width="120" />


                </div>
                
                <div class="clear" style="height:5px"></div>
                <div style="font-size:11px; margin-top:-10px; margin-bottom:10px;"><span class="requiered">*</span> Para registrarse, debe diligenciar todos los campos marcados con asterisco.</div>
              

            </div>
            
            <div class="col_int_b_1 clearfix right">
                <div class="label">Apellidos <span class="requiered">*</span></div>
                <input type="text" class="input_b" tabindex="2"  name="apellidos" id="apellidos" />
                
                <div class="label">No. identificación <span class="requiered">*</span></div>
                <input type="text" tabindex="4" class="input_b" onBlur="verificarCedula(this)"  name="cedula" id="cedula" onkeypress="ValidaSoloNumeros()" />

                <div class="label">Pais Residencia <span class="requiered">*</span></div>
                 <select tabindex="6" class="select" name="pais" id="pais">
                    <option value="1">Colombia</option>
                </select>

                <div class="label">Municipio <span class="requiered">*</span></div>
                <select  tabindex="8" class="select" name="ciudad" id="ciudad">
                    <option value="1">Bogota D.C</option>
                </select>

                <div class="label">Télefono 1 <span class="requiered">*</span></div>
                <input type="text" tabindex="10"  class="input_b"   name="telefono" id="telefono" onkeypress="ValidaSoloNumeros()" />


                <div class="label">Fecha nacimiento <span class="requiered">*</span></div>
                <input type="text" tabindex="12" class="input_b datepicker"   name="fecha_nacimiento" id="fecha_nacimiento" />

				<div class="label">Estado Civil <span class="requiered">*</span></div>
                <select class="select"  tabindex=14 name="estado_civil" id="estado_civil">
                    <option value="Soltero">Soltero</option>
                    <option value="Casado">Casado</option>
                    <option value="Divorciado">Divorciado</option>
                </select>
                
				<div class="label">Twitter (opcional)</div>
                <input type="text"   tabindex="16" class="input_b" name="twitter" id="twitter" />

	            
                <div class="label">Confirmar E-mail <span class="requiered">*</span></div>
                <input tabindex="18" type="text" class="input_b"  name="confirma_email" id="confirma_email"/>

                <div class="label">Confirmar Contraseña <span class="requiered">*</span></div>
                <input type="password"  tabindex="20" class="input_b"  name="confirmar_clave" id="confirmar_clave"/>
				
                <div class="clear" style="margin-bottom:10px;"></div>
                
                
            </div>
              
            <div class="div_line clear" style="margin-bottom:0px"></div>
            <div class="pad_box_10 clearfix">
                <a href="javascript:validarformulario()" class="bt_green right">Registrarme</a>	
            <div class="right" style="padding:5px 20px">
                <a href="#terminos_cond" class="fancybox terms right" style="font-size:13px; line-height:10px; padding-left:5px; color:#3CF !important; text-decoration:underline"> términos y condiciones</a>

                <a class="verde_bt2 right" id="acepto_terminos">Acepto </a>
                <!--br />
                <a class="verde_bt3 right" id="recordarme2">Recordarme</a-->
            </div>
                          	

			</div>
            
            
        </div>
        
        
    </div>
                <input type="hidden" name="newsletter" id="newsletter"/>
                
        </form>
</div>
<style>
    .errorform {
    border: solid 2px #a01f80;  
}
</style>
<script type="text/javascript">
    function checkKey(key){
        var unicode;
        if (key.charCode)
        {unicode=key.charCode;}
        else
        {unicode=key.keyCode;}
        if (unicode == 13){
           conectarse();
        }
    }
    function cambiarcap(){
        $('#captcha').attr("src","<?php echo base_url(); ?>captcha/php/newCaptcha.php?rnd=" + Math.random());
    }
    function validaEmail(email) { 
    var re  = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/; 
    if (!re.test(email)) { 
        alert ("Debe ingresar un E-mail válido."); 
        return false; 
    } 
    return true; 
    }
    
    
    function ValidaSoloNumeros() {
     if ((event.keyCode < 48) || (event.keyCode > 57)) 
      event.returnValue = false;
    }
    
    function validarformulario(){
        var captcha = $('#captchatext').val();
        
        var fecha_nacimiento = $('#fecha_nacimiento').val();
        var nombres = $('#nombres').val();
        var apellidos = $('#apellidos').val();
        var telefono = $('#telefono').val();
        var telefono2 = $('#telefono2').val();
        var cedula = $('#cedula').val();
        var email = $('#email').val();
        var confirma_email = $('#confirma_email').val();
        var pais = $('#pais').val();
        var departamento = $('#departamento').val();
        var direccion = $('#direccion').val();
        var facebook = $('#facebook').val();
        var twitter = $('#twitter').val();
        var ciudad = $('#ciudad').val();
        var sexo = $("input[name='n']:checked").val(); 
        var estado_civil = $('#estado_civil').val();
        var clave = $('#clave').val();
        var confirmar_clave = $('#confirmar_clave').val();
        var validacion = true;
        var newsletter = '';
        var recordarme = '';
       
       
       
        if ($('#newaletter').hasClass('active_recordarme')){
            $('#newsletter').val('si');
        }else{
             $('#newsletter').val('no');
        }
        
        if ($('#recordarme2').hasClass('active_recordarme')){
             $('#recordarme').val('si');
        }else{
             $('#recordarme').val('no');
        }
        
        if ($('#acepto_terminos').hasClass('active_recordarme')){
            
        }else{
            alert('Para registrarse, debe aceptar los términos y condiciones del sitio.');
            return false;
        }
        
        
        
        if(nombres == ''){
             $('#nombres').addClass("errorform");
             $('#nombres').focus();
             validacion = false;
        }else{
             $('#nombres').removeClass("errorform");
            
        }
        if(apellidos == ''){
             $('#apellidos').addClass("errorform");
             $('#apellidos').focus();
             validacion = false;
        }else{
             $('#apellidos').removeClass("errorform");
            
        }
        if(telefono == ''){
             $('#telefono').addClass("errorform");
             $('#telefono').focus();
             validacion = false;
        }else{
             $('#telefono').removeClass("errorform");
            
        }
       
        if(cedula == ''){
             $('#cedula').addClass("errorform");
             $('#cedula').focus();
             validacion = false;
        }else{
             $('#cedula').removeClass("errorform");
            
        }
        if(email == ''){
             $('#email').addClass("errorform");
             $('#email').focus();
             validacion = false;
        }else{
             $('#email').removeClass("errorform");
            
        }
         if(confirma_email == ''){
             $('#confirma_email').addClass("errorform");
             $('#confirma_email').focus();
             validacion = false;
        }else{
             $('#confirma_email').removeClass("errorform");
            
        }
        
        
        
         if(pais == ''){
             $('#pais').addClass("errorform");
             $('#pais').focus();
             validacion = false;
        }else{
             $('#pais').removeClass("errorform");
            
        }
        
         if(departamento == ''){
             $('#departamento').addClass("errorform");
             $('#departamento').focus();
             validacion = false;
        }else{
             $('#departamento').removeClass("errorform");
            
        }
        
        
         if(direccion == ''){
             $('#direccion').addClass("errorform");
             $('#direccion').focus();
             validacion = false;
        }else{
             $('#direccion').removeClass("errorform");
            
        }
        
         if(fecha_nacimiento == ''){
             $('#fecha_nacimiento').addClass("errorform");
             $('#fecha_nacimiento').focus();
             validacion = false;
        }else{
             $('#fecha_nacimiento').removeClass("errorform");
            
        }
    
    
        if(ciudad == ''){
             $('#ciudad').addClass("errorform");
             $('#ciudad').focus();
             validacion = false;
        }else{
             $('#ciudad').removeClass("errorform");
            
        }
        if(clave == ''){
             $('#clave').addClass("errorform");
             $('#clave').focus();
             validacion = false;
        }else{
             $('#clave').removeClass("errorform");
            
        }
        if(confirmar_clave == ''){
             $('#confirmar_clave').addClass("errorform");
             $('#confirmar_clave').focus();
             validacion = false;
        }else{
             $('#confirmar_clave').removeClass("errorform");
            
        }
        
       
        
        if(validacion == false){
            alert('Para registrarse, debe diligenciar todos los campos obligatorios.');
            return false;
        }
        
        if(validaEmail(email) == false){
            return false;
        }
        if(email != confirma_email){
            alert("El E-mail suministrado debe coincidir en los dos campos.");
             $('#confirma_email').addClass("errorform");
            $('#confirma_email').focus();
            return false;
        }
        if(clave != confirmar_clave){
            alert("Las contraseñas no coinciden.");
            $('#confirmar_clave').addClass("errorform");
            $('#confirmar_clave').focus();
            return false;
        }
        
        
        
        
        
         $.post( "<?php echo base_url(); ?>captcha/actual.php", { })
          .done(function( data ) {
            if(captcha != data){
                alert("El captcha es incorrecto.");
                return false;
            }else{
               $('#formularioregistro').submit(); 
            }
          });
        
    }
        
        
     
     
     
     function conectarse(){
         var emaillog = $('#emaillog').val();
         var password = $('#password').val();
         var validacion = true;
         
         
         if(emaillog == ''){
             $('#emaillog').addClass("errorform");
             
             $('#emaillog').focus();
             validacion = false;
        }else{
             $('#emaillog').removeClass("errorform");
            
        }
        
        if(password == ''){
             $('#password').addClass("errorform");
             $('#password').focus();
             validacion = false;
        }else{
             $('#password').removeClass("errorform");
            
        }
        
        if(validacion == false){
            return false;
        }
        $.post( "<?php echo base_url(); ?>registro/login", { emaillog : emaillog , password : password})
          .done(function( data ) {
                if(data == 'Error en los datos suministrados'){
                     alert('Debe ingresar un usuario y contraseña válidos.');
                }else{
                    window.location = '<?php echo base_url(); ?>home/index/1';
                }
          });
        
        
     }
	$('.sex_f').sexCheck();
        

</script>
<script>
<?php if($accion == 1){?>
    alert('Para completar el proceso de registro debe activar su cuenta, haciendo clic en el link enviado al correo electrónico que ha proporcionado. De lo contrario, no quedará registrado en nuestra plataforma.');
<?php }?>
<?php if($accion == 2){?>
    alert('tu cuenta ha sido activada puedes conectarte con tu correo y clave suministrada.');
<?php }?>
<?php if($accion == 3){?>
    alert('Se han enviado las instrucciones para recuperar su contraseña al correo electrónico suministrado.');
<?php }?>
<?php if($accion == 4){?>
    setTimeout(function(){
        $('#modalRecuperar').click()
        },2000
    );
<?php }?>
<?php if($accion == 5){?>
    alert('Ahora podras ingresar con tu correo electronico y la nueva contraseña.');
<?php }?>
</script>

    
<!-- //popup -->
<div id="terminos_cond" style="display:none;" class="device_250">
	<div class="device_250">
        <h2 class="h2_b" style="margin-bottom:20px;">Terminos y Condiciones</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet libero viverra mi tristique ultrices. Fusce nec leo dapibus, pulvinar justo et, lacinia tortor. Duis a consequat magna. Cras sodales, lectus ut pellentesque congue, diam metus mattis enim, ac tincidunt nisi turpis ut metus. Ut in sapien at lacus semper facilisis vitae sed odio. Morbi facilisis faucibus eros vitae pharetra. Fusce eu ipsum congue, viverra velit a, mollis nunc. Nam mattis ligula eget metus aliquet, sed commodo justo aliquet.
        Donec consequat at sapien nec ullamcorper. <br />
        <br />
        Aliquam erat volutpat. Morbi dolor lacus, bibendum eu velit sit amet, cursus facilisis arcu. Fusce eu facilisis lorem. Pellentesque eu augue at mauris fringilla volutpat. Suspendisse tincidunt sit amet nisi id adipiscing. Integer porttitor nisl quis dignissim cursus. In suscipit sapien eros, ac vulputate quam iaculis vitae. Suspendisse justo odio, porttitor quis eros ut, adipiscing gravida nunc.
        <br /><br />
        Praesent accumsan eget nulla eget faucibus. Fusce eu nulla in odio pharetra gravida. Curabitur sollicitudin enim tellus, vitae commodo nunc dapibus vel. Ut rutrum quam id quam tempor, in pulvinar justo ultrices. Morbi ultricies, dolor eu tempus pretium, neque leo scelerisque neque, sit amet blandit ligula diam ac tortor. Cras eleifend scelerisque nibh, tempor vehicula ligula auctor at. Nullam arcu justo, egestas et libero non, placerat aliquam augue. Fusce elementum et sem nec malesuada. Suspendisse interdum vitae diam quis hendrerit. Etiam convallis malesuada porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
        <br /><br />
        Proin laoreet gravida eleifend. Ut varius semper metus vel consectetur. Integer ut massa sed ligula aliquam tincidunt. Aenean purus nunc, pulvinar eu scelerisque vel, commodo et erat. Sed consequat, diam sed lacinia congue, mi lectus porttitor nisi, ac fermentum justo erat a nisi. Vivamus ac rutrum felis, et commodo ante. Aliquam dignissim, neque vel volutpat rutrum, nisl ante sollicitudin lectus, at interdum nulla elit consectetur tellus. In commodo nisi quis sem euismod feugiat. Aenean accumsan porta tempor. Nullam porta faucibus risus, nec rhoncus orci feugiat in. Nunc hendrerit consequat laoreet. Aliquam congue vestibulum porta.</p>
	</div>
</div>
    
    
    <!-- //popup OLVIOD -->
<div id="olvido" style="display:none; width:350px;">
	<div>
        <form id="formrecuperar" method="post" action="<?php echo base_url(); ?>registro/recuperarClave">
        <h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">¿Olvidó su contraseña?</h2>
        <p>Ingrese la cuenta de correo con la que se registró.<br /> Le enviaremos un correo para restablecer su clave.</p>
        <input type="text" id="correo"  name="email" class="input_j" placeholder="E-mail" />
        <input type="hidden" name="id" id="idusuario">
        <input type="button" onclick="verificarCorreo()" value="Enviar" class="bt_green" style="width:347px; background-size:100% 100%; margin:0px 0px" />
        </form>
	</div>
</div>
        <a href="#recuperar" id="modalRecuperar" class="right link_a fancybox"></a>

        <div id="recuperar" style="display:none; width:350px; height:230px">
            <div>
                <form method="post" id="formrvalidar" action="<?php echo base_url(); ?>registro/nuevaClave">
                    <h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">Recuperar contraseña</h2>

                    <div class="label">Contraseña</div>
                    <input type="password" name="clave" id="clave1" class="input_j" />
                    <input type="hidden" value="<?php echo $idUser ?>" name="id">
                    <div class="label">Confirmar Contraseña</div>
                    <input type="password" name="clave2" id="clave2" class="input_j" />

                    <input type="button" onclick="compararClaves()" class="bt_green3" value="Guardar" style="float:left" />
                </form>
            </div>
        </div>

        <script>

            function compararClaves(){
                if($('#clave1').val() == ''){
                    alert('Ingrese su nueva clave');
                    return false;
                }

                if($('#clave2').val() == ''){
                    alert('Confirme su nueva clave');
                    return false;
                }

                if($('#clave1').val() == $('#clave2').val()){
                    $('#formrvalidar').submit();
                }else{

                    $('#clave1').val('');
                    $('#clave2').val('');
                    $('#clave1').focus();

                    alert('Las contraseñas no coinciden.');
                }
            }

                function verificarCorreo(){

                    if(validaEmail($('#correo').val())){

                    $.ajax({
                        url: "<?php echo base_url(); ?>registro/verificarCorreo",
                        type: "post",
                        dataType: "text",
                        data: 'correo='+$('#correo').val()
                    }).done(function (data) {
                       if(data == 0){
                           $('#correo').val('');
                           $('#correo').focus();
                           alert('Este E-mail no ha sido registrado.');
                       }else{
                           $('#idusuario').val(data);
                           $('#formrecuperar').submit();
                       }
                    })
                 }else{
                        $('#correo').val('');
                        $('#correo').focus();

                    }

                }

            function verificarCorreo2(elemento){

                    $.ajax({
                        url: "<?php echo base_url(); ?>registro/verificarCorreo",
                        type: "post",
                        dataType: "text",
                        data: 'correo='+$(elemento).val()
                    }).done(function (data) {
                        if(data > 0){
                            $('#email').val('');
                            $('#email').focus();
                            alert('Este E-mail ya se encuentra registrado en nuestra plataforma.');
                        }
                    })
                }

            function verificarCedula(elemento){

                var tipo = $('#tipo_identificacion').val();

                                $.ajax({
                                    url: "<?php echo base_url(); ?>registro/verificarCedula",
                                    type: "post",
                                    dataType: "text",
                                    data: 'cedula='+$(elemento).val()+'&tipo='+tipo
                                }).done(function (data) {
                                    if(data > 0){
                                        $(elemento).val('');
                                        alert('Este No. de Identificación ya está registrado');
                                        return false;
                                    }
                                })
                            }

        </script>
    
    