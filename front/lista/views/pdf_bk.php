<img src="<?php echo base_url() ?>assets/img/header-logo.png"><br><br>

<h3>Señores:</h3>
<?php echo nl2br($nombre."<br>".$correo)?><br>


<p>Atendiendo su amable solicitud sometemos a su consideración la siguiente propuesta para la adquisisción en arrendamiento por parte de ustedes, muebles y electrodomésticos los cuales relacionamos a continuación:
</p>



<table>

    <?php
   $valorArrendamiento = 0;
    foreach ($subcategorias as $key => $val) {
        $subtotal = 0;
        $i = 0;
        ?>
        <tr>
          <td   colspan="4">
          </td>
        </tr>
        <?php
        foreach ($val as $item) {

            ?>

            <tr>
                <td bgcolor="#ffe0a0" valign="middle" height="30">

                </td>
                <td colspan="2" bgcolor="#ffe0a0" valign="middle" height="30">
                    <?php echo $item['qty']." " ?>
                    <?php echo $item['name'] ?>
                </td>
                <td bgcolor="#ffe0a0" style="border-left:1px solid #999" height="30" valign="middle">
                    <?php echo '$'.number_format($item['subtotal'],0,'.','.');
                    $subtotal += $item['subtotal'];
                    ?>
                </td>
            </tr>
            <?php if(isset($item['options']['descripcion']) && !empty($item['options']['descripcion'])) {
                if($item['options']['descripcion'] != 'null'){
                ?>
            <tr>
                <td bgcolor="#edb6c0" valign="middle" ></td>
                <td bgcolor="#edb6c0" valign="middle"><?php echo nl2br($item['options']['descripcion']) ?></td>
            </tr>
                <?php }
            }
                ?>
        <?php
            $i++;
        }
        $valorArrendamiento += $subtotal;
        ?>
        <tr>
            <td colspan="4"></td>
        </tr>
        <tr>
        <td colspan="3">
            <!--<b>VALOR ARRENDAMIENTO MENSUAL</b>-->
        </td>
        <td class="td1">

            <b><?php //echo '$'.number_format($subtotal,0,'.','.') ?></b>
        </td>
        </tr>
    <?php
    }
    ?>
    <tr>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td colspan="4"></td>
    </tr>
    <?php

    foreach ($categorias as $key => $val) {
        $subtotal = 0;
        $i = 0;
        ?>
        <tr>
            <td colspan="3">
            </td>
        </tr>
        <?php
        foreach ($val as $item) {
            ?>

            <tr>
                <td>

                </td>
                <td colspan="2">
                    <?php echo $item['qty']." " ?>
                    <?php echo $item['name'] ?>
                </td>
                <td>
                    <?php echo '$'.number_format($item['subtotal'],0,'.','.');
                    $subtotal += $item['subtotal'];
                    ?>
                </td>
            </tr>
            <?php if(isset($item['options']['descripcion']) && !empty($item['options']['descripcion'])) {
                if($item['options']['descripcion'] != 'null'){
                ?>
                <tr>
                    <td></td>
                    <td><?php echo nl2br($item['options']['descripcion']) ?></td>
                </tr>
            <?php }
            }
                ?>


           <?php
            $a=0;
            for($i=0; $i<5;$i++){
                if(isset($item['options'][$i])){
                    if($i == 0){
                        ?>
                        <tr>
                            <td>
                                <b>ADICIONALES</b>
                            </td>
                        </tr>
                    <?php
                    }
                ?>
            <tr>
                <td>
                </td>
                <td colspan="2" >
                    <?php
                    $pieces = explode("-", $item['options'][$i]);
                    echo $pieces[1] ?>
                </td>
                <td>
                    <?php
                    $pieces = explode("-", $item['options'][$i]);
                    $subtotal+=$item['options'][$i];
                    echo number_format($pieces[0],0,'.','.') ?>
                </td>
            </tr>

            <?php }
            $a++;
            } ?>

            <?php
            $i++;
        }
        ?>
<tr>
    <td colspan="4" ></td>
</tr>
        <tr>
            <td colspan="3">
                <!--<b>VALOR ARRENDAMIENTO MENSUAL</b>-->
            </td>
            <td>

                <b><?php //echo '$'.number_format($subtotal,0,'.','.') ?></b>
            </td>
        </tr>

    <?php
        $valorArrendamiento += $subtotal;
    }
    ?>
<tr>
    <td colspan="4"></td>
</tr>
    <tr>
        <td colspan="4"></td>
    </tr>

    <tr>
        <td colspan="3">
            ALQUILER MENSUAL
        </td>
        <td>
            <?php echo '$'.number_format($valorArrendamiento,0,'.','.') ?>
        </td>
        </tr>
        <tr>
        <td colspan="3">
            TRANSPORTE IDA Y REGRESO
        </td>
        <td>
            <?php
            $trasporte = $zona;
            echo '$'.number_format($trasporte,0,'.','.') ?>
        </td>

    </tr>
    <tr>
        <td colspan="3">
            IVA 16%
        </td>
        <td>
            <?php
            $iva = ($valorArrendamiento + $trasporte)* 0.16;
            echo '$'.number_format($iva,0,'.','.') ?>
        </td>

    </tr>
    <tr>
        <td colspan="3">
            <b>TOTAL A PAGAR PRIMER MES</b>
        </td>
        <td>
            <?php
            $valorPrimerMes = ($valorArrendamiento + $trasporte)+ $iva;
            echo '$'.number_format($valorPrimerMes,0,'.','.') ?>
        </td>

    </tr>
<tr>
    <td colspan="4"></td>
</tr>
    <tr>
        <td colspan="3"><b>ALQUILER MESES SIGUIENTES</b></td>
    </tr>

    <tr>
        <td colspan="4"></td>
    </tr>

    <tr>
        <td colspan="3">
            ALQUILER MENSUAL
        </td>
        <td>
            <?php echo '$'.number_format($valorArrendamiento,0,'.','.') ?>
        </td>
    </tr>

    <tr>
        <td colspan="3">
            IVA 16%
        </td>
        <td>
            <?php
            $iva = $valorArrendamiento * 0.16;
            echo '$'.number_format($iva,0,'.','.') ?>
        </td>

    </tr>

    <tr>
        <td colspan="3">
            <b>TOTAL ALQUILER MENSUAL A PAGAR</b>
        </td>
        <td>
            <?php
            $total = $valorArrendamiento + $iva;
            echo '$'.number_format($total,0,'.','.') ?>
        </td>

    </tr>



</table>




<h4>CLÁUSULAS INFORMATIVAS</h4>
<p>
    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.   Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.
</p>


<h5>
    Atentamente,
</h5>
<h4>SC RENTAMUEBLES SAS</h4>