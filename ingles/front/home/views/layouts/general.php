<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="no-js" lang="en" xmlns:og="http://ogp.me/ns#/fb#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- Twitter Card data -->

<meta name="twitter:card" value="summary">
<meta content="activity" property="og:type">
<meta content="en" http-equiv="Content-language">
<?php 
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (false !== strpos($url,'detalle_proyecto')) {?>
 
<meta property="og:title" content="Wepopuli - <?php echo $proyecto->titulo; ?>"/>
<meta property="og:image" content="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $proyecto->imagen_miniatura; ?>"/>
<meta property="og:image:type" content="<?php echo base_url(); ?>uploads/proyectominiatura/<?php echo $proyecto->imagen_miniatura; ?>" />
<meta property="og:image:width" content="400" /> 
<meta property="og:image:height" content="300" />
<meta property="og:site_name" content="Wepopuli - <?php echo $proyecto->titulo; ?>"/>
<meta property="og:description" content="<?php echo $proyecto->descripcion; ?>"/>


<?php }
?>

<?php if (!strpos($url,'detalle_proyecto')) { ?>
  <meta property="og:title" content="Wepopuli"/>
  <meta property="og:image" content="http://wepopuli.com/assets/img/face_001.png"/>
  <meta property="og:image:type" content="http://wepopuli.com/assets/img/001.png" />
  <meta property="og:image:width" content="322" /> 
  <meta property="og:image:height" content="85" />
  <meta property="og:site_name" content="Wepopuli"/>
  <meta property="og:description" content="Wepopuli believes in the power of the Internet to connect the world with the capacity to lead humanity toward collective evolution as beings of peace, who are in harmony with the surrounding environment. Therefore, we spread projects from around the globe that need your help and could make a good use of your talents to collaborate as a volunteer on-field or from a distance. Here you can find one or more projects to support and create a positive change on our planet!"/>
<?php }
?>


<!-- Styles -->
<link href="<?php echo base_url('assets');?>/css/colombia_incluyente.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/responsiveslides.css">
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/css/jquery.classyedit.css">
<link rel="stylesheet" media="screen, projection" href="<?php echo base_url('assets');?>/css/fancySelect.css">

<!-- Metadata -->
<title>Wepopuli</title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets');?>/img/favicon.png" />
<meta http-equiv="pragma" content="No-Cache" />
<meta name="designer" content="romerorodrigo.com">
<meta name="viewport" content="width=1160, user-scalable = yes">
<meta http-equiv="Content-Language" content="en"/>
<!-- JS -->
   
<style type="text/css">
#atftbx p {
	display:none;
}
</style>
<!-- style type="text/css">
@media only screen
and (min-width : 768px)
and (max-width : 1024px)
and (orientation : portrait) {
/* Styles */
body, html {
  width:1100px;
}
@viewport {
   width:1500px;
   zoom: 0.6;
}
}

</style>

<!-- type="text/javascript">
if (window.matchMedia("(orientation: portrait)").matches) {
   // you're in PORTRAIT mode
            viewportmeta.content = 'width=device-width, initial-scale=0.6, maximum-scale=2';

}

if (window.matchMedia("(orientation: landscape)").matches) {
   // you're in LANDSCAPE mode
  viewportmeta.content = 'width=device-width, initial-scale=0.9';

}
</script-->

<script type="text/javascript" src="<?php echo base_url('assets');?>/js/jquery-1.11.0.min.js"></script>
   
        
<script type="text/javascript">
    $(document).ready(function() {
		
		
  $('.closed2').click(function() {
    $('.none, .footer, .back_top, .buttons, .div_line, .at4-share-outer').show(100);
	$('body, html').css("background", "none");
    return false;
	
  });

		
        $('.profile').hover(function() {
            $('.menu_profile').slideToggle(10);
            return false;
        });

    });
</script>
<!-- script type="text/javascript">
if (window.matchMedia("(orientation: portrait)").matches) {
   // you're in PORTRAIT mode
            viewportmeta.content = 'width=device-width, initial-scale=0.6, maximum-scale=2';

}

if (window.matchMedia("(orientation: landscape)").matches) {
   // you're in LANDSCAPE mode
            viewportmeta.content = 'width=device-width, initial-scale=0.9';

}
</script -->
<link href="<?php echo base_url('assets');?>/css/jquery.tagsinput.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url('assets');?>/js/jquery.tagsinput.js"></script>
<script type="text/javascript">
$(document).ready(function () {


function onAddTag(tag) {
  alert("Added a tag: " + tag);
}
function onRemoveTag(tag) {
  alert("Removed a tag: " + tag);
}

function onChangeTag(input,tag) {
  alert("Changed a tag: " + tag);
}

$(function() {
  $('#tags_1').tagsInput({width:'auto'});
});
});

</script>

<script type="text/javascript" src="<?php echo base_url('assets');?>/js/responsiveslides.js"></script>
<script type="text/javascript">
$(function () {
  $("#slider2").responsiveSlides({
  auto: true,
  pager: true,
  speed: 300,
  maxwidth:1000
  });
  $("#slider3").responsiveSlides({
  auto: true,
  pager: true,
  speed: 300,
  maxwidth:1000
  });
  $("#slider4").responsiveSlides({
  auto: true,
  pager: true,
  speed: 300,
  maxwidth:1000
  });
});
</script>

<script src="<?php echo base_url('assets');?>/js/fancySelect.js"></script>
<script> 
    $(document).ready(function() {        
        setTimeout(function(){ $('.select').fancySelect() },1000);
    }); 
</script>

<script>
        
  $(document).ready(function() {
               

    // Boilerplate
    var repoName = 'fancyselect'

    $.get('https://api.github.com/repos/octopuscreative/' + repoName, function(repo) {
      var el = $('#top').find('.repo');

      el.find('.stars').text(repo.watchers_count);
      el.find('.forks').text(repo.forks_count);
    });

    var menu = $('#top').find('menu');

    function positionMenuArrow() {
      var current = menu.find('.current');

      menu.find('.arrow').css('left', current.offset().left + (current.outerWidth() / 2));
    }

    $(window).on('resize', positionMenuArrow);

    menu.on('click', 'a', function(e) {
      var el = $(this),
        href = el.attr('href'),
        currentSection = $('#main').find('.current');

      e.preventDefault();

      menu.find('.current').removeClass('current');

      el.addClass('current');

      positionMenuArrow();

      if (currentSection.length) {
        currentSection.fadeOut(300).promise().done(function() {
          $(href).addClass('current').fadeIn(300);
        });
      } else {
        $(href).addClass('current').fadeIn(300);
      }
    });

    menu.find('a:first').trigger('click')
  });
</script>




<script src="<?php echo base_url('assets');?>/js/slides.min.jquery.js"></script>
<script>
  $(function(){
    $('.slides').slides({
      preload: true,
      generateNextPrev: true,
      generatePagination: false
    });
        
  });
</script>
<script type="text/javascript">
/*
 * customRadioCheck: jQuery plguin for checkbox and radio replacement
 * Usage: $('input[type=checkbox], input[type=radio]').customRadioCheck();
 * Author: Cedric Ruiz
 * License: MIT
*/
;(function(){
$.fn.customRadioCheck = function() {
 
  return this.each(function() {
 
    var $this = $(this);
    var $span = $('<span/>');
 
    $span.addClass('custom-'+ ($this.is(':checkbox') ? 'check' : 'radio'));
    $this.is(':checked') && $span.addClass('checked'); // init
    $span.insertAfter($this);
 
    $this.parent('label').addClass('custom-label')
      .attr('onclick', ''); // Fix clicking label in iOS
    // hide by shifting left
    $this.css({ position: 'absolute', left: '-9999px' });
 
    // Events
    $this.on({
      change: function() {
        if ($this.is(':radio')) {
          $this.parent().siblings('label')
            .find('.custom-radio').removeClass('checked');
        }
        $span.toggleClass('checked', $this.is(':checked'));
      },
      focus: function() { $span.addClass('focus'); },
      blur: function() { $span.removeClass('focus'); }
    });
  });
};
}());



;(function(){
$.fn.sexCheck = function() {
 
  return this.each(function() {
 
    var $this = $(this);
    var $span = $('<span/>');
 
    $span.addClass('custom2-'+ ($this.is(':checkbox') ? 'check' : 'radio'));
    $this.is(':checked') && $span.addClass('checked'); // init
    $span.insertAfter($this);
 
    $this.parent('label').addClass('custom2-label')
      .attr('onclick', ''); // Fix clicking label in iOS
    // hide by shifting left
    $this.css({ position: 'absolute', left: '-9999px' });
 
    // Events
    $this.on({
      change: function() {
        if ($this.is(':radio')) {
          $this.parent().siblings('label')
            .find('.custom2-radio').removeClass('checked');
        }
        $span.toggleClass('checked', $this.is(':checked'));
      },
      focus: function() { $span.addClass('focus'); },
      blur: function() { $span.removeClass('focus'); }
    });
  });
};
}());


;(function(){
$.fn.sexMCheck = function() {
 
  return this.each(function() {
 
    var $this = $(this);
    var $span = $('<span/>');
 
    $span.addClass('custom3-'+ ($this.is(':checkbox') ? 'check' : 'radio'));
    $this.is(':checked') && $span.addClass('checked'); // init
    $span.insertAfter($this);
 
    $this.parent('label').addClass('custom3-label')
      .attr('onclick', ''); // Fix clicking label in iOS
    // hide by shifting left
    $this.css({ position: 'absolute', left: '-9999px' });
 
    // Events
    $this.on({
      change: function() {
        if ($this.is(':radio')) {
          $this.parent().siblings('label')
            .find('.custom3-radio').removeClass('checked');
        }
        $span.toggleClass('checked', $this.is(':checked'));
      },
      focus: function() { $span.addClass('focus'); },
      blur: function() { $span.removeClass('focus'); }
    });
  });
};
}());
</script>

<script type="text/javascript">
$(document).ready(function () { 
  $(".verde_bt").click(function(){
    $(".verde_bt").toggleClass("active_recordarme");
  });
  $(".verde_bt2").click(function(){
    $(".verde_bt2").toggleClass("active_recordarme");
  });
  $(".verde_bt3").click(function(){
    $(".verde_bt3").toggleClass("active_recordarme");
  });
});
</script>
<script src="<?php echo back_asset('components/uploadify/swfobject.js') ?>"></script>
<script src="<?php echo back_asset('components/uploadify/uploadify.js') ?>"></script>
</head>
<body>
       <!-- Go to www.addthis.com/dashboard to customize your tools -->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-545c058a62ed475c" async="async"></script>
		<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js%23pubid=ra-545c058a62ed475c" async="async"></script>

        <!-- AddThis Button BEGIN 
        <div addthis:url="http://mysite.org/blog/entry/{url_title}" class="addthis_toolbox addthis_default_style">
        <a class="addthis_button_facebook"></a>
        <a class="addthis_button_twitter"></a>
        <a href="http://www.addthis.com/bookmark.php" class="addthis_button_expanded"></a>
        </div -->
        <!-- AddThis Button END -->      
<?php

echo $template['partials']['header'];
echo $template['body'];
echo $template['partials']['footer'];

?> 



<script src ="<?php echo base_url('assets');?>/js/imgLiquid.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
  $(".imgLiquidFill").imgLiquid({fill:true});
  $(".imgLiquidNoFill").imgLiquid({fill:false});
});
</script>

<!-- ***************   FANCYBOX ****************  -->

<script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="js/fancy.js"></script>
</body>

</html>