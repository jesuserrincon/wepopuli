<?php

class Clientes extends DataMapper {

    public $model = 'clientes';
    public $table = 'clientes';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getClientes(){
        return $this->get();
    }
    
    public function saveClientes($object = "") {
        $this->imagen = $object['imagen'];
        return $this->save();
    }
    
    public function getClientesById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateClientes($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

