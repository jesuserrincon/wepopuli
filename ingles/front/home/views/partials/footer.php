<div class="div_line" style="margin-top:10px"></div>
<div class="footer clearfix device_320">
    <div class="box_a device_320">
        <div class="device_300">
        <div class="col_footer">
            <h3>Projects</h3>
            <a href="<?php echo base_url().'resultados_busqueda/index/0/3';?>">Most Backed Projects</a>
            <a href="<?php echo base_url().'resultados_busqueda/index/0/4';?>">Successful Projects</a>
            <a href="<?php echo base_url().'resultados_busqueda/index/0/5';?>">Urgent Projects</a>
            <?php
            foreach($s_proyectos as $data){ ?>
                <a href="<?php echo base_url().'seccion/index/'.$data->id ?>"><?php echo $data->titulo ?></a>
            <?php } ?>
        </div>
        <!--<div class="col_footer">
            <h3>Learn</h3>
            <?php
            foreach($s_aprende as $data){ ?>
                <a href="<?php //echo base_url().'seccion/index/'.$data->id ?>"><?php //echo $data->titulo ?></a>
            <?php } ?>
        </div>-->
        </div>


        <div class="device_300 clear_mobile">
        <div class="col_footer">
            <h3>Wepopuli</h3>
            <a href="<?php echo base_url().'seccion/index/3' ?>">About us</a>
            <a href="<?php echo base_url().'seccion/index/4' ?>">How it works</a>
            <a href="<?php echo base_url().'seccion/index/5' ?>">FAQ</a>
            <?php foreach($s_acerca as $data){ ?>
                <a href="<?php echo base_url().'seccion/index/'.$data->id ?>"><?php echo $data->titulo ?></a>
            <?php } ?>
        </div>
        <div class="col_footer">
            <h3>Social</h3>
            <a href="#newsletter_box" class="fancybox">Newsletter</a>
            <a href="https://twitter.com/wepopuli" target="_blank">Twitter</a>
            <a href="https://facebook.com/wepopuli" target="_blank">Facebook</a>
            <?php
            foreach($s_social as $data){ ?>
                <a href="<?php echo base_url().'seccion/index/'.$data->id ?>"><?php echo $data->titulo ?></a>
            <?php } ?>
        </div>
        </div>
        
        <div class="clear pad_top_mobile device_320"></div>


        <div class="device_300" style="padding-top:50px">
            <div class="foter_social ft_1" style="margin-left:370px">
                <a href="https://facebook.com/wepopuli" target="_blank" title="facebook"><img src="img/026.png" /></a>
                Like
            </div>
            <div class="foter_social">
                <a href="https://twitter.com/wepopuli" target="_blank" title="twitter"><img src="img/027.png" /></a>
                Tweet
            </div>
            <div class="foter_social">
                <a href="https://www.youtube.com/user/wepopuli" target="_blank" title="youtube"><img src="img/028.png" /></a>
                Follow us!
            </div>
        </div>
       
        
    </div>
</div>

<div id="newsletter_box" style="display:none; width:350px; overflow:hidden; height:156px;">
    <h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">Subscribe to our newsletter:</h2>
    <input type="text" placeholder="E-mail" id="emailsuscrito" class="input_j" />
    <input type="submit" value="Subscribe" onclick="suscribirse()" class="bt_green left" style=" width:349px; background-size:100% 100%"  />
</div>
<script>
$( document ).ready(function() {
	//p.style.fontSize = "25px";

     
});

    
    
   
</script>
    