<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

<?php include"metadata.php" ?>

<link href="assets/css/jquery-filestyle.css" rel="stylesheet" />

<script src="assets/js/jquery.tools.min.js"></script>
<script>
$(function() {
	$(".tool").tooltip();
});
</script>
  	
<script type="text/javascript">
<!--
// copyright 1999 Idocs, Inc. http://www.idocs.com
// Distribute this script freely but keep this notice in place
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

//-->
</script>
<script type="text/javascript">
$(document).ready(function() {
 // hides the slickbox as soon as the DOM is ready
  $('.menu_profile').hide(1);
 // toggles the slickbox on clicking the noted link  
  $('.profile').hover(function() {
    $('.menu_profile').fadeToggle(100);
    return false;
  });

});
</script>
<!-- Add mousewheel plugin (this is optional) -->	
<script type="text/javascript" src="assets/js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="assets/js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="assets/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!-- Add Button helper (this is optional) -->
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="assets/js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});
		});
</script>
</head>

<body>
<?php include"header.php" ?>  

<div class="relative b1000">
	<div class="profile">
    	<a href="#" class="name_id">Rodrigo Romero</a>
        <div class="menu_profile">
        	<div class="arrow_blue"></div>
        	<a href="#">Mis proyectos</a>
        	<a href="#">Proyectos apoyados</a>
        	<a href="#">Datos de usuario</a>
        	<a href="#">Log Out</a>
        </div>
    </div>
   
</div>

<div class="txt_reglamento" style="margin-top:40px; padding-left:40px;">
  <h2 class="h2_b" style="font-size:44px">Proyectos <span>Apoyados</span></h2>
</div>

    
    
<div class="box_shadow_big clearfix">
	
  <div class="pad_box_10">
  	<h2>Registro de apoyo suministrado</h2>
  </div>
  
  
  <div class="div_line"></div>
  
  
  <div class="col_e pad_int_20">
    	
        <div class="box_shadow2 box_margin clearfix" style="width:98%">
        	<div class="left line_right">
            	<div class="pic_project" style="margin-right:8px !important">
                	<img src="assets/img/067.png" width="226" height="145" />
                </div>
                	
                <div class="div_line" style="margin-bottom:10px"></div>
				
                <div style="margin-bottom:5px"><center>Porcentaje total</center></div>
                <div class="barra_porcentaje2" style="margin:5px 0px 25px 10px; width:88%">
                    <div class="bar2" style="width:35%">35%</div>
                </div>

            </div>
            
            
            <div class="right" style="width:320px">
            	<div class="des_project_int_mis_proyectos">
            		<div class="title_my_projects left">
                	    <h3 class="h3_purpure">Scripserit</h3>
                	</div>                    
                    <div class="clear"></div>
                    <div class="txt_des">
                    Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                    </div>
                    <div class="city">Bogotá, Colombia.</div>
                    
                    <div style="margin:10px 10px 0px 10px;">
                    	<h3 style="margin-bottom:5px">Tipo de proyecto</h3>
                    	 <div class="percent_icon_box">
                         <div class="money_icon_a icons"></div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a icons"></div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a icons"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="estado left" style="margin-right:10px; padding-left:10px"><div class="active_point left"><span>Status:</span> Activo</div><span class="tool" title="Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.">?</span> </div>
                    <div class="estado left"><span>Hasta:</span> 20/08/2013</div>
                        <div class="clear" style="margin-bottom:10px"></div>
                </div>
            </div>
            
        </div>
        
    	
	</div>	
  
  <div class="col_f">
    	
        <div class="logo_fundacion">
        	<img src="assets/img/060.jpg" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato"><b>Por:</b> Fundación Píes descalzos</div>
            <div class="dato"><b>Liderado por:</b> <a href="#inline2"  class="fancybox"  style="color:#27aae1">Rodrigo Romero  Castro</a> </div>
            <div class="dato"><b>www.website1.com</b></div>
            <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
            <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
            <div class="dato"><b>Bogotá, Colombia</b></div>
        </div>
        <div class="div_line" style="margin-top:20px"></div>
    </div>
        
        
  <!-- // ************* fin proyecto ************** -->       

  <div class="col_e pad_int_20">
    	
        <div class="box_shadow2 box_margin clearfix" style="width:98%">
        	<div class="left line_right">
            	<div class="pic_project" style="margin-right:8px !important">
                	<img src="assets/img/067.png" width="226" height="145" />
                </div>
                	
                <div class="div_line" style="margin-bottom:10px"></div>
				
                <div style="margin-bottom:5px"><center>Porcentaje total</center></div>
                <div class="barra_porcentaje2" style="margin:5px 0px 25px 10px; width:88%">
                    <div class="bar2" style="width:35%">35%</div>
                </div>

            </div>
            
            
            <div class="right" style="width:320px">
            	<div class="des_project_int_mis_proyectos">
            		<div class="title_my_projects left">
                	    <h3 class="h3_purpure">Scripserit</h3>
                	</div>                    
                    <div class="clear"></div>
                    <div class="txt_des">
                    Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                    </div>
                    <div class="city">Bogotá, Colombia.</div>
                    
                    <div style="margin:10px 10px 0px 10px;">
                    	<h3 style="margin-bottom:5px">Tipo de proyecto</h3>
                    	 <div class="percent_icon_box">
                         <div class="money_icon_a icons"></div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a icons"></div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a icons"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="estado left" style="margin-right:10px; padding-left:10px"><div class="active_point left"><span>Status:</span> Activo</div><span class="tool" title="Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.">?</span> </div>
                    <div class="estado left"><span>Hasta:</span> 20/08/2013</div>
                        <div class="clear" style="margin-bottom:10px"></div>
                </div>
            </div>
            
        </div>
        
    	
	</div>	
  
  <div class="col_f">
    	
        <div class="logo_fundacion">
        	<img src="assets/img/060.jpg" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato"><b>Por:</b> Fundación Píes descalzos</div>
            <div class="dato"><b>Liderado por:</b> <a href="#" style="color:#27aae1">Jhon Mccain</a> </div>
            <div class="dato"><b>www.website1.com</b></div>
            <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
            <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
            <div class="dato"><b>Bogotá, Colombia</b></div>
        </div>
        <div class="div_line" style="margin-top:20px"></div>
    </div>

  <!-- // ************* fin proyecto ************** -->       

  <div class="col_e pad_int_20">
    	
        <div class="box_shadow2 box_margin clearfix" style="width:98%">
        	<div class="left line_right">
            	<div class="pic_project" style="margin-right:8px !important">
                	<img src="assets/img/067.png" width="226" height="145" />
                </div>
                	
                <div class="div_line" style="margin-bottom:10px"></div>
				
                <div style="margin-bottom:5px"><center>Porcentaje total</center></div>
                <div class="barra_porcentaje2" style="margin:5px 0px 25px 10px; width:88%">
                    <div class="bar2" style="width:35%">35%</div>
                </div>

            </div>
            
            
            <div class="right" style="width:320px">
            	<div class="des_project_int_mis_proyectos">
            		<div class="title_my_projects left">
                	    <h3 class="h3_purpure">Scripserit</h3>
                	</div>                    
                    <div class="clear"></div>
                    <div class="txt_des">
                    Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.
                    </div>
                    <div class="city">Bogotá, Colombia.</div>
                    
                    <div style="margin:10px 10px 0px 10px;">
                    	<h3 style="margin-bottom:5px">Tipo de proyecto</h3>
                    	 <div class="percent_icon_box">
                         <div class="money_icon_a icons"></div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="members_icon_a icons"></div>
                        </div>
                        <div class="percent_icon_box">
                            <div class="shop_icon_a icons"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="estado left" style="margin-right:10px; padding-left:10px"><div class="active_point left"><span>Status:</span> Activo</div><span class="tool" title="Eos dicat oratio partem ut, id cum ignota senserit intellegat, Eos dicat oratio partem ut, id cum ssdsdd, frfgrtohtl.">?</span> </div>
                    <div class="estado left"><span>Hasta:</span> 20/08/2013</div>
                        <div class="clear" style="margin-bottom:10px"></div>
                </div>
            </div>
            
        </div>
        
    	
	</div>	
  
  <div class="col_f">
    	
        <div class="logo_fundacion">
        	<img src="assets/img/060.jpg" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato"><b>Por:</b> Fundación Píes descalzos</div>
            <div class="dato"><b>Liderado por:</b> <a href="#" style="color:#27aae1">Jhon Mccain</a> </div>
            <div class="dato"><b>www.website1.com</b></div>
            <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
            <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
            <div class="dato"><b>Bogotá, Colombia</b></div>
        </div>
        <div class="div_line spacer" style="margin-top:20px"></div>
    </div>

  <div class="col_e pad_int_20 left">
    <div class="div_line_2"></div>
        <div class="pg">
        	<div class="left"><a href="#">Primero </a></div>
        	<div class="left"><a href="#" class="prev2"></a></div>
        	<div class="left"><a href="#" class="prev"></a></div>
        	<div class="left num">
            	<a href="#">1</a>
            	<a href="#">2</a>
            	<a href="#">3</a>
            	<a href="#">4</a>
            </div>
        	<div class="left"><a href="#" class="next2"></a></div>
        	<div class="left"><a href="#" class="next"></a></div>
            <div class="left"><a href="#">Último </a></div>
        </div>
        
	</div>	
    
</div>
    
    
<script type="text/javascript" src="assets/js/jquery-filestyle.js"></script>
<script type="text/javascript" src="assets/js/aplication.js"></script>
<?php include"footer.php" ?>  


<!-- //popup -->
<div id="inline2" style="width:650px;display:none;">
	<div class="left col_line_right" style="width:350px;">
    	<h2 class="h2_b" style="font-size:18px; color:#999; margin-bottom:0px">Lider</h2>
    	<h2 class="h2_b" style="color:#662d91; margin-bottom:5px;">Rodrigo Romero Castro</h2>
    	<h3 class="h3_b" style="color:#1c75bc; font-size:16px; margin-top:0px; margin-bottom:20px">Fundación pies descalzos</h3>
    
    	<div class="txt_user">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent purus turpis, tincidunt in malesuada ut, mattis eget purus. Vivamus semper fringilla nunc, iaculis ultricies nibh scelerisque eget. Nam tellus orci, elementum et erat at, sollicitudin ultrices mauris. Maecenas id purus at nisi eleifend dignissim. Duis non mi id nisi venenatis ornare. Donec suscipit enim eget mauris pharetra, venenatis commodo elit congue. Maecenas non eleifend libero, non interdum nisi. Nunc a lorem ornare, volutpat nisi sed, molestie eros. Nam in interdum libero. Donec hendrerit vitae dui ut imperdiet.</p>
            <p>Vestibulum tempus id ante a eleifend. Morbi at elementum erat. Nam in nulla ac ante fermentum ultrices quis nec sem. Donec ultricies in est et dignissim. Donec sollicitudin tortor eget mauris pharetra, a congue metus feugiat. Integer velit risus, hendrerit et pulvinar eget, mollis a odio. Donec sed sodales nisi. Aenean accumsan tellus in est pretium euismod. Praesent eu risus eros. Donec pulvinar, eros vel tincidunt sagittis, eros velit tempus diam, et feugiat elit enim at augue.</p>
            <p>Sed adipiscing nisi ac justo suscipit lobortis. Suspendisse potenti. Integer a ullamcorper orci. Curabitur eget suscipit metus, et aliquam est. Praesent eget leo sed purus tempus eleifend sit amet a mauris. Suspendisse lacus dolor, rhoncus sed hendrerit at, laoreet eu turpis. Aliquam massa libero, pellentesque eget risus vel, vehicula tristique ipsum.</p>
        </div>
    </div>

	<div class="right" style="width:270px">    
        <div class="logo_fundacion">
        	<img src="assets/img/080.png" class="rounded_img" width="200" />
        </div>
        <div class="data_fundacion">
            <div class="dato" style="margin-bottom:10px"><span style="color:#00aeef; font-size:16px">Contactame en:</span></div>
            <div class="dato"><b>Email:</b> <a href="mailto:info@miemail.es" style="color:#602483"> info@romerorodrigo.com</a></div>
            <div class="dato"><b>Facebook: </b> /Píesdescalzos</div>
            <div class="dato"><b>Twitter: </b> /@píesdescalzos</div>
            <div class="dato"><b>Bogotá, Colombia</b></div>
        </div>
	</div>
    
</div>

</body>
</html>