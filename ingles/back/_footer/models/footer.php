<?php

class Footer extends DataMapper {

    public $model = 'footer';
    public $table = 'footer';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getFooter(){
        return $this->get();
    }
    
    public function saveFooter($object = "") {
        $this->direccion = $object['direccion'];
        $this->ciudad = $object['ciudad'];
        $this->telefono1 = $object['telefono1'];
        $this->telefono2 = $object['telefono2'];
        $this->pbx = $object['pbx'];
        $this->correo = $object['correo'];

        return $this->save();
    }
    
    public function getFooterById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateFooter($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

