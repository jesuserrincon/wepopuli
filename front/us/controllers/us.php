<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author rigobcastro
 * @author Brayan Acebo
 * @author Jose Fonseca
 */
class Us extends Front_Controller {

    public function __construct() {
        $this->load->library('session');
        $idioma = $this->session->userdata('lang');
        if($idioma == false){           
            return redirect('home/index');
        }
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index() {
        $this->load->model(array(
                    CMSPREFIX."quienes/quienes",
                    CMSPREFIX."lineas/lineas",
                    CMSPREFIX."puntos/puntos",
                    CMSPREFIX."servicios/servicios",
                    CMSPREFIX."directorios/directorios",
                    CMSPREFIX."redes/redes",
                    )
                );
        $lang = $this->session->userdata('lang');
        
        $cServi = new Servicios();
        $dServi = '';
        $cServi->limit(9, 0);
        $servi = $cServi->getServiciosByIdioma($lang);
        foreach ($servi as $data) {
            $dServi .='<li><a href="' . base_url() . 'services/detail/' . $data->id . '">' . $data->titulo . '</a></li>';
        }
        $cQuienes = new Quienes();
        $quienes = $cQuienes->getQuienesByIdioma($lang);
        
        $cLineas = new Lineas();
        $lineas = $cLineas->order_by('fecha', 'ACS')->get();
        $dLineas = '';
        foreach ($lineas as $data){
            $dLineas .='<li>
                            <a href="'.  base_url() .'uploads/lineas/new/'.$data->imagen_dos.'" rel="prettyPhoto"><img src="'.  base_url() .'uploads/lineas/new/'.$data->imagen.'" alt=""></a>
                            <span name="line-bottom"></span>';
            $fecha = explode('-', $data->fecha);
            $dLineas .='    <h4>'.$fecha[0].'</h4>
                            <span name="line-top"></span>
                        </li>';
        }
        
        $cDirectorios = new Directorios();
        $directorios = $cDirectorios->getDirectorios();
        $cant = $cDirectorios->count();
        $total = ($cant / 2);
       
       $dDirectorios = '';
        for($j = 0 ; $j < ceil($total) ; $j++){
           $h = $j*2;
           $cDirectorios->limit(2,$h)->order_by("id", "ASC");           
           $directorios = $cDirectorios->getDirectorios();        
        $i = 1;
            $dDirectorios .='<div class="clearfix">';
            foreach ($directorios as $data){
                if($i == 1){
                    $dDirectorios .='<ul class="left">';
                }else{
                    $dDirectorios .='<ul class="right">';
                }
                $dDirectorios .='
                                    <li><h4>'.$data->ciudad.'</h4></li>
                                    <li class="footer-icon-1">'.nl2br($data->direccion).'</li>
                                    <li class="footer-icon-2">'.$data->telefono.'</li>
                                    <li class="footer-icon-3"><a href="mailto:'.$data->email.'">'.$data->email.'</a></li>
                                </ul>';
            $i = $i + 1;
            }
            $dDirectorios .='</div>';
        }
        
        $cRedes = new Redes();
        $redes = $cRedes->getRedesById(1);
        
        $cPuntos = new Puntos();
        $puntos = $cPuntos->getPuntos();
        $dPuntos ="";
        foreach ($puntos as $data){
            $dPuntos .="var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(".$data->latitud.",".$data->longitud."),
                        map: map,
                        title: '".$data->titulo."',
                        icon:('assets/img/icon_map.png')
                    });";
        }
        
        $this->_data["seccion"] = 'us';
        $this->_data["quienes"] = $quienes;
        $this->_data["lineas"] = $dLineas;
        $this->_data["puntos"] = $dPuntos;
        $this->_data["directorios"] = $dDirectorios;
        $this->_data["redes"] = $redes;    
        $this->_data["servi"] = $dServi;    
        $this->build('nosotros');
    }

    // ----------------------------------------------------------------------
   
}
