-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 24-09-2013 a las 08:44:31
-- Versión del servidor: 5.5.31-0ubuntu0.13.04.1
-- Versión de PHP: 5.4.9-4ubuntu2.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `usuariodg_estrategias`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_antecedentes`
--

CREATE TABLE IF NOT EXISTS `cms_antecedentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_antecedentes_cms_idiomas` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cms_antecedentes`
--

INSERT INTO `cms_antecedentes` (`id`, `idioma_id`, `titulo`, `imagen`) VALUES
(2, 1, 'Kimberly Clark', '3bcca63725a81f2fdb924b0b2ec1aa8b.png'),
(3, 1, 'Casa Luker ', '7f112d60032ee421eaba0c4b518f1a33.png'),
(4, 1, 'Colgate Palmolive', '87bf4a11b1746625beb9ed4571f1bf7f.png'),
(5, 1, 'Alimentos Polar', 'a6bd123b1c1d92236d80ed1a1c8512e6.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_banners`
--

CREATE TABLE IF NOT EXISTS `cms_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_banners_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `cms_banners`
--

INSERT INTO `cms_banners` (`id`, `idioma_id`, `imagen`, `link`) VALUES
(1, 1, '73bc0d739dcc9e627f6db4c48afdde89.jpg', ''),
(6, 1, '182b107b86caa2231eaa1787bf1aa5c1.png', ''),
(7, 2, 'a376ab5b85ec871c1708bb433274065e.jpg', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_campanas`
--

CREATE TABLE IF NOT EXISTS `cms_campanas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `antecedente_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_campanas_cms_antecedentes1` (`antecedente_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `cms_campanas`
--

INSERT INTO `cms_campanas` (`id`, `antecedente_id`, `titulo`, `texto`) VALUES
(6, 2, 'Regreso a Clases', '<div><div style="text-align: left;"><span style="font-size: 10pt;"><b>Actividad:</b> Regreso a Clases con Huggies, Juega y Gana</span></div><span style="font-size: 10pt;"><div style="text-align: left;"><span style="font-size: 10pt;"><b>Marca:</b> Huggies&nbsp;</span></div></span><span style="font-size: 10pt;"><div style="text-align: left;"><span style="font-size: 10pt;"><b>Canal:</b> Pañaleras</span></div></span><span style="font-size: 10pt;"><div style="text-align: left;"><span style="font-size: 10pt;"><b>Cubrimiento:</b> Bogotá, Eje Cafetero, Cali, Medellín, Bucaramanga, Cúcuta, Villavicencio , Neiva, Ibagué, Girardot, Fusa.</span></div></span></div><div style="font-weight: normal;"><br></div>\n\n<img src="http://dayone.ca/resource/logo-15.png" height="60px">\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_contactos`
--

CREATE TABLE IF NOT EXISTS `cms_contactos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_contactos_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_contactos`
--

INSERT INTO `cms_contactos` (`id`, `idioma_id`, `titulo`, `texto`, `imagen`) VALUES
(1, 1, 'ontactos', '<br>', 'ade6bae2df6b5a16c507e965a7d15a70.png'),
(2, 2, 'Contact Us', '<br>', '95e686c628087467283fd25f9060cb01.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_directorios`
--

CREATE TABLE IF NOT EXISTS `cms_directorios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_directorios`
--

INSERT INTO `cms_directorios` (`id`, `ciudad`, `direccion`, `telefono`, `email`) VALUES
(1, 'Bogotá', 'Calle  97 No. 10-39 Of 306 \nEdificio Comercial 97', '(0571) 7443925 ', 'info@estrategiasymercadeo.com'),
(2, 'Cali', 'Av.6 A Bis No. 35N-100 Of 604 ', '(0572) 6594084', 'info@estrategiasymercadeo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_groups`
--

CREATE TABLE IF NOT EXISTS `cms_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cms_groups`
--

INSERT INTO `cms_groups` (`id`, `name`, `description`) VALUES
(1, 'superadmin', 'Super Administrador'),
(2, 'admin', 'Administrador'),
(3, 'usuarios', 'Usuarios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_idiomas`
--

CREATE TABLE IF NOT EXISTS `cms_idiomas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_idiomas`
--

INSERT INTO `cms_idiomas` (`id`, `titulo`) VALUES
(1, 'Español'),
(2, 'Ingles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_imgcampanas`
--

CREATE TABLE IF NOT EXISTS `cms_imgcampanas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campana_id` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_imgcampanas_cms_campanas1` (`campana_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `cms_imgcampanas`
--

INSERT INTO `cms_imgcampanas` (`id`, `campana_id`, `imagen`) VALUES
(7, 6, 'fa21ad4b126ce0e905def6ea1ddaf0d9.JPG'),
(8, 6, '9c3cbf90d6eb1384504cd23ffd4194e7.JPG'),
(9, 6, '9ae21aebbabd946453de11aad3a28625.JPG'),
(10, 6, '5e2211dcbc4c4af0aabe28c5f3884b15.JPG');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_imgservicios`
--

CREATE TABLE IF NOT EXISTS `cms_imgservicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servicio_id` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_lineas`
--

CREATE TABLE IF NOT EXISTS `cms_lineas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `fecha` date NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `imagen_dos` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_login_attempts`
--

CREATE TABLE IF NOT EXISTS `cms_login_attempts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_logos`
--

CREATE TABLE IF NOT EXISTS `cms_logos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_logos_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `cms_logos`
--

INSERT INTO `cms_logos` (`id`, `idioma_id`, `imagen`, `link`) VALUES
(8, 2, '623fb33375a100ac7ac107ef1a27b6bf.png', 'http://www.google.com'),
(9, 2, 'bf6cb65071bf543fee1b1f997478324c.png', 'http://www.google.com'),
(10, 2, 'e195519859268f78a09bfb7a667a7fd1.png', 'http://www.google.com'),
(12, 1, '521077f1955c7de5377abda473184d37.png', 'http://www.google.com'),
(20, 1, '69f4ded318cfa2994d0c0e27489a63c6.png', 'http://www.google.com'),
(23, 1, 'f173936e9a6cbc0b90e6ce1d1313abd8.png', 'http://www.google.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menu`
--

CREATE TABLE IF NOT EXISTS `cms_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` text,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `cms_menu`
--

INSERT INTO `cms_menu` (`id`, `title`, `url`, `icon`) VALUES
(12, 'Destacados', 'cms/destacados', 'pictures_folder');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_newsletters`
--

CREATE TABLE IF NOT EXISTS `cms_newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cms_newsletters`
--

INSERT INTO `cms_newsletters` (`id`, `email`) VALUES
(2, 'jucasecu_92@hotmail.com'),
(3, 'danielvillareal@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_puntos`
--

CREATE TABLE IF NOT EXISTS `cms_puntos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `latitud` varchar(100) NOT NULL,
  `longitud` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_puntos`
--

INSERT INTO `cms_puntos` (`id`, `titulo`, `latitud`, `longitud`) VALUES
(1, 'Oficina Bogotá', '4.624335270608115', '-74.07394409179688'),
(2, 'cali', '3.4256915244180743', '-76.5087890625');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_quienes`
--

CREATE TABLE IF NOT EXISTS `cms_quienes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_quienes_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cms_quienes`
--

INSERT INTO `cms_quienes` (`id`, `idioma_id`, `titulo`, `texto`, `imagen`) VALUES
(1, 1, 'Nosotros', '<div style="text-align: justify;">Estrategias y Mercadeo es una compañía Colombiana especializada que nació, creció y se ha desarrollado en el Canal Tradicional (TAT) por más de 18 años. &nbsp;Presta servicios integrales en estrategia, táctica, logística e implementación con cubrimiento nacional , buscando aumentar la Colocación y Rotación de &nbsp;P/S.</div><div style="text-align: justify;">&nbsp;</div><div style="text-align: justify;">El conocimiento del Canal (Tiendas ,Mayoristas y Distribuidores) y de el Shopper en el país nos permite diseñar e implementar campañas que permitan a su marca estar presente en el momento de verdad (compra) mejorando los resultados (Ventas) mediante una optima rotación (Consumidor).</div><div style="text-align: justify;">&nbsp;</div><div style="text-align: justify;">Nuestro equipo está liderado por coordinadores de campo (nomina directa y permanente) con gran experiencia, capacidad, &nbsp;habilidad, conocimiento en la ejecución y orientación de campañas que soportan los resultados y efectividad de las mismas.</div><div style="text-align: justify;">&nbsp;</div><div style="text-align: justify;">Entrega diaria, semanal y mensual de reportes de actividad que permitan detectar oportunidades y un seguimiento completo de la campaña.</div><div style="text-align: justify;">&nbsp;</div><div style="text-align: justify;">Pólizas de cumplimiento respaldan y ratifican el éxito de nuestro trabajo.</div><div><br></div>', '34faad76d14361dc31e9a10f90c7ef52.jpg'),
(2, 2, 'About Us', 'afasdf engdfghgfgfnvnb', '4fa47a6873d47353eddf7c61e91111e2.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_redes`
--

CREATE TABLE IF NOT EXISTS `cms_redes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `twitter` text NOT NULL,
  `facebook` text NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cms_redes`
--

INSERT INTO `cms_redes` (`id`, `twitter`, `facebook`, `email`) VALUES
(1, 'https://www.google.com.co/', 'http://www.facebook.com/estrategiasymercadeo', ' https://www.google.com/a/estrategiasymercadeo.com/ServiceLogin?service=mail&passive=true&rm=false&continue=http%3A%2F%2Fmail.google.com%2Fa%2Festrategiasymercadeo.com&ltmpl=default&ltmplcache=2&hl=es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_servicios`
--

CREATE TABLE IF NOT EXISTS `cms_servicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `link` text NOT NULL,
  `Contactos` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_servicios_cms_idiomas1` (`idioma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `cms_servicios`
--

INSERT INTO `cms_servicios` (`id`, `idioma_id`, `titulo`, `texto`, `imagen`, `link`, `Contactos`) VALUES
(7, 1, 'Ruta Ganadora', '<div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;"><b style="font-size: 10pt;"><font size="4">La Ruta Ganadora</font></b></div><div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;"><br></div><div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;">Campaña cooperativa para máximo (3) marcas de categorías diferentes que visita un número determinado de tiendas donde se genera siembra y rotación de producto mediante actividad promocional.</div><div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;"><br></div><div style="font-size: 10pt;"><div><font face="Arial, Verdana" size="2"><b>Objetivos</b>&nbsp;</font></div><div style="font-weight: normal;"><font face="Arial, Verdana" size="2"><br></font></div><div style="font-weight: normal;"><ul><li><span style="font-size: small;">Apoyar la labor de su fuerza de ventas y distribuidor (es) en los diferentes circuitos del país, mediante la siembra de producto.</span></li></ul></div><div style="font-weight: normal;"><font face="Arial, Verdana" size="2"><br></font></div><div style="font-weight: normal;"><ul><li><span style="font-size: small;">Impulsar la rotación del producto codificado para disipar los temores del tendero</span></li></ul></div><div style="font-weight: normal;"><font face="Arial, Verdana" size="2"><br></font></div><div style="font-weight: normal;"><ul><li><span style="font-size: small;">Generar bases de datos de las tiendas visitadas sean manejantes o no.</span></li></ul></div><div style="font-weight: normal;"><font face="Arial, Verdana" size="2"><br></font></div><div style="font-weight: normal;"><ul><li><span style="font-size: small;">Incentivar la fidelidad del consumidor mediante redención de empaques vacíos por premios.</span></li></ul></div></div><div style="font-weight: normal; font-size: 10pt; font-family: Arial, Verdana; font-style: normal; font-variant: normal; line-height: normal;"><br></div>\n\n', '07a34bd2a3f6ba094c9ca7e78e4f770e.png', '', '0'),
(8, 1, 'Estrategias y P.O.P', 'Lorem ipsum prueba<div><br></div>', '8306813392757fdc1c5e2566906f819c.png', 'https://www.google.com.mx/', 'prueba Contactos<div><br></div>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_sessions`
--

CREATE TABLE IF NOT EXISTS `cms_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_sessions`
--

INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('033ab5e1e935fc565476ba1e1a12b887', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376590576, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('05471b64b755b0d53697153c9fc99da4', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379352692, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('0643e90b908e852f55b293ab7e16abbb', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377119169, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('070abc5dae3467dd8fa6cb4237861734', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377117417, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('0bee5cbc43464119cc30b1548119d18e', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36', 1373481507, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('0d2822a5b99feb9bfd25d8b6e55440c4', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377198676, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('0d5c9d8ab8bd591cc3595400d11d883e', '65.52.100.214', '0', 1373508576, ''),
('0f39b943e4c98e6b789ba0e08b9ee7ff', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377532966, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";}'),
('1202691701b63e50594dd3f280a4b4dd', '181.54.113.54', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36', 1373511291, ''),
('14c5c4196af90c309d8fabfef2bd0a53', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378834699, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('1637fb78be2d576245f76d55c791c94c', '190.60.239.146', 'Shockwave Flash', 1378848611, ''),
('191d7602be32ec9b0a8e428946652e02', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377199367, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('1bb8ebbe1634565014b057283925430b', '181.48.71.245', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378934809, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('21d947d201ad7222cc895047cf66e1f6', '181.135.215.106', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376345843, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:37:"gerencia.general@blpconstructores.com";s:8:"username";s:16:"Campo Elias Leal";s:5:"email";s:37:"gerencia.general@blpconstructores.com";s:7:"user_id";s:1:"7";s:14:"old_last_login";s:4:"2013";}'),
('27d3815f3846077a8e45ee80e0f104ee', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378736932, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2e24048da95b5df375a04b99dc92d69e', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378789335, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('2e8302aa0d835b81703b2c0337b84d77', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377525111, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('308493fbece54ee38c54c0e34a52bf1e', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378478751, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('30ce1f9dd1eb90367f113738bad781c1', '181.68.209.250', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375508411, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('3236273ea21e01246de83a83a657b22c', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377033792, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('3dbaced8dfa5caaf91edc645f8a57709', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377199484, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('3e897bd67fdbac8101735de3fea29baa', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377101112, 'a:5:{s:8:"identity";s:21:"prueba@imaginamos.com";s:8:"username";s:6:"Prueba";s:5:"email";s:21:"prueba@imaginamos.com";s:7:"user_id";s:1:"9";s:14:"old_last_login";s:4:"2013";}'),
('466b35d2bcdee6c64cd60f1446ee3734', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377208623, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('4adba7e5c17c422757b1301e655d8ea6', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376077379, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:21:"prueba@imaginamos.com";s:8:"username";s:17:"Prueba Imaginamos";s:5:"email";s:21:"prueba@imaginamos.com";s:7:"user_id";s:1:"6";s:14:"old_last_login";s:4:"2013";}'),
('4d3be568cf1b36a6d87d470f8e5f8189', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379362508, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('5125bea56a4637d590acd7b9861f2d83', '190.242.126.59', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378942569, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('532229848f9faaa0980877c1e04d54a8', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376593249, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('56ac92f2a3f360ec36cd5520b1c9a39d', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377012514, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('572b698b94a76e61e385a6124005b30e', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376077498, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('57a8e30c7f0be8bad006646bbc5cdcf9', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377199325, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('580efdacdc82a6e1ecafbde9cc18a97f', '186.168.32.173', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378772730, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('5866242aa1115a8c7285e18c4856a5ab', '181.68.209.250', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375504918, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('64d2755e53e91b43ba6e5438562ba6c8', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378830710, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('6559b732e3575a8e4e0b0fc8b0b0c448', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377528052, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('66950fe47b219d83ae05bc7f9fbd4aa0', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377525217, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('6e5df7c72c431fc6c072c8d0b30f961f', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375742013, 'a:5:{s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";}'),
('70746272bf483c99e88843a666f04350', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377028848, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('707c0ff42a75c124cf99583ecba894d9', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378829750, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('749e3233c6b8388651d8e188394e9a93', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1378819629, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('755d1b6c6802cbc4a75406f79db24a9c', '190.24.187.240', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.59.8 (KHTML, like Gecko) Version/5.1.9 Safari/534.59.8', 1378760604, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:1:"1";}'),
('797d049b4b5fd374a5e7fd46041e9581', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379352693, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('7a396216f92b754cc36538fedcd50763', '181.48.71.245', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378747627, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('7e33cc1d1e468b06a81bda10d9450b6a', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1378853115, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('812a20ba6ce24e59c350dcf27b6d256b', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1378746845, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('818bbd2e770694371d8a7df7ab94abb1', '186.168.32.173', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378788637, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('84fab7291d9124dfc8d626dba92b113b', '181.48.71.245', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1379362730, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"2";}'),
('8c3c84311ec395c2fc8ccd73bf3a2001', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1379106819, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('8eb8f21f379ea1a8e2eee91d1f813dee', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378398241, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('8f6bffe6b096319ab138a964b6d4df3f', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375743078, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('903678f0b89996d74c3d7d1d76a37e57', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380029347, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('934b580d236681e1975dc2a8c3f566e3', '186.29.119.248', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.59.8 (KHTML, like Gecko) Version/5.1.9 Safari/534.59.8', 1376325122, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:21:"prueba@imaginamos.com";s:8:"username";s:6:"Prueba";s:5:"email";s:21:"prueba@imaginamos.com";s:7:"user_id";s:1:"9";s:14:"old_last_login";s:4:"2013";}'),
('98ab739eae834aa5050be631bfac79ce', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379361899, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"1";}'),
('9a82f0ce99b090a623ccd87edaa4291d', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0', 1378839642, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('9a87d65f7302dee325202d3249793aa1', '181.48.71.245', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1379106820, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('a34289d25f8182e7265ad2c5d01bda01', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378841680, 'a:7:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";s:4:"lang";i:1;}'),
('a68db0b1760af068c34bb629942a04ec', '181.69.217.146', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377143187, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('ac942fb780178075facfc7dd56a149dd', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379352693, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('adf25c427d1684b13ce8bc849b876100', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36', 1378751130, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('aec605889706d851655f91666c0186c7', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1378500741, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('b068e1face5d289b8e82d9b758cb133c', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377198730, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('b13cd6f3a037398de7818153898482d9', '190.66.80.112', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1375940978, ''),
('b73de278ad87f5db36909a3e313abd4e', '181.48.71.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22', 1379968374, 'a:7:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";s:4:"lang";s:1:"1";}'),
('b876b0e17186cf30b101b46aa2767bc3', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377115918, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('ba494b9c997c6f2d18f9035213862f84', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376324643, ''),
('ba8e9f9ada8d3f290ec8c1f1bc371d79', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377116463, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('bd5fd34082380895abdacfbc030fd35a', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36', 1380029420, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"2";}'),
('bee5647abe1bf9d8e5dc0fe005819150', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377204305, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('bef9d45f6e0c6626ced634b24a279911', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378419902, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('c3d763daed6424c37977ce82a6ebc2eb', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377119317, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('c5f27c10dc46d38044ed1587f731386d', '190.147.153.73', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B350', 1379279292, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('c753e1c72d7eb75388ebfee3e1848aa0', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377209349, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('c8bdb52d5ee30b789d655d29fcbbc303', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1376593249, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('c9a9c7b815944ee3f137de350d44d15a', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377119443, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('cc61a15f75dbcbb319a401eb752e7e36', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1378483413, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('d084c40da530ef8a3bb275dc0d64a327', '190.147.153.73', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465', 1379631965, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d262e59a19bfa2eab75f8b385b36dc65', '181.48.71.245', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1379955357, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('d54e0ea3e6d961dec17e8b035bf6e847', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379350853, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";i:1;}'),
('d6307c05119ae9f80ac95c80bca42756', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1378846765, 'a:5:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";s:4:"lang";s:1:"2";}'),
('d71cdd43b4592b8dd7ed6b17cac48c87', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377120361, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('d7329f284a4824b6ced395ac7617b622', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377118779, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('daddff7621e936c63fbb2078ba99b9ba', '190.60.239.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0', 1378848592, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('dbf2d3ac964757591cee9d4b5fe5b5aa', '181.69.217.146', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377145681, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('e78f577145e1f33e5f970fea256f86fd', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377119785, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('edf0b5f20f1a0f3941be284959b6fbbd', '186.80.87.225', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329', 1378873958, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('eff6e6a883b41c9b388ddb556f88ad19', '190.27.71.197', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_2 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B146', 1379245071, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f13ff313b029d10627f3c257e7c1e6b4', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377269455, ''),
('f5cdf7e7c5346dc0cb1625399745d483', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377118033, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('f7ca798f72fbf6d1d73a80acb025c7cf', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377113005, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:18:"cms@imaginamos.com";s:2:"id";s:1:"5";s:7:"user_id";s:1:"5";}'),
('f7fef4569f300af5e07befcbd6681976', '181.48.71.245', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', 1378747944, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";i:1;}'),
('f8955d3e2a990aaecde1d239db8500dc', '181.54.113.54', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36', 1379429811, 'a:2:{s:9:"user_data";s:0:"";s:4:"lang";s:1:"1";}'),
('f9a8c02816c0ef25b088efd04410b680', '200.119.7.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', 1377204200, 'a:4:{s:9:"user_data";s:0:"";s:5:"email";s:21:"prueba@imaginamos.com";s:2:"id";s:1:"9";s:7:"user_id";s:1:"9";}'),
('fbdf79f0e6a1e05d5776ca3f8a724817', '190.60.239.146', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36', 1377115848, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:18:"cms@imaginamos.com";s:8:"username";s:13:"administrator";s:5:"email";s:18:"cms@imaginamos.com";s:7:"user_id";s:1:"5";s:14:"old_last_login";s:4:"2013";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_users`
--

CREATE TABLE IF NOT EXISTS `cms_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `cms_users`
--

INSERT INTO `cms_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(5, '\0\0', 'administrator', '092e624ccaf41c1b9c0dd32a1041043a82507bc7', 'e0efe63787', 'cms@imaginamos.com', NULL, NULL, NULL, '1218e83c71363e71c292b071dace76d3f56b47af', 1343253917, 2013, 1, NULL, NULL, NULL, NULL),
(9, '¾<ï’', 'Prueba', '092e624ccaf41c1b9c0dd32a1041043a82507bc7', 'e0efe63787', 'prueba@imaginamos.com', NULL, NULL, NULL, '1218e83c71363e71c292b071dace76d3f56b47af', 2013, 2013, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_users_groups`
--

CREATE TABLE IF NOT EXISTS `cms_users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_users_groups` (`user_id`),
  KEY `group_users_groups` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `cms_users_groups`
--

INSERT INTO `cms_users_groups` (`id`, `user_id`, `group_id`) VALUES
(5, 5, 1),
(9, 9, 2);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cms_antecedentes`
--
ALTER TABLE `cms_antecedentes`
  ADD CONSTRAINT `fk_cms_antecedentes_cms_idiomas` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_banners`
--
ALTER TABLE `cms_banners`
  ADD CONSTRAINT `fk_cms_banners_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_campanas`
--
ALTER TABLE `cms_campanas`
  ADD CONSTRAINT `fk_cms_campanas_cms_antecedentes1` FOREIGN KEY (`antecedente_id`) REFERENCES `cms_antecedentes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_contactos`
--
ALTER TABLE `cms_contactos`
  ADD CONSTRAINT `fk_cms_contactos_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_imgcampanas`
--
ALTER TABLE `cms_imgcampanas`
  ADD CONSTRAINT `fk_cms_imgcampanas_cms_campanas1` FOREIGN KEY (`campana_id`) REFERENCES `cms_campanas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_imgservicios`
--
ALTER TABLE `cms_imgservicios`
  ADD CONSTRAINT `fk_cms_imgservicios_cms_servicios1` FOREIGN KEY (`id`) REFERENCES `cms_servicios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_logos`
--
ALTER TABLE `cms_logos`
  ADD CONSTRAINT `fk_cms_logos_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_quienes`
--
ALTER TABLE `cms_quienes`
  ADD CONSTRAINT `fk_cms_quienes_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_servicios`
--
ALTER TABLE `cms_servicios`
  ADD CONSTRAINT `fk_cms_servicios_cms_idiomas1` FOREIGN KEY (`idioma_id`) REFERENCES `cms_idiomas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cms_users_groups`
--
ALTER TABLE `cms_users_groups`
  ADD CONSTRAINT `group_users_groups` FOREIGN KEY (`group_id`) REFERENCES `cms_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_users_groups` FOREIGN KEY (`user_id`) REFERENCES `cms_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
