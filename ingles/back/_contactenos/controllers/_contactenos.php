<?php

class _Contactenos extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "CONTACTENOS";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {

        $this->_data['save'] = $value;
        $b = new Contactenos();
        $info = $b->getContactenos();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('contactenos');

    }

    public function add() {
        $this->_data['nombremodulo'] = "COLORES / New";
        $this->build('contactenos_new');
    }

    public function new_contactenos() {
        $datos = array(
            'texto' => $this->input->post('texto'),
            'informacion' => $this->input->post('informacion'),
            'mapa' => $this->input->post('mapa'),
        );

        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'contactenos';
        $b = new Contactenos();

        if ($datos['mision'] != ''){

            $this->_data['save'] = FALSE;
            $build = 'contactenos_new';
            if ($b->saveContactenos($datos)){
                $build = 'contactenos';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getContactenos();
        $this->_data["info"] = $info;
        return redirect('cms/contactenos/index/'.$save);

    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'contactenos/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."contactenos/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;
            $resize['width']	 =300;
            $resize['height']	= 160;
            $resize['new_image'] = UPLOADSFOLDER."contactenos/new/".$data["data"]["file_name"];

            $this->load->library('image_lib', $resize);

            if ( ! $this->image_lib->resize())
            {
                $data = array(
                    'ok' => false,
                    'resize' => $this->image_lib->display_errors()
                );
            }else{
                $data = array(
                    'ok' => true,
                    'resize' => true,
                    'data' => $this->upload->data()
                );
            }
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Contactenos();
        $dat = $b->getContactenosById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("contactenos_edit");
    }
    
    public function update_contactenos(){
        $b = new Contactenos();
        $datos = array(
            'texto' => $this->input->post('texto'),
            'informacion' => $this->input->post('informacion'),
            'mapa' => $this->input->post('mapa'),
            'id' => $this->input->post('id')
        );
        $update = $b->updateContactenos($datos); 
        $a = FALSE;

        if ($update){
            $a = TRUE;
        }
        return redirect("cms/contactenos/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Contactenos();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/contactenos/index/".$return);
    }

}

