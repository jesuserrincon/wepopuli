<?php

class Textos extends DataMapper {

    public $model = 'textos';
    public $table = 'textos';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getTextos(){
        return $this->get();
    }
    
    public function saveTextos($object = "") {
        $this->titulo = $object['titulo'];
        $this->texto = $object['texto'];
        return $this->save();
    }
    
    public function getTextosById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateTextos($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

