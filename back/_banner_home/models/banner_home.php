<?php

class Banner_home extends DataMapper {

    public $model = 'banner_home';
    public $table = 'banner_home';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getBanner_home(){
        return $this->get();
    }
    
    
    public function getBanner_home_by_tipo($id){
        return $this->query('SELECT * FROM cms_banner_home WHERE tipo = '.$id);
    }
    
    
    public function saveBanner_home($object = "") {
        $this->link = $object['link'];
        $this->texto = $object['texto'];
        $this->imagen = $object['imagen'];
        $this->titulo = $object['titulo'];
        $this->pie_de_pagina = $object['pie_de_pagina'];
        $this->seccion = $object['seccion'];
        $this->tipo = $object['tipo'];
        return $this->save();
    }
    
    public function getBanner_homeById($id = ""){
        
        return $this->get_by_id($id);
    }
    
    public function updateBanner_home($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function eliminar($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }

}

