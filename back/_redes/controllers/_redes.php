<?php

class _Redes extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('imgs');
        $this->_data['nombremodulo'] = "REDES SOCIALES";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ----------------------------------------------------------------------

    public function index($value = '') {


        $this->_data['save'] = $value;
        $b = new Redes();
        $info = $b->getRedes();
        $this->_data["info"] = $info;
        $this->_data["count"] = $b->count();
        return $this->build('redes');
    }

    public function add() {
        $this->_data['nombremodulo'] = "REDES / New";
        $this->build('redes_new');
    }

    public function new_Redes() {
        $datos = array(
            'youtube' => $this->input->post('youtube'),
            'facebook' => $this->input->post('facebook'),
            'twitter' => $this->input->post('twitter'),
            'pinterest' => $this->input->post('pinterest'),
        );
        $this->_data['save'] = FALSE;
        $save = FALSE;
        $build = 'redes';
        $b = new Redes();
        if ($datos['youtube'] != ''){
            
            $this->_data['save'] = FALSE;
            $build = 'redes_new';
            if ($b->saveRedes($datos)){
                $build = 'redes';
                $this->_data["save"] = TRUE;
                $save = TRUE;
            }
        }
        $info = $b->getRedes();
        $this->_data["info"] = $info;
        return redirect('cms/redes/index/'.$save);
        
    }

    public function upload($width = '',$height = '') {
        $config = array(
            'allowed_types' => 'gif|jpg|png|jpeg|rgb|psd',
            'upload_path' => UPLOADSFOLDER.'redes/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {

            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );

        } else {
            $data = array(
                'data' => $this->upload->data()
            );
            $resize['image_library'] = 'GD2';
            $resize['source_image']	= UPLOADSFOLDER."redes/".$data["data"]["file_name"];
            $resize['maintain_ratio'] = TRUE;

            $this->load->library('image_lib', $resize);

            $data = array(
                'ok' => true,
                'resize' => true,
                'data' => $this->upload->data()
            );

            //redimension
            $redimension = new imgs();
            if(!$redimension->rimg('redes/'.$data["data"]["file_name"], array('longside'=>'redes/new','width' => 272, 'height' => 304,
                'alt' => $data["data"]["file_name"], 'title' => $data["data"]["file_name"]))){
                $data = array(
                    'ok' => false,
                    'resize' => false,
                    'data' => 'error ..'
                );
            }
            //fin Redimension
        }
        echo json_encode($data);
    }
    
    public function edit($id = "",$update = ""){
        $b = new Redes();
        $dat = $b->getRedesById($id);
        $this->_data["info"] = $dat;
        $this->_data["update"] = $update;
        return $this->build("redes_edit");
    }
    
    public function update_redes(){
        $b = new Redes();
        $datos = array(
            'youtube' => $this->input->post('youtube'),
            'facebook' => $this->input->post('facebook'),
            'twitter' => $this->input->post('twitter'),
            'pinterest' => $this->input->post('pinterest'),
            'id' => $this->input->post('id')

        );

                    $update = $b->updateRedes($datos);

        if ($update){
            $a = TRUE;
        }
        return redirect("cms/redes/edit/".$this->input->post("id")."/".$a);
    }
    
    public function delete($id = ""){
        $b = new Redes();
        $return = FALSE;
        if ($b->eliminar($id)){
            $return = TRUE;
        };
        return redirect("cms/redes/index/".$return);
    }

}

