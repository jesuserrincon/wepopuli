<link rel="stylesheet" href="../captcha/css/bootstrap-responsive.min.css">
<link rel="stylesheet" href="../captcha/css/main.css">
<script data-main="js/app" src="js/vendor/require.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
$(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    yearRange: '1930:2000',
    dateFormat: 'yy-mm-dd'
});
});
</script>
<script type="text/javascript" src="js/source/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="js/source/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="js/source/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript" src="js/fancys.js"></script>
  
<div class="box_a pad_box clearfix form_ax1">
    <form method="POST" name="formularioregistro" id="formularioregistro" action="registro/UsuarioAdd"> 
	<div class="col_a">
        <h2 class="h2_b" style="margin-left:80px">Log in</h2>
        <div class="ingresar">

        	<div class="int_pad font_12">
                <div class="label">E-mail  <span class="requiered">*</span></div>
                <input type="text" class="input_a" id="emaillog" name="emaillog"  />
                
                <div class="label">Password  <span class="requiered">*</span></div>
                <input type="password" id="password"  name="password" onkeypress="checkKey(event);" class="input_a" />
                <div style="font-size:11px; margin-top:-10px; margin-bottom:16px;"><span class="requiered">*</span> Indicates required

</div>
                
                <a href="#olvido" class="right link_a fancybox">Forgot Password? Click Here</a>
                <div class="clear"></div>
                <!--a class="recordarme right">Recordarme</a-->
            </div>
            <div class="div_line" style="margin:10px 0px;"></div>
            <a href="javascript:conectarse()" class="bt_green">Log in</a>	

        </div>
    
    </div>
    
    <div class="col_b">
    	<h2 class="h2_b">New User? Sign up!</h2>
        
        <div class="box_shadow clearfix">
            <div class="col_int_b_1 clearfix">
                <div class="label">Name <span class="requiered">*</span></div>
                <input type="text" class="input_b" name="nombres" tabindex="1" id="nombres" />
                <div class="label">Type of identification <span class="requiered">*</span></div>
                <select tabindex="3" class="select" id="tipo_identificacion" name="tipo_identificacion" onchange="verificarCedula($('#cedula'))" >
                    <option value="Tarjeta de identidad">Child ID Card</option>
                    <option value="Cédula">National ID Number</option>
                    <option value="Cédula de extrajería">Resident ID Number</option>
                    <option value="Pasaporte">Passport </option>
                </select>
                <div class="label">Country of Nationality <span class="requiered">*</span></div>
                 <select  tabindex=5 class="select" name="pais_nacionalidad" id="pais_nacionalidad">
                    <option value="1">Colombia</option>
                </select>

				<div class="label">Province/State <span class="requiered">*</span></div>
               <select class="select" tabindex="7" name="departamento" id="departamento">
                    <option value="1">Bogota D.C</option>
                </select>

                <div class="label">Address <span class="requiered">*</span></div>
                <input type="text" tabindex="9" class="input_b"    name="direccion" id="direccion"/>


              <div class="label">Phone 2 (Optional)</div>
                <input type="text" tabindex="11" class="input_b"    name="telefono2" id="telefono2" onkeypress="ValidaSoloNumeros()" />
              
                <div class="label">Sex <span class="requiered">*</span></div>
                <select tabindex="13" class="select" name="sexo" id="sexo">
                    <option value="Female">Female</option>
                    <option value="masculino">Male </option>
                </select>
                
                <div class="label">Facebook (Optional)</div>
                <input tabindex="15" type="text" class="input_b" name="facebook" id="facebook"/>


                <div class="label">E-mail <span class="requiered">*</span></div>
                <input type="text" tabindex="17" class="input_b" onchange="verificarCorreo2(this)"  name="email" id="email"/>


                <div class="label">Password <span class="requiered">*</span></div>
                <input tabindex="19" type="password" class="input_b"  name="clave" id="clave" />

                
                <div class="label">Enter Captcha <span class="requiered">*</span></div>
                <div id="captcha-wrap">
	
                    <input type="text" tabindex="21" name="captcha" id="captchatext" class="input_b" style="float:left; width:78px;">
                <img onclick="javascript:cambiarcap()" src="img/084.png" alt="refresh captcha" id="refresh-captcha" style="float: left; cursor:pointer" />
	
    			<img src="../captcha/php/newCaptcha.php" alt="" id="captcha" style="float:left;" height="42" width="120" />


                </div>
                
                <div class="clear" style="height:5px"></div>
                <div style="font-size:11px; margin-top:-10px; margin-bottom:10px;"><span class="requiered">*</span> Indicates required</div>              

            </div>
            
            <div class="col_int_b_1 clearfix right">
                <div class="label">Last Name <span class="requiered">*</span></div>
                <input type="text" class="input_b" tabindex="2"  name="apellidos" id="apellidos" />
                
                <div class="label">Identification Number <span class="requiered">*</span></div>
                <input type="text" tabindex="4" class="input_b" onBlur="verificarCedula(this)"  name="cedula" id="cedula" onkeypress="ValidaSoloNumeros()" />

                <div class="label">Residence Country <span class="requiered">*</span></div>
                 <select tabindex="6" class="select" name="pais" id="pais">
                    <option value="1">Colombia</option>
                </select>

                <div class="label">City/Town <span class="requiered">*</span></div>
                <select  tabindex="8" class="select" name="ciudad" id="ciudad">
                    <option value="1">Bogota D.C</option>
                </select>

                <div class="label">Phone 1 <span class="requiered">*</span></div>
                <input type="text" tabindex="10"  class="input_b"   name="telefono" id="telefono" onkeypress="ValidaSoloNumeros()" />


                <div class="label">Date of Birth  <span class="requiered">*</span></div>
                <input type="text" tabindex="12" class="input_b datepicker"   name="fecha_nacimiento" id="fecha_nacimiento" />

				<div class="label">Marital Status <span class="requiered">*</span></div>
                <select class="select"  tabindex=14 name="estado_civil" id="estado_civil">
                    <option value="Single">Single</option>
                    <option value="Married">Married</option>
                    <option value="Divorced">Divorced</option>
                </select>
                
				<div class="label">Twitter (Optional)</div>
                <input type="text"   tabindex="16" class="input_b" name="twitter" id="twitter" />

	            
                <div class="label">Re-enter E-mail <span class="requiered">*</span></div>
                <input tabindex="18" type="text" class="input_b"  name="confirma_email" id="confirma_email"/>

                <div class="label">Re-enter Password <span class="requiered">*</span></div>
                <input type="password"  tabindex="20" class="input_b"  name="confirmar_clave" id="confirmar_clave"/>
				
                <div class="clear" style="margin-bottom:10px;"></div>
                
                
            </div>
              
            <div class="div_line clear" style="margin-bottom:0px"></div>
            <div class="pad_box_10 clearfix">
            <div class="left" style="padding:5px 5px 5px 0px; margin-top:10px; font-size:12px; line-height:11px;">
               <a class="verde_bt2 left" id="acepto_terminos">Agree to our </a>
                <a href="#terminos_cond" class="fancybox terms left" style="color:#3CF !important; padding-left:4px; text-decoration:underline"> terms of use and privacy policy </a>
                <!--br />
                <a class="verde_bt3 right" id="recordarme2">Recordarme</a-->
            </div>
                <a href="javascript:validarformulario()" class="bt_green right">Sign up!</a>	
            
                          	

			</div>
            
            
        </div>
        
        
    </div>
                <input type="hidden" name="newsletter" id="newsletter"/>
                
        </form>
</div>
<style>
    .errorform {
    border: solid 2px #a01f80;  
}
</style>
<script type="text/javascript">
    function checkKey(key){
        var unicode;
        if (key.charCode)
        {unicode=key.charCode;}
        else
        {unicode=key.keyCode;}
        if (unicode == 13){
           conectarse();
        }
    }
    function cambiarcap(){
        $('#captcha').attr("src","<?php echo base_url(); ?>captcha/php/newCaptcha.php?rnd=" + Math.random());
    }
    function validaEmail(email) { 
    var re  = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/; 
    if (!re.test(email)) { 
        alert ("This E-mail address is not valid."); 
        return false; 
    } 
    return true; 
    }
    
    
    function ValidaSoloNumeros() {
     if ((event.keyCode < 48) || (event.keyCode > 57)) 
      event.returnValue = false;
    }
    
    function validarformulario(){
        var captcha = $('#captchatext').val();
        
        var fecha_nacimiento = $('#fecha_nacimiento').val();
        var nombres = $('#nombres').val();
        var apellidos = $('#apellidos').val();
        var telefono = $('#telefono').val();
        var telefono2 = $('#telefono2').val();
        var cedula = $('#cedula').val();
        var email = $('#email').val();
        var confirma_email = $('#confirma_email').val();
        var pais = $('#pais').val();
        var departamento = $('#departamento').val();
        var direccion = $('#direccion').val();
        var facebook = $('#facebook').val();
        var twitter = $('#twitter').val();
        var ciudad = $('#ciudad').val();
        var sexo = $("input[name='n']:checked").val(); 
        var estado_civil = $('#estado_civil').val();
        var clave = $('#clave').val();
        var confirmar_clave = $('#confirmar_clave').val();
        var validacion = true;
        var newsletter = '';
        var recordarme = '';
       
       
       
        if ($('#newaletter').hasClass('active_recordarme')){
            $('#newsletter').val('si');
        }else{
             $('#newsletter').val('no');
        }
        
        if ($('#recordarme2').hasClass('active_recordarme')){
             $('#recordarme').val('si');
        }else{
             $('#recordarme').val('no');
        }
        
        if ($('#acepto_terminos').hasClass('active_recordarme')){
            
        }else{
            alert('You must agree to our terms of use and privacy police.');
            return false;
        }
        
        
        
        if(nombres == ''){
             $('#nombres').addClass("errorform");
             $('#nombres').focus();
             validacion = false;
        }else{
             $('#nombres').removeClass("errorform");
            
        }
        if(apellidos == ''){
             $('#apellidos').addClass("errorform");
             $('#apellidos').focus();
             validacion = false;
        }else{
             $('#apellidos').removeClass("errorform");
            
        }
        if(telefono == ''){
             $('#telefono').addClass("errorform");
             $('#telefono').focus();
             validacion = false;
        }else{
             $('#telefono').removeClass("errorform");
            
        }
       
        if(cedula == ''){
             $('#cedula').addClass("errorform");
             $('#cedula').focus();
             validacion = false;
        }else{
             $('#cedula').removeClass("errorform");
            
        }
        if(email == ''){
             $('#email').addClass("errorform");
             $('#email').focus();
             validacion = false;
        }else{
             $('#email').removeClass("errorform");
            
        }
         if(confirma_email == ''){
             $('#confirma_email').addClass("errorform");
             $('#confirma_email').focus();
             validacion = false;
        }else{
             $('#confirma_email').removeClass("errorform");
            
        }
        
        
        
         if(pais == ''){
             $('#pais').addClass("errorform");
             $('#pais').focus();
             validacion = false;
        }else{
             $('#pais').removeClass("errorform");
            
        }
        
         if(departamento == ''){
             $('#departamento').addClass("errorform");
             $('#departamento').focus();
             validacion = false;
        }else{
             $('#departamento').removeClass("errorform");
            
        }
        
        
         if(direccion == ''){
             $('#direccion').addClass("errorform");
             $('#direccion').focus();
             validacion = false;
        }else{
             $('#direccion').removeClass("errorform");
            
        }
        
         if(fecha_nacimiento == ''){
             $('#fecha_nacimiento').addClass("errorform");
             $('#fecha_nacimiento').focus();
             validacion = false;
        }else{
             $('#fecha_nacimiento').removeClass("errorform");
            
        }
    
    
        if(ciudad == ''){
             $('#ciudad').addClass("errorform");
             $('#ciudad').focus();
             validacion = false;
        }else{
             $('#ciudad').removeClass("errorform");
            
        }
        if(clave == ''){
             $('#clave').addClass("errorform");
             $('#clave').focus();
             validacion = false;
        }else{
             $('#clave').removeClass("errorform");
            
        }
        if(confirmar_clave == ''){
             $('#confirmar_clave').addClass("errorform");
             $('#confirmar_clave').focus();
             validacion = false;
        }else{
             $('#confirmar_clave').removeClass("errorform");
            
        }
        
       
        
        if(validacion == false){
            alert('Please fill out all fields.');
            return false;
        }
        
        if(validaEmail(email) == false){
            return false;
        }
        if(email != confirma_email){
            alert("The email address you entered does not match.");
             $('#confirma_email').addClass("errorform");
            $('#confirma_email').focus();
            return false;
        }
        if(clave != confirmar_clave){
            alert("The password you entered does not match.");
            $('#confirmar_clave').addClass("errorform");
            $('#confirmar_clave').focus();
            return false;
        }
        
        
        
        
        
         $.post( "<?php echo base_url(); ?>captcha/actual.php", { })
          .done(function( data ) {
            if(captcha != data){
                alert("That Catcha was incorrect. Try again.");
                return false;
            }else{
               $('#formularioregistro').submit(); 
            }
          });
        
    }
        
        
     
     
     
     function conectarse(){
         var emaillog = $('#emaillog').val();
         var password = $('#password').val();
         var validacion = true;
         
         
         if(emaillog == ''){
             $('#emaillog').addClass("errorform");
             
             $('#emaillog').focus();
             validacion = false;
        }else{
             $('#emaillog').removeClass("errorform");
            
        }
        
        if(password == ''){
             $('#password').addClass("errorform");
             $('#password').focus();
             validacion = false;
        }else{
             $('#password').removeClass("errorform");
            
        }
            if(validacion == false){
                return false;
            }
        
        $.post( "<?php echo base_url(); ?>registro/login", { emaillog : emaillog , password : password})
          .done(function( data ) {
                if(data == 'Error on data supplied'){
                     alert('You must enter a valid user name and password.');
                }else{
                    window.location = '<?php echo base_url(); ?>home/index/1';
                }
          });
        
        
     }
	$('.sex_f').sexCheck();
        

</script>
<script>
<?php if($accion == 1){?>
    alert('To complete the registration you must click on the link sent to your E-mail address and activate your account.');
<?php }?>
<?php if($accion == 2){?>
    alert('Congratulations, your account has been activated! You can log in with your email address and password.');
<?php }?>
<?php if($accion == 3){?>
    alert('We have sent the instructions to retrieve your password to your registered email address.');
<?php }?>
<?php if($accion == 4){?>
    setTimeout(function(){
        $('#modalRecuperar').click()
        },2000
    );
<?php }?>
<?php if($accion == 5){?>
    alert('You can now login with your email address and password.');
<?php }?>
</script>

    
<!-- //popup -->
<div id="terminos_cond" style="display:none;" class="device_250">
	<div class="device_250">
        <h2 class="h2_b" style="margin-bottom:20px;">Terms and Conditions</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet libero viverra mi tristique ultrices. Fusce nec leo dapibus, pulvinar justo et, lacinia tortor. Duis a consequat magna. Cras sodales, lectus ut pellentesque congue, diam metus mattis enim, ac tincidunt nisi turpis ut metus. Ut in sapien at lacus semper facilisis vitae sed odio. Morbi facilisis faucibus eros vitae pharetra. Fusce eu ipsum congue, viverra velit a, mollis nunc. Nam mattis ligula eget metus aliquet, sed commodo justo aliquet.
        Donec consequat at sapien nec ullamcorper. <br />
        <br />
        Aliquam erat volutpat. Morbi dolor lacus, bibendum eu velit sit amet, cursus facilisis arcu. Fusce eu facilisis lorem. Pellentesque eu augue at mauris fringilla volutpat. Suspendisse tincidunt sit amet nisi id adipiscing. Integer porttitor nisl quis dignissim cursus. In suscipit sapien eros, ac vulputate quam iaculis vitae. Suspendisse justo odio, porttitor quis eros ut, adipiscing gravida nunc.
        <br /><br />
        Praesent accumsan eget nulla eget faucibus. Fusce eu nulla in odio pharetra gravida. Curabitur sollicitudin enim tellus, vitae commodo nunc dapibus vel. Ut rutrum quam id quam tempor, in pulvinar justo ultrices. Morbi ultricies, dolor eu tempus pretium, neque leo scelerisque neque, sit amet blandit ligula diam ac tortor. Cras eleifend scelerisque nibh, tempor vehicula ligula auctor at. Nullam arcu justo, egestas et libero non, placerat aliquam augue. Fusce elementum et sem nec malesuada. Suspendisse interdum vitae diam quis hendrerit. Etiam convallis malesuada porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
        <br /><br />
        Proin laoreet gravida eleifend. Ut varius semper metus vel consectetur. Integer ut massa sed ligula aliquam tincidunt. Aenean purus nunc, pulvinar eu scelerisque vel, commodo et erat. Sed consequat, diam sed lacinia congue, mi lectus porttitor nisi, ac fermentum justo erat a nisi. Vivamus ac rutrum felis, et commodo ante. Aliquam dignissim, neque vel volutpat rutrum, nisl ante sollicitudin lectus, at interdum nulla elit consectetur tellus. In commodo nisi quis sem euismod feugiat. Aenean accumsan porta tempor. Nullam porta faucibus risus, nec rhoncus orci feugiat in. Nunc hendrerit consequat laoreet. Aliquam congue vestibulum porta.</p>
	</div>
</div>
    
    
    <!-- //popup OLVIOD -->
<div id="olvido" style="display:none; width:350px;">
	<div>
        <form id="formrecuperar" method="post" action="<?php echo base_url(); ?>registro/recuperarClave">
        <h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">Forgot your password?</h2>
        <p>Enter the email address you used to sign up and we'll send you a link to reset your password.</p>
        <input type="text" id="correo"  name="email" class="input_j" placeholder="E-mail" />
        <input type="hidden" name="id" id="idusuario">
        <input type="button" onclick="verificarCorreo()" value="Continue" class="bt_green" style="width:347px; background-size:100% 100%; margin:0px 0px" />
        </form>
	</div>
</div>
        <a href="#recuperar" id="modalRecuperar" class="right link_a fancybox"></a>

        <div id="recuperar" style="display:none; width:350px; height:230px">
            <div>
                <form method="post" id="formrvalidar" action="<?php echo base_url(); ?>registro/nuevaClave">
                    <h2 class="h2_b" style="margin-bottom:20px; color:#7f3f98">Recuperar contraseña</h2>

                    <div class="label">Password</div>
                    <input type="password" name="clave" id="clave1" class="input_j" />
                    <input type="hidden" value="<?php echo $idUser ?>" name="id">
                    <div class="label">Confirme Password</div>
                    <input type="password" name="clave2" id="clave2" class="input_j" />

                    <input type="button" onclick="compararClaves()" class="bt_green3" value="Save" style="float:left" />
                </form>
            </div>
        </div>

        <script>

            function compararClaves(){
                if($('#clave1').val() == ''){
                    alert('Ingrese su nueva clave');
                    return false;
                }

                if($('#clave2').val() == ''){
                    alert('Confirme su nueva clave');
                    return false;
                }

                if($('#clave1').val() == $('#clave2').val()){
                    $('#formrvalidar').submit();
                }else{

                    $('#clave1').val('');
                    $('#clave2').val('');
                    $('#clave1').focus();

                    alert('The passwords you entered do not match. ');
                }
            }

                function verificarCorreo(){

                    if(validaEmail($('#correo').val())){

                    $.ajax({
                        url: "<?php echo base_url(); ?>registro/verificarCorreo",
                        type: "post",
                        dataType: "text",
                        data: 'correo='+$('#correo').val()
                    }).done(function (data) {
                       if(data == 0){
                           $('#correo').val('');
                           $('#correo').focus();
                           alert('This is not a registered E-mail');
                       }else{
                           $('#idusuario').val(data);
                           $('#formrecuperar').submit();
                       }
                    })
                 }else{
                        $('#correo').val('');
                        $('#correo').focus();

                    }

                }

            function verificarCorreo2(elemento){

                    $.ajax({
                        url: "<?php echo base_url(); ?>registro/verificarCorreo",
                        type: "post",
                        dataType: "text",
                        data: 'correo='+$(elemento).val()
                    }).done(function (data) {
                        if(data > 0){
                            $('#email').val('');
                            $('#email').focus();
                            alert('The email address is already registered.');
                        }
                    })
                }

            function verificarCedula(elemento){

                var tipo = $('#tipo_identificacion').val();

                                $.ajax({
                                    url: "<?php echo base_url(); ?>registro/verificarCedula",
                                    type: "post",
                                    dataType: "text",
                                    data: 'cedula='+$(elemento).val()+'&tipo='+tipo
                                }).done(function (data) {
                                    if(data > 0){
                                        $(elemento).val('');
                                        alert('This ID number is already already registered.');
                                        return false;
                                    }
                                })
                            }

        </script>
    
    